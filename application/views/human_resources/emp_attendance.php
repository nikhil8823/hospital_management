<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <?php echo form_open_multipart('human_resources/employee/emp_attendance') ?>

                            <div class="form-group row">
                                <label for="month" class="col-xs-3 col-form-label">Month<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="month" type="text" class="datepicker form-control" id="month" placeholder="Month" value="<?php echo $employee->month; ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="emp_id" class="col-xs-3 col-form-label">Employee Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        echo form_dropdown('emp_id', $employee_list, $employee->emp_id, 'class="form-control"'); 
                                    ?>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="start_time" class="col-xs-3 col-form-label">Start Time</label>
                                <div class="col-xs-9">
                                    <input name="start_time" type="text" class="form-control" id="start_time" placeholder="Start Time" value="<?php echo $employee->start_time; ?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="end_time" class="col-xs-3 col-form-label">End Time</label>
                                <div class="col-xs-9">
                                    <input name="end_time" type="text" class="form-control" id="end_time" placeholder="End Time" value="" >
                                </div>
                            </div>
                        
<!--                            <div class="form-group row">
                                <label for="total_hrs" class="col-xs-3 col-form-label">Total Hours</label>
                                <div class="col-xs-9">
                                    <input name="total_hrs" type="text" class="form-control" id="total_hrs" placeholder="Total Hours" value="<?php echo $employee->end_time; ?>" >
                                </div>
                            </div>-->

                             <div class="form-group row">
                                <label for="leave_type" class="col-xs-3 col-form-label">Leave Type</label>
                                <div class="col-xs-9"> 
                                   <?php
                                        echo form_dropdown('leave_type', $leave_type, $employee->leave_type, 'class="form-control"'); 
                                    ?> 
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="remark" class="col-xs-3 col-form-label">Remark</label>
                                <div class="col-xs-9">
                                    <input name="remark" type="text" class="form-control" id="remark" placeholder="Remark" value="" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>

</div>