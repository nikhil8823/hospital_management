<div class="row">
    
    <div class="col-sm-12">
        <div  class="panel panel-default">
            <a href="emp_attendance_list.php"></a>
            <div class="panel-body"> 

                <form class="form-inline" action="<?php echo base_url('human_resources/employee/emp_attendance_list') ?>">

                    <div class="form-group">
                        <label class="sr-only" for="start_date"><?php echo display('start_date') ?></label>
                        <input type="text" name="start_date" class="form-control datepicker" id="start_date" placeholder="<?php echo display('start_date') ?>" value="<?php echo $date->start_date ?>">
                    </div> 

                    <div class="form-group">
                        <label class="sr-only" for="end_date"><?php echo display('end_date') ?></label>
                        <input type="text" name="end_date" class="form-control datepicker" id="end_date" placeholder="<?php echo display('end_date') ?>" value="<?php echo $date->end_date ?>">
                    </div>  

                    <button type="submit" class="btn btn-success"><?php echo display('filter') ?></button>

                </form>

            </div>
        </div>
    </div>
    
    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">

            <div class="panel-body">
                <table width="100%" class="datatable table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th>Employee Id</th>
                            <th>Full Name</th>
                            <th>Month</th>
                            <th width="80"><?php echo display('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 

                        if (!empty($result)) {
                            $sl = 1;
                            foreach ($result as $value) {
                        ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->emp_code; ?></td>
                                <td><?php echo $value->fullname; ?></td>
                                <td><?php echo $value->month; ?></td>
                                <td class="center">
                                    <a href="<?php echo base_url("human_resources/employee/attendance_list_view/$value->id") ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a> 
                                </td>
                            </tr>
                        <?php 
                            $sl++;

                            }
                        } 
                        ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
