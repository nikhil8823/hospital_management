<div class="row">
    <!--  form area -->
    <div class="col-sm-12"  id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                   <!-- <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/salary_list_view") ?>"> <i class="fa fa-list"></i>Salary Slip </a>  -->
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <tbody>
                                <tr>  
                                    <td width="10%" class=""><center><img src="<?php echo base_url($website->logo); ?>" height="50px" width="50px"></center></td>
                                    <td width="70%" class="">
                                        <center><ul class="list-unstyled">
                                            <li><strong><?php echo $website->title; ?></strong></li>  
                                            <li><?php echo $website->description; ?></li>  
                                            <li><?php echo $website->email; ?></li>  
                                            <li><?php echo $website->phone; ?></li>  
                                        </ul></center>
                                    </td> 
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        
                        <table class="table">
                            <tbody>
                                <tr class="">
                                    <td width="45%">
                                        <strong>Employee Id</strong>: <?php echo ($salary->emp_code != '') ? $salary->emp_code : '-'; ?> 
                                    </td>
                                    <td width="45%">
                                        <strong>Name</strong>: <?php echo ($salary->fullname != '') ? $salary->fullname : '-'; ?>
                                    </td> 
                                </tr>
                                
                                <tr class="">
                                    <td width="45%">
                                        <strong>Bank name</strong>: <?php echo ($salary->bankname != '') ? $salary->bankname : '-'; ?> 
                                    </td>
                                    <td width="45%">
                                        <strong>Account Number</strong>: <?php echo ($salary->account_number != '') ? $salary->account_number : '-'; ?>
                                    </td> 
                                </tr>
                                
                                <tr class="">
                                    <td width="45%">
                                        <strong>Department Name</strong>: <?php echo ($salary->department_name != '') ? $salary->department_name : '-'; ?> 
                                    </td>
                                    <td width="45%">
                                        <strong>Designation</strong>: <?php echo ($salary->user_role != '') ? $userRoles[$salary->user_role] : '-'; ?>
                                    </td> 
                                </tr>
                                
                                <tr class="">
                                    <td width="45%">
                                        <strong>Date Of Birth</strong>: <?php echo ($salary->date_of_birth != '') ? $salary->date_of_birth : '-'; ?> 
                                    </td>
                                    <td width="45%">
                                        <strong>Date Of Joining</strong>: <?php echo ($salary->date_of_joining != '') ? $salary->date_of_joining : '-'; ?>
                                    </td> 
                                </tr>
                                
                                <tr class="">
                                    <td width="45%">
                                        <strong>PAN Number</strong>: <?php echo ($salary->pan_card_number != '') ? $salary->pan_card_number : '-'; ?> 
                                    </td>
                                    <td width="45%">
                                        <strong>PF Account Number</strong>: <?php echo ($salary->pf_account_number != '') ? $salary->pf_account_number : '-'; ?>
                                    </td> 
                                </tr>
                                
                                <tr class="">
                                    <td width="45%">
                                        <?php
                                       
                                        $total_day = cal_days_in_month(CAL_GREGORIAN, date('m',  strtotime($month)), date('Y',  strtotime($month))); 
                                         ?><strong>Total Days</strong>: <?php echo ($total_day != '') ? $total_day : '-'; ?>


                                          <?php /* $total_num_of_days = cal_days_in_month(CAL_GREGORIAN, date('m',  strtotime($salary->month)), date('Y',  strtotime($salary->month))); 
                                        ?>
                                        <strong>Total Days</strong>: <?php echo ($total_num_of_days != '') ? $total_num_of_days : '-'; */?> 
                                    </td>
                                    <?php 
                                        $sundays = 0;
                                        for ($i = 1; $i <= $total_day; $i++) {
                                            $date = date('Y',  strtotime($month)).'/'.date('m',  strtotime($month)).'/'.$i; //format date
                                            $get_name = date('l', strtotime($date)); //get week day
                                            $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
                                            //if not a weekend add day to array
                                            if($day_name == 'Sun'){
                                                $sundays++;
                                            }

                                        }
                                        $total_working_days = $total_day - $sundays;
                                    ?>


                                     <?php 
                            $tothalfdays = 0;$totfulldays = 0; $fulldays=0; $halfdays=0; $totalleave =0; $leave =0;
                        if (!empty($attendance)) {
                           
                            foreach ($attendance as $value) {
                             
                               if($value->total_hours >= 9)
                                { 
                                   "FD"; 
                                $fulldays = count($value->total_hours);
                                  
                                }else{  "HD";
                                $halfdays = count($value->total_hours);
                                }
                               
                                $value->leave_type;

                               $tothalfdays = $tothalfdays + $halfdays;
                               $totfulldays = $totfulldays + $fulldays;
                              // $totalleave = $totalleave + $leave;
                            }
                            
                        }
                           
                        ?>


                                  <td width="45%">
                                        <strong>Weekly off/paid holiday</strong>: <?php echo ($sundays != '') ? $sundays : '-'; ?> 
                                    </td>
                                    
                                    
                                </tr>
                                
                                <tr class="">
                                   
                                    <td width="45%">
                                        <strong>Total full Days</strong>: <?php echo ($totfulldays != '') ? $totfulldays : '-'; ?>
                                    </td> 

                                     <td width="45%">
                                        <strong>Total Half Days</strong>: <?php echo ($tothalfdays != '') ? $tothalfdays : '-'; ?>
                                    </td> 

                                
                                </tr>

                                 <tr class="">
                                   
                                   
                                    <td width="45%">
                                        <strong>Leave without pay days</strong>: <?php echo ($salary->designation != '') ? $salary->designation : '-'; ?>
                                    </td> 
                                     <td width="45%">
                                      <strong>Total Leave</strong>: <?php  ?>                                       
                                    </td> 
                                </tr>

                                 <tr class="">
                                   
                                   
                                    <td width="45%">
                                        <strong>Basic Salary</strong>: <?php echo ($salary->basic_salary != '') ? $salary->basic_salary : '-'; ?>
                                    </td> 
                                     <td width="45%">
                                        
                                    </td> 
                                </tr>
                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        
                        <table class="table">                            
                            <thead>
                                <tr colspan="4">  
                                    <td><strong>Earnings</strong></td>
                                    <td><strong>Amount</strong></td>
                                    <td><strong>Deductions</strong></td>
                                    <td><strong>Amount</strong></td>
                                </tr>
                            </thead>
                             <tbody>
                                <tr colspan="4">  
                                    <td>Basic Salary</td>
                                    <?php  $calculate_sal = $salary->basic_salary/$total_day;
                                    $calculate_finalsal = round($calculate_sal, 2);    
                                    $calculate_fullsal = $calculate_finalsal * $totfulldays;
                                    $calculate_halfsal = $calculate_finalsal * $tothalfdays;
                                    $calsal = $calculate_fullsal + ($calculate_halfsal/2);
                                    ?>
                                    <td><?php echo ($calsal != '') ? $calsal : '-'; ?></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>DA</td>
                                    <td><?php echo ($salary->da != '') ? $salary->da : '-'; ?></td>
                                    <td>ESIC /Insurance</td>
                                    <td><?php echo ($salary->insurance != '') ? $salary->insurance : '-'; ?></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>HRA</td>
                                    <td><?php echo ($salary->hra != '') ? $salary->hra : '-'; ?></td>
                                    <td>Labour welfare fund</td>
                                    <td><?php echo ($salary->labour_welfare_fund != '') ? $salary->labour_welfare_fund : '-'; ?></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>Conveyance allowance</td>
                                    <td><?php echo ($salary->conveyance_allowance != '') ? $salary->conveyance_allowance : '-'; ?></td>
                                    <td>Professional tax</td>
                                    <td><?php echo ($salary->professional_tax != '') ? $salary->professional_tax : '-'; ?></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>Grade allowance</td>
                                    <td><?php echo ($salary->grade_allowance != '') ? $salary->grade_allowance : '-'; ?></td>
                                    <td>EPF</td>
                                    <td><?php echo ($salary->epf != '') ? $salary->epf : '-'; ?></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>Medical reimbursement</td>
                                    <td><?php echo ($salary->medical_reimbursement != '') ? $salary->medical_reimbursement : '-'; ?></td>
                                    <td>Income tax</td>
                                    <td><?php echo ($salary->income_tax != '') ? $salary->income_tax : '-'; ?></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>LTA(leave travel allowance)</td>
                                    <td><?php echo ($salary->lta != '') ? $salary->lta : '-'; ?></td>
                                    <td>Loan amount</td>
                                    <?php
                                        $loan_amount = json_decode($salary->loan_detail, true);
                                        $loan_amount = array_sum(array_column($loan_amount, 'per_month_amount'));
                                    ?>
                                    <td><?php echo ($loan_amount != '') ? $loan_amount : '-'; ?></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>Mobile reimbursement</td>
                                    <td><?php echo ($salary->mobile_reimbursement != '') ? $salary->mobile_reimbursement : '-'; ?></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>Bonus</td>
                                    <td><?php echo ($salary->bonus != '') ? $salary->bonus : '-'; ?></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td>Other</td>
                                    <td><?php echo ($salary->other != '') ? $salary->other : '-'; ?></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                                <tr colspan="4">  
                                    <td><strong>Total Earning</strong></td>
                                    <?php $cmp1 = $salary->total_of_component - $salary->basic_salary;
                                    $final_tot_of_componennt = $cmp1 + $calsal;
                                    ?>
                                    <td><?php echo ($final_tot_of_componennt != '') ? $final_tot_of_componennt : '-'; ?></td>

                                    <td><strong>Total Deductions</strong></td>
                                    <td><?php echo ($salary->total_of_deduction != '') ? $salary->total_of_deduction : '-'; ?></td>
                                </tr>
                                
                                
                            </tbody>
                            
                        </table>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Signature -->
                        <table class="table" style="margin-top:50px"> 
                            <thead> 
                                
                                <tr>
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Employer's Signature</i>
                                    </td>
                                    
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Employee's Signature</i>
                                    </td>
                                </tr>
                            </thead>
                        </table> 

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

