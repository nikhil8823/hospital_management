<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        <?php echo form_open_multipart('human_resources/employee/emp_salary/'.$salary->emp_id,'class="form-inner"') ?>
                            <?php if(!empty($salary->id)) { 
                                echo form_hidden('id',$salary->id);
                            } ?>
<!--                            <div class="form-group row">
                                <label for="month" class="col-xs-3 col-form-label">Month<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="month" type="text" class="datepicker form-control" id="month" placeholder="Month" value="<?php echo $salary->month?>" >
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="emp_id" class="col-xs-3 col-form-label">Employee Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        echo form_dropdown('emp_id', $employee_list, $selected_emp, 'class="form-control emp_select"'); 
                                    ?>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="basic_salary" class="col-xs-3 col-form-label">Basic Salary</label>
                                <div class="col-xs-9">
                                    <input name="basic_salary" type="text" class="form-control" id="basic_salary" placeholder="Basic Salary" value="<?php echo $salary->basic_salary?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="da" class="col-xs-3 col-form-label">DA</label>
                                <div class="col-xs-9">
                                    <input name="da" type="text" class="form-control" id="da" placeholder="DA" value="<?php echo $salary->da?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="hra" class="col-xs-3 col-form-label">HRA</label>
                                <div class="col-xs-9">
                                    <input name="hra" type="text" class="form-control" id="hra" placeholder="HRA" value="<?php echo $salary->hra?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="conveyance_allowance" class="col-xs-3 col-form-label">Conveyance Allowance</label>
                                <div class="col-xs-9">
                                    <input name="conveyance_allowance" type="text" class="form-control" id="conveyance_allowance" placeholder="Conveyance Allowance" value="<?php echo $salary->conveyance_allowance?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="grade_allowance" class="col-xs-3 col-form-label">Grade Allowance</label>
                                <div class="col-xs-9">
                                    <input name="grade_allowance" type="text" class="form-control" id="grade_allowance" placeholder="Grade Allowance" value="<?php echo $salary->grade_allowance?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="medical_reimbursement" class="col-xs-3 col-form-label">Medical Reimbursement</label>
                                <div class="col-xs-9">
                                    <input name="medical_reimbursement" type="text" class="form-control" id="medical_reimbursement" placeholder="Medical Reimbursement" value="<?php echo $salary->medical_reimbursement?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="lta" class="col-xs-3 col-form-label">LTA</label>
                                <div class="col-xs-9">
                                    <input name="lta" type="text" class="form-control" id="lta" placeholder="LTA" value="<?php echo $salary->lta?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="mobile_reimbursement" class="col-xs-3 col-form-label">Mobile Reimbursement</label>
                                <div class="col-xs-9">
                                    <input name="mobile_reimbursement" type="text" class="form-control" id="mobile_reimbursement" placeholder="Mobile Reimbursement" value="<?php echo $salary->mobile_reimbursement?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="bonus" class="col-xs-3 col-form-label">Bonus</label>
                                <div class="col-xs-9">
                                    <input name="bonus" type="text" class="form-control" id="bonus" placeholder="Bonus" value="<?php echo $salary->bonus?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="other" class="col-xs-3 col-form-label">Other</label>
                                <div class="col-xs-9">
                                    <input name="other" type="text" class="form-control" id="other" placeholder="Other" value="<?php echo $salary->other?>" >
                                </div>
                            </div>
                        
                            
<!--                            <div class="form-group row">
                                <label for="pf" class="col-xs-3 col-form-label">PF</label>
                                <div class="col-xs-9">
                                    <input name="pf" type="text" class="form-control" id="pf" placeholder="PF" value="<?php echo $salary->pf?>" >
                                </div>
                            </div>-->
                        
                            <div class="form-group row">
                                <label for="insurance" class="col-xs-3 col-form-label">ESIC /Insurance</label>
                                <div class="col-xs-9">
                                    <input name="insurance" type="text" class="form-control" id="insurance" placeholder="ESIC /Insurance" value="<?php echo $salary->insurance?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="labour_welfare_fund" class="col-xs-3 col-form-label">Labour welfare fund</label>
                                <div class="col-xs-9">
                                    <input name="labour_welfare_fund" type="text" class="form-control" id="labour_welfare_fund" placeholder="Labour welfare fund" value="<?php echo $salary->labour_welfare_fund?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="professional_tax" class="col-xs-3 col-form-label">Professional tax</label>
                                <div class="col-xs-9">
                                    <input name="professional_tax" type="text" class="form-control" id="professional_tax" placeholder="Professional tax" value="<?php echo $salary->professional_tax?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="epf" class="col-xs-3 col-form-label">EPF</label>
                                <div class="col-xs-9">
                                    <input name="epf" type="text" class="form-control" id="epf" placeholder="EPF" value="<?php echo $salary->epf?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="income_tax" class="col-xs-3 col-form-label">Income tax</label>
                                <div class="col-xs-9">
                                    <input name="income_tax" type="text" class="form-control" id="income_tax" placeholder="Income tax" value="<?php echo $salary->income_tax?>" >
                                </div>
                            </div>
                        
<!--                            <div class="form-group row">
                                <label for="add_loan" class="col-xs-3 col-form-label">Add loan</label>
                                <div class="col-xs-9">
                                    <input name="add_loan" type="text" class="form-control" id="add_loan" placeholder="Add loan" value="<?php echo $salary->add_loan?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="loan_amount" class="col-xs-3 col-form-label">Loan Amount</label>
                                <div class="col-xs-9">
                                    <input name="loan_amount" type="text" class="form-control" id="loan_amount" placeholder="Loan Amount" value="<?php echo $salary->loan_amount?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="loan_from_date" class="col-xs-3 col-form-label">From Date</label>
                                <div class="col-xs-9">
                                    <input name="loan_from_date" type="text" class="datepicker form-control" id="loan_from_date" placeholder="From Date" value="<?php echo $salary->loan_from_date?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="loan_to_date" class="col-xs-3 col-form-label">To Date</label>
                                <div class="col-xs-9">
                                    <input name="loan_to_date" type="text" class="datepicker form-control" id="loan_to_date" placeholder="To Date" value="<?php echo $salary->loan_to_date?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="instalment_for_month" class="col-xs-3 col-form-label">Instalment for month(total month)</label>
                                <div class="col-xs-9">
                                    <input name="instalment_for_month" type="text" class="form-control" id="instalment_for_month" placeholder="Instalment for month(total month)" value="<?php echo $salary->instalment_for_month?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="per_month_amount" class="col-xs-3 col-form-label">Per month loan amount</label>
                                <div class="col-xs-9">
                                    <input name="per_month_amount" type="text" class="form-control" id="per_month_amount" placeholder="Per month loan amount" value="<?php echo $salary->per_month_amount?>" >
                                </div>
                            </div>-->
                        <div class="col-md-12 col-sm-12 table-responsive row">
                                    <table class="table table-striped"> 
                                    <thead>
                                        <tr class="bg-primary">
                                            <th width="130">Add loan</th>
                                            <th width="100">Loan Amount</th>
                                            <th>From date</th>
                                            <th width="">To Date</th>
                                            <th width="">Instalment for month</th>
                                            <th width="160">Per month loan amount</th>
                                            <th width="160"><?php echo display('add_or_remove') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody id="medicine" class="clone_count">
                                            <?php 
                                            $loop_for_loan = json_decode($salary->loan_detail);
                                            //echo "<pre>";print_r($loop_for_loan);die("innnn");
                                            if(empty($salary->id) || empty($loop_for_loan)) { ?>
                                            <tr id="first_medicine">
                                            <td>
                                                <input name="loan_detail[0][add_loan]" data-unique-name="add_loan" type="text" class="form-control" id="loan_amount" placeholder="Add Loan">
                                            </td>
                                            <td>
                                                <input name="loan_detail[0][loan_amount]" data-unique-name="loan_amount" type="text" class="form-control" id="loan_amount" placeholder="Loan Amount">
                                            </td>
                                            <td>
                                                <input name="loan_detail[0][loan_from_date]" data-unique-name="loan_from_date" type="text" class="form-control" placeholder="From Date" >
                                            </td> 
                                            <td>
                                                <input name="loan_detail[0][loan_to_date]" data-unique-name="loan_to_date" type="text" class="form-control" placeholder="To Date" >
                                            </td> 
                                            <td>
                                                <input name="loan_detail[0][instalment_for_month]" data-unique-name="instalment_for_month" type="text" class="form-control" id="instalment_for_month" placeholder="Instalment for month(total month)">
                                            </td>
                                            <td>
                                                <input name="loan_detail[0][per_month_amount]" data-unique-name="per_month_amount" type="text" class="form-control" id="instalment_for_month" placeholder="Instalment for month(total month)">
                                            </td>
                                            <td>
                                              <div class="btn btn-group">
                                                <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                                <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                                </div>
                                            </td>
                                            </tr>
                                            <?php } 
                                            else{
                                               $is_first = true;
                                               foreach($loop_for_loan as $key=>$data) {
                                            ?>
                                            <tr <?php ($is_first == true) ? 'id="first_medicine"' : '';?> >
                                            <td>
                                                <input name="loan_detail[<?php echo $key;?>][add_loan]" data-unique-name="add_loan" type="text" class="form-control" id="loan_amount" placeholder="Add Loan" value="<?php echo $data->add_loan; ?>">
                                            </td>
                                            <td>
                                                <input name="loan_detail[<?php echo $key;?>][loan_amount]" data-unique-name="loan_amount" type="text" class="form-control" id="loan_amount" placeholder="Loan Amount" value="<?php echo $data->loan_amount; ?>">
                                            </td>
                                            <td>
                                                <input name="loan_detail[<?php echo $key;?>][loan_from_date]" data-unique-name="loan_from_date" type="text" class="form-control" placeholder="From Date" value="<?php echo $data->loan_from_date; ?>">
                                            </td> 
                                            <td>
                                                <input name="loan_detail[<?php echo $key;?>][loan_to_date]" data-unique-name="loan_to_date" type="text" class="form-control" placeholder="To Date" value="<?php echo $data->loan_to_date; ?>">
                                            </td> 
                                            <td>
                                                <input name="loan_detail[<?php echo $key;?>][instalment_for_month]" data-unique-name="instalment_for_month" type="text" class="form-control" id="instalment_for_month" value="<?php echo $data->instalment_for_month; ?>" placeholder="Instalment for month(total month)">
                                            </td>
                                            <td>
                                                <input name="loan_detail[<?php echo $key;?>][per_month_amount]" data-unique-name="per_month_amount" type="text" class="form-control" id="per_month_amount" value="<?php echo $data->per_month_amount; ?>" placeholder="Instalment for month(total month)">
                                            </td>
                                            <td>
                                              <div class="btn btn-group">
                                                <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                                <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                                </div>
                                            </td>
                                            <?php
                                            $is_first = false;
                                                }
                                            } ?>
                                        </tr>  
                                    </tbody> 
                                </table> 
                            </div>
                        
                            <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
$('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent();
       // $('#medicine').append("<tr>"+itemData.html()+"</tr>");
        //$('#medicine tr:last-child').find(':input').val('');
        
        
        var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var cloneCount = $('.diet_count').length;
        
        var clone = $("#first_medicine").clone();
        clone.attr("id", "first_medicine" + newId);
        clone.attr("class", "clone_count");
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
            if($(this).is(':checkbox')) {
             $(this).prop('checked' , false);
             $(this).attr('name', "loan_detail[" + newId + "][" + name + "][]");
            }
            else {
                $(this).val('');
                $(this).attr('name', "loan_detail[" + newId + "][" + name + "]");
            }
            
        });
        console.log(clone);
        
        //$('#medicine').append("<tr>"+clone+"</tr>");
        $(".clone_count").last().after(clone);

    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });
    
    
    
    //department_id
    $(document).on('change', '.emp_select', function(){
        $.ajax({
            url  : '<?= base_url('human_resources/employee/get_emp_salary_by_emp_id/') ?>',
            type : 'post',
            dataType : 'JSON',
            data : {
                '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
                emp_id :this.value
            },
            success : function(data) 
            {
                console.log(data);
                if (data.status == true) {
                    window.location.href = '<?= base_url('human_resources/employee/emp_salary/') ?>'+data.id;
                }
            }, 
            error : function()
            {
                alert('failed');
            }
        });
    }); 
    
    
    
    
</script>