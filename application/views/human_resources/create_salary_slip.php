<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        <?php echo form_open_multipart('human_resources/employee/create_salary_slip_view') ?>
                            <?php /*if(!empty($salary->id)) { 
                                echo form_hidden('id',$salary->id);
                            } */?>
                            <div class="form-group row">
                                <label for="month" class="col-xs-3 col-form-label">Month<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="month" type="text" class="datepicker form-control" id="month" placeholder="Month" value="" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="emp_id" class="col-xs-3 col-form-label">Employee Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        echo form_dropdown('emp_id', $employee_list, 'class="form-control"'); 
                                    ?>
                                </div>
                            </div>
                        
                           
                           
                        
                            <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
$('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent();
       // $('#medicine').append("<tr>"+itemData.html()+"</tr>");
        //$('#medicine tr:last-child').find(':input').val('');
        
        
        var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var cloneCount = $('.diet_count').length;
        
        var clone = $("#first_medicine").clone();
        clone.attr("id", "first_medicine" + newId);
        clone.attr("class", "clone_count");
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
            if($(this).is(':checkbox')) {
             $(this).prop('checked' , false);
             $(this).attr('name', "loan_detail[" + newId + "][" + name + "][]");
            }
            else {
                $(this).val('');
                $(this).attr('name', "loan_detail[" + newId + "][" + name + "]");
            }
            
        });
        console.log(clone);
        
        //$('#medicine').append("<tr>"+clone+"</tr>");
        $(".clone_count").last().after(clone);

    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });
</script>