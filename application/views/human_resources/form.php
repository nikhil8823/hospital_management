<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
<!--            <div class="panel-heading no-print">
                <div class="btn-group">
                    <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/index/accountant") ?>"> <i class="fa fa-list"></i>  <?php echo display('accountant_list') ?> </a>  
                    <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/index/laboratorist") ?>"> <i class="fa fa-list"></i>  <?php echo display('laboratorist_list') ?> </a>  
                    <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/index/nurse") ?>"> <i class="fa fa-list"></i>  <?php echo display('nurse_list') ?> </a>  
                    <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/index/pharmacist") ?>"> <i class="fa fa-list"></i>  <?php echo display('pharmacist_list') ?> </a>  
                    <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/index/receptionist") ?>"> <i class="fa fa-list"></i>  <?php echo display('receptionist_list') ?> </a>  
                    <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/index/representative") ?>"> <i class="fa fa-list"></i>  <?php echo display('representative_list') ?> </a>  
                    <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/index/case_manager") ?>"> <i class="fa fa-list"></i>  <?php echo display('case_manager') ?> </a>  
                </div>
            </div> -->


            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-9 col-sm-12">

                        <?php echo form_open_multipart('human_resources/employee/form/'.$employee->user_id,'class="form-inner"') ?>

                            <?php echo form_hidden('user_id',$employee->user_id) ?>


<!--                            <div class="form-group row">
                                <label for="user_role" class="col-xs-3 col-form-label"><?php echo display('user_role') ?>  <i class="text-danger">*</i></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        echo form_dropdown('user_role', $userRoles, $employee->user_role, 'class="form-control" id="user_role" '); 
                                    ?>
                                </div>
                            </div> -->

<!--                            <div class="form-group row">
                                <label for="firstname" class="col-xs-3 col-form-label"><?php echo display('first_name') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="firstname" type="text" class="form-control" id="firstname" placeholder="<?php echo display('first_name') ?>" value="<?php echo $employee->firstname ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lastname" class="col-xs-3 col-form-label"><?php echo display('last_name') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="lastname" type="text" class="form-control" id="lastname" placeholder="<?php echo display('last_name') ?>" value="<?php echo $employee->lastname ?>">
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="fullname" class="col-xs-3 col-form-label">Full Name <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="user_details[fullname]" type="text" class="form-control" id="fullname" placeholder="Full Name" value="<?php echo $employee_details->fullname?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fathername" class="col-xs-3 col-form-label">Father Name <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="user_details[fathername]" type="text" class="form-control" id="fathername" placeholder="Father Name" value="<?php echo $employee_details->fathername?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-xs-3 col-form-label"><?php echo display('email')?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="email" class="form-control" type="text" placeholder="<?php echo display('email')?>" id="email"  value="<?php echo $employee->email ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-xs-3 col-form-label"><?php echo display('password') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="password" class="form-control" type="password" placeholder="<?php echo display('password') ?>" id="password" >
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="mobile" class="col-xs-3 col-form-label"><?php echo display('mobile') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="mobile" class="form-control" type="text" placeholder="<?php echo display('mobile') ?>" id="mobile"  value="<?php echo $employee->mobile ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone_1" class="col-xs-3 col-form-label">Phone 1</label>
                                <div class="col-xs-9">
                                    <input name="user_details[phone_1]" class="form-control" type="text" placeholder="Phone 1" id="phone_1"  value="<?php echo $employee_details->phone_1 ?>">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="phone_2" class="col-xs-3 col-form-label">Phone 2</label>
                                <div class="col-xs-9">
                                    <input name="user_details[phone_2]" class="form-control" type="text" placeholder="Phone 2" id="phone_2"  value="<?php echo $employee_details->phone_2 ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('sex') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                            <input type="radio" name="sex" value="Male" <?php echo  set_radio('sex', 'Male', ($employee->sex == 'Male') ? true :false); ?> ><?php echo display('male') ?>
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sex" value="Female" <?php echo  set_radio('sex', 'Female',($employee->sex == 'Female') ? true :false); ?> ><?php echo display('female') ?>
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sex" value="Other" <?php echo  set_radio('sex', 'Other',($employee->sex == 'Other') ? true :false); ?> ><?php echo display('other') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <!-- if employee picture is already uploaded -->
                            <?php if(!empty($employee->picture)) {  ?>
                            <div class="form-group row">
                                <label for="picturePreview" class="col-xs-3 col-form-label"></label>
                                <div class="col-xs-9">
                                    <img src="<?php echo base_url($employee->picture) ?>" alt="Picture" class="img-thumbnail" />
                                </div>
                            </div>
                            <?php } ?>

                            <div class="form-group row">
                                <label for="picture" class="col-xs-3 col-form-label"><?php echo display('picture') ?></label>
                                <div class="col-xs-9">
                                    <input type="file" name="picture" id="picture" value="<?php echo $employee->picture ?>">
                                    <input type="hidden" name="old_picture" value="<?php echo $employee->picture ?>">
                                </div>
                            </div>

<!--                            <div class="form-group row">
                                <label for="address" class="col-xs-3 col-form-label"><?php echo display('address') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <textarea name="address" class="form-control" id="address" placeholder="<?php echo display('address') ?>" maxlength="140" rows="7"><?php echo $employee->address ?></textarea>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="permantaddress" class="col-xs-3 col-form-label">Permanent Address</label>
                                <div class="col-xs-9">
                                    <textarea name="user_details[permantaddress]" class="form-control" id="permantaddress" placeholder="Permanent Address" maxlength="140" rows="7"><?php echo $employee_details->permantaddress ?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="presentaddress" class="col-xs-3 col-form-label">Present Address</label>
                                <div class="col-xs-9">
                                    <textarea name="user_details[presentaddress]" class="form-control" id="presentaddress" placeholder="Present Address" maxlength="140" rows="7"><?php echo $employee_details->presentaddress ?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="aadhar_number" class="col-xs-3 col-form-label">Aadhar Number</label>
                                <div class="col-xs-9">
                                    <input name="user_details[aadhar_number]" class="form-control" type="text" placeholder="Aadhar Number" id="aadhar_number"  value="<?php echo $employee_details->aadhar_number ?>">
                                </div>
                            </div>
 
                            <div class="form-group row">
                                <label for="date_of_birth" class="col-xs-3 col-form-label"><?php echo display('date_of_birth') ?></label>
                                <div class="col-xs-9">
                                    <input name="date_of_birth" class="datepicker form-control" type="text" placeholder="<?php echo display('date_of_birth') ?>" id="date_of_birth" value="<?php echo $employee->date_of_birth ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="body_mark" class="col-xs-3 col-form-label">Body Mark</label>
                                <div class="col-xs-9">
                                    <input name="user_details[body_mark]" class="form-control" type="text" placeholder="Body mark" id="body_mark"  value="<?php echo $employee_details->body_mark ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="blood_group" class="col-xs-3 col-form-label"><?php echo display('blood_group') ?></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        $bloodList = array( 
                                            ''   => display('select_option'),
                                            'A+' => 'A+',
                                            'A-' => 'A-',
                                            'B+' => 'B+',
                                            'B-' => 'B-',
                                            'O+' => 'O+',
                                            'O-' => 'O-',
                                            'AB+' => 'AB+',
                                            'AB-' => 'AB-'
                                        );
                                        echo form_dropdown('blood_group', $bloodList, $employee->blood_group, 'class="form-control" id="blood_group" ');

                                    ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="religion" class="col-xs-3 col-form-label">Religion</label>
                                <div class="col-xs-9">
                                    <input name="user_details[religion]" class="form-control" type="text" placeholder="Religion" id="religion"  value="<?php echo $employee_details->religion ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="driving_license_number" class="col-xs-3 col-form-label">Driving License Number</label>
                                <div class="col-xs-9">
                                    <input name="user_details[driving_license_number]" class="form-control" type="text" placeholder="Driving License Number" id="driving_license_number"  value="<?php echo $employee_details->driving_license_number ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="school_detail" class="col-xs-3 col-form-label">School name & address</label>
                                <div class="col-xs-9">
                                    <textarea name="user_details[school_detail]" class="form-control" id="school_detail" placeholder="School name & address" maxlength="140" rows="7"><?php echo $employee_details->school_detail ?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="college_detail" class="col-xs-3 col-form-label">College name & address</label>
                                <div class="col-xs-9">
                                    <textarea name="user_details[college_detail]" class="form-control" id="college_detail" placeholder="College name & address" maxlength="140" rows="7"><?php echo $employee_details->college_detail ?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_job_place" class="col-xs-3 col-form-label">Last Job Place</label>
                                <div class="col-xs-9">
                                    <input name="user_details[last_job_place]" class="form-control" type="text" placeholder="Last Job Place" id="last_job_place"  value="<?php echo $employee_details->last_job_place ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_job_type" class="col-xs-3 col-form-label">Last Job Type</label>
                                <div class="col-xs-9">
                                    <input name="user_details[last_job_type]" class="form-control" type="text" placeholder="Last Job Type" id="last_job_type"  value="<?php echo $employee_details->last_job_type ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3">Verification Done</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[verification_done]" value="0" <?php echo  set_radio('user_details[verification_done]', '0', ($employee_details->verification_done == '0') ? true :false); ?> >No
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[verification_done]" value="1" <?php echo  set_radio('user_details[verification_done]', '1', ($employee_details->verification_done == '1') ? true :false); ?> >Yes
                                        </label>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3">Marital status</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[marital_status]" value="1" <?php echo  set_radio('user_details[marital_status]', '1',($employee_details->marital_status == '1') ? true :false); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[marital_status]" value="0" <?php echo  set_radio('user_details[marital_status]', '0', ($employee_details->marital_status == '0') ? true :false); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="wife_name" class="col-xs-3 col-form-label">Wife Name</label>
                                <div class="col-xs-9">
                                    <input name="user_details[wife_name]" class="form-control" type="text" placeholder="Wife Name" id="wife_name"  value="<?php echo $employee_details->wife_name ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3">Wife Job Status</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[wife_job_status]" value="1" <?php echo  set_radio('user_details[wife_job_status]', '1', ($employee_details->wife_job_status == '1') ? true :false); ?> >Housewife
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[wife_job_status]" value="0" <?php echo  set_radio('user_details[wife_job_status]', '0',($employee_details->wife_job_status == '0') ? true :false); ?> >Worker
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="num_of_child" class="col-xs-3 col-form-label">No. Of Children</label>
                                <div class="col-xs-9">
                                    <input name="user_details[num_of_child]" class="form-control" type="text" placeholder="No. Of Children" id="num_of_child"  value="<?php echo $employee_details->num_of_child ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mother_name" class="col-xs-3 col-form-label">Mother Name</label>
                                <div class="col-xs-9">
                                    <input name="user_details[mother_name]" class="form-control" type="text" placeholder="Mother Name" id="mother_name"  value="<?php echo $employee_details->mother_name ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3">Fitness certificate Submitted</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[fitness_submitted]" value="1" <?php echo  set_radio('user_details[fitness_submitted]', '1', ($employee_details->fitness_submitted == '1') ? true :false); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[fitness_submitted]" value="0" <?php echo  set_radio('user_details[fitness_submitted]', '0', ($employee_details->fitness_submitted == '0') ? true :false); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="height" class="col-xs-3 col-form-label">Height</label>
                                <div class="col-xs-9">
                                    <input name="user_details[height]" class="form-control" type="text" placeholder="Height" id="height"  value="<?php echo $employee_details->height ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="weight" class="col-xs-3 col-form-label">Weight</label>
                                <div class="col-xs-9">
                                    <input name="user_details[weight]" class="form-control" type="text" placeholder="Weight" id="weight"  value="<?php echo $employee_details->weight ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="complexion" class="col-xs-3 col-form-label">Complexion</label>
                                <div class="col-xs-9">
                                    <input name="user_details[complexion]" class="form-control" type="text" placeholder="Complexion" id="complexion"  value="<?php echo $employee_details->complexion ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="allergies" class="col-xs-3 col-form-label">Allergies</label>
                                <div class="col-xs-9">
                                    <input name="user_details[allergies]" class="form-control" type="text" placeholder="Allergies" id="allergies"  value="<?php echo $employee_details->allergies ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="family_dr_name" class="col-xs-3 col-form-label">Family Dr name</label>
                                <div class="col-xs-9">
                                    <input name="user_details[family_dr_name]" class="form-control" type="text" placeholder="Family Dr name" id="family_dr_name"  value="<?php echo $employee_details->family_dr_name ?>">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="any_disability" class="col-xs-3 col-form-label">Any disability</label>
                                <div class="col-xs-9">
                                    <input name="user_details[any_disability]" class="form-control" type="text" placeholder="Any disability" id="any_disability"  value="<?php echo $employee_details->any_disability ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>

</div>