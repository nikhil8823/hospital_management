<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-9 col-sm-12">

                        <?php echo form_open_multipart('human_resources/employee/emp_details/'.$employee->user_id,'class="form-inner"') ?>

                            <input name="user_id" type="hidden" id="hidden_user_id" value="<?php echo $employee_details->user_id ?>">
                            <div class="form-group row">
                                <label for="search_employee" class="col-xs-3 col-form-label"><?php echo "Search Employee"; ?></label>
                                <div class="col-xs-9">
                                    <input name="search_employee" type="text" class="form-control ui-autocomplete-input" id="search_employee" placeholder="<?php echo "Search Employee"; ?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="user_role" class="col-xs-3 col-form-label">Employee Role<i class="text-danger">*</i></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        echo form_dropdown('user_role', $userRoles, $employee->user_role, 'class="form-control" id="user_role" '); 
                                    ?>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-3">Employee Category</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[emp_category]" value="1" <?php echo  set_radio('user_details[emp_category]', '1', ($employee_details->emp_category == '1') ? true :false); ?> >Permanent
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[emp_category]" value="0" <?php echo  set_radio('user_details[emp_category]', '0', ($employee_details->emp_category == '0') ? true :false); ?> >Temporary
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="date_of_joining" class="col-xs-3 col-form-label">Date Of Joining</label>
                                <div class="col-xs-9">
                                    <input name="user_details[date_of_joining]" class="datepicker form-control" type="text" placeholder="Date Of Joining" id="date_of_joining" value="<?php echo $employee_details->date_of_joining ?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="time_of_joining" class="col-xs-3 col-form-label">Time Of Joining</label>
                                <div class="col-xs-9">
                                    <input name="user_details[time_of_joining]" type="text" class="form-control" id="time_of_joining" placeholder="Time Of Joining" value="<?php echo $employee_details->time_of_joining?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bankname" class="col-xs-3 col-form-label">Bank Name</label>
                                <div class="col-xs-9">
                                    <input name="user_details[bankname]" type="text" class="form-control" id="bankname" placeholder="Bank Name" value="<?php echo $employee_details->bankname?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="account_number" class="col-xs-3 col-form-label">Account Number</label>
                                <div class="col-xs-9">
                                    <input name="user_details[account_number]" class="form-control" type="text" placeholder="Account Number" id="account_number"  value="<?php echo $employee_details->account_number ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pf_account_number" class="col-xs-3 col-form-label">PF Account Number</label>
                                <div class="col-xs-9">
                                    <input name="user_details[pf_account_number]" class="form-control" type="text" placeholder="PF Account Number" id="pf_account_number"  value="<?php echo $employee_details->pf_account_number ?>">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="ifsc_code" class="col-xs-3 col-form-label">IFSC code</label>
                                <div class="col-xs-9">
                                    <input name="user_details[ifsc_code]" class="form-control" type="text" placeholder="IFSC code" id="ifsc_code"  value="<?php echo $employee_details->ifsc_code ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pan_card_number" class="col-xs-3 col-form-label">PAN Card Number</label>
                                <div class="col-xs-9">
                                    <input name="user_details[pan_card_number]" class="form-control" type="text" placeholder="PAN Card Number" id="pan_card_number"  value="<?php echo $employee_details->pan_card_number ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3">Shift name</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[shift_name]" value="morning" <?php echo  set_radio('user_details[shift_name]', 'morning', ($employee_details->shift_name == 'morning') ? true :false); ?> >Morning
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[shift_name]" value="afternoon" <?php echo  set_radio('user_details[shift_name]', 'afternoon', ($employee_details->shift_name == 'afternoon') ? true :false); ?> >Afternoon
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[shift_name]" value="night" <?php echo  set_radio('user_details[shift_name]', 'night', ($employee_details->shift_name == 'night') ? true :false); ?> >Night
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="shift_timing" class="col-xs-3 col-form-label">Shift Timing</label>
                                <div class="col-xs-9">
                                    <input name="user_details[shift_timing]" class="form-control" type="text" placeholder="Shift Timing" id="shift_timing"  value="<?php echo $employee_details->shift_timing ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="off_day" class="col-xs-3 col-form-label">Off Day</label>
                                <div class="col-xs-9">
                                    <input name="user_details[off_day]" class="form-control" type="text" placeholder="Off Day" id="off_day"  value="<?php echo $employee_details->off_day ?>">
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-3">Over Time Applicable</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[ot_applicable]" value="1" <?php echo  set_radio('user_details[ot_applicable]', '1',($employee_details->ot_applicable == '1') ? true :false); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[ot_applicable]" value="0" <?php echo  set_radio('user_details[ot_applicable]', '0', ($employee_details->ot_applicable == '0') ? true :false); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-3">Bonus Applicable</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[bonus_applicable]" value="1" <?php echo  set_radio('user_details[bonus_applicable]', '1',($employee_details->bonus_applicable == '1') ? true :false); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[bonus_applicable]" value="0" <?php echo  set_radio('user_details[bonus_applicable]', '0', ($employee_details->bonus_applicable == '0') ? true :false); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-3">Pension</label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[pension]" value="1" <?php echo  set_radio('user_details[pension]', '1',($employee_details->pension == '1') ? true :false); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="user_details[pension]" value="0" <?php echo  set_radio('user_details[pension]', '0', ($employee_details->pension == '0') ? true :false); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pf_limit" class="col-xs-3 col-form-label">PF Limit</label>
                                <div class="col-xs-9">
                                    <input name="user_details[pf_limit]" class="form-control" type="text" placeholder="PF Limit" id="pf_limit"  value="<?php echo $employee_details->pf_limit ?>">
                                </div>
                            </div>

                        <div class="is_doctor" style="display: none;">
                            <div class="form-group row">
                                <label for="designation" class="col-xs-3 col-form-label"><?php echo display('designation') ?></label>
                                <div class="col-xs-9">
                                    <input name="designation" type="text" class="form-control" id="designation" placeholder="<?php echo display('designation') ?>" value="<?php echo $employee->designation ?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="department_id" class="col-xs-3 col-form-label"><?php echo display('department') ?> </label>
                                <div class="col-xs-9">
                                    <?php echo form_dropdown('department_id',$department_list,$employee->department_id,'class="form-control" id="department_id"') ?>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="short_biography" class="col-xs-3 col-form-label"><?php echo display('short_biography') ?></label>
                                <div class="col-xs-9">
                                    <textarea name="short_biography" class="tinymce form-control" placeholder="Address" id="short_biography" maxlength="140" rows="7"><?php echo $employee->short_biography ?></textarea>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="specialist" class="col-xs-3 col-form-label"><?php echo display('specialist') ?></label>
                                <div class="col-xs-9">
                                    <input type="text" name="specialist" class="form-control" placeholder="<?php echo display('specialist') ?>" id="specialist" value="<?php echo $employee->specialist ?>" >
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="degree" class="col-xs-3 col-form-label"><?php echo display('education_degree') ?></label>
                                <div class="col-xs-9">
                                    <textarea name="degree" class="tinymce form-control" placeholder="<?php echo display('education_degree') ?>" id="degree" maxlength="140" rows="7"><?php echo $employee->degree ?></textarea>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>    
                        
                        
                            <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_employee', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_employee' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('human_resources/employee/get_employee') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
     var userid = ui.item.value; // selected value
     $("#hidden_user_id").val(userid);
     return false;
   }
  });
    
   }); 
   
   $(document).ready(function(){
        var user_role = "<?php echo $employee->user_role; ?>";
        (user_role == "2") ? $('.is_doctor').show() : $('.is_doctor').hide();
   });
   
   
   
   $(document).on('change','#user_role', function() {
        if( this.value == '2')
        {
            $('.is_doctor').show();
        }
        else
        {
           $('.is_doctor').hide();
        }
   });
   
</script>