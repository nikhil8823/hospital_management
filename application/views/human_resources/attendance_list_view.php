<div class="row">
    <!--  form area -->
    <div class="col-sm-12"  id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("human_resources/employee/emp_attendance_list") ?>"> <i class="fa fa-list"></i>Attendance List </a>  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <tbody>
                                <tr>  
                                    <td width="10%" class=""><center><img src="<?php echo base_url($website->logo); ?>" height="50px" width="50px"></center></td>
                                    <td width="70%" class="">
                                        <center><ul class="list-unstyled">
                                            <li><strong><?php echo $website->title; ?></strong></li>  
                                            <li><?php echo $website->description; ?></li>  
                                            <li><?php echo $website->email; ?></li>  
                                            <li><?php echo $website->phone; ?></li>  
                                        </ul></center>
                                    </td> 
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>

                        <table class="table">
                            <tbody>
                                <tr class="">
                                    <td width="45%">
                                        <strong>Employee Id</strong>: <?php echo (isset($salary[0]->emp_code) && $salary[0]->emp_code != '') ? $salary[0]->emp_code : '';?> 
                                    </td>
                                    <td width="45%">
                                        <strong>Name</strong>: <?php echo (isset($salary[0]->fullname) && $salary[0]->fullname != '') ? $salary[0]->fullname : ''; ?>
                                    </td> 
                                </tr>
                                
                               
                              
                              
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        
                        
                        <table class="table">                            
                            <thead>
                                <tr>  
                                    <td><strong>Date</strong></td>
                                    <td><strong>Start Time</strong></td>
                                    <td><strong>End Time</strong></td>
                                    <td><strong>Total Hour</strong></td>
                                    <td><strong>Days</strong></td>
                                    <td><strong>Leave Type</strong></td>

                                </tr>
                            </thead>
                             <tbody>
                             
                              <?php 
                            $tothalfdays = 0;$totfulldays = 0; $fulldays=0; $halfdays=0;
                        if (!empty($salary)) {
                            $sl = 1;
                            foreach ($salary as $value) {
                             
                               ?>
                                <tr>  
                               <td><?php echo $value->month;?></strong></td>
                               <td><?php echo $value->start_time;?></strong></td>
                               <td><?php echo $value->end_time;?></strong></td>
                               <td><?php echo $value->total_hours;?></strong></td>
                               <td><?php if($value->total_hours >= 9)
                                { 
                                  echo "FD"; 
                                $fulldays = count($value->total_hours);
                                  
                                }else{ echo "HD";
                                $halfdays = count($value->total_hours);
                                }?></strong></td>
                               <td><?php echo $value->leave_type;?></strong></td>
                               </tr>
                               <?php 
                               $tothalfdays = $tothalfdays + $halfdays;
                               $totfulldays = $totfulldays + $fulldays;
                            }
                            
                        }

                        ?>
                                
                            </tbody>
                            
                        </table>

                          <table class="table">
                            <tbody>
                                <!--<tr class="">
                                    <td width="45%">
                                        <strong>Employee Id</strong>: <?php echo ($salary->emp_code != '') ? $salary->emp_code : '-'; ?> 
                                    </td>
                                    <td width="45%">
                                        <strong>Name</strong>: <?php echo ($salary->fullname != '') ? $salary->fullname : '-'; ?>
                                    </td> 
                                </tr>-->
                                
                               
                               <tr class="">
                                    <td width="30%">
                                        <strong>Total Full Days</strong>: <?php echo $totfulldays; ?>
                                    </td>
                                    <td width="30%">
                                        <strong>Total Half Days</strong>:<?php echo $tothalfdays; ?> 
                                    </td> 
                                    <td width="30%">
                                        <strong>Total Leaves</strong>: <?php ?> 
                                    </td>
                                    
                                </tr>
                                
                                 
                              
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Signature -->
                        <table class="table" style="margin-top:50px"> 
                            <thead> 
                                
                                <tr>
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Employer's Signature</i>
                                    </td>
                                    
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Employee's Signature</i>
                                    </td>
                                </tr>
                            </thead>
                        </table> 

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

