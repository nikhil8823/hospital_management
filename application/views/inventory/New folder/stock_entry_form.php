<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("inventory/inventory/stock_entry_list") ?>"> <i class="fa fa-list"></i>Purchase_order List  </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
               <?php echo form_open_multipart('inventory/inventory/create_stock_entry','class="form-inner"') ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                          <input name="id" type="hidden" id="hidden_patient_id_test" value="<?php echo $supplier->id;?>">

                             <div class="form-group row">
                                <label for="bill_no" class="col-xs-3 col-form-label">Bill No<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="bill_no" class="form-control" type="text" placeholder="Bill No" id="bill_no"  value="<?php echo $supplier->bill_no ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="bill_date" class="col-xs-3 col-form-label">Bill Date<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="bill_date" class="datepicker form-control" type="text" placeholder="Bill Date" id="bill_date"  value="<?php echo $supplier->bill_date ?>">
                                </div>
                            </div>


                    </div>
                    <div class="col-md-6 col-sm-12">


                            <div class="form-group row">
                                <label for="supplier_name" class="col-xs-3 col-form-label">Firm Name</label>
                                <div class="col-xs-9">
                                   <?php
                                      
                                      echo form_dropdown('supplier_name', $supplier_list, $supplier->supplier_name, 'class="form-control" id="supplier_name" onchange="getField(this.value)"'); 
                                    ?>
                                </div>
                            </div>

                            
                           
                        </div>
                    
                   
                </div>

                   <div class="col-md-12 col-sm-12">

                      <!-- Medicine -->
                       <table class="table table-striped" id="dataTable"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width="130">Product</th>
                                    <th width="60">Rate</th>
                                    <th width="40">Qty</th>
                                    <th width="40">Free Qty</th>
                                    <th width="110">Batch No</th>
                                    <th width="80">Exp Date</th>
                                    <th width="40">GST</th>
                                    <th width="80">Amount</th>
                                    <th width="50">MRP</th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                            <tbody id="purchaseorder" class="clone_count">
                                <tr>
                                    <td>
                                    <select  name="prescription[0][product_name]"  autocomplete="off" class="product_name">
                                    <option value="">Select Product</option>
                                    </select></td>
                                    <td><input type="text" data-unique-name="rate" name="prescription[0][rate]" autocomplete="off" class="form-control rate"  onkeyup="test();"></td>
                                    <td><input type="text" data-unique-name="qty" name="prescription[0][qty]" autocomplete="off" class="form-control qty" onkeyup="test();"></td>
                                    <td><input type="text" data-unique-name="free_qty" name="prescription[0][free_qty]" autocomplete="off" class="form-control gst_value" "></td> 
                                    <td><input type="text" data-unique-name="batch_no" name="prescription[0][batch_no]" autocomplete="off" class="form-control"></td> 
                                    <td><input type="text" data-unique-name="exp_date" name="prescription[0][exp_date]" autocomplete="off" class="form-control exp_date"></td> 
                                   
                                     <td><input type="text" data-unique-name="tax" name="prescription[0][tax]" autocomplete="off" class="form-control tax"></td> 
                                    
                                     <td><input type="text" data-unique-name="amount" name="prescription[0][amount]" autocomplete="off" class="form-control amount"></td> 
                                      <td><input type="text" data-unique-name="mrp" name="prescription[0][mrp]" autocomplete="off" class="form-control mrp"></td> 
                                   
                                    <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr>  
                            </tbody> 
                        </table>


                     </div>


                  <div class="col-md-6 col-sm-12">


                       <div class="form-group row">
                                <label for="total_amount" class="col-xs-3 col-form-label">Total Amount<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="total_amount" type="text" class="form-control" id="total_amount" placeholder="Total Amount" value="<?php echo $supplier->total_amount ?>" readonly="true">
                                </div>
                            </div>


                          <div class="form-group row">
                                <label for="credit_note" class="col-xs-3 col-form-label">Credit Note<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="credit_note" type="text" class="form-control" id="credit_note" placeholder="Credit Note" value="<?php echo $supplier->credit_note ?>" onkeyup="credit_notcal123();">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="discount" class="col-xs-3 col-form-label">Discount<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="discount" type="text" class="form-control" id="discount" placeholder="Discount" value="<?php echo $supplier->discount ?>" onkeyup="discount_cal123();">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="other" class="col-xs-3 col-form-label">Other<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="other" type="text" class="form-control" id="other" placeholder="Other" value="<?php echo $supplier->other ?>">
                                </div>
                            </div>
    
                           
                        </div>


                         <div class="col-md-6 col-sm-12">

                             <div class="form-group row">
                                <label for="gst" class="col-xs-3 col-form-label">GST<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="gst" type="text" class="form-control" id="gst" placeholder="GST" value="<?php echo $supplier->gst ?>">
                                </div>
                            </div>
                     
                            <div class="form-group row">
                                <label for="grand_tot" class="col-xs-3 col-form-label">Grand Total<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="grand_tot" type="text" class="form-control" id="grand_tot" placeholder="Grand Total" value="<?php echo $supplier->grand_tot ?>">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label for="adv_deposit" class="col-xs-3 col-form-label">Advance Deposit<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="adv_deposit" type="text" class="form-control" id="adv_deposit" placeholder="Advance Deposit" value="<?php echo $supplier->adv_deposit ?>" onkeyup="calculation();">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="netpay_amt" class="col-xs-3 col-form-label">Net Payable Amount<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="netpay_amt" type="text" class="form-control" id="netpay_amt" placeholder="Net Payable Amount" value="<?php echo $supplier->netpay_amt ?>">
                                </div>
                            </div>


                            

                            
                           
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                           
                        </div>


              <div class="col-md-12 col-sm-12"> 
                  <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>

                </div>
                    
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">

function discount_cal()
{
    var grandtotal = document.getElementById('grand_tot').value;
    var discountot = document.getElementById('discount').value;
     var tot = document.getElementById('total_amount').value;

    // var discountcal = (tot/discountot);
    // var final = grandtotal - discountcal;
    var final = grandtotal - discountot;
    document.getElementById('netpay_amt').value = final;
   // document.getElementById('grand_tot').value = grandtot;
}

function calculation()
{
     var advance = document.getElementById('adv_deposit').value;
     var grandtotal = document.getElementById('grand_tot').value;

     var grandtot =  grandtotal - advance;
  //  document.getElementById('grand_tot').value = grandtot;
    document.getElementById('netpay_amt').value = grandtot;
}


function credit_notcal()
{
     var credit = document.getElementById('credit_note').value;
     var gdtotal = document.getElementById('total_amount').value;

     var finaldtot = gdtotal - credit;
    document.getElementById('grand_tot').value = finaldtot;
   // document.getElementById('netpay_amt').value = finaldtot;
}

function test()
{

    var tableElem = document.getElementById('dataTable');
    var rowElems = tableElem.getElementsByTagName('tr');
    var i, nRows;
    var sum = 0;
    nRows = rowElems.length;
    for (i=1; i<nRows; i++)     // start at 1, since the first row (index 0) contains the table's header
    {
        var curRowInputs = rowElems[i].getElementsByTagName('input');

        var firstVal = parseInt(curRowInputs[0].value);
        var secondVal = parseInt(curRowInputs[1].value);

        
        if (isNaN(firstVal) == true)
            firstVal = 0;
           
        if (isNaN(secondVal) == true)
            secondVal = 0;

        var result = secondVal * firstVal;
        curRowInputs[6].value = result;
       
        sum = sum + result;

    }
    sum++;
    
    total = sum - 1;
    document.getElementById('total_amount').value = total;
    document.getElementById('netpay_amt').value = total;
    document.getElementById('grand_tot').value = total;
}

 function getField(val)
 {
  
    $.ajax({
     url: "<?= base_url('inventory/inventory/get_supplier_product') ?>",
     method: 'post',
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      id: val,request:1
     },
     success: function( data ) {

    $('.product_name').html(data);
    /*    var toAppend = '';
           $.each(data,function(i,o){
           toAppend = '<option value="'+ data['id'] +'">'+ data['product_name'] +'</option>';

          });

         $('#product_name').append(toAppend);*/
       
     }
    });

 }

$(document).on('change', '.product_name', function(){
        var currValue = $(this).val();
        var parentTrId = $(this).closest('tbody').attr('id');
        console.log(parentTrId);
       $.ajax({
     url: "<?= base_url('inventory/inventory/get_product_details') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      id: currValue,request:1
     },
     success: function( data ) {
        amount = 0;
     /*   $("#"+parentTrId).find('.rate').val(data.rate);
        $("#"+parentTrId).find('.qty').val(data.quantity);
        $("#"+parentTrId).find('.gst_value').val(data.gst_rate);*/
       
    var tableElem = document.getElementById('dataTable');
    var rowElems = tableElem.getElementsByTagName('tr');
    var i, nRows;
    var sum = 0;
    nRows = rowElems.length;
    for (i=1; i<nRows; i++)     // start at 1, since the first row (index 0) contains the table's header
    {
        var curRowInputs = rowElems[i].getElementsByTagName('input');

        var firstVal = parseInt(curRowInputs[0].value);
        var secondVal = parseInt(curRowInputs[1].value);

        
        if (isNaN(firstVal) == true)
            firstVal = 0;
           
        if (isNaN(secondVal) == true)
            secondVal = 0;

        var result = secondVal * firstVal;
        curRowInputs[6].value = result;

        sum = sum + result;

    }

     sum++;
    
    total = sum - 1;
    document.getElementById('total_amount').value = total;
  
     }
    });
});

/*function getsupplier_product(val)
 {

    var self = $(this);
    $.ajax({
     url: "<?= base_url('inventory/inventory/get_product_details') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      id: val,request:1
     },
     success: function( data ) {
         
         var parentTrId = self.closest('tr');
         console.log(parentTrId);

        amount = 0;
       $("#rate2").val(data.rate);
       
    var tableElem = document.getElementById('dataTable');
    var rowElems = tableElem.getElementsByTagName('tr');
    var i, nRows;
    var sum = 0;
    nRows = rowElems.length;
    for (i=1; i<nRows; i++)     // start at 1, since the first row (index 0) contains the table's header
    {
        var curRowInputs = rowElems[i].getElementsByTagName('input');

         var tot = $("#rate2").val();
       curRowInputs[3].value = tot;
            //var tot = document.getElementById('rate2'); 

    }
  
   


     }
    });

 }
*/

  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
    var userid = ui.item.value; // selected value

    // AJAX
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
 console.log(response);//return false;
      
       var id = response.id;
       var fname = response.firstname;
       var lname = response.lastname;
       var email = response.email;

       var textBoxArray = ['patient_id','firstname','lastname','email','phone','mobile','address','guardianname','birthmark','aadharno',
       'reffer_doctor_name','reffer_doctor_phone','consultant_doctor_name','consultantation_fee','followup_consultation','service_tax',
        'total_fee'];
       // Set value to textboxes
       

$.each(response, function(key , value) {    
   if(jQuery.inArray( key, textBoxArray ) != '-1') {
        $('#'+key).val(value); 
   }
   if(key == 'id') {
       $("#hidden_patient_id").val(value);
   }
});

     }
    });

    return false;
   }
  });
    
   }); 
</script>

<script type="text/javascript">

$(document).ready(function() {   
 
    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent(); 
      //  $('#purchaseorder').append("<tr>"+itemData.html()+"</tr>");
      //  $('#purchaseorder tr:last-child').find(':input').val('');

 var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var cloneCount = $('.diet_count').length;
        
        var clone = $("#purchaseorder").clone();
        clone.attr("id", "purchaseorder" + newId);
        clone.attr("class", "clone_count");
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
           
                $(this).val('');
                $(this).attr('name', "prescription[" + newId + "][" + name + "]");
            
            
        });
        console.log(clone);
        
        //$('#medicine').append("<tr>"+clone+"</tr>");
        $(".clone_count").last().after(clone);
    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


});

</script>



<SCRIPT language="javascript">


   /* function addRow55(tableID) {

      var table = document.getElementById(tableID);

      var rowCount = table.rows.length;
      var row = table.insertRow(rowCount);
      var colCount = table.rows[0].cells.length;

      for(var i=0; i<colCount; i++) {

        var newcell = row.insertCell(i);
    
        newcell.innerHTML = table.rows[0].cells[i].innerHTML;
        //alert(newcell.childNodes);


        switch(newcell.childNodes[0].type) {
          case "text":
              newcell.childNodes[0].value = "";
             //  newcell.childNodes[0].id = rowCount;
              break;
          case "checkbox":
              newcell.childNodes[0].checked = false;
             // newcell.childNodes[0].id = "checkbox" + rowCount;      
              break;
          case "select-one":
              newcell.childNodes[0].selectedIndex = 0;
            //  newcell.childNodes[0].id = "select" + rowCount; 
              break;
        }
      }
    }

    function deleteRow55(tableID) {
      try {
      var table = document.getElementById(tableID);
      var rowCount = table.rows.length;

      for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        var chkbox = row.cells[0].childNodes[0];
        if(null != chkbox && true == chkbox.checked) {
          if(rowCount <= 1) {
            alert("Cannot delete all the rows.");
            break;
          }
          table.deleteRow(i);
          rowCount--;
          i--;
        }


      }
      }catch(e) {
        alert(e);
      }
    }*/

  </SCRIPT>
      