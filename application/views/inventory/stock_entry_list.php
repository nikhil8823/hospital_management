<div class="row">
    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("inventory/inventory/create_stock_entry") ?>"> <i class="fa fa-plus"></i> Add Stock Entry </a>  
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th>Bill No</th>
                            <th>Bill Date</th>
                            <th></th>
                            <th>Total Amount</th>
                            <th>Advance</th>
                            <th>Net Pay Amt</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($supplier)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($supplier as $supplier) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $supplier->bill_no; ?></td>
                                    <td><?php echo $supplier->bill_date; ?></td>
                                    <td><?php echo "";?></td>
                                    <td><?php echo $supplier->total_amount; ?></td>
                                    <td><?php echo $supplier->adv_deposit; ?></td>
                                    <td><?php echo $supplier->netpay_amt; ?></td>
                                    <td class="center">
                                        <!--<a href="<?php echo base_url("inventory/inventory/view_stock_entry/$supplier->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-eye"></i></a>-->
                                        <a href="<?php echo base_url("inventory/inventory/delete_stock_return/$supplier->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
