<div class="row">
    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("inventory/inventory/create_stock_return") ?>"> <i class="fa fa-plus"></i> Stock Return Entry List </a>  
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th>Order Id</th>
                            <th>Order By</th>
                            <th>Order Date</th>
                            <th>Max Amount</th>
                            <th>Total Amount</th>
                            <th>Net Pay Amt</th>
                             <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($supplier)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($supplier as $supplier) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $supplier->order_id; ?></td>
                                    <td><?php echo $supplier->order_raised_by_name; ?></td>
                                    <td><?php echo $supplier->order_date; ?></td>
                                    <td><?php echo $supplier->max_amount; ?></td>
                                    <td><?php echo $supplier->total_amount; ?></td>
                                    <td><?php echo $supplier->netpay_amt; ?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url("inventory/inventory/view_stock_return/$supplier->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-eye"></i></a>
                                        <a href="<?php echo base_url("inventory/inventory/delete_stock_return/$supplier->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
