<div class="row">
    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("inventory/inventory/create_supplier") ?>"> <i class="fa fa-plus"></i> Add Supplier </a>  
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th>Owner Name</th>
                            <th>Supplier Name</th>
                            <th>Firm Name</th>
                            <th>Party Code</th>
                            <th>Reg No</th>
                            <th>DL No</th>
                            <th>Pan No</th>
                            <th>Phone No</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($supplier)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($supplier as $supplier) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $supplier->owner_name; ?></td>
                                    <td><?php echo $supplier->supplier_name; ?></td>
                                    <td><?php echo $supplier->firm_name; ?></td>
                                    <td><?php echo $supplier->party_code; ?></td>
                                    <td><?php echo $supplier->reg_no; ?></td>
                                    <td><?php echo $supplier->dl_no; ?></td>
                                    <td><?php echo $supplier->pan_no; ?></td>
                                    <td><?php echo $supplier->phone; ?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url("inventory/inventory/edit_supplier_form/$supplier->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-edit"></i></a> 
                                        <a href="<?php echo base_url("inventory/inventory/delete_supplier/$supplier->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
