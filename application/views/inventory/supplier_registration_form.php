<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("inventory/inventory/supplier_list") ?>"> <i class="fa fa-list"></i>Supplier List  </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <?php echo form_open_multipart('inventory/inventory/create_supplier','class="form-inner"') ?>

                            <input name="id" type="hidden" id="hidden_patient_id_test" value="<?php echo $supplier->id;?>">

                             <div class="form-group row">
                                <label for="owner_name" class="col-xs-3 col-form-label">Owner Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="owner_name" type="text" class="form-control" id="owner_name" placeholder="Owner Name" value="<?php echo $supplier->owner_name ?>">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label for="supplier_name" class="col-xs-3 col-form-label">Supplier name <i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="supplier_name" type="text" class="form-control" id="supplier_name" placeholder="Supplier Name" value="<?php echo $supplier->supplier_name ?>">
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="reg_no" class="col-xs-3 col-form-label">Registration No</label>
                                <div class="col-xs-9">
                                    <input name="reg_no" type="text" class="form-control" id="reg_no" placeholder="Registration No" value="<?php echo $supplier->reg_no ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="firm_name" class="col-xs-3 col-form-label">Firm Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="firm_name" type="text" class="form-control" id="firm_name" placeholder="Firm Name" value="<?php echo $supplier->firm_name ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-xs-3 col-form-label"><?php echo display('address') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <textarea name="address" class="form-control"  placeholder="<?php echo display('address') ?>" maxlength="140" rows="7"><?php echo $supplier->address ?></textarea>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <label for="phone" class="col-xs-3 col-form-label"><?php echo display('phone') ?><i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="phone" class="form-control" type="text" placeholder="<?php echo display('phone') ?>" id="phone"  value="<?php echo $supplier->phone ?>">
                                </div>
                            </div>


                               <div class="form-group row">
                                <label for="state" class="col-xs-3 col-form-label">State</label>
                                <div class="col-xs-9">
                                    <input name="state" type="text" class="form-control" id="state" placeholder="State" value="<?php echo $supplier->state ?>" >
                                </div>
                            </div>


                               <div class="form-group row">
                                <label for="city" class="col-xs-3 col-form-label">City</label>
                                <div class="col-xs-9">
                                    <input name="city" type="text" class="form-control" id="city" placeholder="City" value="<?php echo $supplier->city ?>" >
                                </div>
                            </div>


                               <div class="form-group row">
                                <label for="pincode" class="col-xs-3 col-form-label">PinCode</label>
                                <div class="col-xs-9">
                                    <input name="pincode" type="text" class="form-control" id="pincode" placeholder="PinCode" value="<?php echo $supplier->pincode ?>" >
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="licence_no" class="col-xs-3 col-form-label">Licence No </label>
                                <div class="col-xs-9">
                                    <input name="licence_no" class="form-control" type="text" placeholder="Licence No" id="licence_no"  value="<?php echo $supplier->licence_no ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="gst_no" class="col-xs-3 col-form-label">GST No </label>
                                <div class="col-xs-9">
                                    <input name="gst_no" class="form-control" type="text" placeholder="GST No" id="gst_no"  value="<?php echo $supplier->gst_no ?>">
                                </div>
                            </div>

                            
 
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                          
                    </div>
                    <div class="col-md-6 col-sm-12">

                     <div class="form-group row">
                                <label for="party_code" class="col-xs-4 col-form-label">Party Code</label>
                                <div class="col-xs-8">
                                    <input name="party_code" type="text" class="form-control" id="party_code" placeholder="Party Code" value="<?php echo $supplier->party_code ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                            <label for="dl_no" class="col-xs-4 col-form-label">DL No</label>
                                <div class="col-xs-8">
                                    <input name="dl_no" type="text" class="form-control" id="dl_no" placeholder="DL No" value="<?php echo $supplier->dl_no ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="credit_limit" class="col-xs-4 col-form-label">Credit Limit</label>
                                <div class="col-xs-8">
                                    <input name="credit_limit" type="text" class="form-control" id="credit_limit" placeholder="Credit Limit" value="<?php echo $supplier->credit_limit ?>">
                                </div>
                            </div>


                        <div class="form-group row">
                                <label for="payment_mode" class="col-xs-4 col-form-label">Payment Mode</label>
                                <div class="col-xs-8">
                                   <?php
                                        $modeList = array(
                                            ''   => display('select_option'),
                                            'Cash' => 'Cash',
                                            'Check' => 'Check',
                                            'NEFT' => 'NEFT',
                                            'RTGS' => 'RTGS',
                                            'Card' => 'Card',
                                           
                                        );
                                        echo form_dropdown('payment_mode', $modeList, $supplier->payment_mode, 'class="form-control" id="payment_mode" '); 
                                    ?>
                                </div>
                            </div>
                            
                            
                             <div class="form-group row">
                                <label for="side_days" class="col-xs-4 col-form-label">Side Days</label>
                                <div class="col-xs-8">
                                    <input name="side_days" type="text" class="form-control" id="side_days" placeholder="Side Days" value="<?php echo $supplier->side_days ?>">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="guar_name" class="col-xs-4 col-form-label">Guarantor Name</label>
                                <div class="col-xs-8">
                                    <input name="guar_name" type="text" class="form-control" id="guar_name" placeholder="Guarantor Name" value="<?php echo $supplier->guar_name ?>">
                                </div>
                            </div>
                            

                           <div class="form-group row">
                                <label for="email_id" class="col-xs-4 col-form-label">Email Id</label>
                                <div class="col-xs-8">
                                    <input name="email_id" type="text" class="form-control" id="email_id" placeholder="Email Id" value="<?php echo $supplier->email_id ?>" >
                                </div>
                            </div>


                             <div class="form-group row">
                                <label for="pan_no" class="col-xs-4 col-form-label">Pan No </label>
                                <div class="col-xs-8">
                                    <input name="pan_no" type="text" class="form-control" id="pan_no" placeholder="Pan No" value="<?php echo $supplier->pan_no ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="transporter_name" class="col-xs-4 col-form-label">Transporter Name</label>
                                <div class="col-xs-8">
                                    <input name="transporter_name" type="text" class="form-control" id="transporter_name" placeholder="Transporter Name" value="<?php echo $supplier->transporter_name ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="transporter_address" class="col-xs-4 col-form-label">Transporter Address</label>
                                <div class="col-xs-8">
                                    <input name="transporter_address" type="text" class="form-control" id="transporter_address" placeholder="Transporter Address" value="<?php echo $supplier->transporter_address ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="transporter_phone" class="col-xs-4 col-form-label">Transporter Phone No</label>
                                <div class="col-xs-8">
                                    <input name="transporter_phone" type="text" class="form-control" id="transporter_phone" placeholder="Transporter Phone No" value="<?php echo $supplier->transporter_phone ?>" >
                                </div>
                            </div>

                           
                           

                           
                        </div>
                    
                   
                </div>

                   <div class="col-md-12 col-sm-12">
                      
                  <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>

                </div>
                    
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
    var userid = ui.item.value; // selected value

    // AJAX
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
 console.log(response);//return false;
      
       var id = response.id;
       var fname = response.firstname;
       var lname = response.lastname;
       var email = response.email;

       var textBoxArray = ['patient_id','firstname','lastname','email','phone','mobile','address','guardianname','birthmark','aadharno',
       'reffer_doctor_name','reffer_doctor_phone','consultant_doctor_name','consultantation_fee','followup_consultation','service_tax',
        'total_fee'];
       // Set value to textboxes
       

$.each(response, function(key , value) {    
   if(jQuery.inArray( key, textBoxArray ) != '-1') {
        $('#'+key).val(value); 
   }
   if(key == 'id') {
       $("#hidden_patient_id").val(value);
   }
});

     }
    });

    return false;
   }
  });
    
   }); 
</script>

<script type="text/javascript">
$(document).ready(function() {   
 
    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#itemdept').append("<tr>"+itemData.html()+"</tr>");
        $('#itemdept tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


     //#------------------------------------
    //   STARTS OF Approval limit 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn_limit' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#limit_approval').append("<tr>"+itemData.html()+"</tr>");
        $('#limit_approval tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn_limit' ,function() {
        $(this).parent().parent().parent().remove();
    });

});

</script>


      