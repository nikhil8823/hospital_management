<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("inventory/inventory/supplier_product_list") ?>"> <i class="fa fa-list"></i>Supplier Product List  </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <?php echo form_open_multipart('inventory/inventory/create_supplier_product','class="form-inner"') ?>

                            <input name="id" type="hidden" id="hidden_patient_id_test" value="<?php echo $supplier->id;?>">

                            <div class="form-group row">
                                <label for="supplier_name" class="col-xs-4 col-form-label">Supplier Name</label>
                                <div class="col-xs-8">
                                   <?php
                                      
                                        echo form_dropdown('supplier_name', $supplier_list, $supplier->supplier_name, 'class="form-control" id="supplier_name" '); 
                                    ?>
                                </div>
                            </div>


                              <div class="form-group row">
                                <label for="item_dept" class="col-xs-4 col-form-label">Item Department</label>
                                <div class="col-xs-8">
                                   <?php
                                        $item_deptList = array(
                                            ''   => display('select_option'),
                                            'Medical' => 'Medical',
                                            'Hospital' => 'Hospital',
                                            'Surgical' => 'Surgical',
                                            'Lab' => 'Lab',
                                            'Instruments' => 'Instruments',
                                            'OT' => 'OT',
                                           
                                        );
                                        echo form_dropdown('item_dept', $item_deptList, $supplier->item_dept, 'class="form-control" id="item_dept" '); 
                                    ?>
                                </div>
                            </div>
                            


                             <div class="form-group row">
                                <label for="pack_name" class="col-xs-4 col-form-label">Pack Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="pack_name" type="text" class="form-control" id="pack_name" placeholder="Pack Name" value="<?php echo $supplier->pack_name ?>">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label for="unit" class="col-xs-4 col-form-label">Unit<i class="text-danger"></i></label>
                                <div class="col-xs-8">
                                    <input name="unit" type="text" class="form-control" id="unit" placeholder="Unit" value="<?php echo $supplier->unit ?>">
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="product_name" class="col-xs-4 col-form-label">Product Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="product_name" type="text" class="form-control" id="product_name" placeholder="Product Name" value="<?php echo $supplier->product_name ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="strength" class="col-xs-4 col-form-label">Strength<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="strength" type="text" class="form-control" id="strength" placeholder="Strength" value="<?php echo $supplier->strength ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="size" class="col-xs-4 col-form-label">Size<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="size" type="text" class="form-control" id="size" placeholder="Size" value="<?php echo $supplier->size ?>" >
                                </div>
                            </div>
 

                            <div class="form-group row">
                                <label for="ptr" class="col-xs-4 col-form-label">PTR</label>
                                <div class="col-xs-8">
                                    <input name="ptr" type="text" class="form-control" id="ptr" placeholder="PTR" value="<?php echo $supplier->ptr ?>" >
                                </div>
                            </div>

                           
 
                            <div class="form-group row">
                                <label class="col-sm-4"><?php echo display('status') ?></label>
                                <div class="col-xs-8">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                          
                    </div>
                    <div class="col-md-6 col-sm-12">

                     <div class="form-group row">
                                <label for="mfg_date" class="col-xs-4 col-form-label">MFG Date</label>
                                <div class="col-xs-8">
                                    <input name="mfg_date" type="text" class="form-control" id="mfg_date" placeholder="MFG Date" value="<?php echo $supplier->mfg_date ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                            <label for="exp_date" class="col-xs-4 col-form-label">EXP Date<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="exp_date" type="text" class="form-control" id="exp_date" placeholder="EXP Date" value="<?php echo $supplier->exp_date ?>">
                                </div>
                            </div>

                            
                             <div class="form-group row">
                                <label for="hsn_code" class="col-xs-4 col-form-label">HSN Code</label>
                                <div class="col-xs-8">
                                    <input name="hsn_code" type="text" class="form-control" id="hsn_code" placeholder="HSN Code" value="<?php echo $supplier->hsn_code ?>">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="product_type" class="col-xs-4 col-form-label">Product Type</label>
                                <div class="col-xs-8">
                                   <?php
                                        $item_deptList = array(
                                            ''   => display('select_option'),
                                            'Asset' => 'Asset',
                                            'Non Asset' => 'Non Asset',
                                           
                                           
                                        );
                                        echo form_dropdown('product_type', $item_deptList, $supplier->product_type, 'class="form-control" id="product_type" '); 
                                    ?>
                                </div>
                            </div>
                            
                            

                           <div class="form-group row">
                                <label for="gst_rate" class="col-xs-4 col-form-label">GSt Rate %</label>
                                <div class="col-xs-8">
                                    <input name="gst_rate" type="text" class="form-control" id="gst_rate" placeholder="GSt Rate %" value="<?php echo $supplier->gst_rate ?>" >
                                </div>
                            </div>


                             <div class="form-group row">
                                <label for="quantity" class="col-xs-4 col-form-label">Quantity </label>
                                <div class="col-xs-8">
                                    <input name="quantity" type="text" class="form-control" id="quantity" placeholder="Quantity" value="<?php echo $supplier->quantity ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="rate" class="col-xs-4 col-form-label">Rate</label>
                                <div class="col-xs-8">
                                    <input name="rate" type="text" class="form-control" id="rate" placeholder="Rate" value="<?php echo $supplier->rate ?>" >
                                </div>
                            </div>


                           
                        </div>
                    
                   
                </div>

                   <div class="col-md-12 col-sm-12">
                      
                  <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>

                </div>
                    
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
    var userid = ui.item.value; // selected value

    // AJAX
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
 console.log(response);//return false;
      
       var id = response.id;
       var fname = response.firstname;
       var lname = response.lastname;
       var email = response.email;

       var textBoxArray = ['patient_id','firstname','lastname','email','phone','mobile','address','guardianname','birthmark','aadharno',
       'reffer_doctor_name','reffer_doctor_phone','consultant_doctor_name','consultantation_fee','followup_consultation','service_tax',
        'total_fee'];
       // Set value to textboxes
       

$.each(response, function(key , value) {    
   if(jQuery.inArray( key, textBoxArray ) != '-1') {
        $('#'+key).val(value); 
   }
   if(key == 'id') {
       $("#hidden_patient_id").val(value);
   }
});

     }
    });

    return false;
   }
  });
    
   }); 
</script>

<script type="text/javascript">
$(document).ready(function() {   
 
    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#itemdept').append("<tr>"+itemData.html()+"</tr>");
        $('#itemdept tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


     //#------------------------------------
    //   STARTS OF Approval limit 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn_limit' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#limit_approval').append("<tr>"+itemData.html()+"</tr>");
        $('#limit_approval tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn_limit' ,function() {
        $(this).parent().parent().parent().remove();
    });

});

</script>


      