<div class="row">
    <!--  form area -->
    <div class="col-sm-12"  id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("inventory/inventory/purchase_order_list") ?>"> <i class="fa fa-list"></i>  <?php echo "Purchase Order List"; ?> </a>  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body">

                   <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <thead>
                                <tr  class="">
                                    <td colspan="2">
                                       
                                    </td>
                                    <td  class="text-right">
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="20%" class=""><center><img src="<?php echo base_url($website->logo); ?>" height="50px" width="50px"></center></td>
                                       
                                    <td width="80%" class="text-right">
                                        <center><ul class="list-unstyled">
                                            <li><strong><?php echo $website->title; ?></strong></li>  
                                            <li><?php echo $website->description; ?></li>  
                                            <li><?php echo $website->email; ?></li>  
                                            <li><?php echo $website->phone; ?></li>  
                                        </ul></center>
                                    </td> 

                                      <td width="" class="text-left">
                                         <!--<center> <ul class="list-unstyled">
                                            <li><strong><?php echo ($bill->doctor_name != '') ? $bill->doctor_name : '-'; ?></strong></li> 
                                            <li><?php echo $bill->specialist; ?></li>  
                                        </ul> </center> -->
                                    </td> 
                                </tr>  
                            </tbody>
                            <tfoot>
                                <tr class="">
                                    <td colspan="3">
                                        
                                        <strong><?php echo "Order Date" ?></strong>: <?php echo $bill->order_date; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Order Raised By" ?></strong>: <?php echo $bill->order_raised_by_name; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Max Amount" ?></strong>: <?php echo $bill->max_amount; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                    </td> 
                                </tr>
                                <tr class="">
                                <td colspan="3">
                                    
                                        <strong><?php echo "Firm Name" ?></strong>: <?php  echo $bill->firm_name; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                       <!-- <strong><?php echo "Bed" ?></strong>: <?php echo ($bill->bed_name != '') ? $bill->bed_name : '-'; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Admission Date" ?></strong>: <?php echo ($bill->admission_date != '') ? $bill->admission_date : '-'; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;-->
                                    </td> 
                                </tr>
                                
                            </tfoot>
                        </table>

                    </div>
                </div>
                
              

                   <div class="col-md-12 col-sm-12">
                      <?php if($bill->medicine != '') { ?>
                       <table class="table table-striped" id="datatable"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width=""><?php echo 'Product Name' ?></th>
                                    <th width=""><?php echo 'Rate' ?></th>
                                    <th><?php echo 'Qty' ?></th>
                                    <th><?php echo 'Amount' ?></th>
                                     <th><?php echo 'Remark' ?></th>
                                </tr>
                            </thead>
                            <tbody id="medicine">
                            <?php /*print_r($bill->medicine);*/?>
                            <?php foreach(json_decode($bill->medicine) as $data) { 

                                     $product_details = $this->db->select("*")->from('tbl_supplier_products')->where('id',$data->product)->get()->result();

                                    foreach ($product_details as $products) {

                                  ?> 

                                    <tr>
                                        <td><?php echo ($products->product_name != '') ? $products->product_name : '-'; ?></td>
                                        <td><?php echo ($data->rate != '') ? $data->rate : '-'; ?></td>
                                        <td><?php echo ($data->qty != '') ? $data->qty : '-'; ?></td>
                                        <td><?php echo ($data->amount != '') ? $data->amount : '-'; ?></td> 
                                        <td><?php echo ($data->remark != '') ? $data->remark : '-'; ?></td> 
                                    </tr>
                            <?php } } ?>
                            </tbody>
                        </table>
                      <?php } ?>
                  </div>
                 </div>
                
                 <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <thead>
                                <tr  class="">
                                    <td colspan="2">
                                       
                                    </td>
                                    <td  class="text-right">
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                              
                            </tbody>
                            <tfoot>
                                <tr class="">
                                    <td>
                                        
                                        <strong><?php echo "Total Amount" ?></strong>: <?php echo $bill->total_amount; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  </td> 
                                     <td>
                                         <strong><?php echo "Advance Payment" ?></strong>: <?php echo ($bill->adv_deposit != '') ? $bill->adv_deposit : '-'; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  </td>

                                      <td>
                                        <strong><?php echo "Credit Note" ?></strong>: <?php echo $bill->credit_note; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  

                                       
                                    </td> 
                                </tr>
                                <tr class="">
                                <td>
                                       
                                        <strong><?php echo "Discount" ?></strong>: <?php echo $bill->discount; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;</td>
                                <td>

                                        <strong><?php echo "Grand Total" ?></strong>: <?php  echo "$bill->grand_tot"; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  </td>
                                <td>
                                        <strong><?php echo "Net Payable Amount" ?></strong>: <?php echo ($bill->netpay_amt != '') ? $bill->netpay_amt : '-'; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 

                                      
                                    </td> 
                                </tr>

                                 <tr class="">
                                <td colspan="3">
                                          
                                     
                                        <strong><?php echo "Other" ?></strong>: <?php echo $bill->other; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;

                                      
                                    </td> 
                                </tr>
                                
                            </tfoot>
                        </table>

                    </div>
                </div>
                
                
                
            </div>
        </div>
    </div>
</div>

