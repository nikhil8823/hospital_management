<div class="row">
    <!--  form area -->
    <div class="col-sm-12"  id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("ot_manager/patient/prescription_list") ?>"> <i class="fa fa-list"></i>  <?php echo display('prescription_list') ?> </a>  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <thead>
                                <tr  class="bg-primary">
                                    <td colspan="3">
                                        <strong><?php echo display('patient_id') ?></strong>: <?php echo $prescription->patient_id; ?>, 
                                        
                                    </td>
                                    <!--<td  class="text-right"><strong><?php echo display('date') ?></strong>: <?php echo date('m/d/Y', strtotime($prescription->date)); ?>
                                    </td>-->
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="10%" class=""><center><img src="<?php echo base_url($website->logo); ?>" height="50px" width="50px"></center></td>

                                     <td width="50%" class="">
                                        <center><ul class="list-unstyled">
                                            <li><strong><?php echo $website->title; ?></strong></li>  
                                            <li><?php echo $website->description; ?></li>  
                                            <li><?php echo $website->email; ?></li>  
                                            <li><?php echo $website->phone; ?></li>  
                                        </ul></center>
                                    </td> 

                                    <td width="50%" class="text-left">
                                         <!--<center> <ul class="list-unstyled">
                                            <li><strong><?php echo $prescription->doctor_name; ?></strong></li> 
                                            <li><?php echo $prescription->specialist; ?></li>
                                            <li><strong><?php echo $prescription->department_name; ?></strong></li>   
                                            <li><?php echo $prescription->designation; ?></li>  
                                            <li><?php echo $prescription->address; ?></li>   
                                        </ul> </center> -->
                                    </td>      
                                   
                                </tr>  
                            </tbody>
                            <tfoot>
                                <tr class="">
                                    <td colspan="3">
                                        <?php
                                            $date1=date_create($prescription->date_of_birth);
                                            $date2=date_create(date('Y-m-d'));
                                            $diff=date_diff($date1,$date2); 
                                            
                                        ?>
                                        <strong><?php echo display('patient_name') ?></strong>: <?php echo $prescription->patient_name; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo display('age') ?></strong>: <?php echo "$diff->y Years $diff->m Months"; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Gender" ?></strong>: <?php echo $prescription->sex; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                    </td> 
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>


<div class="row">
                        <div style="float:left;width:100%;padding-left:10px">
                            <!-- Medicine -->
                            <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info header-2">
                                        <th colspan="3">Before Operation Checklist</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                               
                                if (!empty($prescription->before_operation)) {
                                    $medicine = json_decode($prescription->before_operation);
                                  
                                   /* if (sizeof($medicine) > 0) 
                                        foreach ($medicine as $value) { */
                                ?>
                                    <tr colspan="3">
                                        <td><strong>Main Doctor availability :</strong>  <?php  if($medicine->main_doctor_availability ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Assistant Doctor availability :</strong>  <?php  if($medicine->assistant_doctor_availability ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Anaesthetic availability : </strong> <?php  if($medicine->anaesthetic_availability ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                    <tr colspan="3">
                                        <td><strong>Power availability : </strong> <?php  if($medicine->power_availability ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Water availability : </strong> <?php  if($medicine->water_availability ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Manpower availability : </strong> <?php  if($medicine->manpower_availability ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>


                                    <tr colspan="3">
                                        <td><strong>Blood availability : </strong> <?php  if($medicine->blood_availability ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Pathological test report availability : </strong> <?php  if($medicine->pathological_test_report_availability ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Oxygen supply availability : </strong> <?php  if($medicine->oxygen_availability ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                    <tr colspan="3">
                                        <td><strong>Related chemical availability : </strong> <?php  if($medicine->chemical_availability ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Machine set up check up : </strong> <?php  if($medicine->machine_check_up ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>OT cleaning date & time : </strong> <?php  if($medicine->ot_cleaning ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                    <tr colspan="3">
                                        <td><strong>OT set up ready or not : </strong> <?php  if($medicine->ot_setup ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Shifting facility availability : </strong> <?php  if($medicine->shifting_facility_availability ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>All related drugs availability : </strong> <?php  if($medicine->related_drugs_availability ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                    <tr colspan="3">
                                        <td><strong>Any special Equipment device required : </strong> <?php  if($medicine->special_equipment_required ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Patient medical history check up : </strong> <?php  if($medicine->medical_history_check_up ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Prophylactic antibiotic infused availability : </strong> <?php  if($medicine->antibiotic_availability ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                    <tr colspan="3">
                                        <td><strong>Brief meeting conducted : </strong> <?php  if($medicine->brief_meeting_conducted ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Parent or guardian confirmation report : </strong> <?php  if($medicine->parent_confirmation_report ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Concent form received : </strong> <?php  if($medicine->concent_form_received ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                    <tr colspan="3">
                                        <td><strong>Known allergies : </strong> <?php  if($medicine->known_allergies ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Risk of blood loss : </strong>  <?php  if($medicine->risk_of_blood_loss ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Antibiotic before skin infution : </strong> <?php  if($medicine->antibiotic_skin_infution ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                     <tr colspan="3">
                                        <td><strong>Instrument count check list : </strong> <?php  if($medicine->instrument_count_check_list ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Sterility indication check : </strong> <?php  if($medicine->sterility_indication_check ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Patient operation site marked :</strong>  <?php  if($medicine->operation_site_marked ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                     <tr colspan="3">
                                        <td><strong>Remark : </strong> <?php  echo $medicine->remark; ?></td> 
                                        

                                    </tr>
                                <?php  
                                        }
                                    
                                ?> 

                                
                                </tbody> 
                            </table> 

                              </br> </br> </br>
                              <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info header-2">
                                        <th colspan="3">After Operation Checklist</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                               
                                if (!empty($prescription->after_operation)) {
                                    $medicine = json_decode($prescription->after_operation);
                                  
                                   /* if (sizeof($medicine) > 0) 
                                        foreach ($medicine as $value) { */
                                ?>
                                    <tr colspan="3">
                                        <td><strong>Instrument/sponge/needles etc. count are correct
 :</strong>  <?php  if($medicine->instrument_count ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Medicine started before leaving OT :</strong>  <?php  if($medicine->medicine_started ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Documentation of implanted material is filled in correctly : </strong> <?php  if($medicine->documentation_of_material ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>

                                    <tr colspan="3">
                                        <td><strong>Guardian or parents need to be address about equipment : </strong> <?php  if($medicine->parents_address ==1){echo "Yes";}else{echo "No";}; ?></td> 
                                        <td><strong>Consultant Doctor order for post operative care have been documented: </strong> <?php  if($medicine->post_care ==1){echo "Yes";}else{echo "No";}; ?></td>
                                        <td><strong>Remark : </strong> <?php  if($medicine->remark ==1){echo "Yes";}else{echo "No";}; ?></td>  

                                    </tr>


                                    
                                   
                                <?php  
                                        }
                                    
                                ?> 

                                
                                </tbody> 
                            </table> 


                        </div> 
                   </div>

                     <div class="row">
                    <div class="col-sm-12">
                        <div style="float:left;width:100%;word-break:all;border-right:1px solid #e4e5e7;padding-right:10px">
                            <!-- chief_complain -->
                            <!--<p>
                                <strong><?php echo display('chief_complain') ?></strong>: <?php echo $prescription->chief_complain; ?>
                            </p>-->
                            
                            <!-- patient_notes -->
                           <!-- <p>
                                <strong><?php echo display('patient_notes') ?></strong>: <?php echo $prescription->patient_notes; ?>
                            </p>--> 
                        </div>
                         </div>
                     </div><br>

                <div class="row">
                    <div class="col-sm-12">
                        <!-- Signature -->
                        <table class="table" style="margin-top:50px"> 
                            <thead> 
                                <tr>
                                    <th style="width:50%;"></th>
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Signature</i>
                                    </td>
                                </tr>
                            </thead>
                        </table> 

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

