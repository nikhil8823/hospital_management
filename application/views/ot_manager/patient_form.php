<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("ot_manager/patient") ?>"> <i class="fa fa-list"></i>  <?php echo display('patient_list') ?> </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                     <?php echo form_open_multipart('ot_manager/patient/create','class="form-inner"') ?>
                    <div class="col-md-6 col-sm-12">
                            <input name="id" type="hidden" id="hidden_patient_id" value="<?php echo $patient->id ?>">
                            <input name="patient_id" type="hidden" id="hidden_p_id">

                            <div class="form-group row">
                                <label for="search_patient" class="col-xs-3 col-form-label"><?php echo "Search Patient"; ?></label>
                                <div class="col-xs-9">
                                    <input name="search_patient" type="text" class="form-control ui-autocomplete-input" id="search_patient" placeholder="<?php echo "search_patient"; ?>" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="firstname" class="col-xs-3 col-form-label"><?php echo display('first_name') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="firstname" type="text" class="form-control" id="firstname" placeholder="<?php echo display('first_name') ?>" value="<?php echo $patient->firstname ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lastname" class="col-xs-3 col-form-label"><?php echo display('last_name') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="lastname" type="text" class="form-control" id="lastname" placeholder="<?php echo display('last_name') ?>" value="<?php echo $patient->lastname ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-xs-3 col-form-label"><?php echo display('email') ?></label>
                                <div class="col-xs-9">
                                    <input name="email" type="text" class="form-control" id="email" placeholder="<?php echo display('email') ?>" value="<?php echo $patient->email ?>">
                                </div>
                            </div>

                            <!--<div class="form-group row">
                                <label for="patient_id" class="col-xs-3 col-form-label"><?php echo "Patient Id" ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="patient_id" type="text" class="form-control" id="patient_id" placeholder="<?php echo "Patient Id" ?>">
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="phone" class="col-xs-3 col-form-label"><?php echo display('phone') ?></label>
                                <div class="col-xs-9">
                                    <input name="phone" class="form-control" type="text" placeholder="<?php echo display('phone') ?>" id="phone"  value="<?php echo $patient->phone ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mobile" class="col-xs-3 col-form-label"><?php echo display('mobile') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="mobile" class="form-control" type="text" placeholder="<?php echo display('mobile') ?>" id="mobile"  value="<?php echo $patient->mobile ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="blood_group" class="col-xs-3 col-form-label"><?php echo display('blood_group') ?></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'A+' => 'A+',
                                            'A-' => 'A-',
                                            'B+' => 'B+',
                                            'B-' => 'B-',
                                            'O+' => 'O+',
                                            'O-' => 'O-',
                                            'AB+' => 'AB+',
                                            'AB-' => 'AB-'
                                        );
                                        echo form_dropdown('blood_group', $bloodList, $patient->blood_group, 'class="form-control" id="blood_group" '); 
                                    ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('sex') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Male" <?php echo  set_radio('sex', 'Male', TRUE); ?> ><?php echo display('male') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Female" <?php echo  set_radio('sex', 'Female'); ?> ><?php echo display('female') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Other" <?php echo  set_radio('sex', 'Other'); ?> ><?php echo display('other') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="date_of_birth" class="col-xs-3 col-form-label"><?php echo display('date_of_birth') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="date_of_birth" class="datepicker form-control" type="text" placeholder="<?php echo display('date_of_birth') ?>" id="date_of_birth"  value="<?php echo $patient->date_of_birth ?>">
                                </div>
                            </div>

                            <!-- if patient picture is already uploaded -->
                            <?php if(!empty($patient->picture)) {  ?>
                            <div class="form-group row">
                                <label for="picturePreview" class="col-xs-3 col-form-label"></label>
                                <div class="col-xs-9">
                                    <img src="<?php echo base_url($patient->picture) ?>" alt="Picture" class="img-thumbnail" />
                                </div>
                            </div>
                            <?php } ?>

                            <!--<div class="form-group row">
                                <label for="picture" class="col-xs-3 col-form-label"><?php echo display('picture') ?></label>
                                <div class="col-xs-9">
                                    <input type="file" name="picture" id="picture" value="<?php echo $patient->picture ?>">
                                    <input type="hidden" name="old_picture" value="<?php echo $patient->picture ?>">
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="address" class="col-xs-3 col-form-label"><?php echo display('address') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <textarea id="patinet_address" name="address" class="form-control"  placeholder="<?php echo display('address') ?>" maxlength="140" rows="7"><?php echo $patient->address ?></textarea>
                                </div>
                            </div> 
 
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            
                             <div class="form-group row">
                                <label for="Payment_mode" class="col-xs-3 col-form-label">Payment Mode</label>
                                <div class="col-xs-9"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'DD' => 'DD',
                                            'Cheque' => 'Cheque',
                                            'Transfer' => 'Transfer',
                                            'Cash' => 'Cash',
                                           
                                        );
                                        echo form_dropdown('payment_mode', $bloodList, $patient->payment_mode, 'class="form-control" id="payment_mode" '); 
                                    ?>
                                </div>
                            </div>
                        
                            
                            <div class="form-group row">
                                <label for="bed_id" class="col-xs-3 col-form-label"><?php echo display('bed_type') ?></label>
                                <div class="col-xs-9">
                                    <?php echo form_dropdown('bed_id', $bed_list, array(), 'class="form-control dateChange" id="bed_id"') ?>
                                </div>
                            </div>
                            
                             <div class="form-group row">
                                <label for="assign_date" class="col-xs-3 col-form-label"><?php echo display('assign_date') ?> </label>
                                <div class="col-xs-9">
                                    <input name="assign_date"  type="text" class="form-control cdatepicker dateChange" id="assign_date" placeholder="<?php echo display('assign_date') ?>" value="<?php echo date('d-m-Y'); ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="discharge_date" class="col-xs-3 col-form-label"><?php echo display('discharge_date') ?></label>
                                <div class="col-xs-9">
                                    <input name="discharge_date"  type="text" class="form-control cdatepicker dateChange" id="discharge_date" placeholder="<?php echo display('discharge_date') ?>"  value="<?php echo date('d-m-Y'); ?>" >
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            
                                                        
                            <div class="form-group row">
                                <label for="admission_fee" class="col-xs-3 col-form-label">Admission Fee<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="admission_fee" type="text" class="form-control" id="admission_fee" placeholder="Admission Fee" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="operation_name" class="col-xs-3 col-form-label">Operation Name<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="operation_name" type="text" class="form-control" id="operation_name" placeholder="Operation Name" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="operation_date" class="col-xs-3 col-form-label">Operation Date<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="operation_date" type="text" class="form-control" id="operation_date" placeholder="Operation Date" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="patient_condition" class="col-xs-3 col-form-label">Patient Condition<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="patient_condition" type="text" class="form-control" id="patient_condition" placeholder="Patient Condition" >
                                </div>
                        </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo "Staff Finalised" ?></label>
                                <div class="col-xs-9"> 
                                    <div class="form-check">
                                        <label class="radio-inline"><input type="radio" name="staff_finalised" value="1"><?php echo "Yes"; ?></label>
                                        <label class="radio-inline"><input type="radio" name="staff_finalised" value="0" checked><?php echo "No"; ?></label>
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                    <div class="col-md-6 col-sm-12">
                        
                        
                        <div class="form-group row">
                                <label for="department_id" class="col-xs-4 col-form-label"><?php echo display('department_name') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <?php echo form_dropdown('department_id',$department_list,$patient->department_id,'class="form-control" id="department_id"') ?>
                                    <span class="doctor_error"></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="doctor_id" class="col-xs-4 col-form-label"><?php echo display('doctor_name') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <?php echo form_dropdown('doctor_id','','','class="form-control" id="doctor_id"') ?>
                                    <div id="available_days"></div>
                                </div>
                            </div>
                        
                        <div class="form-group row">
                                <label for="package_id" class="col-xs-4 col-form-label">Package Name</label>
                                <div class="col-xs-8">
                                    <?php echo form_dropdown('package_id',$package_list,array(),'class="form-control" id="department_id"') ?>
                                </div>
                            </div>
                        
                        
                        <div class="form-group row">
                                <label for="guardianname" class="col-xs-4 col-form-label">Guardian Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="guardianname" type="text" class="form-control" id="guardianname" placeholder="Guardian Name" value="<?php echo $patient->guardianname ?>" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="birthmark" class="col-xs-4 col-form-label">Birth Mark</label>
                                <div class="col-xs-8">
                                    <input name="birthmark" type="text" class="form-control" id="birthmark" placeholder="Birth Mark" value="<?php echo $patient->birthmark ?>" >
                                </div>
                            </div>
                            
                            
                             <div class="form-group row">
                                <label for="aadharno" class="col-xs-4 col-form-label">Aadhar No</label>
                                <div class="col-xs-8">
                                    <input name="aadharno" type="text" class="form-control" id="aadharno" placeholder="Aadhar No" value="<?php echo $patient->aadharno ?>" >
                                </div>
                            </div>
                            
                             <div class="form-group row">
                                <label for="religion" class="col-xs-4 col-form-label">Religion</label>
                                <div class="col-xs-8"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'Hindu' => 'Hindu',
                                            'Muslim' => 'Muslim',
                                            'Shikh' => 'Shikh',
                                            'Isai' => 'Isai',
                                            
                                        );
                                        echo form_dropdown('religion', $bloodList, $patient->religion, 'class="form-control" id="religion" '); 
                                    ?>
                                </div>
                            </div>

                           <div class="form-group row">
                                <label for="case_type" class="col-xs-4 col-form-label">Case Type</label>
                                <div class="col-xs-8"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'OPD' => 'OPD',
                                            'IPD' => 'IPD',
                                            'OT' => 'OT',
                                           
                                        );
                                        echo form_dropdown('case_type', $bloodList, $patient->case_type, 'class="form-control" id="case_type" '); 
                                    ?>
                                </div>
                            </div>

                           <div class="form-group row">
                                <label for="reffer_doctor_name" class="col-xs-4 col-form-label">Reffer Doctor Name</label>
                                <div class="col-xs-8">
                                    <input name="reffer_doctor_name" type="text" class="form-control" id="reffer_doctor_name" placeholder="Doctor Name" value="<?php echo $patient->reffer_doctor_name ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="reffer_doctor_phone" class="col-xs-4 col-form-label">Reffer Doctor Phone</label>
                                <div class="col-xs-8">
                                    <input name="reffer_doctor_phone" class="form-control" type="text" placeholder="Reffer Doctor Phone" id="reffer_doctor_phone"  value="<?php echo $patient->reffer_doctor_phone ?>">
                                </div>
                            </div>
                            
                            
                            
                              <div class="form-group row">
                                <label class="col-sm-4">Maritual Status</label>
                                <div class="col-xs-8">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="maritual_status" value="Married" <?php echo  set_radio('maritual_status', 'Married', TRUE); ?> >Married
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="maritual_status" value="Unmarried" <?php echo  set_radio('maritual_status', 'Unmarried'); ?> >Unmarried
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <!--<div class="form-group row">
                                <label for="consultant_doctor_dept" class="col-xs-4 col-form-label">Operating Doctor Department</label>
                                <div class="col-xs-8"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_depart'),
                                           
                                        );
                                        echo form_dropdown('consultant_doctor_dept', $bloodList, $patient->consultant_doctor_dept, 'class="form-control" id="consultant_doctor_dept" '); 
                                    ?>
                                </div>
                            </div>-->

                           <div class="form-group row">
                                <label for="consultant_doctor_name" class="col-xs-4 col-form-label">Operating Doctor Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="consultant_doctor_name" type="text" class="form-control" id="consultant_doctor_name" placeholder="Doctor Name" value="<?php echo $patient->consultant_doctor_name ?>" >
                                </div>
                            </div>
                            
                             <!--<div class="form-group row">
                                <label for="consultantation_fee" class="col-xs-4 col-form-label">Consultantation Fee<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="consultantation_fee" type="text" class="form-control" id="consultantation_fee" placeholder="Consultation Fee" value="<?php echo $patient->consultantation_fee ?>" >
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="followup_consultation" class="col-xs-4 col-form-label">Follow Up Consultantation <i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="followup_consultation" type="text" class="form-control" id="followup_consultation" placeholder="Follow Up Consultation" value="<?php echo $patient->followup_consultation ?>" >
                                </div>
                            </div>
                            
                             <div class="form-group row">
                                <label for="service_tax" class="col-xs-4 col-form-label">Entry Person Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="service_tax" type="text" class="form-control" id="service_tax" placeholder="Entry Person Name" value="<?php echo $patient->service_tax ?>" >
                                </div>
                            </div>


                             <div class="form-group row">
                                <label for="total_fee" class="col-xs-4 col-form-label">Total Fee</label>
                                <div class="col-xs-8">
                                    <input name="total_fee" type="text" class="form-control" id="total_fee" placeholder="Total Fee" value="<?php echo $patient->total_fee ?>" >
                                </div>
                            </div>
                       
                            
                        <!--Radio-->
<!--                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9"> 
                                    <div class="form-check">
                                        <label class="radio-inline"><input type="radio" name="status" value="1" checked><?php echo display('active') ?></label>
                                        <label class="radio-inline"><input type="radio" name="status" value="0"><?php echo display('inactive') ?></label>
                                    </div>
                                </div>
                            </div>-->
                        
                        <div class="form-group row">
                                <label for="operation_type" class="col-xs-4 col-form-label">Operation type<i class="text-danger"></i></label>
                                <div class="col-xs-8">
                                    <input name="operation_type" type="text" class="form-control" id="operation_type" placeholder="Operation type" >
                                </div>
                        </div>

                        <div class="form-group row">
                                <label for="operation_remark" class="col-xs-4 col-form-label">Brief remark of operation<i class="text-danger"></i></label>
                                <div class="col-xs-8">
                                    <input name="operation_remark" type="text" class="form-control" id="operation_remark" placeholder="Operation Remark" >
                                </div>
                        </div>
                        
                        <div class="form-group row">
                                <label for="operation_time" class="col-xs-4 col-form-label">Operation Time<i class="text-danger"></i></label>
                                <div class="col-xs-8">
                                    <input name="operation_time" type="text" class="form-control timepicker-hour-min-only" id="operation_time" placeholder="Operation Time" >
                                </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-sm-4"><?php echo "OT Checklist Finished" ?></label>
                                <div class="col-xs-8"> 
                                    <div class="form-check">
                                        <label class="radio-inline"><input type="radio" name="ot_checklist_finished" value="1"><?php echo "Yes"; ?></label>
                                        <label class="radio-inline"><input type="radio" name="ot_checklist_finished" value="0" checked><?php echo "No"; ?></label>
                                    </div>
                                </div>
                            </div>


                        </div>
                    
            <div class="row">
                <div class="col-md-12">

                 <!-- Medicine -->
                        <table class="table table-striped"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width=""><?php echo 'Consultant Dr' ?></th>
                                    <th width=""><?php echo 'Assistant Dr' ?></th>
                                    <th><?php echo 'OT Technician' ?></th>
                                    <th><?php echo 'OT Assistant' ?></th>
                                    <th><?php echo 'Anaesthetist Doctor' ?></th>
                                    <th><?php echo 'Anaesthesia Type' ?></th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="first_medicine" class="clone_count">
                                    <td><input type="text" data-unique-name="consultant_dr" name="ot[0][consultant_dr]" autocomplete="off" class="consultant_dr form-control" placeholder="Consultant Dr" ></td>
                                    <td><input type="text" data-unique-name="assistant_dr" name="ot[0][assistant_dr]" autocomplete="off" class="assistant_dr form-control" placeholder="Assistant Dr" ></td>                                 
                                    <td><input type="text" name="ot[0][ot_technician]" data-unique-name="ot_technician" autocomplete="off" class="ot_technician form-control" placeholder="OT Technician"  ></td> 
                                    <td><input type="text" data-unique-name="ot_assistant" name="ot[0][ot_assistant]" autocomplete="off" class="ot_assistant form-control" placeholder="OT Assistant" ></td>
                                    <td><input type="text" data-unique-name="anaesthetist_doctor" name="ot[0][anaesthetist_doctor]" autocomplete="off" class="anaesthetist_doctor form-control" placeholder="Anaesthetist Doctor" ></td>                                 
                                    <td><input type="text" name="ot[0][anaesthetist_type]" data-unique-name="anaesthetist_type" autocomplete="off" class="anaesthetist_type form-control" placeholder="Anaesthesia Type"  ></td> 
                                   
                                    <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr>  
                            </tbody> 
                        </table> 



                </div>
                </div>
                    
                    
                    
                    
                      <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    
                   
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('ot_manager/patient/get_patient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
     var userid = ui.item.value; // selected value

    // AJAX
    $.ajax({
     url: "<?= base_url('ot_manager/patient/get_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
      
       var id = response.id;
       var fname = response.firstname;
       var lname = response.lastname;
       var email = response.email;

       var textBoxArray = ['firstname','lastname','email','phone','mobile','guardianname','birthmark','aadharno',
       'reffer_doctor_name','reffer_doctor_phone','consultant_doctor_name','consultantation_fee','followup_consultation','service_tax',
        'total_fee'];
       // Set value to textboxes
       var selectBoxArray = ['payment_mode', 'department_id', 'doctor_id', 'religion', 'case_type', 'consultant_doctor_dept','blood_group'];
       var radioArray = ['sex','status','maritual_status'];
$.each(response, function(key , value) {    
   if(jQuery.inArray( key, textBoxArray ) != '-1') {
        $('#'+key).val(value); 
   }
   if(key == 'id') {
       $("#hidden_patient_id").val(value);
   }
   if(key == 'patient_id') {
       $("#hidden_p_id").val(value);
   }
   
   if(jQuery.inArray( key, selectBoxArray ) != '-1') {
        $("#"+key).val(value).trigger('change');
   }
   
   if(jQuery.inArray( key, radioArray ) != '-1') {
       $("input[name="+key+"][value=" + value + "]").prop('checked', true);
   }
   
   if(key == "address"){
       $("textarea#patinet_address").val(value);
   }
   
});

     }
    });

    return false;
   }
  });
    
   }); 
   
$( window ).load(function() {
    <?php if($is_edit) { ?>
            $("#department_id").trigger("change");
            $('#doctor_id option[value="' + <?php echo $patient->doctor_id; ?> + '"]').prop('selected', true);
    <?php } ?>
});
   
$(document).ready(function() {
    
    //department_id
    $("#department_id").change(function(){
        var output = $('.doctor_error'); 
        var doctor_list = $('#doctor_id');
        var available_day = $('#available_day');

        $.ajax({
            url  : '<?= base_url('appointment/doctor_by_department/') ?>',
            type : 'post',
            dataType : 'JSON',
            data : {
                '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
                department_id : $(this).val()
            },
            success : function(data) 
            {
                if (data.status == true) {
                    doctor_list.html(data.message);
                    available_day.html(data.available_days);
                    output.html('');
                } else if (data.status == false) {
                    doctor_list.html('');
                    output.html(data.message).addClass('text-danger').removeClass('text-success');
                } else {
                    doctor_list.html('');
                    output.html(data.message).addClass('text-danger').removeClass('text-success');
                }
            }, 
            error : function()
            {
                alert('failed');
            }
        });
    }); 
    
    
     //custom date picker
    $('#assign_date').datepicker({
        minDate:0,
        dateFormat: "dd-mm-yy"
    });
    
    $('#discharge_date').datepicker({
        minDate:0,
        dateFormat: "dd-mm-yy"
    });
    
    $('#operation_date').datepicker({
        minDate:0,
        dateFormat: "dd-mm-yy"
    });
    
    $('body').on('click','.MedAddBtn' ,function() {
                
        var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var clone = $("#first_medicine").clone();
        clone.attr("id", "first_medicine" + newId);
        clone.attr("class", "clone_count");
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
                $(this).val('');
                $(this).attr('name', "ot[" + newId + "][" + name + "]");
        });
        $(".clone_count").last().after(clone);

    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });




  //check assign_date
    var assign_date    = $('#assign_date');
    var discharge_date = $('#discharge_date');
    var dateChange     = $('.dateChange');
    var bed_id         = $("#bed_id"); 

    dateChange.change(function(){ 
        $.ajax({
            url  : '<?= base_url('bed_manager/bed_assign/check_bed/') ?>',
            type : 'POST',
            dataType : 'JSON',
            data : {
                '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
                assign_date : assign_date.val(), 
                discharge_date : discharge_date.val(), 
                bed_id : bed_id.val()  
            },
            success : function(data) 
            {
                discharge_date.next().html(data.message);
            }, 
            error : function()
            {
                alert('failed');
            }
        });
    });


    //custom date picker
    $('.cdatepicker').datepicker({
        minDate:0,
        dateFormat: "dd-mm-yy"
    });




    
    
});
</script>