<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    After Operation Checklist
                </div>
            </div> 

             <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("ot_manager/patient/after_before_operation_list") ?>"> <i class="fa fa-plus"></i>  <?php echo "Before After Operation List" ?> </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-9 col-sm-12">

                        <?php echo form_open_multipart('ot_manager/patient/after_operation') ?>
                            <input type ="hidden" id="patient_id" name="patient_id">
                            <input type ="hidden" id="cheklist_id" name="id">
                            <div class="form-group row">
                                <label for="search_patient" class="col-xs-6 col-form-label"><?php echo "Search Patient"; ?></label>
                                <div class="col-xs-6">
                                    <input name="search_patien" type="text" class="form-control ui-autocomplete-input" id="search_patient" placeholder="<?php echo "search_patient"; ?>" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Instrument/sponge/needles etc. count are correct" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[instrument_count]" value="1" <?php echo  set_radio('after_operation[instrument_count]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[instrument_count]" value="0" <?php echo  set_radio('after_operation[instrument_count]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Medicine started before leaving OT" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[medicine_started]" value="1" <?php echo  set_radio('after_operation[medicine_started]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[medicine_started]" value="0" <?php echo  set_radio('after_operation[medicine_started]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Documentation of implanted material is filled in correctly" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[documentation_of_material]" value="1" <?php echo  set_radio('after_operation[documentation_of_material]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[documentation_of_material]" value="0" <?php echo  set_radio('after_operation[documentation_of_material]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Guardian or parents need to be address about equipment" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[parents_address]" value="1" <?php echo  set_radio('after_operation[parents_address]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[parents_address]" value="0" <?php echo  set_radio('after_operation[parents_address]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Consultant Doctor order for post operative care have been documented" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[post_care]" value="1" <?php echo  set_radio('after_operation[post_care]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="after_operation[post_care]" value="0" <?php echo  set_radio('after_operation[post_care]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="remark" class="col-xs-6 col-form-label"><?php echo "Remark" ?></label>
                                <div class="col-xs-6">
                                    <input name="after_operation[remark]" type="text" class="form-control" placeholder="Remark" >
                                </div>
                            </div>
                        
                        
                        
                        
                            <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

   $( '#search_patient' ).autocomplete({
    source: function( request, response ) {
        $.ajax({
         url: "<?= base_url('ot_manager/patient/get_patient') ?>",
         type: 'post',
         dataType: "json",
         data: {
          '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
          search: request.term,request:1
         },
         success: function( data ) {
             if(data.status){
                response( data.message );
             }
         }
        });
   },
   select: function (event, ui) {
        $("#search_patient").val(ui.item.label); // display the selected text
        var userid = ui.item.value; // selected value
        $("#patient_id").val(userid);
        
        // AJAX
        $.ajax({
            url: "<?= base_url('ot_manager/patient/get_ot_checklist') ?>",
            type: 'post',
            data: {
                   '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
                   userid:userid
                  },
            dataType: 'json',
        success:function(response){
        console.log(response);
        if(response.after_operation != '') {
            $("#cheklist_id").val(response.id);
            var after_operation = obj = JSON.parse(response.after_operation);
            $.each(after_operation, function(key , value) {

               if(key == 'remark')
               {
                   $("input[name*="+key+"]").val(value);
               }
               else
               {
                   $("input[name*="+key+"][value=" + value + "]").prop('checked', true);
               }

        });
        }

     }
    });

    return false;
        
   }
  });
    
   }); 
   </script>