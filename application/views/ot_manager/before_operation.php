<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    Before Operation Checklist
                </div>
            </div> 

             <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("ot_manager/patient/after_before_operation_list") ?>"> <i class="fa fa-plus"></i>  <?php echo "Before After Operation List" ?> </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-9 col-sm-12">

                        <?php echo form_open_multipart('ot_manager/patient/before_operation') ?>
                            <input type ="hidden" id="patient_id" name="patient_id">
                            <input type ="hidden" id="cheklist_id" name="id">
                            <div class="form-group row">
                                <label for="search_patient" class="col-xs-6 col-form-label"><?php echo "Search Patient"; ?></label>
                                <div class="col-xs-6">
                                    <input name="search_patien" type="text" class="form-control ui-autocomplete-input" id="search_patient" placeholder="<?php echo "search_patient"; ?>" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Main Doctor availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[main_doctor_availability]" value="1" <?php echo  set_radio('before_operation[main_doctor_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[main_doctor_availability]" value="0" <?php echo  set_radio('before_operation[main_doctor_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Assistant Doctor availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[assistant_doctor_availability]" value="1" <?php echo  set_radio('before_operation[assistant_doctor_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[assistant_doctor_availability]" value="0" <?php echo  set_radio('before_operation[assistant_doctor_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Anaesthetic availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[anaesthetic_availability]" value="1" <?php echo  set_radio('before_operation[anaesthetic_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[anaesthetic_availability]" value="0" <?php echo  set_radio('before_operation[anaesthetic_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Power availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[power_availability]" value="1" <?php echo  set_radio('before_operation[power_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[power_availability]" value="0" <?php echo  set_radio('before_operation[power_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Water availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[water_availability]" value="1" <?php echo  set_radio('before_operation[water_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[water_availability]" value="0" <?php echo  set_radio('before_operation[water_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Manpower availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[manpower_availability]" value="1" <?php echo  set_radio('before_operation[manpower_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[manpower_availability]" value="0" <?php echo  set_radio('before_operation[manpower_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Blood availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[blood_availability]" value="1" <?php echo  set_radio('before_operation[blood_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[blood_availability]" value="0" <?php echo  set_radio('before_operation[blood_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Pathological test report availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[pathological_test_report_availability]" value="1" <?php echo  set_radio('before_operation[pathological_test_report_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[pathological_test_report_availability]" value="0" <?php echo  set_radio('before_operation[pathological_test_report_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Oxygen supply availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[oxygen_availability]" value="1" <?php echo  set_radio('before_operation[oxygen_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[oxygen_availability]" value="0" <?php echo  set_radio('before_operation[oxygen_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Related chemical availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[chemical_availability]" value="1" <?php echo  set_radio('before_operation[chemical_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[chemical_availability]" value="0" <?php echo  set_radio('before_operation[chemical_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Machine set up check up" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[machine_check_up]" value="1" <?php echo  set_radio('before_operation[machine_check_up]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[machine_check_up]" value="0" <?php echo  set_radio('before_operation[machine_check_up]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "OT cleaning date & time" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[ot_cleaning]" value="1" <?php echo  set_radio('before_operation[ot_cleaning]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[ot_cleaning]" value="0" <?php echo  set_radio('before_operation[ot_cleaning]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "OT set up ready or not" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[ot_setup]" value="1" <?php echo  set_radio('before_operation[ot_setup]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[ot_setup]" value="0" <?php echo  set_radio('before_operation[ot_setup]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Shifting facility availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[shifting_facility_availability]" value="1" <?php echo  set_radio('before_operation[shifting_facility_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[shifting_facility_availability]" value="0" <?php echo  set_radio('before_operation[shifting_facility_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "All related drugs availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[related_drugs_availability]" value="1" <?php echo  set_radio('before_operation[related_drugs_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[related_drugs_availability]" value="0" <?php echo  set_radio('before_operation[related_drugs_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Any special Equipment device required" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[special_equipment_required]" value="1" <?php echo  set_radio('before_operation[special_equipment_required]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[special_equipment_required]" value="0" <?php echo  set_radio('before_operation[special_equipment_required]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Patient medical history check up" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[medical_history_check_up]" value="1" <?php echo  set_radio('before_operation[medical_history_check_up]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[medical_history_check_up]" value="0" <?php echo  set_radio('before_operation[medical_history_check_up]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Prophylactic antibiotic infused availability" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[antibiotic_availability]" value="1" <?php echo  set_radio('before_operation[antibiotic_availability]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[antibiotic_availability]" value="0" <?php echo  set_radio('before_operation[antibiotic_availability]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Brief meeting conducted" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[brief_meeting_conducted]" value="1" <?php echo  set_radio('before_operation[brief_meeting_conducted]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[brief_meeting_conducted]" value="0" <?php echo  set_radio('before_operation[brief_meeting_conducted]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Parent or guardian confirmation report" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[parent_confirmation_report]" value="1" <?php echo  set_radio('before_operation[parent_confirmation_report]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[parent_confirmation_report]" value="0" <?php echo  set_radio('before_operation[parent_confirmation_report]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Concent form received" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[concent_form_received]" value="1" <?php echo  set_radio('before_operation[concent_form_received]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[concent_form_received]" value="0" <?php echo  set_radio('before_operation[concent_form_received]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Known allergies" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[known_allergies]" value="1" <?php echo  set_radio('before_operation[known_allergies]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[known_allergies]" value="0" <?php echo  set_radio('before_operation[known_allergies]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Risk of blood loss" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[risk_of_blood_loss]" value="1" <?php echo  set_radio('before_operation[risk_of_blood_loss]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[risk_of_blood_loss]" value="0" <?php echo  set_radio('before_operation[risk_of_blood_loss]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Antibiotic before skin infution" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[antibiotic_skin_infution]" value="1" <?php echo  set_radio('before_operation[antibiotic_skin_infution]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[antibiotic_skin_infution]" value="0" <?php echo  set_radio('before_operation[antibiotic_skin_infution]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Instrument count check list" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[instrument_count_check_list]" value="1" <?php echo  set_radio('before_operation[instrument_count_check_list]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[instrument_count_check_list]" value="0" <?php echo  set_radio('before_operation[instrument_count_check_list]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Sterility indication check" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[sterility_indication_check]" value="1" <?php echo  set_radio('before_operation[sterility_indication_check]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[sterility_indication_check]" value="0" <?php echo  set_radio('before_operation[sterility_indication_check]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-6"><?php echo "Patient operation site marked" ?> <i class="text-danger"></i></label>
                                <div class="col-xs-6">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[operation_site_marked]" value="1" <?php echo  set_radio('before_operation[operation_site_marked]', '1'); ?> >Yes
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="before_operation[operation_site_marked]" value="0" <?php echo  set_radio('before_operation[operation_site_marked]', '0', true); ?> >No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label for="remark" class="col-xs-6 col-form-label"><?php echo "Remark" ?></label>
                                <div class="col-xs-6">
                                    <input name="before_operation[remark]" type="text" class="form-control" placeholder="Remark" >
                                </div>
                            </div>
                        
                        
                        
                        
                            <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

   $( '#search_patient' ).autocomplete({
    source: function( request, response ) {
        $.ajax({
         url: "<?= base_url('ot_manager/patient/get_patient') ?>",
         type: 'post',
         dataType: "json",
         data: {
          '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
          search: request.term,request:1
         },
         success: function( data ) {
             if(data.status){
                response( data.message );
             }
         }
        });
   },
   select: function (event, ui) {
        $("#search_patient").val(ui.item.label); // display the selected text
        var userid = ui.item.value; // selected value
        $("#patient_id").val(userid);
        
        // AJAX
        $.ajax({
            url: "<?= base_url('ot_manager/patient/get_ot_checklist') ?>",
            type: 'post',
            data: {
                   '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
                   userid:userid
                  },
            dataType: 'json',
        success:function(response){
        console.log(response);
        if(response.before_operation != '') {
            $("#cheklist_id").val(response.id);
            var before_operation = obj = JSON.parse(response.before_operation);
            $.each(before_operation, function(key , value) {

               if(key == 'remark')
               {
                   $("input[name*="+key+"]").val(value);
               }
               else
               {
                   $("input[name*="+key+"][value=" + value + "]").prop('checked', true);
               }

        });
        }

     }
    });

    return false;
        
   }
  });
    
   }); 
   </script>