<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("ot_manager/patient/prescription_list") ?>"> <i class="fa fa-list"></i>  <?php echo display('prescription_list') ?> </a>  
                </div>
            </div> 

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 table-responsive">
                        <?php echo form_open('ot_manager/patient/edit_prescription',array('novalidate'=>'novalidate')) ?> 
                        <input type="hidden" name="id" value="<?php echo $prescription->id; ?>">
                        <!-- Information -->
                        <table class="table">
                            <thead>
                                <tr>  
                                    <th width="33.33%">
                                        <ul class="list-unstyled">
                                            <li> 
                                                <input type="text" placeholder="<?php echo display('patient_id') ?>" required name="patient_id" id="patient_id" class="invoice-input form-control" value="<?php echo (!empty($prescription->patient_id)?$prescription->patient_id:null) ?>">
                                                <p class="text-danger  invlid_patient_id"></p>
                                            </li>   
                                            <li> 
                                                <input type="text" placeholder="<?php echo display('patient_name') ?>" class="invoice-input form-control" id="patient_name" value="<?php echo ($prescription->patient_name != '') ? $prescription->patient_name : ''; ?>">
                                            </li>  
                                            <li>  
                                                <input type="text" placeholder="<?php echo display('sex') ?>" class="invoice-input form-control" id="sex" value="<?php echo ($prescription->sex != '') ? $prescription->sex : ''; ?>">
                                            </li>  
                                            <li>  
                                                <input type="text" placeholder="<?php echo display('date_of_birth') ?>" class="invoice-input form-control datepicker" id="date_of_birth" value="<?php echo ($prescription->date_of_birth != '') ? $prescription->date_of_birth : ''; ?>">
                                            </li>  
                                        </ul>
                                    </th>  
                                    <th width="33.33%">
                                        <ul class="list-unstyled">
                                            <li> 
                                                <input type="text" name="weight" placeholder="<?php echo display('weight') ?>" required class="invoice-input form-control" value="<?php echo ($prescription->weight != '') ? $prescription->weight : ''; ?>">
                                            </li>   
                                            <li> 
                                                <input type="text" name="blood_pressure" placeholder="<?php echo display('blood_pressure') ?>" class="invoice-input form-control" value="<?php echo ($prescription->blood_pressure != '') ? $prescription->blood_pressure : ''; ?>">
                                            </li> 
                                            <li> 
                                                <input type="text"  name="reference_by" placeholder="<?php echo display('reference') ?>" class="invoice-input form-control" value="<?php echo ($prescription->reference_by != '') ? $prescription->reference_by : ''; ?>">
                                            </li>  
                                            <li>  
                                                <div class="form-check form-control invoice-input">
                                                    <label class="radio-inline" style="padding:0 10px 0 0 ">Type </label>
                                                    <label class="radio-inline"><input type="radio" name="patient_type" value="New" <?php echo ($prescription->patient_type == 'New') ? "checked" : ""; ?> ><?php echo display('new') ?></label>
                                                    <label class="radio-inline"><input type="radio" name="patient_type" value="Old" <?php echo ($prescription->patient_type == 'Old') ? "checked" : ""; ?>><?php echo display('old') ?></label>
                                                </div> 
                                            </li>    
                                        </ul>
                                    </th>   
                                    <th width="33.33%">
                                        <ul class="list-unstyled">
                                            <li><input type="text" name="appointment_id" value="<?php echo (!empty($prescription->appointment_id)?$prescription->appointment_id:null) ?>" class="invoice-input form-control" placeholder="<?php echo display('appointment_id') ?>"></li>
                                            <li><input type="text" name="date" required class="datepicker invoice-input form-control" placeholder="<?php echo display('date') ?>"  value="<?php echo ($prescription->date != '') ? $prescription->date : ''; ?>"></li> 
                                            <li><input type="text" value="<?php echo $website->title; ?>" class="invoice-input form-control" placeholder="<?php echo display('hospital_name') ?>"></li> 
                                            <li><input type="text" value="<?php echo $website->description; ?>" class="invoice-input form-control" placeholder="<?php echo display('address') ?>"></li> 
                                        </ul>
                                    </th> 
                                </tr> 
                                
                            </thead>
                        </table>

                        

                        <!-- Medicine -->
                        <table class="table table-striped"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width="160"><?php echo 'Date' ?></th>
                                    <th width="160"><?php echo 'Time' ?></th>
                                    <th><?php echo 'Operation Regd Complaints' ?></th>
                                    <th width="160"><?php echo 'Operative Findings' ?></th>
                                    <th width="160"><?php echo 'Operative Diet' ?></th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                            <tbody id="medicine" class="clone_count">
                                <?php 
                                if($prescription->ipd_medicine != null) {
                                    foreach(json_decode($prescription->ipd_medicine) as $key=>$data) {
                                ?>
                                <tr id="first_medicine">
                                    <td><input type="text" value ="<?php echo $data->date; ?>" data-unique-name="date" name="prescription[<?php echo $key; ?>][date]" autocomplete="off" class="medicine  form-control" placeholder="Date" ></td>
                                    <td><input type="text" value ="<?php echo $data->time; ?>" data-unique-name="time" name="prescription[<?php echo $key; ?>][time]" autocomplete="off" class="form-control time" placeholder="Time" ></td>
                                    <td><input type="text" value ="<?php echo $data->complaints; ?>" name="prescription[<?php echo $key; ?>][complaints]" data-unique-name="complaints" autocomplete="off" class="form-control" placeholder="Complaints"  ></td> 
                                     <td><input type="text" value ="<?php echo $data->finding_dr; ?>" name="prescription[<?php echo $key; ?>][finding_dr]" data-unique-name="finding_dr" autocomplete="off" class="form-control" placeholder="Finding Dr"  ></td> 
                                      <td><input type="text" value ="<?php echo $data->diet; ?>" name="prescription[<?php echo $key; ?>][diet]" data-unique-name="diet" autocomplete="off" class="form-control" placeholder="Diet"  ></td> 
                                    <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr> 
                                
                                <?php }
                                }
                                ?>
                            </tbody> 
                        </table> 


                        
                        <!-- Fees & Comments -->
                        <div class="row">
                            <div class="col-sm-12">

                                <!--<div class="form-group row">
                                    <label for="visiting_fees" class="col-xs-3 col-form-label"><?php echo display('visiting_fees') ?></label>
                                    <div class="col-xs-9">
                                        <input name="visiting_fees"  type="text" class="form-control" id="visiting_fees" placeholder="<?php echo display('visiting_fees') ?>">
                                    </div>
                                </div>-->

                                <div class="form-group row">
                                    <label for="patient_notes" class="col-xs-3 col-form-label"><?php echo display('patient_notes') ?></label>
                                    <div class="col-xs-9">
                                        <textarea name="patient_notes" class="form-control"  placeholder="<?php echo display('patient_notes') ?>"><?php echo $prescription->patient_notes; ?></textarea>
                                    </div>
                                </div> 
                                
                                <div class="form-group row">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <div class="ui buttons">
                                            <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                            <div class="or"></div>
                                            <button class="ui positive button"><?php echo display('save') ?></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


 

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo display('case_study') ?></h4>
      </div>
      <div class="modal-body" id="caseStudyOutput">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a href="<?php echo base_url("dashboard_doctor/prescription/case_study/create") ?>" class="btn btn-primary"><?php echo display('add_patient_case_study') ?></a>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$(document).ready(function() {   
 
    // medicine list
    $('body').on('keyup change click', '.medicine', function(){
        $(this).autocomplete({
            source: [
                <?php 
                    if(!empty($medicine_category)) {
                        foreach ($medicine_category as $key=>$value) { 
                            //echo '"'.(!empty($medicine_category[$i])?$medicine_category[$i]:null).'",'; 
                                //echo '"label":"'. $i.'","value" :"'.$medicine_category[$i].'",';
                            ?>
                            { label : "<?php echo $value; ?>" ,
                             value : "<?php echo $value; ?>",
                             data_value : "<?php echo $key; ?>"
                            } ,
                       <?php }
                    } 
                ?>
            ],
            select :function (event, ui) {
                $(this).data('category-id',ui.item.data_value);
              }
        });
    });    

$('.medicine').on('autocompleteselect', function (e, ui) {
        $(this).data('category-id',ui.item.data_value);
       
    });

$('body').on('keyup change click', '.medicine_name', function(){
 var cat_id = ($(this).closest('td').prev().find(':input').data('category-id'));
 console.log(cat_id);
 $(this).autocomplete({
source: function(request, response){
    $.ajax({
        url     : '<?php echo base_url('ot_manager/patient/get_medicine') ?>',
        method  : 'post',
        dataType: 'json', 
        data    : {
            'medicine_cat_id' : cat_id,
            '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        success : function(data) {
            if(data != false) {
            response(data);
            }
            else{
                alert("No Medicine found");
            }
        },
        error   : function(jqXHR, textStatus, errorThrown) {
            alert('failed!');
        } 
    });
}

});
});



/*$('.medicine').on('autocompleteselect', function (e, ui) {
        
        $.ajax({
            url     : '<?php echo base_url('ot_manager/patient/get_medicine') ?>',
            method  : 'post',
            dataType: 'json', 
            data    : {
                'medicine_cat_id' : ui.item.data_value,
                '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
            },
            success : function(data) {
                console.log(data);
                
                $(".medicine_name").autocomplete({
                    source: [
                       data
                    ]
                })
            },
            error   : function(jqXHR, textStatus, errorThrown) {
                alert('failed!');
            } 
        });
        
        
    });
*/

    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent();
       // $('#medicine').append("<tr>"+itemData.html()+"</tr>");
        //$('#medicine tr:last-child').find(':input').val('');
        
        
        var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var cloneCount = $('.diet_count').length;
        
        var clone = $("#first_medicine").clone();
        clone.attr("id", "first_medicine" + newId);
        clone.attr("class", "clone_count");
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
            if($(this).is(':checkbox')) {
             $(this).prop('checked' , false);
             $(this).attr('name', "prescription[" + newId + "][" + name + "][]");
            }
            else {
                $(this).val('');
                $(this).attr('name', "prescription[" + newId + "][" + name + "]");
            }
            
        });
        console.log(clone);
        
        //$('#medicine').append("<tr>"+clone+"</tr>");
        $(".clone_count").last().after(clone);

    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


    //#------------------------------------
    //   ENDS OF PATIENT INFORMATION
    //#------------------------------------

    $(window).on('load', function(){
        var patient_id = '<?php echo $this->input->get('pid') ?>';
        if(patient_id.length > 0)
        patientInfo(patient_id);
    });

    $('body').on('keyup change', '#patient_id', function() {
        var patient_id = $(this).val();
        patientInfo(patient_id);
    });

    function patientInfo(patient_id)
    { 
        if(patient_id.length > 0)
        $.ajax({
            url     : '<?php echo base_url('ot_manager/patient/patient') ?>',
            method  : 'post',
            dataType: 'json', 
            data    : {
                'patient_id' : patient_id,
                '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
            },
            success : function(data) {
                if (data.status == true) { 
                    $(".invlid_patient_id").text('');
                    $("#patient_name").val(data.name);
                    $("#sex").val(data.sex);
                    $("#date_of_birth").val(data.date_of_birth);
                } else {
                    $(".invlid_patient_id").text('<?php echo display("invalid_patient_id") ?>');
                }
            },
            error   : function() {
                alert('failed!');
            } 
        });
    } 

});
</script>