<div class="row">
    <!--  form area -->
    <div class="col-sm-12"  id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("ot_manager/patient/dischargeReport") ?>"> <i class="fa fa-list"></i>  <?php echo "Discharge Report"; ?> </a>  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <thead>
                                <tr  class="bg-primary" colspan="3">
                                   <td></td>
                                    <td>
                                        <strong><?php echo display('patient_id') ?></strong>: <?php echo $prescription->patient_id; ?> 
                                        <!-- <strong>App ID</strong>: <?php echo $prescription->appointment_id; ?> -->
                                    </td>
                                    <td  class="text-right"><strong><?php echo display('date') ?></strong>: <?php echo date('m/d/Y', strtotime($prescription->discharge_date)); ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>  
                                      <td width="10%" class=""><center><img src="http://localhost/hospital_management.git/assets/images/apps/2017-11-05/h.png" height="50px" width="50px"></center></td>   

                                    <td width="50%" class="">
                                        <center><ul class="list-unstyled">
                                            <li><strong><?php echo $website->title; ?></strong></li>  
                                            <li><?php echo $website->description; ?></li>  
                                            <li><?php echo $website->email; ?></li>  
                                            <li><?php echo $website->phone; ?></li>  
                                        </ul></center>
                                    </td> 

                                     <td width="40%">
                                        <ul class="list-unstyled">
                                            <li><strong><?php echo $prescription->doctor_name; ?></strong></li> 
                                            <li><?php echo $prescription->specialist; ?></li>
                                            <li><strong><?php echo $prescription->department_name; ?></strong></li>   
                                            <li><?php echo $prescription->designation; ?></li>  
                                            <li><?php echo $prescription->address; ?></li>   
                                        </ul>
                                    </td> 
                                </tr>  
                            </tbody>
                            <tfoot>
                                <tr class="">
                                    <td colspan="3">
                                        <?php
                                            $date1=date_create($prescription->date_of_birth);
                                            $date2=date_create(date('Y-m-d'));
                                            $diff=date_diff($date1,$date2); 
                                        ?>
                                        <strong><?php echo display('patient_name') ?></strong>: <?php echo $prescription->patient_name; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <strong><?php echo display('age') ?></strong>: <?php echo "$diff->y Years $diff->m Months"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <strong><?php echo "Gender" ?></strong>: <?php echo $prescription->sex; ?>
                                        
                                    </td> 
                                </tr>

                                 <tr class="">
                                    <td colspan="3">
                                       
                                        <strong><?php echo display('mobile') ?></strong>: <?php echo $prescription->mobile; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <strong><?php echo "Discharge Date" ?></strong>: <?php echo $prescription->discharge_date; ?>
                                    </td> 
                                </tr>
                                 <tr class="">
                                    <td colspan="3">
                                        We As M/S:- <strong><?php echo $website->title; ?> <?php echo $prescription->address; ?> </strong> Descharging. Mr/Miss/Master  : <strong><?php echo $prescription->patient_name; ?></strong> On Date: -------------  Time :- -----------</br>Thanks
                                    </td> 
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Signature -->
                        <table class="table" style="margin-top:50px"> 
                            <thead> 
                                <tr>
                                    <th style="width:50%;"></th>
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Signature</i>
                                    </td>
                                </tr>
                            </thead>
                        </table> 

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

