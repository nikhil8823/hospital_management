<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("billing/billing/packege_list") ?>"> <i class="fa fa-list"></i>Packege List </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <?php echo form_open('billing/billing/create_packege','class="form-inner"') ?>
                <div class="row">
                    <div class="col-md-9 col-sm-12">
                     <?php echo form_hidden('id',$packege->id) ?>

                            <div class="form-group row">
                                <label for="packege_name" class="col-xs-3 col-form-label">Packege Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="packege_name"  type="text" class="form-control" id="packege_name" placeholder= "Packege Name" value="">
                                </div>
                            </div>

                    </div>
                </div>


                <div class="row">
                <div class="col-md-12">

                 <!-- Medicine -->
                        <table class="table table-striped"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width=""><?php echo 'Charges Name' ?></th>
                                    <th width=""><?php echo 'charges Amount' ?></th>
                                    <th><?php echo 'Notes' ?></th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                            <tbody id="medicine">
                             <?php if (!empty($packege->medicine)) {
                                    $medicine = json_decode($packege->medicine);

                                    if (sizeof($medicine) > 0) 
                                        foreach ($medicine as $value) { 
                                ?>
                            
                                 <tr id="first_medicine"  class="clone_count">
                                    <td><input type="text" data-unique-name="charges_name" name="prescription[0][charges_name]" autocomplete="off" class="medicine form-control" placeholder="Charges Name" value="<?php echo $value->name ?>" ></td>
                                    <td><input type="text" data-unique-name="charges_amount" name="prescription[0][charges_amount]" autocomplete="off" class="form-control medicine_name" placeholder="Charges Amount" value="<?php echo $value->amount ?>" ></td>                                 
                                    <td><input type="text" name="prescription[0][notes]" data-unique-name="notes" autocomplete="off" class="form-control" placeholder="Notes" value="<?php echo $value->notes ?>" ></td> 
                                   
                                    <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr>  
                              <?php } } ?>
                            </tbody> 
                        </table> 



                </div>
                </div>



                <div class="row">
                <div class="col-md-12">
                            <!--Radio-->
                            <div class="form-group row">
                                <label class="col-sm-2"><?php echo display('status') ?></label>
                                <div class="col-xs-6"> 
                                    <div class="form-check">
                                        <label class="radio-inline"><input type="radio" name="status" value="1" checked><?php echo display('active') ?></label>
                                        <label class="radio-inline"><input type="radio" name="status" value="0"><?php echo display('inactive') ?></label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>

                        <?php echo form_close() ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    
 //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent();
       // $('#medicine').append("<tr>"+itemData.html()+"</tr>");
        //$('#medicine tr:last-child').find(':input').val('');
        
        
        var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var cloneCount = $('.diet_count').length;
        
        var clone = $("#first_medicine").clone();
        clone.attr("id", "first_medicine" + newId);
        clone.attr("class", "clone_count");
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
           
                $(this).val('');
                $(this).attr('name', "prescription[" + newId + "][" + name + "]");
            
            
        });
        console.log(clone);
        
        //$('#medicine').append("<tr>"+clone+"</tr>");
        $(".clone_count").last().after(clone);

    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });



</script>