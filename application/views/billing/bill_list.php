<div class="row">
    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("billing/billing/create_bill") ?>"> <i class="fa fa-plus"></i> Create New Bill </a>  
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th>Department</th>
                            <th>Patient Id</th>
                            <th>Total Amount</th>
                            <th>Advance Amount</th>
                            <th>Discount</th>
                            <th>Date</th>
                             <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($bill)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($bill as $supplier) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $supplier->hosp_dept; ?></td>
                                    <td><?php echo $supplier->patient_id; ?></td>
                                    <td><?php echo $supplier->total_amt; ?></td>
                                    <td><?php echo $supplier->advance_amt; ?></td>
                                    <td><?php echo $supplier->discount; ?></td>
                                     <td><?php echo $supplier->date; ?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url("billing/billing/view_bill/$supplier->id") ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a> 
                                        <!--<a href="<?php echo base_url("billing/billing/edit_bill/$supplier->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-edit"></i></a> -->
                                        <a href="<?php echo base_url("billing/billing/delete_bill/$supplier->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
