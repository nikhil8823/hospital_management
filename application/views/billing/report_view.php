<div class="row">
    <!--  form area -->
    <div class="col-sm-12"  id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("billing/billing/bill_list") ?>"> <i class="fa fa-list"></i>  <?php echo "Final Report"; ?> </a>  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body">

                   <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <thead>
                                <tr  class="">
                                    <td colspan="2">
                                       
                                    </td>
                                    <td  class="text-right">
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="20%" class=""><center><img src="<?php echo base_url($website->logo); ?>" height="50px" width="50px"></center></td>
                                       
                                    <td width="40%" class="text-right">
                                        <center><ul class="list-unstyled">
                                            <li><strong><?php echo $website->title; ?></strong></li>  
                                            <li><?php echo $website->description; ?></li>  
                                            <li><?php echo $website->email; ?></li>  
                                            <li><?php echo $website->phone; ?></li>  
                                        </ul></center>
                                    </td> 

                                      <td width="40%" class="text-left">
                                         <center> <ul class="list-unstyled">
                                            <li><strong><?php echo ($bill->doctor_name != '') ? $bill->doctor_name : '-'; ?></strong></li> 
                                            <li><?php echo $bill->specialist; ?></li>  
                                        </ul> </center> 
                                    </td> 
                                </tr>  
                            </tbody>
                            <tfoot>
                                <tr class="">
                                    <td colspan="3">
                                        
                                        <strong><?php echo "Registration Number" ?></strong>: <?php echo $bill->registration_number; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Patient Id" ?></strong>: <?php echo $bill->patient_id; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Patient Name" ?></strong>: <?php echo $bill->patient_name; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                    </td> 
                                </tr>
                                <tr class="">
                                <td colspan="3">
                                         <?php
                                            $date1=date_create($bill->date_of_birth);
                                            $date2=date_create(date('Y-m-d'));
                                            $diff=date_diff($date1,$date2);
                                            ?>
                                        <strong><?php echo "Age" ?></strong>: <?php  echo "$diff->y Years $diff->m Months"; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Bed" ?></strong>: <?php echo ($bill->bed_name != '') ? $bill->bed_name : '-'; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Admission Date" ?></strong>: <?php echo ($bill->admission_date != '') ? $bill->admission_date : '-'; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                    </td> 
                                </tr>
                                 <tr class="">
                                <td colspan="3">
                                         
                                        <strong><?php echo "Discharge Date" ?></strong>: <?php echo ($bill->discharge_date != '') ? $bill->discharge_date : '-'; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Packege Name" ?></strong>: <?php echo ($bill->packege_name != '') ? $bill->packege_name : '-'; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Payment Mode" ?></strong>: <?php echo ($bill->payment_mode != '') ? $bill->payment_mode : '-'; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                    </td> 
                                </tr>
                                 <tr class="">
                                <td colspan="3">
                                      
                                    </td> 
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
                
               <!-- <div class="row">
                            <div class="col-md-6 col-sm-12">

                              <div class="form-group row">
                                <label for="total_amt" class="col-xs-4 col-form-label">Registration Number</label>
                                <div class="col-xs-8">
                                    <?php echo $bill->registration_number; ?>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="discount" class="col-xs-4 col-form-label">Patient Id</label>
                                <div class="col-xs-8">
                                    <?php echo $bill->patient_id; ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment_mode" class="col-xs-4 col-form-label">Patient Name</label>
                                <div class="col-xs-8">
                                   <?php echo $bill->patient_name; ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_name" class="col-xs-4 col-form-label">Age</label>
                                <div class="col-xs-8">
                                    <?php
                                            $date1=date_create($bill->date_of_birth);
                                            $date2=date_create(date('Y-m-d'));
                                            $diff=date_diff($date1,$date2);
                                            echo "$diff->y Years $diff->m Months";
                                    ?>
                                </div>
                            </div>
 
                            
                            <div class="form-group row">
                                <label class="col-sm-4">Doctor Name</label>
                                <div class="col-xs-8">
                                    <?php echo ($bill->doctor_name != '') ? $bill->doctor_name : '-'; ?>
                                </div>
                            </div>
                                
                            <div class="form-group row">
                                <label class="col-sm-4">Payment Mode</label>
                                <div class="col-xs-8">
                                    <?php echo ($bill->payment_mode != '') ? $bill->payment_mode : '-'; ?>
                                </div>
                            </div>

                          
                    </div>
                    <div class="col-md-6 col-sm-12">

                           <div class="form-group row">
                                <label for="advance_amt" class="col-xs-4 col-form-label">Speciality</label>
                                <div class="col-xs-8">
                                    <?php echo ($bill->specialist != '') ? $bill->specialist : '-'; ?>
                                </div>
                            </div>

                              <div class="form-group row">
                            <label for="net_amount" class="col-xs-4 col-form-label">Bed</label>
                                <div class="col-xs-8">
                                    <?php echo ($bill->bed_name != '') ? $bill->bed_name : '-'; ?>
                                </div>
                            </div>

                            
                             <div class="form-group row">
                                <label for="account_no" class="col-xs-4 col-form-label">Admission Date</label>
                                <div class="col-xs-8">
                                    <?php echo ($bill->admission_date != '') ? $bill->admission_date : '-'; ?>
                                </div>
                            </div>

                           <div class="form-group row">
                                <label for="chq_no" class="col-xs-4 col-form-label">Discharge Date</label>
                                <div class="col-xs-8">
                                    <?php echo ($bill->discharge_date != '') ? $bill->discharge_date : '-'; ?>
                                </div>
                            </div>


                             <div class="form-group row">
                                <label for="other" class="col-xs-4 col-form-label">Package Name</label>
                                <div class="col-xs-8">
                                    <?php echo ($bill->packege_name != '') ? $bill->packege_name : '-'; ?>
                                </div>
                             </div>
                            
                             <div class="form-group row">
                                <label for="other" class="col-xs-4 col-form-label">Advance Deposit</label>
                                <div class="col-xs-8">
                                    <?php echo ($bill->advance_amt != '') ? $bill->advance_amt : '-'; ?>
                                </div>
                            </div>
                            
                           
                    </div>-->

                     <div class="col-md-12 col-sm-12">

                      <p><strong><?php echo "Packege Charges Details";?> : </strong></p>

                      <?php if($bill->medicine_pack != '') { ?>
                       <table class="table table-striped" id="datatable"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width=""><?php echo 'Charges Name' ?></th>
                                    <th width=""><?php echo 'charges Amount' ?></th>
                                    <th><?php echo 'Note' ?></th>
                                    
                                </tr>
                            </thead>
                            <tbody id="medicine">
                            <?php foreach(json_decode($bill->medicine_pack) as $data) { ?> 
                                    <tr>
                                        <td><?php echo ($data->name != '') ? $data->name : '-'; ?></td>
                                        <td><?php echo ($data->amount != '') ? $data->amount : '-'; ?></td>
                                        <td><?php echo ($data->notes != '') ? $data->notes : '-'; ?></td>
                                       
                                    </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                      <?php } ?>
                  </div>

                   <div class="col-md-12 col-sm-12">
                   
                   <p><strong><?php echo "Other Charges Details";?> : </strong></p>

                      <?php if($bill->medicine != '') { ?>
                       <table class="table table-striped" id="datatable"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width=""><?php echo 'Charges Name' ?></th>
                                    <th width=""><?php echo 'charges Amount' ?></th>
                                    <th><?php echo 'No of day' ?></th>
                                    <th><?php echo 'Total Amount' ?></th>
                                </tr>
                            </thead>
                            <tbody id="medicine">
                            <?php foreach(json_decode($bill->medicine) as $data) { ?> 
                                    <tr>
                                        <td><?php echo ($data->name != '') ? $data->name : '-'; ?></td>
                                        <td><?php echo ($data->amount != '') ? $data->amount : '-'; ?></td>
                                        <td><?php echo ($data->day != '') ? $data->day : '-'; ?></td>
                                        <td><?php echo ($data->totalamount != '') ? $data->totalamount : '-'; ?></td> 
                                    </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                      <?php } ?>
                  </div>
                 </div>
                
                 <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <thead>
                                <tr  class="">
                                    <td colspan="2">
                                       
                                    </td>
                                    <td  class="text-right">
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                              
                            </tbody>
                            <tfoot>
                                <tr class="">
                                    <td colspan="3">
                                        
                                        <strong><?php echo "Total Amount" ?></strong>: <?php echo $bill->total_amt; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                         <strong><?php echo "Advance Payment" ?></strong>: <?php echo ($bill->advance_amt != '') ? $bill->advance_amt : '-'; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   
                                        <strong><?php echo "Discount" ?></strong>: <?php echo $bill->discount; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                    </td> 
                                </tr>
                                <tr class="">
                                <td colspan="3">
                                        
                                        <strong><?php echo "Net Amount" ?></strong>: <?php  echo "$bill->net_amount"; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Other" ?></strong>: <?php echo ($bill->other != '') ? $bill->other : '-'; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                      
                                    </td> 
                                </tr>
                                
                            </tfoot>
                        </table>

                    </div>
                </div>
                
                
                
            </div>
        </div>
    </div>
</div>

