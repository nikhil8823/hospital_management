<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("billing/billing/bill_list") ?>"> <i class="fa fa-list"></i>Bill List</a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
             <?php echo form_open_multipart('billing/billing/create_bill','class="form-inner"') ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                            <input name="id" type="hidden" id="hidden_patient_id_test" value="<?php echo $bill->id;?>">

                              <div class="form-group row">
                                <label for="hosp_dept" class="col-xs-4 col-form-label">Select Department</label>
                                <div class="col-xs-8">
                                   <?php
                                        $item_deptList = array(
                                            ''   => display('select_option'),
                                            'OPD' => 'OPD',
                                            'IPD' => 'IPD',
                                            'OT' => 'OT',
                                           
                                        );
                                        echo form_dropdown('hosp_dept', $item_deptList, $bill->hosp_dept,'class="form-control" id="hosp_dept" '); 
                                    ?>
                                </div>
                            </div>
                            
                             <div class="form-group row">
                                <label for="patient_id" class="col-xs-4 col-form-label">Patient Id<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="patient_id" type="text" class="form-control" id="patient_id" placeholder="Patient Id" value="<?php echo $bill->patient_id ?>">
                                </div>
                            </div>

                    </div>
                  </div>   

                 <div class="row">
                    <div class="col-md-12 col-sm-12">

                  <table class="table table-striped"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width=""><?php echo 'Charges Name' ?></th>
                                    <th width=""><?php echo 'charges Amount' ?></th>
                                    <th><?php echo 'No of day' ?></th>
                                    <th><?php echo 'Total Amount' ?></th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                             <tbody id="medicine">
                                
                                <?php if (!empty($bill->medicine)) {
                                    $medicine = json_decode($bill->medicine);

                                    if (sizeof($medicine) > 0) 
                                        foreach ($medicine as $value) { 
                                ?>
                               <tr id="first_medicine"  class="clone_count">

                                    <td><input type="text" data-unique-name="charges_name" name="prescription[0][charges_name]" autocomplete="off" class="medicine form-control" placeholder="Charges Name" value="<?php echo $value->name ?>"></td>
                                    <td><input type="text" data-unique-name="charges_amount" name="prescription[0][charges_amount]" autocomplete="off" class="form-control medicine_name" placeholder="Charges Amount" value="<?php echo $value->amount ?>"></td>                                 
                                    <td><input type="text" name="prescription[0][no_of_day]" data-unique-name="no_of_day" autocomplete="off" class="form-control" placeholder="No of Day" value="<?php echo $value->day ?>" ></td>

                                    <td><input type="text" name="prescription[0][total_amount]" data-unique-name="total_amount" autocomplete="off" class="form-control" placeholder="Total Amount" value="<?php echo $value->totalamount ?>" ></td> 
                                   
                                    <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td> 
                                     </tr>
                                       
                                <?php } } ?>
                             
                             </tbody>
                        </table>

                            

                    </div>
                  </div>

            
                    <div class="row">
                            <div class="col-md-6 col-sm-12">

                              <div class="form-group row">
                                <label for="total_amt" class="col-xs-4 col-form-label">Total Amount<i class="text-danger"></i></label>
                                <div class="col-xs-8">
                                    <input name="total_amt" type="text" class="form-control" id="total_amt" placeholder="Total Amount" value="<?php echo $bill->total_amt ?>">
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="discount" class="col-xs-4 col-form-label">Discount<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="discount" type="text" class="form-control" id="discount" placeholder="Discount" value="<?php echo $bill->discount ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="payment_mode" class="col-xs-4 col-form-label">Payment Mode</label>
                                <div class="col-xs-8">
                                   <?php
                                        $payment_mode = array(
                                            ''   => display('select_option'),
                                            'Cash' => 'Cash',
                                            'Cheque' => 'Cheque',
                                            'Transfer' => 'Transfer',
                                            'DD' => 'DD',
                                           
                                        );
                                        echo form_dropdown('payment_mode', $payment_mode, $bill->payment_mode, 'class="form-control" id="payment_mode" '); 
                                    ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_name" class="col-xs-4 col-form-label">Bank Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="bank_name" type="text" class="form-control" id="size" placeholder="Bank Name" value="<?php echo $bill->bank_name ?>" >
                                </div>
                            </div>
 

                            
 
                            <div class="form-group row">
                                <label class="col-sm-4"><?php echo display('status') ?></label>
                                <div class="col-xs-8">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                          
                    </div>
                    <div class="col-md-6 col-sm-12">

                           <div class="form-group row">
                                <label for="advance_amt" class="col-xs-4 col-form-label">Advance Amount<i class="text-danger"></i></label>
                                <div class="col-xs-8">
                                    <input name="advance_amt" type="text" class="form-control" id="advance_amt" placeholder="Advance Amount" value="<?php echo $bill->advance_amt ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                            <label for="net_amount" class="col-xs-4 col-form-label">Net Amount<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="net_amount" type="text" class="form-control" id="net_amount" placeholder="Net Amount" value="<?php echo $bill->net_amount ?>">
                                </div>
                            </div>

                            
                             <div class="form-group row">
                                <label for="account_no" class="col-xs-4 col-form-label">Account No</label>
                                <div class="col-xs-8">
                                    <input name="account_no" type="text" class="form-control" id="account_no" placeholder="A/C" value="<?php echo $bill->account_no ?>">
                                </div>
                            </div>
                           

                           <div class="form-group row">
                                <label for="chq_no" class="col-xs-4 col-form-label">Cheque No</label>
                                <div class="col-xs-8">
                                    <input name="chq_no" type="text" class="form-control" id="chq_no" placeholder="Cheque No" value="<?php echo $bill->chq_no ?>" >
                                </div>
                            </div>


                             <div class="form-group row">
                                <label for="other" class="col-xs-4 col-form-label">Other </label>
                                <div class="col-xs-8">
                                    <input name="other" type="text" class="form-control" id="other" placeholder="Other" value="<?php echo $bill->other ?>" >
                                </div>
                            </div>

                            
                           
                    </div>
                    
                   
                

                   <div class="col-md-12 col-sm-12">
                      
                  <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                  </div>
                 </div>
               
                    
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
    var userid = ui.item.value; // selected value

    // AJAX
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
 console.log(response);//return false;
      
       var id = response.id;
       var fname = response.firstname;
       var lname = response.lastname;
       var email = response.email;

       var textBoxArray = ['patient_id','firstname','lastname','email','phone','mobile','address','guardianname','birthmark','aadharno',
       'reffer_doctor_name','reffer_doctor_phone','consultant_doctor_name','consultantation_fee','followup_consultation','service_tax',
        'total_fee'];
       // Set value to textboxes
       

$.each(response, function(key , value) {    
   if(jQuery.inArray( key, textBoxArray ) != '-1') {
        $('#'+key).val(value); 
   }
   if(key == 'id') {
       $("#hidden_patient_id").val(value);
   }
});

     }
    });

    return false;
   }
  });
    
   }); 
</script>

<script type="text/javascript">
$(document).ready(function() {   
 
   //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent();
       // $('#medicine').append("<tr>"+itemData.html()+"</tr>");
        //$('#medicine tr:last-child').find(':input').val('');
        
        
        var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var cloneCount = $('.diet_count').length;
        
        var clone = $("#first_medicine").clone();
        clone.attr("id", "first_medicine" + newId);
        clone.attr("class", "clone_count");
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
           
                $(this).val('');
                $(this).attr('name', "prescription[" + newId + "][" + name + "]");
            
            
        });
        console.log(clone);
        
        //$('#medicine').append("<tr>"+clone+"</tr>");
        $(".clone_count").last().after(clone);

    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


});

</script>


      