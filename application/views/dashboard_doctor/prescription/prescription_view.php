<div class="row">
    <!--  form area -->
    <div class="col-sm-12"  id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("dashboard_doctor/prescription/prescription") ?>"> <i class="fa fa-list"></i>  <?php echo display('prescription_list') ?> </a>  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                         <table class="table">
                            <thead>
                                <tr  class="bg-primary">
                                    <td colspan="2">
                                        <strong><?php echo display('patient_id') ?></strong>: <?php echo $prescription->patient_id; ?>, 
                                        <strong>App ID</strong>: <?php echo $prescription->appointment_id; ?>
                                    </td>
                                    <td  class="text-right"><strong><?php echo display('date') ?></strong>: <?php echo date('m/d/Y', strtotime($prescription->date)); ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr> 
                                 <?php $logo = $this->session->userdata('logo'); ?>
                                  <td width="10%" class=""><center><img src="<?php echo (!empty($logo)?base_url($logo):base_url("assets/images/logo.png")) ?>" height="50px" width="100px"></center></td>
                                 <td width="50%" class="text-left">
                                  <center> <ul class="list-unstyled">
                                            <li><strong><?php echo $website->title; ?></strong></li>  
                                            <li><?php echo $website->description; ?></li>  
                                            <li><?php echo $website->email; ?></li>  
                                            <li><?php echo $website->phone; ?></li>  
                                        </ul></center>                                 
                                    </td> 

                                    <td width="40%">
                                        <ul class="list-unstyled text-left">
                                            <li> <strong>Consultant name</strong>: <?php echo $prescription->doctor_name; ?></li> 
                                            <li><?php echo $prescription->specialist; ?></li>
                                            <li><strong><?php echo $prescription->department_name; ?></strong></li>   
                                            <li><?php echo $prescription->designation; ?></li>  
                                            <li><?php echo $prescription->address; ?></li>   
                                        </ul>
                                    </td>      
                                   
                                </tr>  

                                
                            </tbody>
                            <tfoot> 

                            <tr class="">
                                    <td colspan="3">
                                        <?php
                                            $date1=date_create($prescription->date_of_birth);
                                            $date2=date_create(date('Y-m-d'));
                                            $diff=date_diff($date1,$date2); 
                                        ?>
                                        <strong><?php echo display('patient_name') ?></strong>: <?php echo $prescription->patient_name; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo display('age') ?></strong>: <?php echo "$diff->y Years $diff->m Months"; ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                        <strong><?php echo "Gender"; ?></strong>: <?php echo $prescription->sex; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                      
                                    </td> 
                                </tr>

                                  <tr class="">
                                    <td colspan="3">
                                        <?php
                                            $date1=date_create($prescription->date_of_birth);
                                            $date2=date_create(date('Y-m-d'));
                                            $diff=date_diff($date1,$date2); 
                                        ?>
                                        <strong><?php echo display('weight') ?></strong>: <?php echo $prescription->weight; ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                                        <strong>BP</strong>: <?php echo $prescription->blood_pressure; ?>
                                        <!--<strong><?php echo display('insurance_name') ?></strong>: <?php echo $prescription->insurance_name; ?>-->
                                    </td> 
                                </tr>

                            </tfoot>
                        </table>

                    </div>
                </div>

              

                 <div class="row"> 

                        <div style="float:left;width:100%;padding-left:10px">
                              <!-- Medicine -->
                            <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info header-2">
                                        <th width=""><?php echo "Investigation Name" ?></th>
                                        <th width=""><?php echo "Result" ?></th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                <?php

                                if (!empty($prescription->casestudy)) {
                                    $casestudy = json_decode($prescription->casestudy);
 
                                    if (sizeof($casestudy) > 0) 
                                        foreach ($casestudy as $value2) { 
                                ?>
                                    <tr>
                                        
                                        <td><?php echo $value2->name; ?></td> 
                                        <td><?php echo $value2->result; ?></td>  
                                       
                                    </tr>
                                <?php  
                                        }
                                    }
                                ?> 
                                </tbody> 
                            </table> 

                           
                        </div> 
                    </div>


                 <div class="row">
                    <div class="col-sm-12" style="margin-left:10px;">
                        <div style="float:left;width:100%;word-break:all;border-right:1px solid #e4e5e7;padding-right:10px">
                            <!-- chief_complain -->
                            <p>
                                <strong><?php echo "Allergies" ?></strong>: &nbsp;<?php echo $prescription->food_allergies; ?>
                            </p>

                            <p>
                                <strong><?php echo display('chief_complain') ?></strong>: &nbsp;<?php echo $prescription->chief_complain; ?>
                            </p>
                            
                            <!-- patient_notes -->
                            <p>
                                <strong><?php echo "Patient History" ?></strong>:  &nbsp;<?php echo $prescription->family_medical_history; ?>
                            </p> 
                        </div></div>
                     </div></br>
                     
                      <div class="row"> 

                        <div style="float:left;width:100%;padding-left:10px">

                         <!-- diagnosis -->
                            <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info">
                                        <th><?php echo display('diagnosis') ?></th>
                                        <th><?php echo display('instruction') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($prescription->diagnosis)) {
                                    $diagnosis = json_decode($prescription->diagnosis);

                                    if (sizeof($diagnosis) > 0) 
                                        foreach ($diagnosis as $value) { 
                                ?>
                                    <tr>
                                        <td><?php echo $value->name; ?></td> 
                                        <td><?php echo $value->instruction; ?></td> 
                                    </tr>
                                <?php  
                                        }
                                    }
                                ?> 
                                </tbody> 
                            </table>  

                            <!-- Medicine -->
                          <!--  <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info">
                                        <th><?php echo display('medicine_name') ?></th>
                                        <th width="80"><?php echo display('type') ?></th>
                                        <th width="80"><?php echo display('days') ?></th>
                                        <th><?php echo display('instruction') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($prescription->medicine)) {
                                    $medicine = json_decode($prescription->medicine);

                                    if (sizeof($medicine) > 0) 
                                        foreach ($medicine as $value) { 
                                ?>
                                    <tr>
                                        <td><?php echo $value->name; ?></td> 
                                        <td><?php echo $value->type; ?></td> 
                                        <td><?php echo $value->days; ?></td>  
                                        <td><?php echo $value->instruction; ?></td> 
                                    </tr>
                                <?php  
                                        }
                                    }
                                ?> 
                                </tbody> 
                            </table> -->

                              <!-- Medicine -->
                            <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info header-2">
                                        <!--<th><?php echo "Medicine Category" ?></th>-->
                                        <th width=""><?php echo display('medicine_name') ?></th>
                                        <th width=""><?php echo "Doses" ?></th>
                                        <th><?php echo "Num Of drugs" ?></th>
                                        <th><?php echo "Diet" ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($prescription->medicine)) {
                                    $medicine = json_decode($prescription->medicine);
 
                                    if (sizeof($medicine) > 0) 
                                        foreach ($medicine as $value) { 
                                ?>
                                    <tr>
                                        <!--<td><?php echo $value->category; ?></td>--> 
                                        <td><?php echo $value->name; ?></td> 
                                        <td><?php 
                                                if(!empty($value->doses)) {
                                                    foreach(json_decode($value->doses) as $data) {
                                                        echo "<b>$data</b><br>";
                                                    }
                                                } 
                                                else {
                                                    echo "-";
                                                }
                                            ?>
                                        </td>  
                                        <td><?php echo $value->num_of_doses; ?></td> 
                                        <td><?php echo $value->diet; ?></td> 
                                    </tr>
                                <?php  
                                        }
                                    }
                                ?> 
                                </tbody> 
                            </table> 

                           
                        </div> 
                    </div>



                      <div class="row">
                    <div class="col-sm-12" style="margin-left:10px;">
                        <div style="float:left;width:100%;word-break:all;border-right:1px solid #e4e5e7;padding-right:10px">
                           
                            <!-- patient_notes -->
                            <p>
                                <strong><?php echo display('patient_notes') ?></strong>:  &nbsp;<?php echo $prescription->patient_notes; ?>
                            </p> 
                        </div></div>
                     </div></br>





                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <!-- Signature -->
                        <table class="table" style="margin-top:50px"> 
                            <thead> 
                                <tr>
                                    <th style="width:50%;"></th>
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Signature</i>
                                    </td>
                                </tr>
                            </thead>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

