<div class="row">
    <!--  table area -->
    <div class="col-sm-12" id="PrintMe">
        <div class="panel panel-default thumbnail">

            <div class="panel-heading no-print">
                <div class="btn-group">  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button>
                </div>
            </div> 

            <div class="panel-heading">
                <center><h3 class="heading"><?php echo (!empty($title) ? $title: null)?></h3></center>
            </div>
            <div class="panel-body">

             <div class="row"> 

                        <div style="float:left;width:100%;padding-left:10px">
                             
                            <table class="table table-striped"> 
                                <thead>
                                    <tr class="">
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                               
                                    <tr colspan="3">

                                 <td> <strong><?php echo "Patient Id" ?></strong>: <?php echo $details->patient_id; ?></td> 

                                 <td> <strong><?php echo "Patient Name" ?></strong>: <?php echo $details->patient_name; ?></td> 
                                       
                                <td> <strong><?php echo "Date of Birth" ?></strong>: <?php echo $details->date_of_birth; ?></td> 
                                   
                                    </tr>
                                     <tr colspan="3">
                                        <td> <strong><?php echo "Gender" ?></strong>: <?php echo $details->sex; ?></td>  
                                       
                                        <td> <strong><?php echo "Mobile No" ?></strong> : <?php echo $details->mobile; ?></td> 
                                        <td> <strong><?php echo "Blood Group" ?></strong> : <?php echo $details->blood_group; ?></td>   
                                    
                                    </tr>
                                     <tr colspan="3">
                                     <td> <strong><?php echo "Doctor Name" ?></strong>: <?php echo $details->doctor_name; ?></td>
                                     <td> <strong><?php echo "Address" ?></strong>: <?php echo $details->address; ?></td>
                                    <td><strong> Date</strong> : <?php echo date('d-m-Y');?></td>
                                     </tr>

                                      <tr colspan="3">
                                    <td> <strong><?php  (!empty($title) ? $title: null);
                                     $str= preg_replace('/\W\w+\s*(\W*)$/', '$1', $title);
                                      echo $str;
                                     ?>


                                    <?php echo "Time" ?></strong>: <?php echo $details->time; ?></td>
                                     <td> <strong><?php  (!empty($title) ? $title: null);
                                     $str= preg_replace('/\W\w+\s*(\W*)$/', '$1', $title);
                                      echo $str;
                                     ?>


                                    <?php echo "Date" ?></strong>: <?php echo $details->date; ?></td>
                                    <!--<td><strong>Current date</strong> : <?php echo date('Y-m-d');?></td>-->
                                     </tr>
                                
                                </tbody> 
                            </table> 
                          </div>
                        </div>


                <?php if(!empty($details->picture)){  ?>
                   <a href="<?php echo base_url($details->picture) ?>" target="_blank"><img src="<?php echo base_url($details->picture) ?>" width="200"></a>
                <?php } ?>
                <div class="caption">
                    <h3 class="heading"><strong>Subject : </strong><?php echo $details->title ?></h3>
                    Respected Sir/Madam,
                    <p><?php echo $details->description ?></p>
                </div>
            </div>
            <div class="panel-footer">
                <dl class="dl-horizontal">
                   <!-- <?php if (!empty($details->patient_id)) { ?>
                        <dt><?php echo display('patient_id') ?></dt>
                        <dd><?php echo $details->patient_id ?></dd>
                    <?php } ?>


                    <?php if (!empty($details->doctor_name)) { ?>
                        <dt><?php echo display('doctor_name') ?></dt>
                        <dd><?php echo $details->doctor_name ?></dd>
                    <?php } ?>

                    <?php if (!empty($details->patient_id)) { ?>
                        <dt><?php echo display('date') ?></dt>
                        <dd><?php echo date('d M Y', strtotime($details->date)) ?></dd>
                    <?php } ?>-->
                </dl>
            </div>

             <div class="row">
                    <div class="col-sm-12">
                        <!-- Signature -->
                        <table class="table" style="margin-top:50px"> 
                            <thead> 
                                <tr>
                                    <th style="width:50%;"></th>
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Signature</i>
                                    </td>
                                </tr>
                            </thead>
                        </table> 

                    </div>
                </div>

                
        </div>
    </div>


</div>
 

  


