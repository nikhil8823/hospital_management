<div class="row">

 <div class="col-sm-12">
        <div  class="panel panel-default">
            <div class="panel-body"> 

                <form class="form-inline" action="<?php echo base_url('hospital_activities/birth/dischargeReport') ?>">

                    <div class="form-group">
                        <label class="sr-only" for="start_date"><?php echo display('start_date') ?></label>
                        <input type="text" name="start_date" class="form-control datepicker" id="start_date" placeholder="<?php echo display('start_date') ?>" value="">
                    </div> 

                    <div class="form-group">
                        <label class="sr-only" for="end_date"><?php echo display('end_date') ?></label>
                        <input type="text" name="end_date" class="form-control datepicker" id="end_date" placeholder="<?php echo display('end_date') ?>" value="">
                    </div>  

                    <button type="submit" class="btn btn-success"><?php echo display('filter') ?></button>

                </form>

            </div>
        </div>
    </div>

    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group">
                    <a class="btn btn-success" href="<?php echo base_url("hospital_activities/birth/form") ?>"> <i class="fa fa-plus"></i>  <?php echo display('add_birth_report') ?> </a> 
                </div>
            </div> 
            <div class="panel-body">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th><?php echo display('patient_id') ?></th>
                            <th><?php echo display('title') ?></th>
                            <th><?php echo display('description') ?></th>
                            <th><?php echo "Date"; ?></th>
                            <th><?php echo "Time"; ?></th>
                           <!-- <th><?php echo display('status') ?></th>-->
                            <th><?php echo display('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  if (!empty($result)) { ?>


                         <?php $sl = 1; ?>
                            <?php foreach ($result as $birth1) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $birth1->patient_id; ?></td>
                                    <td><?php echo $birth1->title; ?></td>
                                    <td><?php echo character_limiter($birth1->description, 60); ?></td>
                                    <td><?php echo $birth1->date; ?></td>
                                    <td><?php echo $birth1->time; ?></td>
                                    <!--<td><?php echo (($birth1->status==1)?display('active'):display('inactive')); ?></td>-->
                                    <td class="center" width="80">
                                        <a href="<?php echo base_url("hospital_activities/birth/details/$birth1->id") ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>

                                        <a href="<?php echo base_url("hospital_activities/birth/form/$birth1->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-edit"></i></a> 
                                        <a href="<?php echo base_url("hospital_activities/birth/delete/$birth1->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 



                        <?php } else { ?>
                        <?php if (!empty($births)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($births as $birth) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $birth->patient_id; ?></td>
                                    <td><?php echo $birth->title; ?></td>
                                    <td><?php echo character_limiter($birth->description, 60); ?></td>
                                    <td><?php echo $birth->date; ?></td>
                                    <td><?php echo $birth->time; ?></td>
                                    <!--<td><?php echo (($birth->status==1)?display('active'):display('inactive')); ?></td>-->
                                    <td class="center" width="80">
                                        <a href="<?php echo base_url("hospital_activities/birth/details/$birth->id") ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>

                                        <a href="<?php echo base_url("hospital_activities/birth/form/$birth->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-edit"></i></a> 
                                        <a href="<?php echo base_url("hospital_activities/birth/delete/$birth->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } }?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
