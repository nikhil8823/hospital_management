<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("ambulance/ambulance/get_pass_list") ?>"> <i class="fa fa-list"></i>Get Pass List  </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <?php echo form_open_multipart('ambulance/ambulance/create_get_pass','class="form-inner"') ?>

                            <input name="id" type="hidden" id="hidden_patient_id_test" value="<?php echo $ambulance->id;?>">


                             <div class="form-group row">
                                <label for="uid" class="col-xs-3 col-form-label">Ambulance No<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="uid" type="text" class="form-control" id="uid" placeholder="UID No" value="<?php echo $ambulance->uid ?>">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label for="patient_id" class="col-xs-3 col-form-label">Patient Id<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="patient_id" type="text" class="form-control" id="patient_id" placeholder="Patient Id" value="<?php echo $ambulance->patient_id ?>">
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="firstname" class="col-xs-3 col-form-label">Full Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="firstname" type="text" class="form-control" id="firstname" placeholder="Full Name" value="<?php echo $ambulance->firstname ?>" >
                                </div>
                            </div>


                             <div class="form-group row">
                                <label for="ambulance_name" class="col-xs-3 col-form-label">Ambulance Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="ambulance_name" type="text" class="form-control" id="ambulance_name" placeholder="Ambulance Name" value="<?php echo $ambulance->ambulance_name ?>" >
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="driver_name" class="col-xs-3 col-form-label">Driver Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="driver_name" type="text" class="form-control" id="driver_name" placeholder="Driver Name" value="<?php echo $ambulance->driver_name ?>" >
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="driver_phoneno" class="col-xs-3 col-form-label"><?php echo "Driver Phone" ?></label>
                                <div class="col-xs-9">
                                    <input name="driver_phoneno" class="form-control" type="text" placeholder="<?php echo "Driver Phone" ?>" id="driver_phoneno"  value="<?php echo $ambulance->driver_phoneno ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="driver_id" class="col-xs-3 col-form-label">Driver Id<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="driver_id" type="text" class="form-control" id="driver_id" placeholder="Driver Id" value="<?php echo $ambulance->driver_id ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="driver_address" class="col-xs-3 col-form-label"><?php echo "Driver Address" ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <textarea name="driver_address" class="form-control"  placeholder="<?php echo "Driver Address" ?>" maxlength="140" rows="7"><?php echo $ambulance->driver_address ?></textarea>
                                </div>
                            </div> 


                             <div class="form-group row">
                                <label for="ambulance_dt" class="col-xs-3 col-form-label"><?php echo "Ambulance Date" ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="ambulance_dt" class="datepicker form-control" type="text" placeholder="<?php echo "Ambulance Date" ?>" id="ambulance_dt"  value="<?php echo $ambulance->ambulance_dt ?>">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label for="ambulance_time" class="col-xs-3 col-form-label">Ambulance Time<i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="ambulance_time" type="text" class="form-control" id="ambulance_time" placeholder="Ambulance Time" value="<?php echo $ambulance->ambulance_time ?>">
                                </div>
                            </div>

                           
                    </div>
                    <div class="col-md-6 col-sm-12">

                    <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="meter_reading_from" class="col-xs-3 col-form-label">Meter Reading From</label>
                                <div class="col-xs-9">
                                    <input name="meter_reading_from" type="text" class="form-control" id="meter_reading_from" placeholder="Meter Reading From" value="<?php echo $ambulance->meter_reading_from ?>" >
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="meter_reading_to" class="col-xs-3 col-form-label">Meter Reading To</label>
                                <div class="col-xs-9">
                                    <input name="meter_reading_to" type="text" class="form-control" id="meter_reading_to" placeholder="Meter Reading To" value="<?php echo $ambulance->meter_reading_to ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="total_amount" class="col-xs-3 col-form-label">Total Amount</label>
                                <div class="col-xs-9">
                                <input name="total_amount" type="text" class="form-control" id="total_amount" placeholder="Total Amount" value="<?php echo $ambulance->total_amount ?>" >
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="payment_amount" class="col-xs-3 col-form-label">Payment Amount</label>
                                <div class="col-xs-9">
                                <input name="payment_amount" type="text" class="form-control" id="payment_amount" placeholder="Payment Amount" value="<?php echo $ambulance->payment_amount ?>" >
                                </div>
                            </div>

                           
                           
                             <div class="form-group row">
                                <label for="payment_mode" class="col-xs-3 col-form-label">Payment Mode</label>
                                <div class="col-xs-9"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'Cash'=> 'Cash',
                                            'DD' => 'DD',
                                            'Cheque' => 'Cheque',
                                            'Transfer' => 'Transfer',
                                           
                                        );
                                        echo form_dropdown('payment_mode', $bloodList, 'class="form-control" id="payment_mode" '); 
                                    ?>
                                </div>
                            </div>
                           
                        </div>
                    
                   
                </div>

                   <div class="col-md-12 col-sm-12">
                           

                  <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>

                </div>
                    
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
    var userid = ui.item.value; // selected value

    // AJAX
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
 console.log(response);//return false;
      
       var id = response.id;
       var fname = response.firstname;
       var lname = response.lastname;
       var email = response.email;

       var textBoxArray = ['patient_id','firstname','lastname','email','phone','mobile','address','guardianname','birthmark','aadharno',
       'reffer_doctor_name','reffer_doctor_phone','consultant_doctor_name','consultantation_fee','followup_consultation','service_tax',
        'total_fee'];
       // Set value to textboxes
       

$.each(response, function(key , value) {    
   if(jQuery.inArray( key, textBoxArray ) != '-1') {
        $('#'+key).val(value); 
   }
   if(key == 'id') {
       $("#hidden_patient_id").val(value);
   }
});

     }
    });

    return false;
   }
  });
    
   }); 
</script>

<script type="text/javascript">
$(document).ready(function() {   
 
    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#insurance_limit').append("<tr>"+itemData.html()+"</tr>");
        $('#insurance_limit tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


     //#------------------------------------
    //   STARTS OF Approval limit 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn_limit' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#limit_approval').append("<tr>"+itemData.html()+"</tr>");
        $('#limit_approval tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn_limit' ,function() {
        $(this).parent().parent().parent().remove();
    });

});

</script>


      