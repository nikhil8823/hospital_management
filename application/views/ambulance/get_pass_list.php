<div class="row">
    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("ambulance/ambulance/create_get_pass") ?>"> <i class="fa fa-plus"></i> Add Get Pass </a>  
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th>Patient ID</th>
                            <th>Full Name</th>
                            <th>Driver Name</th>
                            <th>Driver Id</th>
                            <th>Reading From</th>
                            <th>Reading To</th>
                            <th>Total Amount</th>
                            <th>Ambulance No</th>
                             <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($ambulance)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($ambulance as $insurance) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $insurance->patient_id; ?></td>
                                    <td><?php echo $insurance->firstname; ?></td>
                                    <td><?php echo $insurance->driver_name; ?></td>
                                    <td><?php echo $insurance->driver_id; ?></td>
                                    <td><?php echo $insurance->meter_reading_from; ?></td>
                                    <td><?php echo $insurance->meter_reading_to; ?></td>
                                    <td><?php echo $insurance->total_amount; ?></td>
                                    <td><?php echo $insurance->uid; ?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url("ambulance/ambulance/edit/$insurance->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-edit"></i></a> 
                                        <a href="<?php echo base_url("ambulance/ambulance/delete/$insurance->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
