<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("patient") ?>"> <i class="fa fa-list"></i>  <?php echo display('patient_list') ?> </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <?php echo form_open_multipart('patient/create','class="form-inner"') ?>

                            <?php echo form_hidden('id',$patient->id); ?>

                            <div class="form-group row">
                                <label for="firstname" class="col-xs-3 col-form-label"><?php echo display('first_name') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="firstname" type="text" class="form-control" id="firstname" placeholder="<?php echo display('first_name') ?>" value="<?php echo $patient->firstname ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lastname" class="col-xs-3 col-form-label"><?php echo display('last_name') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="lastname" type="text" class="form-control" id="lastname" placeholder="<?php echo display('last_name') ?>" value="<?php echo $patient->lastname ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-xs-3 col-form-label"><?php echo display('email') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="email" type="text" class="form-control" id="email" placeholder="<?php echo display('email') ?>" value="<?php echo $patient->email ?>">
                                </div>
                            </div>

                            <!--<div class="form-group row">
                                <label for="password" class="col-xs-3 col-form-label"><?php echo display('password') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="password" type="password" class="form-control" id="password" placeholder="<?php echo display('password') ?>">
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="phone" class="col-xs-3 col-form-label"><?php echo display('phone') ?></label>
                                <div class="col-xs-9">
                                    <input name="phone" class="form-control" type="text" placeholder="<?php echo display('phone') ?>" id="phone"  value="<?php echo $patient->phone ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mobile" class="col-xs-3 col-form-label"><?php echo display('mobile') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="mobile" class="form-control" type="text" placeholder="<?php echo display('mobile') ?>" id="mobile"  value="<?php echo $patient->mobile ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="blood_group" class="col-xs-3 col-form-label"><?php echo display('blood_group') ?></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'A+' => 'A+',
                                            'A-' => 'A-',
                                            'B+' => 'B+',
                                            'B-' => 'B-',
                                            'O+' => 'O+',
                                            'O-' => 'O-',
                                            'AB+' => 'AB+',
                                            'AB-' => 'AB-'
                                        );
                                        echo form_dropdown('blood_group', $bloodList, $patient->blood_group, 'class="form-control" id="blood_group" '); 
                                    ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('sex') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Male" <?php echo  set_radio('sex', 'Male', TRUE); ?> ><?php echo display('male') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Female" <?php echo  set_radio('sex', 'Female'); ?> ><?php echo display('female') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Other" <?php echo  set_radio('sex', 'Other'); ?> ><?php echo display('other') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="date_of_birth" class="col-xs-3 col-form-label"><?php echo display('date_of_birth') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="date_of_birth" class="datepicker form-control" type="text" placeholder="<?php echo display('date_of_birth') ?>" id="date_of_birth"  value="<?php echo $patient->date_of_birth ?>">
                                </div>
                            </div>

                            <!-- if patient picture is already uploaded -->
                            <?php if(!empty($patient->picture)) {  ?>
                            <div class="form-group row">
                                <label for="picturePreview" class="col-xs-3 col-form-label"></label>
                                <div class="col-xs-9">
                                    <img src="<?php echo base_url($patient->picture) ?>" alt="Picture" class="img-thumbnail" />
                                </div>
                            </div>
                            <?php } ?>

                            <!--<div class="form-group row">
                                <label for="picture" class="col-xs-3 col-form-label"><?php echo display('picture') ?></label>
                                <div class="col-xs-9">
                                    <input type="file" name="picture" id="picture" value="<?php echo $patient->picture ?>">
                                    <input type="hidden" name="old_picture" value="<?php echo $patient->picture ?>">
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="address" class="col-xs-3 col-form-label"><?php echo display('address') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <textarea name="address" class="form-control"  placeholder="<?php echo display('address') ?>" maxlength="140" rows="7"><?php echo $patient->address ?></textarea>
                                </div>
                            </div> 
 
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            
                             <div class="form-group row">
                                <label for="Payment_mode" class="col-xs-3 col-form-label">Payment Mode</label>
                                <div class="col-xs-9"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'DD' => 'DD',
                                            'Cheque' => 'Cheque',
                                            'Transfer' => 'Transfer',
                                            'Cash' => 'Cash',
                                           
                                        );
                                        echo form_dropdown('payment_mode', $bloodList, $patient->payment_mode, 'class="form-control" id="payment_mode" '); 
                                    ?>
                                </div>
                            </div>
                        
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group row">
                                <label for="guardianname" class="col-xs-4 col-form-label">Guardian Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="guardianname" type="text" class="form-control" id="guardianname" placeholder="Guardian Name" value="<?php echo $patient->guardianname ?>" >
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="birthmark" class="col-xs-4 col-form-label">Birth Mark </label>
                                <div class="col-xs-8">
                                    <input name="birthmark" type="text" class="form-control" id="birthmark" placeholder="Birth Mark" value="<?php echo $patient->birthmark ?>" >
                                </div>
                            </div>
                            
                            
                             <div class="form-group row">
                                <label for="aadharno" class="col-xs-4 col-form-label">Aadhar No</label>
                                <div class="col-xs-8">
                                    <input name="aadharno" type="text" class="form-control" id="aadharno" placeholder="Aadhar No" value="<?php echo $patient->aadharno ?>" >
                                </div>
                            </div>
                            
                             <div class="form-group row">
                                <label for="religion" class="col-xs-4 col-form-label">Religion</label>
                                <div class="col-xs-8"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'Hindu' => 'Hindu',
                                            'Muslim' => 'Muslim',
                                            'Shikh' => 'Shikh',
                                            'Isai' => 'Isai',
                                            
                                        );
                                        echo form_dropdown('religion', $bloodList, $patient->religion, 'class="form-control" id="religion" '); 
                                    ?>
                                </div>
                            </div>

                           <div class="form-group row">
                                <label for="case_type" class="col-xs-4 col-form-label">Case Type</label>
                                <div class="col-xs-8"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'OPD' => 'OPD',
                                            'IPD' => 'IPD',
                                            'OT' => 'OT',
                                           
                                        );
                                        echo form_dropdown('case_type', $bloodList, $patient->case_type, 'class="form-control" id="case_type" '); 
                                    ?>
                                </div>
                            </div>

                           <div class="form-group row">
                                <label for="reffer_doctor_name" class="col-xs-4 col-form-label">Reffer Doctor Name</label>
                                <div class="col-xs-8">
                                    <input name="reffer_doctor_name" type="text" class="form-control" id="reffer_doctor_name" placeholder="Doctor Name" value="<?php echo $patient->reffer_doctor_name ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="reffer_doctor_phone" class="col-xs-4 col-form-label">Reffer Doctor Phone</label>
                                <div class="col-xs-8">
                                    <input name="reffer_doctor_phone" class="form-control" type="text" placeholder="Reffer Doctor Phone" id="reffer_doctor_phone"  value="<?php echo $patient->reffer_doctor_phone ?>">
                                </div>
                            </div>
                            
                            
                            
                              <div class="form-group row">
                                <label class="col-sm-4">Maritual Status</label>
                                <div class="col-xs-8">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="maritual_status" value="Married" <?php echo  set_radio('maritual_status', 'Married', TRUE); ?> >Married
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="maritual_status" value="Unmarried" <?php echo  set_radio('maritual_status', 'Unmarried'); ?> >Unmarried
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                               <div class="form-group row">
                                <label for="consultant_doctor_dept" class="col-xs-4 col-form-label">Consultant Doctor Department</label>
                                <div class="col-xs-8"> 
                                    <?php
                                        
                                        echo form_dropdown('consultant_doctor_dept', $department_list, $patient->consultant_doctor_dept, 'class="form-control" id="consultant_doctor_dept" '); 
                                    ?>
                                </div>
                            </div>

                           <div class="form-group row">
                                <label for="consultant_doctor_name" class="col-xs-4 col-form-label">Consultant Doctor Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="consultant_doctor_name" type="text" class="form-control" id="consultant_doctor_name" placeholder="Doctor Name" value="<?php echo $patient->consultant_doctor_name ?>" >
                                </div>
                            </div>
                            
                             <div class="form-group row">
                                <label for="consultantation_fee" class="col-xs-4 col-form-label">Consultantation Fee<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="consultantation_fee" type="text" class="form-control" id="consultantation_fee" placeholder="Consultation Fee" value="<?php echo $patient->consultantation_fee ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="followup_consultation" class="col-xs-4 col-form-label">Follow Up Consultantation </label>
                                <div class="col-xs-8">
                                    <input name="followup_consultation" type="text" class="form-control" id="followup_consultation" placeholder="Follow Up Consultation" value="<?php echo $patient->followup_consultation ?>" >
                                </div>
                            </div>
                            
                             <div class="form-group row">
                                <label for="reception_name" class="col-xs-4 col-form-label">Receptionist Name</label>
                                <div class="col-xs-8">
                                    <input name="reception_name" type="text" class="form-control" id="reception_name" placeholder="Receptionist Name" value="<?php echo $patient->reception_name ?>" >
                                </div>
                            </div>


                             <div class="form-group row">
                                <label for="total_fee" class="col-xs-4 col-form-label">Other Charges<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="total_fee" type="text" class="form-control" id="total_fee" placeholder="Other Charges" value="<?php echo $patient->total_fee ?>" >
                                </div>
                            </div>

                        </div>
                      <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    
                   
                </div>
            </div>
        </div>
    </div>

</div>