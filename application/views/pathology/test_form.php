<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("pathology/test") ?>"> <i class="fa fa-list"></i>  <?php echo display('test_list') ?> </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <?php echo form_open_multipart('pathology/test/create','class="form-inner"') ?>
                              <?php echo form_hidden('id',$patient->id) ?>

                            <div class="form-group row">
                                <label for="testname" class="col-xs-3 col-form-label">Test Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="testname" type="text" class="form-control" id="testname" placeholder="Test Name" value="<?php echo $patient->testname ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="normalvalue" class="col-xs-3 col-form-label">Normal Value <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="normalvalue" type="text" class="form-control" id="normalvalue" placeholder="Normal Value" value="<?php echo $patient->normalvalue ?>">
                                </div>
                            </div>

                       
                    </div>
                   
                      <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    
                   
                </div>
            </div>
        </div>
    </div>

</div>