<div class="row">

    <div class="col-sm-12" id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("pathology/patient/create") ?>"> <i class="fa fa-plus"></i>  <?php echo display('add_patient') ?> </a>  
                    <a class="btn btn-primary" href="<?php echo base_url("pathology/patient") ?>"> <i class="fa fa-list"></i>  <?php echo display('patient_list') ?> </a>  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body"> 
                <!-- Nav tabs --> 
                <ul class="col-xs-12 nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php echo display('patient_information') ?></a>
                    </li>
                    <li role="presentation">
                        <a href="#documents" aria-controls="documents" role="tab" data-toggle="tab"><?php echo display('documents') ?></a>
                    </li>
                </ul>  

                <!-- Tab panes --> 
                <div class="col-xs-12 tab-content">
                    <br>
                    <!-- INFORMATION -->
                    <div role="tabpanel" class="tab-pane active" id="home">

                     <div class="row">

                      <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info header-2">
                                        <th width="50"></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <tr>
                                        
                                        <td width="50"><img alt="Picture" src="<?php echo (!empty($website->logo)?base_url($website->logo):base_url("assets/images/apps/2017-11-05/h.png")) ?>" height="50px;" width="50px;"></td> 
                                        <td width="600"><center><strong><?php echo $website->title ?></strong></br> 
                                <strong><?php echo $website->description ?></strong></br>
                                <strong><?php echo $website->phone ?></strong></br></center></td>  
                                        <td><strong><?php echo $profile->consultant_doctor_name ?></strong></br> 
                                <strong><?php echo $profile->consultant_doctor_dept ?></strong></td> 
                                
                                </tr>
                               
                                </tbody> 
                            </table> 

                           
                    </div></br>

                        <div class="row">

                          <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info header-2">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <tr>
                                        
                                        <td> <strong><?php echo display('patient_id') ?></strong> : <?php echo $profile->patient_id ?></br> 
                                <strong><?php echo display('email') ?></strong> : <?php echo $profile->email ?></br>
                                <strong><?php echo display('date_of_birth') ?></strong> : <?php echo date('d M Y',strtotime($profile->date_of_birth)) ?></br> </td> 

                                        <td> <strong><?php echo display('age') ?></strong> : <?php echo date_diff(date_create($profile->date_of_birth), date_create('now'))->y; ?> Years</br>
                               <strong> <?php echo display('blood_group') ?></strong> : <?php echo $profile->blood_group ?></br> 
                               <strong><?php echo display('sex') ?></strong> : <?php echo $profile->sex ?></br> </td>

                                        <td> <strong><?php echo display('phone') ?></strong> : <?php echo $profile->phone ?></br> 
                                    <strong><?php echo display('mobile') ?></strong> : <?php echo $profile->mobile ?></br>  
                                    <strong><?php echo display('address') ?></strong> : <?php echo $profile->address ?></br>  
                                    <strong><?php echo display('create_date') ?></strong> : <?php echo $profile->create_date ?></br>  </td> 
                                
                                </tr>
                               
                                </tbody> 
                            </table> 

                            <!--<div class="col-sm-3" align="center"> 
                                <img alt="Picture" src="<?php echo (!empty($profile->picture)?base_url($profile->picture):base_url("assets/images/no-img.png")) ?>" class="img-thumbnail img-responsive">
                                <h3><?php echo "$profile->firstname $profile->lastname " ?></h3>
                            </div> -->
                           
                        </div></br>



                        <div class="row"> 

                        <div style="float:left;width:100%;padding-left:10px">
                              <!-- Medicine -->
                            <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info header-2">
                                        <th width=""><?php echo "Test Name" ?></th>
                                        <th width=""><?php echo "Normal Value" ?></th>
                                        <th><?php echo "Actual Value" ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                               
                                if (!empty($profile->medicine)) {
                                    $pathologytest = json_decode($profile->medicine);
 
                                    if (sizeof($pathologytest) > 0) 
                                        foreach ($pathologytest as $value2) { 
                                ?>
                                    <tr>
                                        
                                        <td><?php echo $value2->name; ?></td> 
                                        <td><?php echo $value2->normal_value; ?></td>  
                                        <td><?php echo $value2->actual_value; ?></td> 
                                
                                    </tr>
                                <?php  
                                        }
                                    }
                                ?> 
                                </tbody> 
                            </table> 

                           
                        </div> 
                    </div>



                    </div> 

                    <div role="tabpanel" class="tab-pane" id="documents">
                        <div class="row">
                            <div class="col-sm-12">
                                <table width="100%" class="datatable table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo display('serial') ?></th>
                                            <th><?php echo display('doctor_name') ?></th>
                                            <th><?php echo display('description') ?></th>
                                            <th><?php echo display('date') ?></th>
                                            <th><?php echo display('upload_by') ?></th>
                                            <th class="no-print"><?php echo display('action') ?></th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($documents)) { ?>
                                            <?php $sl = 1; ?>
                                            <?php foreach ($documents as $document) { ?>
                                                <tr>
                                                    <td><?php echo $sl; ?></td>
                                                    <td><?php echo $document->doctor_name; ?></td>
                                                    <td><?php echo character_limiter(strip_tags($document->description),50); ?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($document->date)); ?></td> 
                                                    <td><?php echo $document->upload_by; ?></td> 
                                                    <td class="center no-print" width="110"> 
                                                        <a target="_blank" href="<?php echo base_url($document->hidden_attach_file) ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                                                        <a href="<?php echo base_url("patient/document_form/$document->patient_id") ?>" class="btn btn-xs btn-warning" title="Add Document"><i class="fa fa-plus"></i></a> 
                                                        <a download target="_blank" href="<?php echo base_url($document->hidden_attach_file) ?>" class="btn btn-xs btn-success"><i class="fa fa-download"></i></a>
                                                        <a href="<?php echo base_url("patient/document_delete/$document->id?file=$document->hidden_attach_file") ?>" class="btn btn-xs btn-danger" onclick="return confirm('<?php echo display('are_you_sure') ?>') "><i class="fa fa-trash"></i></a> 
                                                    </td>
                                                </tr>
                                                <?php $sl++; ?>
                                            <?php } ?> 
                                        <?php } ?> 
                                    </tbody>
                                </table>  <!-- /.table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>  

            </div> 

             <div class="row">
                    <div class="col-sm-12">
                        <!-- Signature -->
                        <table class="table" style="margin-top:50px"> 
                            <thead> 
                                <tr>
                                    <th style="width:50%;"></th>
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Signature</i>
                                    </td>
                                </tr>
                            </thead>
                        </table> 

                    </div>
                </div>

            <div class="panel-footer">
                <div class="text-center">
                    <strong><?php echo $this->session->userdata('title') ?></strong>
                    <p class="text-center"><?php echo $this->session->userdata('address') ?></p>
                </div>
            </div>
        </div>
    </div>
  
</div>
