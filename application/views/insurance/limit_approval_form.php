<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("insurance/insurance/limit_approval_list") ?>"> <i class="fa fa-list"></i> Limit Approval List  </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
              <?php echo form_open_multipart('insurance/insurance/create_limit_approval','class="form-inner"') ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                            <input name="id" type="hidden" id="hidden_patient_id" value="<?php echo $insurance->id;?>">

                            <div class="form-group row">
                                <label for="search_patient" class="col-xs-3 col-form-label"><?php echo "Search Patient"; ?></label>
                                <div class="col-xs-9">
                                    <input name="search_patient" type="text" class="form-control ui-autocomplete-input" id="search_patient" placeholder="<?php echo "search_patient"; ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="patient_id" class="col-xs-3 col-form-label">Patient Id<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="patient_id" type="text" class="form-control" id="patient_id" placeholder="Patient Id" value="<?php echo $insurance->patient_id ?>">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label for="ins_disease_name" class="col-xs-3 col-form-label">Disease name <i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="ins_disease_name" type="text" class="form-control" id="ins_disease_name" placeholder="Disease Name" value="<?php echo $insurance->disease_name ?>">
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="firstname" class="col-xs-3 col-form-label">Full Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="firstname" type="text" class="form-control" id="firstname" placeholder="Full Name" value="<?php echo $insurance->firstname ?>" >
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="phone" class="col-xs-3 col-form-label"><?php echo display('phone') ?></label>
                                <div class="col-xs-9">
                                    <input name="phone" class="form-control" type="text" placeholder="<?php echo display('phone') ?>" id="phone"  value="<?php echo $insurance->phone ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="age" class="col-xs-3 col-form-label">Age </label>
                                <div class="col-xs-9">
                                    <input name="age" class="form-control" type="text" placeholder="Age" id="age"  value="<?php echo $insurance->age ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('sex') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Male" <?php echo  set_radio('sex', 'Male', TRUE); ?> ><?php echo display('male') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Female" <?php echo  set_radio('sex', 'Female'); ?> ><?php echo display('female') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Other" <?php echo  set_radio('sex', 'Other'); ?> ><?php echo display('other') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                        
                            <!-- if patient picture is already uploaded -->
                            <?php //if(!empty($patient->picture)) {  ?>
                           <!-- <div class="form-group row">
                                <label for="picturePreview" class="col-xs-3 col-form-label"></label>
                                <div class="col-xs-9">
                                    <img src="<?php //echo base_url($patient->picture) ?>" alt="Picture" class="img-thumbnail" />
                                </div>
                            </div>
                            <?php // } ?>

                            <div class="form-group row">
                                <label for="picture" class="col-xs-3 col-form-label"><?php //echo display('picture') ?></label>
                                <div class="col-xs-9">
                                    <input type="file" name="picture" id="picture" value="<?php //echo $patient->picture ?>">
                                    <input type="hidden" name="old_picture" value="<?php //echo $patient->picture ?>">
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="address" class="col-xs-3 col-form-label"><?php echo display('address') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <textarea name="address" class="form-control"  placeholder="<?php echo display('address') ?>" maxlength="140" rows="7"><?php echo $insurance->address ?></textarea>
                                </div>
                            </div> 
 
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="organisation_name" class="col-xs-3 col-form-label">Organisation Name</label>
                                <div class="col-xs-9">
                                    <input name="organisation_name" type="text" class="form-control" id="organisation_name" placeholder="Organisation Name" value="<?php echo $insurance->organisation_name ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="tpa_name" class="col-xs-3 col-form-label">TPA Name</label>
                                <div class="col-xs-9">
                                <input name="tpa_name" type="text" class="form-control" id="tpa_name" placeholder="TPA Name" value="<?php echo $insurance->tpa_name ?>" >
                                </div>
                            </div>

                           
                    </div>
                    <div class="col-md-6 col-sm-12">

                     <div class="form-group row">
                                <label for="ipd_no" class="col-xs-4 col-form-label">IPD No<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="ipd_no" type="text" class="form-control" id="ipd_no" placeholder="IPD No" value="<?php echo $insurance->ipd_no ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                            <label for="room_no" class="col-xs-4 col-form-label">Room No<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="room_no" type="text" class="form-control" id="room_no" placeholder="Room No" value="<?php echo $insurance->room_no ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="discription" class="col-xs-4 col-form-label">Discription<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="discription" type="text" class="form-control" id="discription" placeholder="Discription" value="<?php echo $insurance->discription ?>">
                                </div>
                            </div>


                        <div class="form-group row">
                                <label for="guardianname" class="col-xs-4 col-form-label">Guardian Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="guardianname" type="text" class="form-control" id="guardianname" placeholder="Guardian Name" value="<?php echo $insurance->guardianname ?>" >
                                </div>
                            </div>
                            
                            
                             <div class="form-group row">
                                <label for="gphone_no" class="col-xs-4 col-form-label">Phone No<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="gphone_no" type="text" class="form-control" id="gphone_no" placeholder="Guardian Phone No" value="<?php echo $insurance->gphone_no ?>">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="guar_address" class="col-xs-4 col-form-label">Address<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="guar_address" type="text" class="form-control" id="guar_address" placeholder="Guardian Address" value="<?php echo $insurance->guar_address ?>">
                                </div>
                            </div>
                            

                           <div class="form-group row">
                                <label for="consultant_doctor_name" class="col-xs-4 col-form-label">Consultant Doctor Name</label>
                                <div class="col-xs-8">
                                    <input name="consultant_doctor_name" type="text" class="form-control" id="consultant_doctor_name" placeholder="Doctor Name" value="<?php echo $insurance->consultant_doctor_name ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="policy_name" class="col-xs-4 col-form-label">Policy Name</label>
                                <div class="col-xs-8">
                                    <input name="policy_name" type="text" class="form-control" id="policy_name" placeholder="Policy Name" value="<?php echo $insurance->policy_name ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="policy_no" class="col-xs-4 col-form-label">Policy No</label>
                                <div class="col-xs-8">
                                    <input name="policy_no" type="text" class="form-control" id="policy_no" placeholder="Policy No" value="<?php echo $insurance->policy_no ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="policy_from_date" class="col-xs-4 col-form-label">Policy From Date</label>
                                <div class="col-xs-8">
                                    <input name="policy_from_date" type="date" class="form-control" id="policy_from_date" placeholder="dd/mm/yyyy" value="" value="<?php echo $insurance->policy_from_date ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="policy_to_date" class="col-xs-4 col-form-label">Policy To Date</label>
                                <div class="col-xs-8">
                                    <input name="policy_to_date" type="date" class="form-control" id="policy_to_date" placeholder="dd/mm/yyyy" value="<?php echo $insurance->policy_to_date ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="policy_holder_name" class="col-xs-4 col-form-label">Policy Holder Name</label>
                                <div class="col-xs-8">
                                    <input name="policy_holder_name" type="text" class="form-control" id="policy_holder_name" placeholder="Policy Holder Name" value="<?php echo $insurance->policy_holder_name ?>">
                                </div>
                            </div>

                           
                        </div>
                    
                   
                </div>

                   <div class="col-md-12 col-sm-12">
                             <!-- Organisation -->
                        <table class="table table-striped"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width="160">Disease name</th>
                                    <th width="160">Limit</th>
                                    <th>Approval</th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                            <tbody id="insurance_limit" class="clone_count">
                                    <?php

                                    if($insurance->id =="")
                                    {
                                    ?>
                                    <tr id="insurance">
                                    <td><input type="text" data-unique-name="disease_name_list" name="prescription[0][disease_name_list]"  autocomplete="off" class="form-control" placeholder="Disease Name" value=""></td>
                                    <td><input data-unique-name="limit" name="prescription[0][limit]" class="form-control" placeholder="Limit" value=""></td>
                                     <td><input data-unique-name="approval" name="prescription[0][approval]" class="form-control" placeholder="Approval" value=""></td> 
                                   <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr>

                                <?php 

                                    }else{

                            if (!empty($insurance->limit_approval)) {
                                $insurance_limit = json_decode($insurance->limit_approval);

                                if (sizeof($insurance_limit) > 0) 
                                    foreach ($insurance_limit as $value) {

                                ?> 
                                <tr id="insurance">
                                    <td><input type="text" data-unique-name="disease_name_list" name="prescription[0][disease_name_list]" autocomplete="off" class="form-control" placeholder="Disease Name" value="<?php echo $value->name ?>"></td>
                                    <td><input data-unique-name="limit" name="prescription[0][limit]" class="form-control" placeholder="Limit" value="<?php echo $value->limit ?>"></td>
                                     <td><input data-unique-name="approval" name="prescription[0][approval]" class="form-control" placeholder="Approval" value="<?php echo $value->approval ?>"></td> 
                                   <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr> 
                                <?php } } } ?> 
                            </tbody> 
                        </table>

                    <!-- approval charges -->
                        <table class="table table-striped"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width="160">Charges Name</th>
                                    <th width="160">Amount</th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                            <tbody id="limit_approval">
                              <?php

                               if($insurance->id =="")
                                    {
                                     ?>
                                         <tr>
                                    <td><input type="text" name="charges_name[]" autocomplete="off" class="form-control" placeholder="Charges Name" value=""></td>
                                    <td><input name="amount[]" class="form-control" placeholder="Amount" value=""></td>
                                    <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn_limit"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn_limit"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr>
                                  

                                   <?php }else{  

                            if (!empty($insurance->approval_break)) {
                                $approval_br = json_decode($insurance->approval_break);

                                if (sizeof($approval_br) > 0) 
                                    foreach ($approval_br as $value2) {

                                ?> 
                                <tr>
                                    <td><input type="text" name="charges_name[]" autocomplete="off" class="form-control" placeholder="Charges Name" value="<?php echo $value2->name ?>"></td>
                                    <td><input name="amount[]" class="form-control" placeholder="Amount" value="<?php echo $value2->amount ?>"></td>
                                    <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn_limit"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn_limit"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr>
                                <?php } } } ?>  
                            </tbody> 
                        </table>

                    


                  <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>

                </div>
                    
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('insurance/insurance/get_patient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
    var userid = ui.item.value; // selected value

    // AJAX
    $.ajax({
     url: "<?= base_url('insurance/insurance/get_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
 console.log(response);//return false;
      
       var id = response.id;
       var fname = response.firstname;
       var lname = response.lastname;
       var email = response.email;

       var textBoxArray = ['patient_id','firstname','lastname','email','phone','mobile','address','guardianname','birthmark','aadharno',
       'reffer_doctor_name','reffer_doctor_phone','consultant_doctor_name','consultantation_fee','followup_consultation','service_tax',
        'total_fee'];
       // Set value to textboxes
       

$.each(response, function(key , value) {    
   if(jQuery.inArray( key, textBoxArray ) != '-1') {
        $('#'+key).val(value); 
   }
   if(key == 'id') {
       $("#hidden_patient_id").val(value);
   }
});

     }
    });

    return false;
   }
  });
    
   }); 
</script>

<script type="text/javascript">
$(document).ready(function() {   
 
    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent(); 
      //  $('#insurance_limit').append("<tr>"+itemData.html()+"</tr>");
      //  $('#insurance_limit tr:last-child').find(':input').val('');
        
        
        var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var cloneCount = $('.diet_count').length;
        
        var clone = $("#insurance").clone();
        clone.attr("id", "insurance" + newId);
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
            if($(this).is(':checkbox')) {
             $(this).prop('checked' , false);   
            }
            else {
                $(this).val('');
            }
            $(this).attr('name', "prescription[" + newId + "][" + name + "]");
        });
        console.log(clone);
        
        //$('#medicine').append("<tr>"+clone+"</tr>");
        $(".clone_count").last().after(clone);



    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


     //#------------------------------------
    //   STARTS OF Approval limit 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn_limit' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#limit_approval').append("<tr>"+itemData.html()+"</tr>");
        $('#limit_approval tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn_limit' ,function() {
        $(this).parent().parent().parent().remove();
    });

});

</script>


      