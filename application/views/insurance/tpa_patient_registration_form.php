<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("insurance/insurance/tpa_patient_list") ?>"> <i class="fa fa-list"></i>TPA Patient List  </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <?php echo form_open_multipart('insurance/insurance/create_tpa_patient','class="form-inner"') ?>

                            <input name="id" type="hidden" id="hidden_patient_id_test" value="<?php echo $insurance->id;?>">

                            <div class="form-group row">
                                <label for="search_patient" class="col-xs-3 col-form-label"><?php echo "Search Patient"; ?></label>
                                <div class="col-xs-9">
                                    <input name="search_patient" type="text" class="form-control ui-autocomplete-input" id="search_patient" placeholder="<?php echo "search_patient"; ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="patient_id" class="col-xs-3 col-form-label">Patient Id<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="patient_id" type="text" class="form-control" id="patient_id" placeholder="Patient Id" value="<?php echo $insurance->patient_id ?>">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label for="ins_disease_name" class="col-xs-3 col-form-label">Disease name <i class="text-danger"></i></label>
                                <div class="col-xs-9">
                                    <input name="ins_disease_name" type="text" class="form-control" id="ins_disease_name" placeholder="Disease Name" value="<?php echo $insurance->disease_name ?>">
                                </div>
                            </div>

                            
                            <div class="form-group row">
                                <label for="firstname" class="col-xs-3 col-form-label">Full Name<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="firstname" type="text" class="form-control" id="firstname" placeholder="Full Name" value="<?php echo $insurance->firstname ?>" >
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="phone" class="col-xs-3 col-form-label"><?php echo display('phone') ?></label>
                                <div class="col-xs-9">
                                    <input name="phone" class="form-control" type="text" placeholder="<?php echo display('phone') ?>" id="phone"  value="<?php echo $insurance->phone ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="age" class="col-xs-3 col-form-label">Age </label>
                                <div class="col-xs-9">
                                    <input name="age" class="form-control" type="text" placeholder="Age" id="age"  value="<?php echo $insurance->age ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="blood_group" class="col-xs-3 col-form-label"><?php echo display('blood_group') ?></label>
                                <div class="col-xs-9"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'A+' => 'A+',
                                            'A-' => 'A-',
                                            'B+' => 'B+',
                                            'B-' => 'B-',
                                            'O+' => 'O+',
                                            'O-' => 'O-',
                                            'AB+' => 'AB+',
                                            'AB-' => 'AB-'
                                        );
                                        echo form_dropdown('blood_group', $bloodList, $insurance->blood_group, 'class="form-control" id="blood_group" '); 
                                    ?>
                                </div>
                            </div>

                               <div class="form-group row">
                                <label for="date_of_birth" class="col-xs-3 col-form-label"><?php echo display('date_of_birth') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="date_of_birth" class="datepicker form-control" type="text" placeholder="<?php echo display('date_of_birth') ?>" id="date_of_birth"  value="<?php echo $insurance->date_of_birth ?>">
                                </div>
                            </div>


                             <div class="form-group row">
                                <label class="col-sm-4">Maritual Status</label>
                                <div class="col-xs-8">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="maritual_status" value="Married" <?php echo  set_radio('maritual_status', 'Married', TRUE); ?> >Married
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="maritual_status" value="Unmarried" <?php echo  set_radio('maritual_status', 'Unmarried'); ?> >Unmarried
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('sex') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Male" <?php echo  set_radio('sex', 'Male', TRUE); ?> ><?php echo display('male') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Female" <?php echo  set_radio('sex', 'Female'); ?> ><?php echo display('female') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="sex" value="Other" <?php echo  set_radio('sex', 'Other'); ?> ><?php echo display('other') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                        
                            <!-- if patient picture is already uploaded -->
                            <?php //if(!empty($patient->picture)) {  ?>
                           <!-- <div class="form-group row">
                                <label for="picturePreview" class="col-xs-3 col-form-label"></label>
                                <div class="col-xs-9">
                                    <img src="<?php //echo base_url($patient->picture) ?>" alt="Picture" class="img-thumbnail" />
                                </div>
                            </div>
                            <?php // } ?>

                            <div class="form-group row">
                                <label for="picture" class="col-xs-3 col-form-label"><?php //echo display('picture') ?></label>
                                <div class="col-xs-9">
                                    <input type="file" name="picture" id="picture" value="<?php //echo $patient->picture ?>">
                                    <input type="hidden" name="old_picture" value="<?php //echo $patient->picture ?>">
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="address" class="col-xs-3 col-form-label"><?php echo display('address') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <textarea name="address" class="form-control"  placeholder="<?php echo display('address') ?>" maxlength="140" rows="7"><?php echo $insurance->address ?></textarea>
                                </div>
                            </div> 
 
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9">
                                    <div class="form-check">
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="1" <?php echo  set_radio('status', '1', TRUE); ?> ><?php echo display('active') ?>
                                        </label>
                                        <label class="radio-inline">
                                        <input type="radio" name="status" value="0" <?php echo  set_radio('status', '0'); ?> ><?php echo display('inactive') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="organisation_name" class="col-xs-3 col-form-label">Organisation Name</label>
                                <div class="col-xs-9">
                                    <input name="organisation_name" type="text" class="form-control" id="organisation_name" placeholder="Organisation Name" value="<?php echo $insurance->organisation_name ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="tpa_name" class="col-xs-3 col-form-label">TPA Name</label>
                                <div class="col-xs-9">
                                <input name="tpa_name" type="text" class="form-control" id="tpa_name" placeholder="TPA Name" value="<?php echo $insurance->tpa_name ?>" >
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="tpa_registration_no" class="col-xs-3 col-form-label">TPA Registration No</label>
                                <div class="col-xs-9">
                                <input name="tpa_registration_no" type="text" class="form-control" id="tpa_registration_no" placeholder="TPA Registration No" value="<?php echo $insurance->tpa_regno ?>" >
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="tpa_code" class="col-xs-3 col-form-label">TPA Code</label>
                                <div class="col-xs-9">
                                <input name="tpa_code" type="text" class="form-control" id="tpa_code" placeholder="TPA Code" value="<?php echo $insurance->tpa_code ?>" >
                                </div>
                            </div>

                           
                    </div>
                    <div class="col-md-6 col-sm-12">

                     <div class="form-group row">
                                <label for="ipd_no" class="col-xs-4 col-form-label">IPD No<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="ipd_no" type="text" class="form-control" id="ipd_no" placeholder="IPD No" value="<?php echo $insurance->ipd_no ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                            <label for="room_no" class="col-xs-4 col-form-label">Room No<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="room_no" type="text" class="form-control" id="room_no" placeholder="Room No" value="<?php echo $insurance->room_no ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="discription" class="col-xs-4 col-form-label">Discription<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="discription" type="text" class="form-control" id="discription" placeholder="Discription" value="<?php echo $insurance->discription ?>">
                                </div>
                            </div>


                        <div class="form-group row">
                                <label for="guardianname" class="col-xs-4 col-form-label">Guardian Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="guardianname" type="text" class="form-control" id="guardianname" placeholder="Guardian Name" value="<?php echo $insurance->guardianname ?>" >
                                </div>
                            </div>
                            
                            
                             <div class="form-group row">
                                <label for="gphone_no" class="col-xs-4 col-form-label">Phone No<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="gphone_no" type="text" class="form-control" id="gphone_no" placeholder="Guardian Phone No" value="<?php echo $insurance->gphone_no ?>">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="guar_address" class="col-xs-4 col-form-label">Address<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="guar_address" type="text" class="form-control" id="guar_address" placeholder="Guardian Address" value="<?php echo $insurance->guar_address ?>">
                                </div>
                            </div>
                            

                           <div class="form-group row">
                                <label for="consultant_doctor_name" class="col-xs-4 col-form-label">Consultant Doctor Name</label>
                                <div class="col-xs-8">
                                    <input name="consultant_doctor_name" type="text" class="form-control" id="consultant_doctor_name" placeholder="Doctor Name" value="<?php echo $insurance->consultant_doctor_name ?>" >
                                </div>
                            </div>


                             <div class="form-group row">
                                <label for="department" class="col-xs-4 col-form-label">Department </label>
                                <div class="col-xs-8">
                                    <input name="department" type="text" class="form-control" id="department" placeholder="Department" value="<?php echo $insurance->depart_name ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="specialisation" class="col-xs-4 col-form-label">Specialisation</label>
                                <div class="col-xs-8">
                                    <input name="specialisation" type="text" class="form-control" id="specialisation" placeholder="Specialisation" value="<?php echo $insurance->doctor_specialisation ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="policy_name" class="col-xs-4 col-form-label">Policy Name</label>
                                <div class="col-xs-8">
                                    <input name="policy_name" type="text" class="form-control" id="policy_name" placeholder="Policy Name" value="<?php echo $insurance->policy_name ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="policy_no" class="col-xs-4 col-form-label">Policy No</label>
                                <div class="col-xs-8">
                                    <input name="policy_no" type="text" class="form-control" id="policy_no" placeholder="Policy No" value="<?php echo $insurance->policy_no ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="policy_from_date" class="col-xs-4 col-form-label">Policy From Date</label>
                                <div class="col-xs-8">
                                    <input name="policy_from_date" type="date" class="form-control" id="policy_from_date" placeholder="dd/mm/yyyy" value="" value="<?php echo $insurance->policy_from_date ?>" >
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="policy_to_date" class="col-xs-4 col-form-label">Policy To Date</label>
                                <div class="col-xs-8">
                                    <input name="policy_to_date" type="date" class="form-control" id="policy_to_date" placeholder="dd/mm/yyyy" value="<?php echo $insurance->policy_to_date ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="policy_holder_name" class="col-xs-4 col-form-label">Policy Holder Name</label>
                                <div class="col-xs-8">
                                    <input name="policy_holder_name" type="text" class="form-control" id="policy_holder_name" placeholder="Policy Holder Name" value="<?php echo $insurance->policy_holder_name ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="drugs_summary" class="col-xs-4 col-form-label">Drugs Summary</label>
                                <div class="col-xs-8">
                                    <input name="drugs_summary" type="text" class="form-control" id="drugs_summary" placeholder="Drugs Summary" value="<?php echo $insurance->drugs_summary ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="advance_amount" class="col-xs-4 col-form-label">Advance Amount</label>
                                <div class="col-xs-8">
                                    <input name="advance_amount" type="text" class="form-control" id="advance_amount" placeholder="Advance Amount" value="<?php echo $insurance->advance_amt ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="case_paper" class="col-xs-4 col-form-label">Case Paper</label>
                                <div class="col-xs-8">
                                    <input name="case_paper" type="text" class="form-control" id="case_paper" placeholder="Case Paper" value="<?php echo $insurance->case_paper ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="payment_type" class="col-xs-4 col-form-label">Payment Type</label>
                                <div class="col-xs-8">
                                    <input name="payment_type" type="text" class="form-control" id="payment_type" placeholder="Payment Type" value="<?php echo $insurance->payment_type ?>">
                                </div>
                            </div>

                              <div class="form-group row">
                                <label for="payment_mode" class="col-xs-4 col-form-label">Payment Mode</label>
                                <div class="col-xs-8"> 
                                    <?php
                                        $bloodList = array(
                                            ''   => display('select_option'),
                                            'Cash'=> 'Cash',
                                            'DD' => 'DD',
                                            'Cheque' => 'Cheque',
                                            'Transfer' => 'Transfer',
                                           
                                        );
                                        echo form_dropdown('payment_mode', $bloodList, 'class="form-control" id="payment_mode" '); 
                                    ?>
                                </div>
                            </div>

                               <div class="form-group row">
                                <label for="admission_dt" class="col-xs-4 col-form-label">Admission Date<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="admission_dt" class="datepicker form-control" type="text" placeholder="Admission Date" id="admission_dt"  value="<?php echo $insurance->admission_dt ?>">
                                </div>
                            </div>

                                <div class="form-group row">
                                <label for="discharge_dt" class="col-xs-4 col-form-label">Discharge Date</label>
                                <div class="col-xs-8">
                                    <input name="discharge_dt" class="datepicker form-control" type="text" placeholder="Discharge Date" id="discharge_dt"  value="<?php echo $insurance->discharge_dt ?>">
                                </div>
                            </div>

                                <div class="form-group row">
                                <label for="packege_amount" class="col-xs-4 col-form-label">Packege Amount</label>
                                <div class="col-xs-8">
                                    <input name="packege_amount" class="form-control" type="text" placeholder="Packege Amount" id="packege_amount"  value="<?php echo $insurance->packege_amount ?>">
                                </div>
                            </div>

                           
                        </div>
                    
                   
                </div>

                   <div class="col-md-12 col-sm-12">
                           

                  <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>

                </div>
                    
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
    var userid = ui.item.value; // selected value

    // AJAX
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
 console.log(response);//return false;
      
       var id = response.id;
       var fname = response.firstname;
       var lname = response.lastname;
       var email = response.email;

       var textBoxArray = ['patient_id','firstname','lastname','email','phone','mobile','address','guardianname','birthmark','aadharno',
       'reffer_doctor_name','reffer_doctor_phone','consultant_doctor_name','consultantation_fee','followup_consultation','service_tax',
        'total_fee'];
       // Set value to textboxes
       

$.each(response, function(key , value) {    
   if(jQuery.inArray( key, textBoxArray ) != '-1') {
        $('#'+key).val(value); 
   }
   if(key == 'id') {
       $("#hidden_patient_id").val(value);
   }
});

     }
    });

    return false;
   }
  });
    
   }); 
</script>

<script type="text/javascript">
$(document).ready(function() {   
 
    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#insurance_limit').append("<tr>"+itemData.html()+"</tr>");
        $('#insurance_limit tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


     //#------------------------------------
    //   STARTS OF Approval limit 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn_limit' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#limit_approval').append("<tr>"+itemData.html()+"</tr>");
        $('#limit_approval tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn_limit' ,function() {
        $(this).parent().parent().parent().remove();
    });

});

</script>


      