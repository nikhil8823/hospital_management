<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("insurance/insurance/sponsers_details_list") ?>"> <i class="fa fa-list"></i> Sponsers Details List  </a>  
                </div>
            </div> 

            <div class="panel-body panel-form">
             <?php echo form_open('insurance/insurance/create_sponser','class="form-inner"') ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                            <?php echo form_hidden('id',$insurance->id) ?>

                            <div class="form-group row">
                                <label for="organisation_name" class="col-xs-4 col-form-label">Organisation Name<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="organisation_name"  type="text" class="form-control" id="organisation_name" placeholder="Organisation Name" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="service_tax" class="col-xs-4 col-form-label">Service Tax</label>
                                <div class="col-xs-8">
                                    <input name="service_tax" class="form-control" id="service_tax"  placeholder="Service Tax" value="">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="discount" class="col-xs-4 col-form-label">Discount %</label>
                                <div class="col-xs-8">
                                    <input name="discount" class="form-control" id="discount"  placeholder="Discount" value="">
                                </div>
                            </div>

                            <!--Radio-->
                            <div class="form-group row">
                                <label class="col-sm-4"><?php echo display('status') ?></label>
                                <div class="col-xs-8"> 
                                    <div class="form-check">
                                        <label class="radio-inline"><input type="radio" name="status" value="1" checked><?php echo display('active') ?></label>
                                        <label class="radio-inline"><input type="radio" name="status" value="0"><?php echo display('inactive') ?></label>
                                    </div>
                                </div>
                            </div>


                            </div>
                             <div class="col-md-6 col-sm-12">

                              <div class="form-group row">
                                <label for="remark" class="col-xs-4 col-form-label">Remark<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="remark"  type="text" class="form-control" id="remark" placeholder="Remark" value="">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="organisation_no" class="col-xs-4 col-form-label">Organisation No<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="organisation_no"  type="text" class="form-control" id="organisation_no" placeholder="Organisation No" value="">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label for="organisation_code_no" class="col-xs-4 col-form-label">Organisation Code No<i class="text-danger">*</i></label>
                                <div class="col-xs-8">
                                    <input name="organisation_code_no"  type="text" class="form-control" id="organisation_code_no" placeholder="Organisation Code No" value="">
                                </div>
                            </div>

                             </div>
                            
                        <div class="col-md-12 col-sm-12">
                             <!-- Medicine -->
                        <table class="table table-striped"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width="160">Grop Charge</th>
                                    <th width="160">Charge Name</th>
                                    <th>Hospital Rate</th>
                                    <th>Insurance Rate</th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                            <tbody id="medicine">
                                <tr>
                                    <td> <?php

                                        echo form_dropdown('group_charge', $insurance_list, 'class="form-control" id="group_charge" '); 
                                  ?>
                                        
                                    </td>
                                    <td><input type="text" name="charge_name[]" autocomplete="off" class="form-control" placeholder="Charge Name" ></td>
                                    <td><input name="hospital_rate[]" class="form-control" placeholder="Hospital Rate"></td>
                                     <td><input name="insurance_rate[]" class="form-control" placeholder="Insurance Rate"></td> 
                                   <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr>  
                            </tbody> 
                        </table>

                         <div class="form-group row">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>

                        </div> 


                        <?php echo form_close() ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
$(document).ready(function() {   
 
    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent(); 
        $('#medicine').append("<tr>"+itemData.html()+"</tr>");
        $('#medicine tr:last-child').find(':input').val('');
    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });

});

</script>