<div class="row">
    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("insurance/insurance/create_tpa_patient") ?>"> <i class="fa fa-plus"></i> Add TPA Patient </a>  
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th>Patient ID</th>
                            <th>Full Name</th>
                            <th>Disease Name</th>
                            <th>Organisation Name</th>
                            <th>TPA Name</th>
                            <th>Policy Name</th>
                            <th>Policy No</th>
                             <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($insurances)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($insurances as $insurance) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $insurance->patient_id; ?></td>
                                    <td><?php echo $insurance->firstname; ?></td>
                                    <td><?php echo $insurance->disease_name; ?></td>
                                    <td><?php echo $insurance->organisation_name; ?></td>
                                    <td><?php echo $insurance->tpa_name; ?></td>
                                    <td><?php echo $insurance->policy_name; ?></td>
                                     <td><?php echo $insurance->policy_no; ?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url("insurance/insurance/edit_tpa_patient_form/$insurance->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-edit"></i></a> 
                                        <a href="<?php echo base_url("insurance/insurance/delete_tpa_patient/$insurance->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
