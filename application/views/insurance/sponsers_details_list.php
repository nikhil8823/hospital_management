<div class="row">
    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("insurance/insurance/create_sponser") ?>"> <i class="fa fa-plus"></i> Add Sponsers Details </a>  
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th>Organisation Name</th>
                            <th>Service Tax</th>
                            <th>Discount</th>
                            <th>Organisation No</th>
                             <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($insurances)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($insurances as $insurance) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $insurance->organisation_name; ?></td>
                                    <td><?php echo character_limiter($insurance->service_tax, 60); ?></td>
                                    <td><?php echo $insurance->discount; ?></td>
                                      <td><?php echo $insurance->organisation_no; ?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url("insurance/insurance/edit_sponser_details/$insurance->id") ?>" class="btn btn-xs  btn-primary"><i class="fa fa-edit"></i></a> 
                                        <a href="<?php echo base_url("insurance/insurance/delete_sponsersdetails/$insurance->id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-xs  btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
