<div class="row">

<div class="col-sm-12">
        <div  class="panel panel-default">
            <div class="panel-body"> 

                <form class="form-inline" action="<?php echo base_url('appointment/dischargeReport') ?>">

                    <div class="form-group">
                        <label class="sr-only" for="start_date"><?php echo display('start_date') ?></label>
                        <input type="text" name="start_date" class="form-control datepicker" id="start_date" placeholder="<?php echo display('start_date') ?>" value="">
                    </div> 

                    <div class="form-group">
                        <label class="sr-only" for="end_date"><?php echo display('end_date') ?></label>
                        <input type="text" name="end_date" class="form-control datepicker" id="end_date" placeholder="<?php echo display('end_date') ?>" value="">
                    </div>  

                    <button type="submit" class="btn btn-success"><?php echo display('filter') ?></button>

                </form>

            </div>
        </div>
    </div>


    <!--  table area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-success" href="<?php echo base_url("appointment/create") ?>"> <i class="fa fa-plus"></i>  <?php echo display('add_appointment') ?> </a>  
                </div>
            </div> 
            <div class="panel-body">
                <table width="100%" class="datatable table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo display('serial') ?></th>
                            <th><?php echo display('appointment_id') ?></th>
                            <th><?php echo display('patient_id') ?></th>
                            <th><?php echo display('department') ?></th>
                            <th><?php echo display('doctor_name') ?></th>
                            <th><?php echo "Patient Name" ?></th>
                            <!--<th><?php echo display('problem') ?></th>-->
                            <th><?php echo display('appointment_date') ?></th>
                            <!--<th><?php echo display('status') ?></th>-->
                            <th>Consultation fee</th>
                            <th><?php echo display('action') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php if (!empty($result)) { ?>

                      <?php $sl = 1; ?>
                            <?php foreach ($result as $appointment1) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $appointment1->appointment_id; ?></td>
                                    <td><?php echo $appointment1->patient_id; ?></td>
                                    <td><?php echo $appointment1->name; ?></td>
                                    <td><?php echo $appointment1->firstname.' '.$appointment1->lastname; ?></td>
                                    <td><?php echo $appointment1->patient_name; ?></td>
                                    <!--<td><?php echo $appointment1->problem; ?></td>-->
                                    <td><?php echo $appointment1->date; ?></td>
                                    <!--<td><?php echo (($appointment1->status==1)?"Active":"Inactive"); ?></td>-->
                                    <td><?php echo $appointment1->consultation_fee; ?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url("appointment/view/$appointment1->appointment_id") ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a> 
                                        <a href="<?php echo base_url("appointment/delete/$appointment1->appointment_id") ?>" onclick="return confirm('<?php echo display('are_you_sure') ?>')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
 
                     <?php } else{ ?>
                        <?php if (!empty($appointments)) { ?>
                            <?php $sl = 1; ?>
                            <?php foreach ($appointments as $appointment) { ?>
                                <tr class="<?php echo ($sl & 1)?"odd gradeX":"even gradeC" ?>">
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $appointment->appointment_id; ?></td>
                                    <td><?php echo $appointment->patient_id; ?></td>
                                    <td><?php echo $appointment->name; ?></td>
                                    <td><?php echo $appointment->firstname.' '.$appointment->lastname; ?></td>
                                    <td><?php echo $appointment->patient_name; ?></td>
                                    <!--<td><?php echo $appointment->problem; ?></td>-->
                                    <td><?php echo $appointment->date; ?></td>
                                    <!--<td><?php echo (($appointment->status==1)?"Active":"Inactive"); ?></td>-->
                                    <td><?php echo $appointment->consultation_fee; ?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url("appointment/view/$appointment->appointment_id") ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a> 
                                        <a href="<?php echo base_url("appointment/delete/$appointment->appointment_id") ?>" onclick="return confirm('<?php echo display('are_you_sure') ?>')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a> 
                                    </td>
                                </tr>
                                <?php $sl++; ?>
                            <?php } ?> 
                        <?php } } ?> 
                    </tbody>
                </table>  <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>