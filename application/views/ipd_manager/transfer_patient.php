<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
<!--            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("ipd_manager/patient") ?>"> <i class="fa fa-list"></i>  <?php echo display('patient_list') ?> </a>  
                </div>
            </div> -->

            <div class="panel-body panel-form">
                <div class="row">
                    <div class="col-md-9 col-sm-12">

                        <?php echo form_open_multipart('ipd_manager/patient/transferPatient','class="form-inner"') ?>

                            

                            <div class="form-group row">
                                <label for="search_patient" class="col-xs-3 col-form-label"><?php echo "Search Patient"; ?></label>
                                <div class="col-xs-9">
                                    <input name="patient_id" type="text" class="form-control ui-autocomplete-input" id="search_patient" placeholder="<?php echo "search_patient"; ?>" >
                                </div>
                            </div>
                            <!--<div class = "hide_it" style="display: none;">-->    
                            <div class="form-group row">
                                <label for="firstname" class="col-xs-3 col-form-label"><?php echo display('first_name') ?></label>
                                <div class="col-xs-9">
                                    <p id="firstname"></p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mobile" class="col-xs-3 col-form-label"><?php echo display('mobile') ?></label>
                                <div class="col-xs-9">
                                    <p id="mobile"></p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('sex') ?> </label>
                                <div class="col-xs-9">
                                    <p id="sex"></p>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo "Admission Date"; ?> </label>
                                <div class="col-xs-9">
                                    <p id="create_date"></p>
                                </div>
                            </div>
                                
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo "Current Bed Assign"; ?> </label>
                                <div class="col-xs-9">
                                    <p id="current_bed"></p>
                                </div>
                            </div>    
                               
                            <div class="form-group row">
                                <label for="bed_id" class="col-xs-3 col-form-label"><?php echo display('bed_type') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <?php echo form_dropdown('bed_id', $bed_list, array(), 'class="form-control dateChange" id="bed_id"') ?>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="assign_date" class="col-xs-3 col-form-label"><?php echo display('assign_date') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="assign_date"  type="text" class="form-control cdatepicker dateChange" id="assign_date" placeholder="<?php echo display('assign_date') ?>" value="<?php echo date('d-m-Y'); ?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="discharge_date" class="col-xs-3 col-form-label"><?php echo display('discharge_date') ?> <i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="discharge_date"  type="text" class="form-control cdatepicker dateChange" id="discharge_date" placeholder="<?php echo display('discharge_date') ?>"  value="<?php echo date('d-m-Y'); ?>" >
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="description" class="col-xs-3 col-form-label"><?php echo display('description') ?></label>
                                <div class="col-xs-9">
                                    <textarea name="description" class="form-control"  placeholder="<?php echo display('description') ?>" rows="7"><?php echo $bed->description ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="admission_fee" class="col-xs-3 col-form-label">Admission Fee<i class="text-danger">*</i></label>
                                <div class="col-xs-9">
                                    <input name="admission_fee" type="text" class="form-control" id="admission_fee" placeholder="Admission Fee" >
                                </div>
                            </div>
                            
                            <!--Radio-->
                            <div class="form-group row">
                                <label class="col-sm-3"><?php echo display('status') ?></label>
                                <div class="col-xs-9"> 
                                    <div class="form-check">
                                        <label class="radio-inline"><input type="radio" name="status" value="1" checked><?php echo display('active') ?></label>
                                        <label class="radio-inline"><input type="radio" name="status" value="0"><?php echo display('inactive') ?></label>
                                    </div>
                                </div>
                            </div>
                            
                                
                        </div>    
                         
                    <!--</div>-->
                    
                      <div class="form-group row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <div class="ui buttons">
                                        <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                        <div class="or"></div>
                                        <button class="ui positive button"><?php echo display('save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    
                   
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
  
$(document).on('keydown', '#search_patient', function() {

  // Initialize jQuery UI autocomplete
  $( '#search_patient' ).autocomplete({
   source: function( request, response ) {
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/bedAssignPatient') ?>",
     type: 'post',
     dataType: "json",
     data: {
      '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',   
      search: request.term,request:1
     },
     success: function( data ) {
         if(data.status){
            response( data.message );
         }
         else {
             $(".hide_it").hide();
             alert('Invalid patient id');
         }
     }
    });
   },
   select: function (event, ui) {
     $(this).val(ui.item.label); // display the selected text
    var userid = ui.item.value; // selected value
    
    // AJAX
    $.ajax({
     url: "<?= base_url('ipd_manager/patient/get_bd_patient_details') ?>",
     type: 'post',
     data: {
            '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
            userid:userid
           },
     dataType: 'json',
     success:function(response){
       $(".hide_it").show();     
       var textBoxArray = ['firstname','lastname','mobile','sex','create_date','current_bed'];
       // Set value to textboxes
       $.each(response, function(key , value) {  
            if(jQuery.inArray( key, textBoxArray ) != '-1') {
                 $('#'+key).text(value); 
            }
            if(key == 'patient_id') {
                $("#search_patient").val(value);
            }
            if(key == "discharge_date") {
                //$("#discharge_date").val(value);
                //$("#discharge_date").datepicker('option', 'maxDate', value);
                //$('#discharge_date').datepicker( "dateFormat", "dd-mm-yy" );
                $( "#discharge_date" ).datepicker( "setDate", value );
                $('#discharge_date').datepicker('option', {  maxDate: new Date(value) });
                
            }
       });
       
     }
     
    });

    return false;
   }
  });
    
   }); 
   
$(document).ready(function() {
    
    //department_id
    $("#department_id").change(function(){
        var output = $('.doctor_error'); 
        var doctor_list = $('#doctor_id');
        var available_day = $('#available_day');

        $.ajax({
            url  : '<?= base_url('appointment/doctor_by_department/') ?>',
            type : 'post',
            dataType : 'JSON',
            data : {
                '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>',
                department_id : $(this).val()
            },
            success : function(data) 
            {
                if (data.status == true) {
                    doctor_list.html(data.message);
                    available_day.html(data.available_days);
                    output.html('');
                } else if (data.status == false) {
                    doctor_list.html('');
                    output.html(data.message).addClass('text-danger').removeClass('text-success');
                } else {
                    doctor_list.html('');
                    output.html(data.message).addClass('text-danger').removeClass('text-success');
                }
            }, 
            error : function()
            {
                alert('failed');
            }
        });
    }); 
    
    //custom date picker
    $('#assign_date').datepicker({
        minDate:0,
        dateFormat: "dd-mm-yy"
    });
    
    $('#discharge_date').datepicker({
        minDate:0,
        dateFormat: "dd-mm-yy"
    });
    
    
});
</script>