<div class="row">
    <!--  form area -->
    <div class="col-sm-12">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("ipd_manager/patient/nursing_notes_list") ?>"> <i class="fa fa-list"></i>  <?php echo "Nursing Notes List" ?> </a>  
                </div>
            </div> 

            <div class="panel-body">
             <?php echo form_open('ipd_manager/patient/add_nursing_notes',array('novalidate'=>'novalidate')) ?> 
                <div class="row">
                    <div class="col-md-12 col-sm-12 table-responsive">
                       
                       <input name="id" type="hidden" id="hidden_patient_id" value="">
                        <!-- Information -->
                        <table class="table">
                            <thead>
                                <tr>  
                                    <th width="33.33%">
                                        <ul class="list-unstyled">
                                            <li> 
                                                <input type="text" placeholder="<?php echo display('patient_id') ?>" required name="patient_id" id="patient_id" class="invoice-input form-control" value="<?php echo (!empty($patient_id)?$patient_id:null) ?>">
                                                <p class="text-danger  invlid_patient_id"></p>
                                            </li>   
                                            <li> 
                                                <input type="text" placeholder="<?php echo display('patient_name') ?>" class="invoice-input form-control" id="patient_name">
                                            </li>  
                                            <li>  
                                                <input type="text" placeholder="<?php echo "Gender" ?>" class="invoice-input form-control" id="sex">
                                            </li>  
                                            <li>  
                                                <input type="text" placeholder="<?php echo display('date_of_birth') ?>" class="invoice-input form-control datepicker" id="date_of_birth">
                                            </li>  
                                        </ul>
                                    </th>  
                                    <th width="33.33%">
                                        <ul class="list-unstyled">
                                            <li> 
                                                <input type="text" name="nurse_name" placeholder="<?php echo "Nurse Name" ?>" required class="invoice-input form-control">
                                            </li>   
                                            <li> 
                                                <input type="text" name="doctor_name" placeholder="<?php echo "Doctor Name" ?>" class="invoice-input form-control">
                                            </li> 
                                            <li> 
                                                <input type="text"  name="other" placeholder="<?php echo "Other" ?>" class="invoice-input form-control">
                                            </li>  
                                              
                                        </ul>
                                    </th>   
                                    <th width="33.33%">
                                        <ul class="list-unstyled">
                                            <li><input type="text" name="appointment_id" value="<?php echo (!empty($appointment_id)?$appointment_id:null) ?>" class="invoice-input form-control" placeholder="<?php echo display('appointment_id') ?>"></li>
                                            <li><input type="text" name="date" required value="<?php echo date('d-m-Y') ?>" class="datepicker invoice-input form-control" placeholder="<?php echo display('date') ?>"></li> 
                                            <li><input type="text" value="<?php echo $website->title; ?>" class="invoice-input form-control" placeholder="<?php echo display('hospital_name') ?>"></li> 
                                            <li><input type="text" value="<?php echo $website->description; ?>" class="invoice-input form-control" placeholder="<?php echo display('address') ?>"></li> 
                                        </ul>
                                    </th> 
                                </tr> 
                                
                            </thead>
                        </table>

                        

                        <!-- Medicine -->
                        <table class="table table-striped"> 
                            <thead>
                                <tr class="bg-primary">
                                    <th width="130"><?php echo 'Date' ?></th>
                                    <th width="100"><?php echo 'Time' ?></th>
                                    <th><?php echo 'Complaints' ?></th>
                                    <th width=""><?php echo 'Findings of Doctor' ?></th>
                                    <th width=""><?php echo 'Investigation of Dr' ?></th>
                                    <th width="160"><?php echo display('add_or_remove') ?></th>  
                                </tr>
                            </thead>
                            <tbody id="medicine" class="clone_count">
                                <tr id="first_medicine">
                                    <td><input type="text" data-unique-name="date" name="prescription[0][date]" autocomplete="off" class="medicine datepicker form-control" placeholder="Date" ></td>
                                    <td><input type="text" data-unique-name="time" name="prescription[0][time]" autocomplete="off" class="form-control time" placeholder="Time" ></td>
                                    <td><input type="text" name="prescription[0][complaints]" data-unique-name="complaints" autocomplete="off" class="form-control" placeholder="Complaints"  ></td> 
                                     <td><input type="text" name="prescription[0][finding_dr]" data-unique-name="finding_dr" autocomplete="off" class="form-control" placeholder="Finding Dr"  ></td> 
                                      <td><input type="text" name="prescription[0][investigation_dr]" data-unique-name="investigation_dr" autocomplete="off" class="form-control" placeholder="Investigation Dr"  ></td> 
                                    <td>
                                      <div class="btn btn-group">
                                        <button type="button" class="btn btn-sm btn-primary MedAddBtn"><?php echo display('add') ?></button>
                                        <button type="button" class="btn btn-sm btn-danger MedRemoveBtn"><?php echo display('remove') ?></button>
                                        </div>
                                    </td>   
                                </tr>  
                            </tbody> 
                        </table> 


                        
                        <!-- Fees & Comments -->
                        <div class="row">
                            <div class="col-sm-12">                             

                                <div class="form-group row">
                                    <label for="patient_notes" class="col-xs-3 col-form-label"><?php echo display('patient_notes') ?></label>
                                    <div class="col-xs-9">
                                        <textarea name="patient_notes" class="form-control"  placeholder="<?php echo display('patient_notes') ?>"></textarea>
                                    </div>
                                </div> 
                                
                                <div class="form-group row">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <div class="ui buttons">
                                            <button type="reset" class="ui button"><?php echo display('reset') ?></button>
                                            <div class="or"></div>
                                            <button class="ui positive button"><?php echo display('save') ?></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


 

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo display('case_study') ?></h4>
      </div>
      <div class="modal-body" id="caseStudyOutput">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a href="<?php echo base_url("dashboard_doctor/prescription/case_study/create") ?>" class="btn btn-primary"><?php echo display('add_patient_case_study') ?></a>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$(document).ready(function() {   
 
    // medicine list
    $('body').on('keyup change click', '.medicine', function(){
        $(this).autocomplete({
            source: [
                <?php 
                    if(!empty($medicine_category)) {
                        foreach ($medicine_category as $key=>$value) { 
                            //echo '"'.(!empty($medicine_category[$i])?$medicine_category[$i]:null).'",'; 
                                //echo '"label":"'. $i.'","value" :"'.$medicine_category[$i].'",';
                            ?>
                            { label : "<?php echo $value; ?>" ,
                             value : "<?php echo $value; ?>",
                             data_value : "<?php echo $key; ?>"
                            } ,
                       <?php }
                    } 
                ?>
            ],
            select :function (event, ui) {
                $(this).data('category-id',ui.item.data_value);
              }
        });
    });    

$('.medicine').on('autocompleteselect', function (e, ui) {
        $(this).data('category-id',ui.item.data_value);
       
    });

$('body').on('keyup change click', '.medicine_name', function(){
 var cat_id = ($(this).closest('td').prev().find(':input').data('category-id'));
 console.log(cat_id);
 $(this).autocomplete({
source: function(request, response){
    $.ajax({
        url     : '<?php echo base_url('ipd_manager/patient/get_medicine') ?>',
        method  : 'post',
        dataType: 'json', 
        data    : {
            'medicine_cat_id' : cat_id,
            '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        success : function(data) {
            if(data != false) {
            response(data);
            }
            else{
                alert("No Medicine found");
            }
        },
        error   : function(jqXHR, textStatus, errorThrown) {
            alert('failed!');
        } 
    });
}

});
});



/*$('.medicine').on('autocompleteselect', function (e, ui) {
        
        $.ajax({
            url     : '<?php echo base_url('ipd_manager/patient/get_medicine') ?>',
            method  : 'post',
            dataType: 'json', 
            data    : {
                'medicine_cat_id' : ui.item.data_value,
                '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
            },
            success : function(data) {
                console.log(data);
                
                $(".medicine_name").autocomplete({
                    source: [
                       data
                    ]
                })
            },
            error   : function(jqXHR, textStatus, errorThrown) {
                alert('failed!');
            } 
        });
        
        
    });
*/

    //#------------------------------------
    //   STARTS OF MEDICINE 
    //#------------------------------------    
    //add row
    $('body').on('click','.MedAddBtn' ,function() {
        var itemData = $(this).parent().parent().parent();
       // $('#medicine').append("<tr>"+itemData.html()+"</tr>");
        //$('#medicine tr:last-child').find(':input').val('');
        
        
        var newId = Math.round(new Date().getTime() + (Math.random() * 100));
        var cloneCount = $('.diet_count').length;
        
        var clone = $("#first_medicine").clone();
        clone.attr("id", "first_medicine" + newId);
        clone.attr("class", "clone_count");
        clone.find(":input").each(function (index, val) {
            var name = $(this).data('unique-name');
            if($(this).is(':checkbox')) {
             $(this).prop('checked' , false);
             $(this).attr('name', "prescription[" + newId + "][" + name + "][]");
            }
            else {
                $(this).val('');
                $(this).attr('name', "prescription[" + newId + "][" + name + "]");
            }
            
        });
        console.log(clone);
        
        //$('#medicine').append("<tr>"+clone+"</tr>");
        $(".clone_count").last().after(clone);

    });
    //remove row
    $('body').on('click','.MedRemoveBtn' ,function() {
        $(this).parent().parent().parent().remove();
    });


    //#------------------------------------
    //   ENDS OF PATIENT INFORMATION
    //#------------------------------------

    $(window).on('load', function(){
        var patient_id = '<?php echo $this->input->get('pid') ?>';
        if(patient_id.length > 0)
        patientInfo(patient_id);
    });

    $('body').on('keyup change', '#patient_id', function() {
        var patient_id = $(this).val();
        patientInfo(patient_id);
    });

    function patientInfo(patient_id)
    { 
        if(patient_id.length > 0)
        $.ajax({
            url     : '<?php echo base_url('ipd_manager/patient/patient') ?>',
            method  : 'post',
            dataType: 'json', 
            data    : {
                'patient_id' : patient_id,
                '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
            },
            success : function(data) {
                if (data.status == true) { 
                    $(".invlid_patient_id").text('');
                    $("#patient_name").val(data.name);
                    $("#sex").val(data.sex);
                    $("#date_of_birth").val(data.date_of_birth);
                } else {
                    $(".invlid_patient_id").text('<?php echo display("invalid_patient_id") ?>');
                }
            },
            error   : function() {
                alert('failed!');
            } 
        });
    } 

});
</script>