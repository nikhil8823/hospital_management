<div class="row">
    <!--  form area -->
    <div class="col-sm-12"  id="PrintMe">
        <div  class="panel panel-default thumbnail">
 
            <div class="panel-heading no-print">
                <div class="btn-group"> 
                    <a class="btn btn-primary" href="<?php echo base_url("ipd_manager/patient/nursing_notes_list") ?>"> <i class="fa fa-list"></i>  <?php echo "Nursing Notes List" ?> </a>  
                    <button type="button" onclick="printContent('PrintMe')" class="btn btn-danger" ><i class="fa fa-print"></i></button> 
                </div>
            </div> 

            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12">

                        <!-- Headline -->
                        <table class="table">
                            <thead>
                                <tr  class="bg-primary">
                                    <td colspan="2">
                                        <strong><?php echo display('patient_id') ?></strong>: <?php echo $prescription->patient_id; ?>, 
                                        <strong>App ID</strong>: <?php echo $prescription->appointment_id; ?>
                                    </td>
                                    <td  class="text-right"><strong><?php echo display('date') ?></strong>: <?php echo date('m/d/Y', strtotime($prescription->date)); ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="10%" class=""><center><img src="<?php echo base_url($website->logo); ?>" height="50px" width="50px"></center></td>
                                        
                                    <td width="50%">
                                        <center><ul class="list-unstyled">
                                            <li><strong><?php echo $website->title; ?></strong></li>  
                                            <li><?php echo $website->description; ?></li>  
                                            <li><?php echo $website->email; ?></li>  
                                            <li><?php echo $website->phone; ?></li>  
                                        </ul></center>
                                    </td> 

                                     <td width="40%" class="text-left">
                                         <center> <ul class="list-unstyled">
                                            <li><strong><?php echo $prescription->doctor_name; ?></strong></li> 
                                            <li><?php echo $prescription->address; ?></li>   
                                        </ul> </center> 
                                    </td> 

                                </tr>  
                            </tbody>
                            <tfoot>
                                <tr class="">
                                    <td colspan="3">
                                        <?php
                                            $date1=date_create($prescription->date_of_birth);
                                            $date2=date_create(date('Y-m-d'));
                                            $diff=date_diff($date1,$date2); 
                                            
                                        ?>
                                        <strong><?php echo display('patient_name') ?></strong>: <?php echo $prescription->patient_name; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo display('age') ?></strong>: <?php echo "$diff->y Years $diff->m Months"; ?>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  
                                        <strong><?php echo "Gender" ?></strong>: <?php echo $prescription->sex; ?>&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;
                                    </td> 
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-12">
                        <div style="float:left;width:100%;word-break:all;border-right:1px solid #e4e5e7;padding-right:10px">
                            <!-- chief_complain -->
                            <p>
                                <strong><?php echo "Other" ?></strong>: <?php echo $prescription->other; ?>
                            </p>
                            
                            <!-- patient_notes -->
                            <p>
                                <strong><?php echo display('patient_notes') ?></strong>: <?php echo $prescription->patient_notes; ?>
                            </p> 
                        </div>
                         </div>
                     </div><br>
<div class="row">
                        <div style="float:left;width:100%;padding-left:10px">
                            <!-- Medicine -->
                            <table class="table table-striped"> 
                                <thead>
                                    <tr class="bg-info header-2">
                                        <th width=""><?php echo "Date" ?></th>
                                        <th width=""><?php echo "time" ?></th>
                                        <th><?php echo "Complaints" ?></th>
                                        <th><?php echo "Dr Finding" ?></th>
                                        <th><?php echo "Dr Investigation" ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($prescription->ipd_medicine)) {
                                    $medicine = json_decode($prescription->ipd_medicine);

                                    if (sizeof($medicine) > 0) 
                                        foreach ($medicine as $value) { 
                                ?>
                                    <tr>
                                        <td><?php echo $value->date; ?></td> 
                                        <td><?php echo $value->time; ?>
                                        </td>  
                                        <td><?php echo $value->complaints; ?></td> 
                                        <td><?php echo $value->finding_dr; ?>
                                        </td> 
                                         <td><?php echo $value->investigation_dr; ?>
                                        </td> 
                                    </tr>
                                <?php  
                                        }
                                    }
                                ?> 
                                </tbody> 
                            </table> 

                        </div> 
                   </div>

                <div class="row">
                    <div class="col-sm-12">
                        <!-- Signature -->
                        <table class="table" style="margin-top:50px"> 
                            <thead> 
                                <tr>
                                    <th style="width:50%;"></th>
                                    <td style="width:50%;text-align:center">
                                        <div style="border-bottom:2px dashed #e4e5e7"></div>
                                        <i>Signature</i>
                                    </td>
                                </tr>
                            </thead>
                        </table> 

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

