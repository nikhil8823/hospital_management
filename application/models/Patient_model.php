<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_model extends CI_Model {

	private $table = "patient";
	private $table2 = "department";
        private $ipd_table = "ipd_patient";
        private $tbl_pathologytest ="tbl_pathologytest";
 
	public function create($data = [])
	{	 
		return $this->db->insert($this->table,$data);
	}
 
	public function read()
	{
		return $this->db->select("*")
			->from($this->table)
			->order_by('id','desc')
			->get()
			->result();
	} 
 
	public function read_by_id($id = null)
	{
		return $this->db->select("*")
			->from($this->table)
			->where('id',$id)
			->get()
			->row();
	} 
 
	public function update($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->table,$data); 
	} 
 
	public function delete($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
        
        public function createIpd($data = [])
	{	 
           
            $query = $this->db
                    ->from($this->ipd_table)
                    ->where('patient_id', $data['patient_id'])
                    ->get();
            $count = $query->num_rows();
            if($count == 0) {
                (object)$data['admission_date'] = date('Y-m-d');
                 return $this->db->insert($this->ipd_table,$data);
            }
//            else {
//                 return $this->db->where('patient_id',$data['patient_id'])->update($this->ipd_table,$data);
//            }
	}
        
        public function getIpdPatient()
	{
		return $this->db->select("patient.id, patient.patient_id, patient.picture, patient.firstname, patient.lastname,"
                        . "patient.email, patient.phone, patient.mobile, patient.address, patient.sex, patient.blood_group, patient.date_of_birth,"
                        . "patient.create_date, patient.status, ipd_patient.ipd_id, ipd_patient.id as ipd_primary,
                        CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name ")
			->from($this->ipd_table)
                        ->join('patient','patient.id = ipd_patient.patient_id', 'left')
                        ->join('user','user.user_id = patient.doctor_id','left')
			->order_by('patient.id','desc')
			->get()
			->result();
	}


        
        public function deleteIpdPatient($id = null)
	{
		$this->db->where('patient_id',$id)
			->delete($this->ipd_table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function createtest($data = [])
	{	 
		return $this->db->insert($this->tbl_pathologytest,$data);
	}

	public function readtest()
	{
		return $this->db->select("*")
			->from($this->tbl_pathologytest)
			->order_by('id','desc')
			->get()
			->result();
	} 

	public function deletetest($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->tbl_pathologytest);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}

	public function updatetest($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->tbl_pathologytest,$data); 
	} 

	public function read_by_id_test($id = null)
	{
		return $this->db->select("*")
			->from($this->tbl_pathologytest)
			->where('id',$id)
			->get()
			->row();
	} 
 

	public function bed_list()
	{
		$result = $this->db->select("*")
			->from($this->tbl_pathologytest)
			->get()
			->result();

		$list[''] = "Select Test";
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->testname; 
			}
			return $list;
		} else {
			return false;
		}
	}



      public function get_normal_value($medicine_cat_id) {
            $result = $this->db->select("*")
			->from("tbl_pathologytest")
			->where('id',$medicine_cat_id)
			->get()
			->result();
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[] = $value->normalvalue; 
			}
			return $list;
		} else {
			return false;
		}
        }



        
        public function discharge_report($data = null)
	{
		$start_date = date('Y-m-d',strtotime($data['start_date']));
		$end_date   = date('Y-m-d',strtotime($data['end_date'])); 
                $todayDate = date('Y-m-d');
                
		return $this->db->select("
				bm_bed_assign.assign_date,
				bm_bed_assign.bed_id,
				bm_bed_assign.discharge_date,
				COUNT(bm_bed_assign.id) as allocated, 
				bm_bed.type, 
				bm_bed.description, 
				bm_bed.limit, 
				bm_bed.charge,
				CONCAT_WS(' ', patient.firstname, patient.lastname) as patient_name,
                                patient.patient_id,
                                ipd_patient.ipd_id,
                                patient.mobile,
                                patient.id
			")
			->from('bm_bed_assign')
                        ->join('patient', 'patient.patient_id = bm_bed_assign.patient_id', 'inner')
                        ->join('ipd_patient', 'ipd_patient.patient_id = patient.id', 'inner')
			->join('user', 'user.user_id = bm_bed_assign.assign_by', 'left')
			->join('bm_bed', 'bm_bed.id = bm_bed_assign.bed_id', 'left')
                        ->where("bm_bed_assign.assign_date <=" , $todayDate)
			->where('bm_bed_assign.status', 1)
                        ->where("bm_bed_assign.discharge_date BETWEEN '$start_date' AND '$end_date'",null,false)
			->group_by(array('discharge_date','bed_id'))
			//->order_by('discharge_date','asc')
			->order_by("IF(discharge_date = DATE(NOW()), 0, 1), discharge_date DESC")
			->get()
			->result();
	}
        
        public function single_view($id = null)
	{		
		return $this->db->select("
				patient.*, 
				CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name,  
				CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name,  
				user.designation,
				user.address,
				user.specialist,
				department.name as department_name,
				patient.sex,
				patient.date_of_birth,
                                discharge_date
			")
			->from('patient') 
			->join('user', 'user.user_id = patient.doctor_id', 'left') 
			->join('bm_bed_assign', 'bm_bed_assign.patient_id = patient.patient_id', 'left') 
			->join('department', 'department.dprt_id = patient.department_id', 'left') 
			->where('patient.id', $id) 
                        ->group_by(array('discharge_date','bed_id'))
			->get()
			->row();
	}
        
        public function pd_with_bd($id = null)
	{
		return $this->db->select("patient.id, patient.firstname, patient.sex, patient.mobile, patient.create_date, bm_bed.type as current_bed,"
                        . "bm_bed_assign.discharge_date,patient.patient_id")
			->from($this->table)
                        ->join('bm_bed_assign', 'bm_bed_assign.patient_id = patient.patient_id', 'inner')
                        ->join('bm_bed', 'bm_bed.id = bm_bed_assign.bed_id', 'inner')
			->where('patient.id',$id)
			->get()
			->row();
	}
        
        public function prescriptionPatient()
	{
		return $this->db->select('*')
			->from('patient')
                        ->join('bm_bed_assign', 'bm_bed_assign.patient_id = patient.patient_id', 'inner')
			->where('patient.patient_id', $this->input->post('patient_id'))
			->where('patient.current_patient_status', 1)
			->get();
	}
        public function medicine_list()
	{
		$result = $this->db->select("*")
			->from('ha_medicine')
			->where('status',1)
			->get()
			->result();
 
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[] = $value->name; 
			}
			return $list;
		} else {
			return false;
		}
	}
        
        public function medicine_category()
	{
		$result = $this->db->select("*")
			->from("ha_category")
			->where('status',1)
			->get()
			->result();
 
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->name; 
			}
			return $list;
		} else {
			return false;
		}
	}
        
        public function get_medicine($medicine_cat_id) {
            $result = $this->db->select("*")
			->from("ha_medicine")
			->where('category_id',$medicine_cat_id)
			->where('status',1)
			->get()
			->result();
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[] = $value->name; 
			}
			return $list;
		} else {
			return false;
		}
        }
        
        public function get_prescription_list() {
            return $this->db->select("pr_prescription.*, CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name")
			->from('pr_prescription') 
			->join('user', 'user.user_id = pr_prescription.doctor_id', 'left')
                        ->where('pr_prescription.status',1)
			->order_by('id','desc')
			->get()
			->result();
        }
        
        public function single_prescription_view($id = null) {
            return $this->db->select("
				pr_prescription.*, 
				CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name,  
				CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name,  
				user.designation,
				user.address,
				user.specialist,
				department.name as department_name,
				patient.sex,
				patient.date_of_birth,
				pr_insurance.name AS insurance_name
			")
			->from('pr_prescription') 
			->join('patient', 'patient.patient_id = pr_prescription.patient_id', 'left') 
			->join('pr_insurance', 'pr_insurance.id = pr_prescription.insurance_id', 'left') 
			->join('user', 'user.user_id = pr_prescription.doctor_id', 'left') 
			->join('department', 'department.dprt_id = user.department_id', 'left') 
			->where('pr_prescription.id', $id) 
			->get()
			->row();
        }


        public function website()
	{
		return $this->db->select('title, description, email, phone, logo')
			->from('setting')
			->get()
			->row();
	}
        
        public function package_list()
	{
		$result = $this->db->select("*")
			->from("tbl_packege")
			->where('status',1)
			->get()
			->result();

		$list[''] = 'Select Package';
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->packege_name; 
			}
			return $list;
		} else {
			return false;
		}
	}


	public function read_by_id_profile($id = null)
	{
		return $this->db->select("patient.*, ot_additional_information.*")
			->from('patient')
			->join('ot_additional_information', 'ot_additional_information.patient_id = patient.id') 
			->where('patient.id',$id)
			->get()
			->row();
	} 



	 public function getpathologyPatient()
	{
		return $this->db->select("patient.*,
                        CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name ")
			->from($this->table)
                        ->join('user','user.user_id = patient.doctor_id','left')
			->order_by('patient.id','desc')
			->get()
			->result();
	}
  
}
