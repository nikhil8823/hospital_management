<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ambulance_model extends CI_Model {

	private $table = 'tbl_ambulance_assign';

	public function create_get_pass($data = [])
	{	 
		return $this->db->insert($this->table,$data);
	}
 
	public function read()
	{
		return $this->db->select("*")
			->from("tbl_ambulance_assign")
			->order_by('id','desc')
			->get()
			->result();
	} 
 
	public function read_by_id($id = null)
	{
		return $this->db->select("*")
			->from("tbl_ambulance_assign")
			->where('id',$id)
			->order_by('id','desc')
			->get()
			->row();
	} 
 
	public function update_gate_pass($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->table,$data); 
	} 
 
	public function delete($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 



	 public function discharge_report($data = null)
	{
		$start_date = date('Y-m-d',strtotime($data['start_date']));
		$end_date   = date('Y-m-d',strtotime($data['end_date'])); 
         
                
		return $this->db->select("*")
			->from('ha_birth')
            ->where("date BETWEEN '$start_date' AND '$end_date'")
			->get()
			->result();
	}
 
	
 }
