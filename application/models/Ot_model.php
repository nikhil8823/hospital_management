<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ot_model extends CI_Model {

	private $table = "patient";
 
        public function getOtPatient()
	{
		return $this->db->select("patient.id, patient.patient_id, patient.picture, patient.firstname, patient.lastname,"
                        . "patient.email, patient.phone, patient.mobile, patient.address, patient.sex, patient.blood_group, patient.date_of_birth, "
                        . "patient.create_date, patient.status")
			->from($this->table)
                        ->where('current_patient_status',2)
			->order_by('patient.id','desc')
			->get()
			->result();
	}
        
        public function addOtAdditionalData($data) {
            return $this->db->insert("ot_additional_information",$data);
        }
        
         public function discharge_report($data = null)
	{
		$start_date = date('Y-m-d',strtotime($data['start_date']));
		$end_date   = date('Y-m-d',strtotime($data['end_date'])); 
                $todayDate = date('Y-m-d');
                
		return $this->db->select("
				bm_bed_assign.assign_date,
				bm_bed_assign.bed_id,
				bm_bed_assign.discharge_date,
				COUNT(bm_bed_assign.id) as allocated, 
				bm_bed.type, 
				bm_bed.description, 
				bm_bed.limit, 
				bm_bed.charge,
				CONCAT_WS(' ', patient.firstname, patient.lastname) as patient_name,
                                patient.patient_id,
                                patient.mobile,
                                patient.id
			")
			->from('bm_bed_assign')
                        ->join('patient', 'patient.patient_id = bm_bed_assign.patient_id', 'inner')
			->join('user', 'user.user_id = bm_bed_assign.assign_by', 'left')
			->join('bm_bed', 'bm_bed.id = bm_bed_assign.bed_id', 'left')
                        ->where("bm_bed_assign.assign_date <=" , $todayDate)
                        ->where("patient.current_patient_status" , 2)
			->where('bm_bed_assign.status', 1)
                        ->where("bm_bed_assign.discharge_date BETWEEN '$start_date' AND '$end_date'",null,false)
			->group_by(array('discharge_date','bed_id'))
			//->order_by('discharge_date','asc')
			->order_by("IF(discharge_date = DATE(NOW()), 0, 1), discharge_date DESC")
			->get()
			->result();
	}
        
        public function single_view($id = null)
	{		
		return $this->db->select("
				patient.*, 
				CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name,  
				CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name,  
				user.designation,
				user.address,
				user.specialist,
				department.name as department_name,
				patient.sex,
				patient.date_of_birth,
                                discharge_date
			")
			->from('patient') 
			->join('user', 'user.user_id = patient.doctor_id', 'left') 
			->join('bm_bed_assign', 'bm_bed_assign.patient_id = patient.patient_id', 'left') 
			->join('department', 'department.dprt_id = patient.department_id', 'left') 
			->where('patient.id', $id) 
                        ->group_by(array('discharge_date','bed_id'))
			->get()
			->row();
	}
        
        public function prescriptionPatient()
	{
		return $this->db->select('*')
			->from('patient')
                        ->join('bm_bed_assign', 'bm_bed_assign.patient_id = patient.patient_id', 'inner')
			->where('patient.patient_id', $this->input->post('patient_id'))
			->where('patient.current_patient_status', 2)
			->get();
	}
        
        public function get_prescription_list() {
            return $this->db->select("pr_prescription.*, CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name")
			->from('pr_prescription') 
			->join('user', 'user.user_id = pr_prescription.doctor_id', 'left')
                        ->where('pr_prescription.status',2)
			->order_by('id','desc')
			->get()
			->result();
        }
        
        public function get_ot_checklist($patient_id = null) {
            return $this->db->select("*")
			->from("ot_checklist")
			->where('patient_id',$patient_id)
			->get()
			->row();
        }
        
        public function addChecklist($data) {
            return $this->db->insert("ot_checklist",$data);
        }
        
        public function updateChecklist($data){
            return $this->db->where('id',$data['id'])
			->update("ot_checklist",$data); 
        }



        public function get_before_after_list() {
            return $this->db->select("ot_checklist.*, patient.*")
			->from('ot_checklist') 
			->join('patient', 'patient.id = ot_checklist.patient_id', 'left')
			->order_by('ot_checklist.id','desc')
			->get()
			->result();
        }

        

         public function single_before_after_view($id = null)
	{		
		return $this->db->select("ot_checklist.*,
				patient.*, 
				CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name 				
			")
			->from('ot_checklist')  
			->join('patient', 'patient.id= ot_checklist.patient_id ', 'left') 
			->where('ot_checklist.patient_id', $id) 
			->get()
			->row();
	}
}
