<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Insurance_model extends CI_Model {

	private $table = 'pr_insurance';
	private $groupcharge_master = 'groupcharge_master';
    private $sponsers_details = 'sponsers_details';
    private $ins_limit_approval = 'ins_limit_approval';

  
	public function create($data = [])
	{	 
		return $this->db->insert($this->table,$data);
	}
 
	public function read()
	{
		return $this->db->select("*")
			->from($this->table)
			->order_by('id','desc')
			->get()
			->result();
	} 
 
	public function read_by_id($id = null)
	{
		return $this->db->select("*")
			->from($this->table)
			->where('id',$id)
			->get()
			->row();
	} 
 
	public function update($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->table,$data); 
	} 
 
	public function delete($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 

	public function insurance_list()
	{
		$result = $this->db->select("*")
			->from($this->table)
			->where('status',1)
			->get()
			->result();

		$list[''] = display('insurance_list');
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->name; 
			}
			return $list;
		} else {
			return false;
		}
	}
	

	/* ----Grop charge function---*/
	public function creategroup_chargemaster($data = [])
	{	 
		return $this->db->insert($this->groupcharge_master,$data);
	}

		public function readgroup_charge()
	{
		return $this->db->select("*")
			->from($this->groupcharge_master)
			->order_by('id','desc')
			->get()
			->result();
	} 

	
	public function read_by_id_groupcharge($id = null)
	{
		return $this->db->select("*")
			->from($this->groupcharge_master)
			->where('id',$id)
			->get()
			->row();
	} 

	
	public function update_groupcharge($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->groupcharge_master,$data); 
	} 

	public function delete_groupcharge($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->groupcharge_master);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 


	public function group_charge_dropdown_list()
	{
		$result = $this->db->select("*")
			->from($this->groupcharge_master)
			->where('status',1)
			->get()
			->result();

		$list[''] = "Disease Name";
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->disease_name; 
			}
			return $list;
		} else {
			return false;
		}
	}
	

	/* ----Grop charge function---*/

	/* ----sponser details function---*/

	public function createsponser_details($data = [])
	{	 
		return $this->db->insert($this->sponsers_details,$data);
	}


		public function update_sponsers_details($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->sponsers_details,$data); 
	} 


	public function read_by_id_sponserdetails($id = null)
	{
		return $this->db->select("*")
			->from($this->sponsers_details)
			->where('id',$id)
			->get()
			->row();
	} 


	public function delete_sponsers_details($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->sponsers_details);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 


		public function read_sponsers_details()
	{
		return $this->db->select("*")
			->from($this->sponsers_details)
			->order_by('id','desc')
			->get()
			->result();
	} 


	/* ----sponser details  function---*/


   /* ----Organisation limit approval  function---*/

	 public function create_limit_approval($data = [])
	{	 
		return $this->db->insert($this->ins_limit_approval,$data);
	}


    	public function update_limit_approval($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->ins_limit_approval,$data); 
	} 



	public function read_limit_approval()
	{
		return $this->db->select("*")
			->from($this->ins_limit_approval)
			->order_by('id','desc')
			->get()
			->result();
	} 


	public function read_by_id_limitapproval($id = null)
	{
		return $this->db->select("*")
			->from($this->ins_limit_approval)
			->where('id',$id)
			->get()
			->row();
	} 


	
	public function delete_limit_approval($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->ins_limit_approval);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 


   /* ----end Organisation limit approval  function---*/


    /* ----Organisation limit approval  function---*/

	 public function create_tpa_patient($data = [])
	{	 
		return $this->db->insert($this->ins_limit_approval,$data);
	}


    	public function update_tpa_patient($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->ins_limit_approval,$data); 
	} 



	public function read_tpa_patient()
	{
		return $this->db->select("*")
			->from($this->ins_limit_approval)
			->order_by('id','desc')
			->get()
			->result();
	} 


	public function read_by_id_tpa_patient($id = null)
	{
		return $this->db->select("*")
			->from($this->ins_limit_approval)
			->where('id',$id)
			->get()
			->row();
	} 


	
	public function delete_tpa_patient($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->ins_limit_approval);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 
 


 }
