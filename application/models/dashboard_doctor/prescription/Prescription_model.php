<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Prescription_model extends CI_Model {

	protected $table = 'pr_prescription';
	protected $category = 'ha_category';
	protected $medicine = 'ha_medicine';

	public function create($data = [])
	{	 
		return $this->db->insert($this->table, $data);
	}
 
	public function read()
	{
		return $this->db->select("pr_prescription.*, CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name")
			->from('pr_prescription') 
			->join('user', 'user.user_id = pr_prescription.doctor_id', 'left')
			->where('pr_prescription.doctor_id',$this->session->userdata('user_id')) 
			->order_by('id','desc')
			->get()
			->result();
	} 
 
	public function single_view($id = null)
	{
		return $this->db->select("
				pr_prescription.*, 
				CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name,  
				CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name,  
				user.designation,
				user.address,
				user.specialist,
				department.name as department_name,
				patient.sex,
				patient.date_of_birth,
				pr_insurance.name AS insurance_name,
				pr_case_study.food_allergies,
				pr_case_study.casestudy,
				pr_case_study.family_medical_history
			")
			->from('pr_prescription') 
			->join('patient', 'patient.patient_id = pr_prescription.patient_id', 'left') 
			->join('pr_insurance', 'pr_insurance.id = pr_prescription.insurance_id', 'left') 
			->join('user', 'user.user_id = pr_prescription.doctor_id', 'left') 
			->join('department', 'department.dprt_id = user.department_id', 'left') 
			->join('pr_case_study', 'pr_case_study.patient_id = pr_prescription.patient_id', 'left') 
			->where('pr_prescription.id', $id) 
			->where('pr_prescription.doctor_id', $this->session->userdata('user_id')) 
			->get()
			->row();
	} 
 
 
	public function update($data = [])
	{
		return $this->db->where('id',$data['id'])
			->where('doctor_id',$this->session->userdata('user_id')) 
			->update($this->table,$data); 
	} 
 
	public function delete($id = null)
	{  
		$this->db->where('id',$id)
			->where('doctor_id',$this->session->userdata('user_id')) 
			->delete($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 
	
	
	public function patient()
	{
		return $this->db->select('*')
			->from('patient')
			->where('patient_id', $this->input->post('patient_id'))
			->get();
	}

	public function website()
	{
		return $this->db->select('title, description, email, phone')
			->from('setting')
			->get()
			->row();
	}



	  public function medicine_category()
	{
		$result = $this->db->select("*")
			->from($this->category)
			->where('status',1)
			->get()
			->result();
 
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->name; 
			}
			return $list;
		} else {
			return false;
		}
	}
        
        public function get_medicine($medicine_cat_id) {
            $result = $this->db->select("*")
			->from('ha_medicine')
			->where('category_id',$medicine_cat_id)
			->where('status',1)
			->get()
			->result();
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[] = $value->name; 
			}
			return $list;
		} else {
			return false;
		}
      }

 

 }
