<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Prescription_model extends CI_Model {

	protected $table = 'pr_prescription';
	protected $tbl_nursing_notes = 'tbl_nursing_notes';

	public function create($data = [])
	{	 
		return $this->db->insert($this->table, $data);
	}
 
	public function read()
	{
		return $this->db->select("pr_prescription.*, CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name")
			->from('pr_prescription') 
			->join('user', 'user.user_id = pr_prescription.doctor_id', 'left')
			->order_by('id','desc')
			->get()
			->result();
	} 
 
	public function single_view($id = null)
	{		
		return $this->db->select("
				pr_prescription.*, 
				CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name,  
				CONCAT_WS(' ', user.firstname, user.lastname) AS doctor_name,  
				user.designation,
				user.address,
				user.specialist,
				department.name as department_name,
				patient.sex,
				patient.date_of_birth,
				pr_insurance.name AS insurance_name,
				pr_case_study.food_allergies,
				pr_case_study.casestudy,
				pr_case_study.family_medical_history
			")
			->from('pr_prescription') 
			->join('patient', 'patient.patient_id = pr_prescription.patient_id', 'left') 
			->join('pr_insurance', 'pr_insurance.id = pr_prescription.insurance_id', 'left') 
			->join('user', 'user.user_id = pr_prescription.doctor_id', 'left') 
			->join('department', 'department.dprt_id = user.department_id', 'left')
			->join('pr_case_study', 'pr_case_study.patient_id = pr_prescription.patient_id', 'left') 
			->where('pr_prescription.id', $id) 
			->get()
			->row();
	} 


	public function website()
	{
		return $this->db->select('title, description, email, phone, logo')
			->from('setting')
			->get()
			->row();
	}


	public function create_nursing_notes($data = [])
	{	 
		return $this->db->insert($this->tbl_nursing_notes, $data);
	}
 
	public function read_nursing_notes()
	{
		return $this->db->select("tbl_nursing_notes.*, CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name")
			->from('tbl_nursing_notes')
			->join('patient', 'patient.patient_id = tbl_nursing_notes.patient_id', 'left') 
			->where('tbl_nursing_notes.status',1) 
			->order_by('tbl_nursing_notes.id','desc')
			->get()
			->result();
	} 


	 public function single_nursing_notes_view($id = null) {
            return $this->db->select("
				tbl_nursing_notes.*, 
				CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name,  
				patient.address,
				patient.sex,
				patient.date_of_birth
				
			")
			->from('tbl_nursing_notes') 
			->join('patient', 'patient.patient_id = tbl_nursing_notes.patient_id', 'left') 
			->where('tbl_nursing_notes.id', $id) 
			->get()
			->row();
        }


        public function read_by_id_nursing_notes($id = null)
	{
		return $this->db->select("tbl_nursing_notes.*, CONCAT_WS(' ', patient.firstname, patient.lastname) AS patient_name,
		        patient.address,
				patient.sex,
				patient.date_of_birth
				  ")
			->from('tbl_nursing_notes')
			->join('patient', 'patient.patient_id = tbl_nursing_notes.patient_id', 'left') 
			->where('tbl_nursing_notes.id',$id)
			->get()
			->row();
	} 


	public function update_nursing_notes($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update('tbl_nursing_notes',$data); 
	}
        
        public function update_prescription($data = []) {
            return $this->db->where('id',$data['id'])
                    ->update('pr_prescription',$data); 
        }
}
