<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Billing_model extends CI_Model {

	private $table = 'tbl_packege';
	private $bill = 'tbl_final_bill';

	public function create_packege($data = [])
	{	 
		return $this->db->insert($this->table,$data);
	}
 
	public function read()
	{
		return $this->db->select("*")
			->from("tbl_packege")
			->order_by('id','desc')
			->get()
			->result();
	} 
 
	public function read_by_id($id = null)
	{
		return $this->db->select("*")
			->from("tbl_packege")
			->where('id',$id)
			->order_by('id','desc')
			->get()
			->row();
	} 
 
	public function update_packege($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->table,$data); 
	} 
 
	public function delete($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 



	 public function discharge_report($data = null)
	{
		$start_date = date('Y-m-d',strtotime($data['start_date']));
		$end_date   = date('Y-m-d',strtotime($data['end_date'])); 
         
                
		return $this->db->select("*")
			->from('tbl_packege')
            ->where("date BETWEEN '$start_date' AND '$end_date'")
			->get()
			->result();
	}



	public function create_bill($data = [])
	{	 
		return $this->db->insert($this->bill,$data);
	}
 
 
	public function read_bill()
	{
		return $this->db->select("*")
			->from("tbl_final_bill")
			->order_by('id','desc')
			->get()
			->result();
	} 
 
	public function read_by_id_bill($id = null)
	{
		return $this->db->select("*")
			->from("tbl_final_bill")
			->where('id',$id)
			->order_by('id','desc')
			->get()
			->row();
	} 
 
	public function update_bill($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->bill,$data); 
	} 
 
	public function delete_bill($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->bill);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 
	


	public function read_by_id_billing($id = null)
	{
		return $this->db->select("*")
			->from("patient")
			->where('id',$id)
			->order_by('id','desc')
			->get()
			->row();
	} 


	public function view_bill($bill_id = null) {
            return $this->db->select("tbl_final_bill.*,CONCAT_WS(' ', patient.firstname, patient.lastname) as patient_name,patient.date_of_birth,"
                    . "CONCAT_WS(' ', user.firstname, user.lastname) as doctor_name,user.specialist,bm_bed.type as bed_name,patient.create_date as admission_date,"
                   . "bm_bed_assign.discharge_date,tbl_packege.packege_name,tbl_packege.medicine as medicine_pack,patient.payment_mode,patient.id as registration_number")
			->from($this->bill)
                        ->join('patient','patient.patient_id = tbl_final_bill.patient_id')
                        ->join('user','user.user_id = patient.doctor_id','left')
                        ->join('bm_bed_assign','tbl_final_bill.patient_id = bm_bed_assign.patient_id','left')
                        ->join('bm_bed','bm_bed.id = bm_bed_assign.bed_id','left')
                        ->join('tbl_packege','tbl_packege.id = tbl_final_bill.packege_id','left')
                        ->where('tbl_final_bill.id',$bill_id)
			->get()
			->row();

       }


       public function read_packege()
	{
		$result = $this->db->select("*")
			->from($this->table)
			->where('status',1)
			->get()
			->result();

		$list[''] = "Select Packege";
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->packege_name; 
			}
			return $list;
		} else {
			return false;
		}
	}

	

	public function read_by_id_packege_name($id = null)
	{
		return $this->db->select("*")
			->from("tbl_packege")
			->where('id',$id)
			->order_by('id','desc')
			->get()
			->row();
	} 

 }
