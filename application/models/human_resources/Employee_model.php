<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

	private $table = "user";
 
	public function create($data = [])
	{	 
		return $this->db->insert($this->table,$data);
	}
 
	public function read($user_type)
	{
		return $this->db->select("*, CONCAT_WS(' ', firstname, lastname) AS fullname ")
			->from($this->table) 
			->where('user_role', $user_type)
			->order_by('user_id','desc')
			->get()
			->result();
	} 
 
	public function read_by_id($user_id = null)
	{
		return $this->db->select("*, CONCAT_WS(' ', firstname, lastname) AS fullname ")
			->from($this->table)
			->where('user_id', $user_id) 
			->get()
			->row();
	} 
 
	public function update($data = [])
	{
		return $this->db->where('user_id',$data['user_id']) 
			->update($this->table,$data); 
	} 
 
	public function delete($user_id = null, $user_role = null)
	{
		$this->db->where('user_id',$user_id) 			
			->where('user_role', $user_role)
			->where_not_in('user_role', 1)
			->where_not_in('user_role', 2)
			->delete($this->table);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 

        public function addEmpDetails($data = [])
	{	 
		return $this->db->insert("user_details",$data);
	}
        
        public function updateEmpDetails($data = [])
	{
		return $this->db->where('user_id',$data['user_id']) 
			->update('user_details',$data); 
	} 

        public function empDetail($user_id = null)
	{
		return $this->db->select("*")
			->from("user_details")
			->where('user_id', $user_id) 
			->get()
			->row();
	}
        
        public function addEmpWorkDetails($data = [])
	{	 
		return $this->db->insert("user_work_details",$data);
	}
        
        public function updateEmpWorkDetails($data = [])
	{
		return $this->db->where('user_id',$data['user_id']) 
			->update('user_work_details',$data); 
	}
        
        public function empWorkDetail($user_id = null)
	{
		return $this->db->select("*")
			->from("user_work_details")
			->where('user_id', $user_id) 
			->get()
			->row();
	}
        
        public function getEmpList($user_type)
	{
		return $this->db->select("*")
			->from("user_details")
                        ->join('user','user.user_id = user_details.user_id','inner')
			->where_in('user.user_role', $user_type)
			->order_by('user_details.user_id','desc')
			->get()
			->result();
	}
        
        public function getEmpListOnly($user_type)
	{
		return $this->db->select("user.user_id as value,user_details.fullname as label")
			->from("user_details")
                        ->join('user','user.user_id = user_details.user_id','inner')
			->where_in('user.user_role', $user_type)
			->order_by('user_details.user_id','desc')
			->get()
			->result_array();
	}
        
        public function add_salary($data = [])
        {
            return $this->db->insert("emp_salary",$data);
        }

        public function salary_slip_list($data = null)
        {
                $start_date = date('Y-m-d',strtotime($data['start_date']));
		$end_date   = date('Y-m-d',strtotime($data['end_date'])); 
                $todayDate = date('Y-m-d');
                
		return $this->db->select("
				user_details.fullname,
                                user_details.emp_code,
                                user_details.aadhar_number,
                                emp_salary.id
			")
			->from('emp_salary')
                        ->join('user_details', 'user_details.user_id = emp_salary.emp_id', 'inner')
                        ->where("emp_salary.month BETWEEN '$start_date' AND '$end_date'",null,false)
			->get()
			->result();
        }
        
        public function single_view_salary($id = null)
        {
            return $this->db->select("
                                user_details.emp_code,
				user_details.fullname,
				user_work_details.bankname,
				user_work_details.account_number,
				department.name as department_name,
				user.designation,
				user.date_of_birth,
				user_work_details.pan_card_number,
				user_work_details.pf_account_number,
				user_work_details.pf_account_number,
				user_work_details.pf_account_number,
				user_work_details.pf_account_number,
                                emp_salary.*,
                                user.user_id as patient_id,
                                user_work_details.date_of_joining,
                                user.user_role
                                
			")
			->from('emp_salary') 
			->join('user_details', 'user_details.user_id = emp_salary.emp_id', 'inner')
			->join('user_work_details', 'user_work_details.user_id = emp_salary.emp_id', 'inner')
			->join('user', 'user.user_id = emp_salary.emp_id', 'inner')
                        ->join('department', 'department.dprt_id = user.department_id', 'left')  
			->where('emp_salary.id', $id) 
			->get()
			->row();
        }
        
        public function get_emp_salary($id = null)
        {
            return $this->db->select("*")
			->from("emp_salary")
			->where('id', $id) 
			->get()
			->row();
        }
        
        public function update_salary($data = [])
        {
            return $this->db->where('id',$data['id']) 
			->update('emp_salary',$data); 
        }
        
        public function add_attendance($data = [])
        {
            return $this->db->insert("emp_attendance",$data);
        }
        
        public function emp_attendance_list($data = null)
        {
                $start_date = date('Y-m-d',strtotime($data['start_date']));
		$end_date   = date('Y-m-d',strtotime($data['end_date'])); 
                $todayDate = date('Y-m-d');
                
		return $this->db->select("
				user_details.fullname,
                                user_details.emp_code,
                                emp_attendance.id,
                                emp_attendance.month,
                                emp_attendance.emp_id
			")
			->from('emp_attendance')
                        ->join('user_details', 'user_details.user_id = emp_attendance.emp_id', 'inner')
                        ->where("emp_attendance.month BETWEEN '$start_date' AND '$end_date'",null,false)
                        ->group_by(array('emp_attendance.emp_id','YEAR(emp_attendance.month)', 'MONTH(emp_attendance.month)'))
			->get()
			->result();
        }


         public function single_view_attendance($id = null)
        {
        	$attendance_data = $this->db->select("*")
			->from('emp_attendance') 
			->where('id', $id)
			->get()
			->row();
                $last_date = date("Y-m-t", strtotime($attendance_data->month));
               
                return  $this->db->select("
				user_details.fullname,
                                user_details.emp_code,
                                emp_attendance.*
			")
			->from('emp_attendance')
                        ->join('user_details', 'user_details.user_id = emp_attendance.emp_id', 'inner')
                        ->where("emp_attendance.month BETWEEN '$attendance_data->month' AND '$last_date'",null,false)
                        ->where("emp_attendance.emp_id",$attendance_data->emp_id)
			->get()
			->result();

        }


         public function single_view_salary_slip($emp_id = null)
        {
            return $this->db->select("
                                user_details.emp_code,
				user_details.fullname,
				user_work_details.bankname,
				user_work_details.account_number,
				department.name as department_name,
				user.designation,
				user.date_of_birth,
				user_work_details.pan_card_number,
				user_work_details.pf_account_number,
				user_work_details.pf_account_number,
				user_work_details.pf_account_number,
				user_work_details.pf_account_number,
                                emp_salary.*,
                                user.user_id as patient_id,
                                user_work_details.date_of_joining,
                                user.user_role
                                
			")
			->from('user_details') 
			->join('emp_salary', 'emp_salary.emp_id = user_details.user_id', 'inner')
			->join('user_work_details', 'user_work_details.user_id = user_details.user_id', 'inner')
			->join('user', 'user.user_id = user_details.user_id', 'inner')
                        ->join('department', 'department.dprt_id = user.department_id', 'left')  
			->where('user_details.user_id', $emp_id) 
			->get()
			->row();
        }
        
        public function get_emp_salary_by_emp_id($id = null)
        {
            return $this->db->select("id")
			->from("emp_salary")
			->where('emp_id', $id) 
			->get()
			->row();
        }
  
}
