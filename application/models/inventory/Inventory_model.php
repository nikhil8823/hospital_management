<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_model extends CI_Model {

	private $tbl_supplier = ' tbl_supplier';
	private $tbl_supplier_products = 'tbl_supplier_products';
    private $tbl_purchase_order = 'tbl_purchase_order';
    private $ins_limit_approval = 'ins_limit_approval';
    private $tbl_stock_return = 'tbl_stock_return';
    private $tbl_stock_entry = 'tbl_stock_entry';

  

	 /* ----supplier Details  function---*/

	 public function create_supplier($data = [])
	{	 
		return $this->db->insert($this->tbl_supplier,$data);
	}


    	public function update_supplier($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->tbl_supplier,$data); 
	} 



	public function read_supplier()
	{
		return $this->db->select("*")
			->from($this->tbl_supplier)
			->order_by('id','desc')
			->get()
			->result();
	} 


	public function read_by_id_supplier($id = null)
	{
		return $this->db->select("*")
			->from($this->tbl_supplier)
			->where('id',$id)
			->get()
			->row();
	} 


	
	public function delete_supplier($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->tbl_supplier);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 


	public function supplier_list()
	{
		$result = $this->db->select("*")
			->from($this->tbl_supplier)
			->where('current_supplier_status',1)
			->get()
			->result();

		$list[''] = "Select Supplier";
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->firm_name; 
			}
			return $list;
		} else {
			return false;
		}
	}
	
 
/* ----End Supplier function---*/
	

	/* ----Supplier Product function---*/
	
	public function create_supplier_product($data = [])
	{	 
		return $this->db->insert($this->tbl_supplier_products,$data);
	}

		public function read_supplier_product()
	{
		return $this->db->select("tbl_supplier_products.*, tbl_supplier.firm_name")
			->from($this->tbl_supplier_products)
			->join('tbl_supplier','tbl_supplier.id = tbl_supplier_products.supplier_name','left')
			->order_by('tbl_supplier_products.id','desc')
			->get()
			->result();
	} 

	
	public function read_by_id_supplier_product($id = null)
	{
		return $this->db->select("*")
			->from($this->tbl_supplier_products)
			->where('id',$id)
			->get()
			->row();
	} 

	
	public function update_supplier_product($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->tbl_supplier_products,$data); 
	} 

	public function delete_supplier_product($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->tbl_supplier_products);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 


	public function group_charge_dropdown_list()
	{
		$result = $this->db->select("*")
			->from($this->tbl_supplier_products)
			->where('status',1)
			->get()
			->result();

		$list[''] = "Disease Name";
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->disease_name; 
			}
			return $list;
		} else {
			return false;
		}
	}
	

	/* ----End Supplier Product function---*/

	/* ----purchase order function---*/

	public function create_purchase_order($data = [])
	{	 
		return $this->db->insert($this->tbl_purchase_order,$data);
	}


		public function update_purchase_order($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->tbl_purchase_order,$data); 
	} 


	public function read_by_id_purchasse_order($id = null)
	{
		return $this->db->select("*")
			->from($this->tbl_purchase_order)
			->where('id',$id)
			->get()
			->row();
	} 


	public function delete_purchase_order($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->tbl_purchase_order);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 


		public function read_purchase_order()
	{
		return $this->db->select("*")
			->from($this->tbl_purchase_order)
			->order_by('id','desc')
			->get()
			->result();
	} 


	public function read_by_id_supplier_product_purchase_order($id = null)
	{

     return  $result = $this->db->select("*")
			->from($this->tbl_supplier_products)
			->where('supplier_name',$id)
			->get()
			->result();

		/*$list[''] = "Select Supplier";
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->product_name; 
			}
			return $list;
		} else {
			return false;
		}*/

	} 


	public function read_by_id_product_details($id = null)
	{
		return $this->db->select("*")
			->from($this->tbl_supplier_products)
			->where('id',$id)
			->get()
			->row();
	}


	public function view_po_bill($id = null) {
            return $this->db->select("tbl_purchase_order.*, tbl_supplier.firm_name")
			->from($this->tbl_purchase_order)
			->join('tbl_supplier','tbl_supplier.id = tbl_purchase_order.supplier_name ','left')
            ->where('tbl_purchase_order.id',$id)
			->get()
			->row();

       }


	/* ----end purchase order  function---*/


	/* ----stock return function---*/

	public function create_stock_return($data = [])
	{	 
		return $this->db->insert($this->tbl_stock_return,$data);
	}


		public function update_stock_return($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->tbl_stock_return,$data); 
	} 


	public function read_by_id_stock_return($id = null)
	{
		return $this->db->select("*")
			->from($this->tbl_stock_return)
			->where('id',$id)
			->get()
			->row();
	} 


	public function delete_stock_return($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->tbl_stock_return);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 


		public function read_stock_return()
	{
		return $this->db->select("*")
			->from($this->tbl_stock_return)
			->order_by('id','desc')
			->get()
			->result();
	} 


	public function read_by_id_supplier_product_stock_return($id = null)
	{

     return  $result = $this->db->select("*")
			->from($this->tbl_supplier_products)
			->where('supplier_name',$id)
			->get()
			->result();

		/*$list[''] = "Select Supplier";
		if (!empty($result)) {
			foreach ($result as $value) {
				$list[$value->id] = $value->product_name; 
			}
			return $list;
		} else {
			return false;
		}*/

	} 


	public function read_by_id_product_details_stock($id = null)
	{
		return $this->db->select("*")
			->from($this->tbl_supplier_products)
			->where('id',$id)
			->get()
			->row();
	}


	public function view_po_return_bill($id = null) {
            return $this->db->select("tbl_stock_return.*, tbl_supplier.firm_name")
			->from($this->tbl_stock_return)
			->join('tbl_supplier','tbl_supplier.id = tbl_stock_return.supplier_name ','left')
            ->where('tbl_stock_return.id',$id)
			->get()
			->row();

       }




	/* ----end stock return  function---*/


	/* ----stock entry function---*/

	public function create_stock_entry($data = [])
	{	 
		return $this->db->insert($this->tbl_stock_entry,$data);
	}


		public function update_stock_entry($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->tbl_stock_entry,$data); 
	} 

		public function read_stock_entry()
	{
		return $this->db->select("*")
			->from($this->tbl_stock_entry)
			->order_by('id','desc')
			->get()
			->result();
	} 


	public function view_stock_entry_bill($id = null) {
            return $this->db->select("tbl_stock_entry.*, tbl_supplier.firm_name")
			->from($this->tbl_stock_return)
			->join('tbl_supplier','tbl_supplier.id = tbl_stock_entry.supplier_name ','left')
            ->where('tbl_stock_entry.id',$id)
			->get()
			->row();

       }

/* ----end stock entry function---*/


/* ---- stock report -----*/


public function read_stock_report()
	{
		return $this->db->select("tbl_stock_entry.*, tbl_supplier.firm_name")
			->from($this->tbl_stock_entry)
			->join('tbl_supplier','tbl_supplier.id = tbl_stock_entry.supplier_name','left')
			->get()
			->result();
	} 


/*-----stock report ------*/




   /* ----Organisation limit approval  function---*/

	 public function create_limit_approval($data = [])
	{	 
		return $this->db->insert($this->ins_limit_approval,$data);
	}


    	public function update_limit_approval($data = [])
	{
		return $this->db->where('id',$data['id'])
			->update($this->ins_limit_approval,$data); 
	} 



	public function read_limit_approval()
	{
		return $this->db->select("*")
			->from($this->ins_limit_approval)
			->order_by('id','desc')
			->get()
			->result();
	} 


	public function read_by_id_limitapproval($id = null)
	{
		return $this->db->select("*")
			->from($this->ins_limit_approval)
			->where('id',$id)
			->get()
			->row();
	} 


	
	public function delete_limit_approval($id = null)
	{
		$this->db->where('id',$id)
			->delete($this->ins_limit_approval);

		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	} 


	


   /* ----end Organisation limit approval  function---*/


   


 }
