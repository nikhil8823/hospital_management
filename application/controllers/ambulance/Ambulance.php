<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ambulance extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model(array(
            'ambulance/ambulance_model',
            'doctor_model'
        ));
        
        if ($this->session->userdata('isLogIn') == false 
            || $this->session->userdata('user_role') != 1
        ) 
        redirect('login'); 
    }
 
    public function index()
    {
        $data['title'] = display('birth_report');
        #-------------------------------#
        $data['births'] = $this->birth_model->read();
        $data['content'] = $this->load->view('hospital_activities/birth_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    } 

    public function create_get_pass()
    { 
        /*----------FORM VALIDATION RULES----------*/
        $this->form_validation->set_rules('patient_id', display('patient_id') ,'required|max_length[20]');
      

        /*-------------STORE DATA------------*/
        $date = $this->input->post('date');

        $data['ambulance'] = (object)$postData = array( 
            'id'          => $this->input->post('id'),
            'uid'  => $this->input->post('uid'),
            'patient_id'       => $this->input->post('patient_id'),
            'firstname'       => $this->input->post('firstname'),
            'ambulance_name' => $this->input->post('ambulance_name',false),
            'driver_name'   => $this->input->post('driver_name'),
            'driver_phoneno'  => $this->input->post('driver_phoneno'),
            'driver_id'       => $this->input->post('driver_id'),
            'driver_address'       => $this->input->post('driver_address'),
            'ambulance_dt'   => $this->input->post('ambulance_dt'),
            'ambulance_time'  => $this->input->post('ambulance_time'),
            'status'      => $this->input->post('status'),
            'meter_reading_from'       => $this->input->post('meter_reading_from'),
            'meter_reading_to' => $this->input->post('meter_reading_to'),
            'total_amount'   => $this->input->post('total_amount'),
            'payment_amount'  => $this->input->post('payment_amount'),
            'payment_mode'       => $this->input->post('payment_mode'),
            'create_date'        => date('d-m-Y', strtotime((!empty($date) ? $date : date('d-m-Y')))),
            'current_ambulance_status'      => 1
        );  

        /*-----------CHECK ID -----------*/
      /*  if (empty($id)) {*/
            /*-----------CREATE A NEW RECORD-----------*/
            if ($this->form_validation->run() === true) { 

              if (empty($postData['id'])) {

                if ($this->ambulance_model->create_get_pass($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('ambulance/ambulance/create_get_pass');
            } else{

                 if ($this->ambulance_model->update_gate_pass($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('update_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('ambulance/ambulance/edit/'.$postData['id']);
            }


        } else {
            
                $data['content'] = $this->load->view('ambulance/get_pass_form',$data,true);
                $this->load->view('layout/main_wrapper',$data);
          
        } 
        
    }

 
    public function get_pass_list()
    {
        $data['title'] = "Get Pass List";
        #-------------------------------#
        $data['ambulance'] = $this->ambulance_model->read();
        $data['content'] = $this->load->view('ambulance/get_pass_list', $data, true);
        $this->load->view('layout/main_wrapper',$data);
    } 


    public function edit($id = null) 
    {
        $data['title'] = "Get Pass Update";
        #-------------------------------#
        $data['ambulance'] = $this->ambulance_model->read_by_id($id);
        $data['content'] = $this->load->view('ambulance/get_pass_form',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }


    public function delete($id = null) 
    {
        if ($this->birth_model->delete($id)) {
            #set success message
            $this->session->set_flashdata('message', display('delete_successfully'));
        } else {
            #set exception message
            $this->session->set_flashdata('exception', display('please_try_again'));
        }
        redirect('ambulance/get_pass_list');
    }


    public function dischargeReport() {
        $data['title'] = display('birth_report');
        #-------------------------------#

        $data['date'] = (object)$getData = [
                'start_date' => (($this->input->get('start_date') != null) ? $this->input->get('start_date'):date('Y-m-d')),
                'end_date'  => (($this->input->get('end_date') != null) ? $this->input->get('end_date'):date('Y-m-d')) 
        ]; 
       
        #-------------------------------#
        $data['result'] = $this->birth_model->discharge_report($getData);
        $data['content'] = $this->load->view('hospital_activities/birth_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }
  
}
