<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array(
			'human_resources/employee_model',
                        'department_model',
                        'prescription/prescription_model'
		));
		
		if ($this->session->userdata('isLogIn') == false 
			|| $this->session->userdata('user_role') != 1
		) 
		redirect('login');  
	}
 
	public function index($user_role = 'Representative')
	{ 	 
		$data['title'] = display($user_role."_list");
		$role_id     = $this->user_roles($user_role);
		#-------------------------------#
		$data['employees'] = $this->employee_model->read($role_id);
                $data['userRoles'] = $this->user_roles();
		$data['content'] = $this->load->view('human_resources/view', $data, true);
		$this->load->view('layout/main_wrapper',$data);
	} 


    public function email_check($email, $user_id)
    { 
    	$emailExists = $this->db->select('email')
    		->where('email',$email) 
    		->where_not_in('user_id',$user_id) 
    		->get('user')
    		->num_rows();

        if ($emailExists > 0) {
            $this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
            return false;
        } else {
            return true;
        }
    }


	public function form($user_id = null)
	{  
		$this->form_validation->set_rules('user_details[fullname]', "Full Name",'required|max_length[150]');
		$this->form_validation->set_rules('user_details[fathername]',"Father Name",'required|max_length[150]');

		if (!empty($user_id)) {
			$this->form_validation->set_rules('email',display('email'), "required|max_length[50]|valid_email|callback_email_check[$user_id]");
		} else {
			$this->form_validation->set_rules('email',display('email'),'required|max_length[50]|valid_email|callback_email_check');
                        $this->form_validation->set_rules('password',display('password'),'required|max_length[32]|md5');
		}
		$this->form_validation->set_rules('mobile',display('mobile'),'required|max_length[20]');
		$this->form_validation->set_rules('sex',display('sex'),'required|max_length[10]');
		//$this->form_validation->set_rules('address',display('address'),'required|max_length[255]');
		//$this->form_validation->set_rules('status',display('status'),'required');
		#-------------------------------#
		//picture upload
		$picture = $this->fileupload->do_upload(
			'assets/images/human_resources/',
			'picture'
		);
		// if picture is uploaded then resize the picture
		if ($picture !== false && $picture != null) {
			$this->fileupload->do_resize(
				$picture, 
				150,
				150
			);
		}
		//if picture is not uploaded
		if ($picture === false) {
			$this->session->set_flashdata('exception', display('invalid_picture'));
		}
		#-------------------------------#
		//when create a user
		$data['employee'] = (object)$postData = array(
			'user_id'      => $this->input->post('user_id'),
			'email' 	   => $this->input->post('email'),
			'password' 	   => md5($this->input->post('password')),
			'mobile'       => $this->input->post('mobile'),
			'sex' 		   => (!empty($this->input->post('sex'))) ? $this->input->post('sex') : 'Male',
			'picture'      => (!empty($picture)?$picture:$this->input->post('old_picture')),
			//'user_role'    => $this->input->post('user_role'),
			'create_date'  => date('Y-m-d'),
			'created_by'   => $this->session->userdata('user_id'),
			'date_of_birth'       => date('Y-m-d', strtotime($this->input->post('date_of_birth'))),
			'blood_group'       => $this->input->post('blood_group'),
                        
		); 

        /*-----------CHECK ID -----------*/
        if (empty($user_id)) {
            
            /*-----------CREATE A NEW RECORD-----------*/
            if ($this->form_validation->run() === true) {
//                echo "<pre>";
//                print_r($postData);
//                print_r($this->input->post('user_details'));die;
                
                if ($this->employee_model->create($postData)) {
                    $user_details = $this->input->post('user_details');
                    $user_details['user_id'] = $this->db->insert_id();
                    $user_details['emp_code'] = "EMP".$this->randStrGen(2,7);
                    $this->employee_model->addEmpDetails($user_details);
                    #set success message
                    $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('human_resources/employee/form');
            } else {
            	//view section
                $data['title'] = display('add_employee');
                $data['userRoles'] = $this->user_roles();
                $data['employee_details'] = (object)array_fill_keys($this->db->list_fields('user_details'), '');
                $data['employee_details']->verification_done = '0';
                $data['employee_details']->marital_status = '0'; 
                $data['employee_details']->wife_job_status = '1';
                $data['employee_details']->fitness_submitted = '0';
                $data['content'] = $this->load->view('human_resources/form',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            } 

        } else {

            /*-----------UPDATE A RECORD-----------*/
            if ($this->form_validation->run() === true) {
               
                if ($this->employee_model->update($postData)) {
                    $user_details = $this->input->post('user_details');
                    $user_details['user_id'] = $postData['user_id'];
                    $this->employee_model->updateEmpDetails($user_details);
                    #set success message
                    $this->session->set_flashdata('message', display('update_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('human_resources/employee/form/'.$postData['user_id']);
            } else {
            	//view section
                $data['title'] = display('employee_edit');
                $data['employee'] = $this->employee_model->read_by_id($user_id);
                $data['employee_details'] = $this->employee_model->empDetail($user_id);
                //echo "<pre>";print_r($data['employee_details']);die;
                $data['userRoles'] = $this->user_roles(); 
                $data['content'] = $this->load->view('human_resources/form',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            } 
        } 
        /*---------------------------------*/
	}
 
	public function profile($user_id = null)
	{  
		$data['title'] =  display('employee_information');
		#-------------------------------#
        $data['userRoles'] = $this->user_roles(); 
		$data['profile'] = $this->employee_model->read_by_id($user_id);
		$data['content'] = $this->load->view('human_resources/profile',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}


	public function delete($user_id = null, $user_role = null) 
	{		 
		if ($this->employee_model->delete($user_id, $user_role)) {
			#set success message
			$this->session->set_flashdata('message',display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception',display('please_try_again'));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}


	public function user_roles($user_role = null)
	{
		$user_list = array(
			'Accountant'     => 3,
                        'Doctor'        => 2,
			'Laboratorist'   => 4,
			'Nurse'          => 5,
			'Pharmacist'     => 6,
			'Receptionist'   => 7,
			'Representative' => 8,
			'Case_manager'   => 9,
		);

		if (!empty($user_role)) {
			$user_role = ucfirst($user_role);
			if (array_key_exists($user_role, $user_list)) {
				return $user_list[$user_role];
			} else {
				return null;
			}			
		} else {
			return array_flip($user_list);
		}

	}	

	//change by user
	public function profile_edit()
	{   
		$user_id       = $this->session->userdata('user_id');
		#-------------------------------#
		$this->form_validation->set_rules('firstname', display('first_name'),'required|max_length[50]');
		$this->form_validation->set_rules('lastname',display('last_name'),'required|max_length[50]');
		$this->form_validation->set_rules('email',display('email'), "required|max_length[50]|valid_email|callback_email_check[$user_id]");
		$this->form_validation->set_rules('password',display('password'),'required|max_length[32]|md5');
		$this->form_validation->set_rules('mobile',display('mobile'),'required|max_length[20]');
		$this->form_validation->set_rules('sex',display('sex'),'required|max_length[10]');
		$this->form_validation->set_rules('address',display('address'),'required|max_length[255]');
		$this->form_validation->set_rules('status',display('status'),'required');
		#-------------------------------#
		//picture upload
		$picture = $this->fileupload->do_upload(
			'assets/images/human_resources/',
			'picture'
		);
		// if picture is uploaded then resize the picture
		if ($picture !== false && $picture != null) {
			$this->fileupload->do_resize(
				$picture, 
				200,
				200
			);
		}
		//if picture is not uploaded
		if ($picture === false) {
			$this->session->set_flashdata('exception', display('invalid_picture'));
		}
		#-------------------------------#
		$data['employee'] = (object)$postData = array(
			'user_id'      => $user_id,
			'firstname'    => $this->input->post('firstname'),
			'lastname' 	   => $this->input->post('lastname'),
			'email' 	   => $this->input->post('email'),
			'password' 	   => md5($this->input->post('password')),
			'mobile'       => $this->input->post('mobile'),
			'sex' 		   => $this->input->post('sex'),
			'address' 	   => $this->input->post('address'),
			'picture'      => (!empty($picture)?$picture:$this->input->post('old_picture')),
		); 
		#-------------------------------#
        if ($this->form_validation->run() === true) { 
            if ($this->employee_model->update($postData)) {
                #set success message
                $this->session->set_flashdata('message', display('update_successfully'));
            } else {
                #set exception message
                $this->session->set_flashdata('exception',display('please_try_again'));
            }
            redirect('dashboard/profile');
        } else {
        	//view section
            $data['title'] = display('edit_profile');
            $data['employee'] = $this->employee_model->read_by_id($user_id);
            $data['userRoles'] = $this->user_roles(); 
            $data['content'] = $this->load->view('human_resources/profile_edit',$data,true);
            $this->load->view('layout/main_wrapper',$data);
        } 
	}
        
        public function randStrGen($mode = null, $len = null){
        $result = "";
        if($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 4):
            $chars = "0123456789";
        endif;

        $charArray = str_split($chars);
        for($i = 0; $i < $len; $i++) {
                $randItem = array_rand($charArray);
                $result .="".$charArray[$randItem];
        }
        return $result;
    }
    
    public function emp_details($user_id = null) {
        
                $this->form_validation->set_rules('user_id', "Select User",'required|max_length[150]');
		$this->form_validation->set_rules('user_role',"User Role",'required');

		//when create a user
		$data['employee'] = (object)$postData = array(
			'user_id'        => $this->input->post('user_id'),
			'department_id'  => $this->input->post('department_id'),
			'short_biography'=> $this->input->post('short_biography'),
			'specialist'     => $this->input->post('specialist'),
			'designation'     => $this->input->post('designation'),
			'degree'         => $this->input->post('degree'),
			'user_role'      => !empty($this->input->post('user_role')) ? $this->input->post('user_role') : '3',
                        'status'         => $this->input->post('status',true)
		); 

        /*-----------CHECK ID -----------*/
        if (empty($user_id)) {
            
            /*-----------CREATE A NEW RECORD-----------*/
            if ($this->form_validation->run() === true) {
//                echo "<pre>";
//                print_r($postData);
//                print_r($this->input->post('user_details'));die;
                
                if ($this->employee_model->update($postData)) {
                    $user_details = $this->input->post('user_details');
                    $user_details['user_id'] = $postData['user_id'];
                    $user_details['date_of_joining'] = !empty($user_details['date_of_joining']) ? date('Y-m-d', strtotime($user_details['date_of_joining'])) : '';
                    $this->employee_model->addEmpWorkDetails($user_details);
                    #set success message
                    $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('human_resources/employee/emp_details');
            } else {
            	//view section
                $data['title'] = display('add_employee');
                $data['userRoles'] = $this->user_roles();
                $data['employee_details'] = (object)array_fill_keys($this->db->list_fields('user_work_details'), '');
                $data['employee_details']->emp_category = '0';
                $data['employee_details']->shift_name = 'morning';
                $data['employee_details']->ot_applicable = '0';
                $data['employee_details']->bonus_applicable = '0';
                $data['employee_details']->pension = '0';
                $data['department_list'] = $this->department_model->department_list();
                //echo "<pre>";print_r($data['employee_details']);die;
                $data['content'] = $this->load->view('human_resources/emp_details',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            } 

        } else {

            /*-----------UPDATE A RECORD-----------*/
            if ($this->form_validation->run() === true) {
                
                if ($this->employee_model->update($postData)) {
                    $user_details = $this->input->post('user_details');
                    $user_details['user_id'] = $postData['user_id'];
                    $user_details['date_of_joining'] = !empty($user_details['date_of_joining']) ? date('Y-m-d', strtotime($user_details['date_of_joining'])) : '';
                    //echo "<pre>";print_r($user_details);die;
                    $this->employee_model->updateEmpWorkDetails($user_details);
                    #set success message
                    $this->session->set_flashdata('message', display('update_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('human_resources/employee/emp_details/'.$postData['user_id']);
            } else {
            	//view section
                $data['title'] = display('employee_edit');
                $data['employee'] = $this->employee_model->read_by_id($user_id);
                $data['employee_details'] = $this->employee_model->empWorkDetail($user_id);
                $data['department_list'] = $this->department_model->department_list();
                $data['userRoles'] = $this->user_roles(); 
                $data['content'] = $this->load->view('human_resources/emp_details',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            } 
        } 
    }
    
    public function get_employee()
    {

        $search = $this->input->post('search');
        if (!empty($search)) {
            $query = $this->db->select('user_id as value,fullname as label')
                    ->from('user_details')
                    ->group_start()
                    ->or_like('fullname', $search)
                    ->or_like('emp_code', $search)
                    ->group_end()
                    ->get();

            if ($query->num_rows() > 0) {
                $data['message'] = $query->result();
                $data['status'] = true;

            } else {
                $data['message'] = display('invalid_patient_id');
                $data['status'] = false;
            }
        } else {
            $data['message'] = display('invlid_input');
            $data['status'] = null;
        }

        echo json_encode($data);
    }
    
    public function emp_list() {
     
        $data['title'] = "Employee List";
        $role_id     = $this->user_roles();
        $data['employees'] = $this->employee_model->getEmpList(array_keys($role_id));
        $data['userRoles'] = $this->user_roles();
	$data['content'] = $this->load->view('human_resources/view', $data, true);
	$this->load->view('layout/main_wrapper',$data);
    }
    
    public function emp_salary($user_id = null) {
        
         $role_id     = $this->user_roles();
         $all_emp = $this->employee_model->getEmpListOnly(array_keys($role_id));//$data['employee_list']
         $all_emp_array = array();
         $all_emp_array[null] = 'Please Select';
         foreach($all_emp as $data) {
             $all_emp_array[$data['value']] = $data['label']; 
         }
         $data['employee_list'] = $all_emp_array;
         //$this->form_validation->set_rules('month', "Select Month",'required');
         $this->form_validation->set_rules('emp_id', "Select Employee",'required');
         $loan_detail = $this->input->post('loan_detail');
            $loan_detail_array = array();
                if (!empty($loan_detail) && is_array($loan_detail) && sizeof($loan_detail) > 0) 
                {
                        foreach ($loan_detail as $key=>$value) {
                                $loan_detail_array[$key] = array(
                                        'add_loan' => isset($value['add_loan']) ? $value['add_loan'] : '' ,
                                        'loan_amount' => isset($value['loan_amount']) ? $value['loan_amount'] : '',
                                        'loan_from_date' => isset($value['loan_from_date']) ? date('Y-m-d', strtotime($value['loan_from_date'])) : '',
                                        'loan_to_date' => isset($value['loan_to_date']) ? date('Y-m-d', strtotime($value['loan_to_date'])) : '',
                                        'instalment_for_month' => isset($value['instalment_for_month']) ? $value['instalment_for_month'] : '',
                                        'per_month_amount' => isset($value['per_month_amount']) ? $value['per_month_amount'] : ''
                                );
                        }
                }
                $loan_detail_array = json_encode($loan_detail_array);
                if(!empty($loan_detail))
                {
                    $per_month_loan_amt = array_sum(array_column($loan_detail, 'per_month_amount'));
                }
            //when create a user
            $data['salary'] = (object)$postData = array(
                    'emp_id'        => $this->input->post('emp_id'),
                    //'month'  => (!empty($this->input->post('month'))) ? date('Y-m-d', strtotime($this->input->post('month'))) : date('Y-m-d'),
                    'basic_salary'=> $this->input->post('basic_salary'),
                    'da'     => $this->input->post('da'),
                    'hra'     => $this->input->post('hra'),
                    'conveyance_allowance'         => $this->input->post('conveyance_allowance'),
                    'grade_allowance'         => $this->input->post('grade_allowance'),
                    'medical_reimbursement'         => $this->input->post('medical_reimbursement'),
                    'lta'         => $this->input->post('lta'),
                    'mobile_reimbursement'         => $this->input->post('mobile_reimbursement'),
                    'bonus'         => $this->input->post('bonus'),
                    'insurance'         => $this->input->post('insurance'),
                    'labour_welfare_fund'         => $this->input->post('labour_welfare_fund'),
                    'professional_tax'         => $this->input->post('professional_tax'),
                    'epf'         => $this->input->post('epf'),
                    'income_tax'  => $this->input->post('income_tax'),
                    //'pf' => $this->input->post('pf'),
                    'other' => $this->input->post('other'),
                    'loan_detail'       => $loan_detail_array,
            );
            
        /*-----------CHECK ID -----------*/
        if (empty($user_id)) {
            
            /*-----------CREATE A NEW RECORD-----------*/
            if ($this->form_validation->run() === true) {
                $postData['total_of_component'] = $postData['basic_salary'] + $postData['da'] + $postData['hra'] + $postData['conveyance_allowance'] + $postData['grade_allowance']
                                      + $postData['medical_reimbursement'] + $postData['lta'] + $postData['mobile_reimbursement'] + $postData['bonus'] + $postData['other'];
                
                $postData['total_of_deduction'] = $postData['pf'] + $postData['insurance'] + $postData['labour_welfare_fund'] + $postData['professional_tax'] + $postData['epf'] + $postData['income_tax'] + $per_month_loan_amt;
                
//                echo "<pre>";
//                print_r($postData);die;
                
                if ($this->employee_model->add_salary($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('human_resources/employee/emp_salary');
            } else {
            	//view section
                $data['title'] = display('add_employee');
                $data['selected_emp'] = null;
                $data['content'] = $this->load->view('human_resources/emp_salary',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            } 

        } else {

            /*-----------UPDATE A RECORD-----------*/
            if ($this->form_validation->run() === true) {
                $postData['id'] = $this->input->post('id');
                //echo "<pre>";print_r($postData);die;
                if ($this->employee_model->update_salary($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('update_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('human_resources/employee/emp_salary/'.$postData['id']);
            } else {
            	//view section
                $data['title'] = display('employee_edit');
                $data['salary'] = $this->employee_model->get_emp_salary($user_id);
                $data['selected_emp'] = $data['salary']->emp_id;
                //echo "<pre>";print_r($data['employee']);die;
                $data['content'] = $this->load->view('human_resources/emp_salary',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            } 
        }
        
    }
    
    public function salary_list()
    {
        $data['title'] = "Salary Slip List";
        $data['date'] = (object)$getData = [
                'start_date' => (($this->input->get('start_date') != null) ? $this->input->get('start_date'):date('d-m-Y')),
                'end_date'  => (($this->input->get('end_date') != null) ? $this->input->get('end_date'):date('d-m-Y')) 
        ];
        $data['result'] = $this->employee_model->salary_slip_list($getData);
        $data['content'] = $this->load->view('human_resources/salary_list',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }
        
    public function salary_list_view($id = null)
    {
        $data['title'] = "Salary Sleep";
        #-------------------------------#
        $data['website'] = $this->prescription_model->website();
        $data['salary'] = $this->employee_model->single_view_salary($id);
        $data['userRoles'] = $this->user_roles();
        //echo "<pre>";print_r($data['salary']);die;
        $data['content'] = $this->load->view('human_resources/salary_list_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }

    

     public function emp_attendance() {
        
         $role_id     = $this->user_roles();
         $all_emp = $this->employee_model->getEmpListOnly(array_keys($role_id));//$data['employee_list']
         $all_emp_array = array();
         $all_emp_array[null] = 'Please Select';
         foreach($all_emp as $data) {
             $all_emp_array[$data['value']] = $data['label']; 
         }
         $data['employee_list'] = $all_emp_array;
         $data['leave_type'] = array('CL','Sick leave', 'Previlage leave', 'Leave Without Pay');
         $data['leave_type'][null] = 'Please Select Leave Type';
         //echo "<pre>";print_r($data['leave_type']);die;
         $this->form_validation->set_rules('month', "Select Month",'required');
         $this->form_validation->set_rules('emp_id', "Select Employee",'required');
       
            //when create a user
            $data['employee'] = (object)$postData = array(
                    'emp_id'        => $this->input->post('emp_id'),
                    'month'  => (!empty($this->input->post('month'))) ? date('Y-m-d', strtotime($this->input->post('month'))) : date('Y-m-d'),
                    'start_time'=> $this->input->post('start_time'),
                    'end_time'     => $this->input->post('end_time'),
                    //'total_hours'     => $this->input->post('total_hours'),
                    'leave_type'         => $this->input->post('leave_type'),
                     'remark'         => $this->input->post('remark'),
                              
            );

            /*-----------CREATE A NEW RECORD-----------*/
            if ($this->form_validation->run() === true) {
                $timeDiff = strtotime($postData['end_time']) - strtotime($postData['start_time']);
                $total_hours = substr('00'.($timeDiff / 3600 % 24),-2)
                    .':'. substr('00'.($timeDiff / 60 % 60),-2)
                    .':'. substr('00'.($timeDiff % 60),-2);
                $postData['total_hours'] = $total_hours;
                if ($this->employee_model->add_attendance($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('human_resources/employee/emp_attendance');
            } else {
                $data['title'] = "Employee Attendance";
                $data['content'] = $this->load->view('human_resources/emp_attendance',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            }
    }
    
    public function emp_attendance_list()
    {
        $data['title'] = "Employee Attendance List";
        $data['date'] = (object)$getData = [
                'start_date' => (($this->input->get('start_date') != null) ? $this->input->get('start_date'):date('d-m-Y')),
                'end_date'  => (($this->input->get('end_date') != null) ? $this->input->get('end_date'):date('d-m-Y')) 
        ];
        $data['result'] = $this->employee_model->emp_attendance_list($getData);
        $data['content'] = $this->load->view('human_resources/emp_attendance_list',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }

     public function attendance_list_view($id = null)
    {
        $data['title'] = "Attendance View";
        #-------------------------------#
        $data['website'] = $this->prescription_model->website();
        $data['salary'] = $this->employee_model->single_view_attendance($id);
        $data['userRoles'] = $this->user_roles();
        //echo "<pre>";print_r($data['salary']);die;
        $data['content'] = $this->load->view('human_resources/attendance_list_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }

     public function create_salary_slip_view()
    {
        $data['title'] = "Salary Sleep";
        #-------------------------------#
        $emp_id        = $this->input->post('emp_id');
        $month        = $this->input->post('month');
        if(empty($emp_id))
        {

         $role_id     = $this->user_roles();
         $all_emp = $this->employee_model->getEmpListOnly(array_keys($role_id));//$data['employee_list']
         $all_emp_array = array();
         $all_emp_array[null] = 'Please Select';
         foreach($all_emp as $data) {
             $all_emp_array[$data['value']] = $data['label']; 
         }
         $data['employee_list'] = $all_emp_array;
         $data['content'] = $this->load->view('human_resources/create_salary_slip',$data,true);
         $this->load->view('layout/main_wrapper',$data);

        }else{

        $data['website'] = $this->prescription_model->website();
        $data['salary'] = $this->employee_model->single_view_salary_slip($emp_id);
        $data['userRoles'] = $this->user_roles();
        $data['month'] = $month;
         

          $last_date = date("Y-m-t", strtotime($month));
          $query = $this->db->select("*")
            ->from('emp_attendance')
                        ->where("emp_attendance.month BETWEEN '$month' AND '$last_date'",null,false)
                        ->where("emp_attendance.emp_id",$emp_id)
            ->get();
           
            if ($query->num_rows() > 0) {
                $data['attendance'] = $query->result();
                $data['status'] = true;

            } else {
                $data['attendance'] = display('attendance_blank');
                $data['status'] = false;
            }

       // echo "<pre>";print_r($data['attendance']);die;
        $data['content'] = $this->load->view('human_resources/salary_list_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);

        }

    }
    
    public function get_emp_salary_by_emp_id() {
        
        $patient_id = $this->input->post('emp_id');
        $emp_data = $this->employee_model->get_emp_salary_by_emp_id($patient_id);
        if($emp_data == null) {
            $data = array('status'=>false);
        }
        else {
            $data = array('status'=>true, 'id'=>$emp_data->id);
        }
        echo json_encode($data);
    }
}

