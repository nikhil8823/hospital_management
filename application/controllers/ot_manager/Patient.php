<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array(
			'ot_model',
                        'patient_model',
                        'document_model',
                        'department_model',
                        'prescription/prescription_model',
                        'bed_manager/bed_model',
                        'bed_manager/bed_assign_model',
		));

		if ($this->session->userdata('isLogIn') == false
			|| $this->session->userdata('user_role') != 1) 
			redirect('login');
	}
        
        public function index()
	{ 
		$data['title'] = display('patient_list');
		$data['patients'] = $this->ot_model->getOtPatient();
		$data['content'] = $this->load->view('ot_manager/patient',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}
        
        public function profile($patient_id = null)
	{ 
		$data['title'] =  display('patient_information');
		#-------------------------------#
		$data['profile'] = $this->patient_model->read_by_id($patient_id);
		$data['profile_additional'] = $this->patient_model->read_by_id_profile($patient_id);
		$data['documents'] = $this->document_model->read_by_patient($patient_id);
                //print_r($data['documents']);die;
		$data['content'] = $this->load->view('ot_manager/patient_profile',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}
        
        
        public function randStrGen($mode = null, $len = null){
        $result = "";
        if($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 4):
            $chars = "0123456789";
        endif;

        $charArray = str_split($chars);
        for($i = 0; $i < $len; $i++) {
                $randItem = array_rand($charArray);
                $result .="".$charArray[$randItem];
        }
        return $result;
    }
        
        
        public function create()
	{
		$data['title'] = display('add_patient');
                $id = $this->input->post('id');
                //echo "<pre>";print_r($this->input->post());die;
		#-------------------------------#
		$this->form_validation->set_rules('firstname', display('first_name'),'required|max_length[50]');
		$this->form_validation->set_rules('lastname', display('last_name'),'required|max_length[50]');
		if ($this->input->post('id') == null) {
			$this->form_validation->set_rules('email', display('email'),'max_length[100]|is_unique[patient.email]|valid_email');
                      
		} else {        
			$this->form_validation->set_rules('email',display('email'), "max_length[50]|valid_email|callback_email_check[$id]");
		}

		
		$this->form_validation->set_rules('mobile', display('mobile'),'required|max_length[20]');
		$this->form_validation->set_rules('sex', display('sex'),'required|max_length[10]');
		$this->form_validation->set_rules('date_of_birth', display('date_of_birth'),'required|max_length[10]');
		$this->form_validation->set_rules('address', display('address'),'required|max_length[255]');
		$this->form_validation->set_rules('status', display('status'),'required');
                $this->form_validation->set_rules('department_id', display('department_name'),'required|max_length[50]');
                $this->form_validation->set_rules('doctor_id', display('doctor_name') ,'required|max_length[50]');
                $this->form_validation->set_rules('status', display('status') ,'required');
                $this->form_validation->set_rules('admission_fee', 'Admission Fee' ,'required');
                $this->form_validation->set_rules('service_tax', 'Entry Person Name' ,'required');
		#-------------------------------#
		//picture upload
		$picture = $this->fileupload->do_upload(
			'assets/images/patient/',
			'picture'
		);
		// if picture is uploaded then resize the picture
		if ($picture !== false && $picture != null) {
			$this->fileupload->do_resize(
				$picture, 
				200,
				150
			);
		}
		//if picture is not uploaded
		if ($picture === false) {
			$this->session->set_flashdata('exception', display('invalid_picture'));
		}
		#-------------------------------#
		if ($this->input->post('id') == null) { //create a patient
			$data['patient'] = (object)$postData = [
				'id'   		   => $this->input->post('id'),
				'patient_id'   => "P".$this->randStrGen(2,7),
				'firstname'    => $this->input->post('firstname'),
				'lastname' 	   => $this->input->post('lastname'),
				'email' 	   => $this->input->post('email'),
				/*'password' 	   => md5($this->input->post('password')),*/
				'phone'   	   => $this->input->post('phone'),
				'mobile'       => $this->input->post('mobile'),
				'blood_group'  => $this->input->post('blood_group'),
				'sex' 		   => $this->input->post('sex'), 
				'date_of_birth' => date('Y-m-d', strtotime(($this->input->post('date_of_birth') != null)? $this->input->post('date_of_birth'): date('Y-m-d'))),
				'address' 	   => $this->input->post('address'),
				/*'picture'      => (!empty($picture)?$picture:$this->input->post('old_picture')),*/
				'affliate'     => null,
				'create_date'  => date('Y-m-d'),
				'created_by'   => $this->session->userdata('user_id'),
				'status'       => $this->input->post('status'),
				'guardianname'       => $this->input->post('guardianname'),
				'birthmark'       => $this->input->post('birthmark'),
				'aadharno'       => $this->input->post('aadharno'),
				'religion'       => $this->input->post('religion'),
				'case_type'       => $this->input->post('case_type'),
				'reffer_doctor_name'       => $this->input->post('reffer_doctor_name'),
				'reffer_doctor_phone'       => $this->input->post('reffer_doctor_phone'),
				'maritual_status'       => $this->input->post('maritual_status'),
				/*'consultant_doctor_dept'       => $this->input->post('consultant_doctor_dept'),*/
				'consultant_doctor_name'       => $this->input->post('consultant_doctor_name'),
				/*'consultantation_fee'       => $this->input->post('consultantation_fee'),*/
				'followup_consultation'       => $this->input->post('followup_consultation'),
				'service_tax'       => $this->input->post('service_tax'),
				'total_fee'       => $this->input->post('total_fee'),
				'payment_mode'       => $this->input->post('payment_mode'),
				'department_id'       => $this->input->post('department_id'),
				'doctor_id'       => $this->input->post('doctor_id'),
                                'package_id'       => $this->input->post('package_id'),
                                'current_patient_status' => 2
			];
                        
                         #-------------------------------#
                            $data['bed'] = (object)$postBedData = array( 
                                'patient_id'  => $data['patient']->patient_id,
                                'serial'      => $this->randStrGen(2,6),
                                'bed_id'      => $this->input->post('bed_id',true),
                                'description' => '',
                                'assign_date' => date('Y-m-d', strtotime(($this->input->post('assign_date',true) != null)? $this->input->post('assign_date',true): date('Y-m-d'))),
                                'discharge_date' => date('Y-m-d', strtotime(($this->input->post('discharge_date',true) != null)? $this->input->post('discharge_date',true): date('Y-m-d'))),
                                'assign_by'   => $this->session->userdata('user_id'),
                                'status'      => $this->input->post('status',true),
                                'admission_fee' => $this->input->post('admission_fee',true)
                            );  
                            #-------------------------------#
                        
		} else { // update patient
			$data['patient'] = (object)$postData = [
				'id'   		   => $this->input->post('id'),
				'firstname'    => $this->input->post('firstname'),
				'lastname' 	   => $this->input->post('lastname'),
				'email' 	   => $this->input->post('email'),
				/*'password' 	   => md5($this->input->post('password')),*/
				'phone'   	   => $this->input->post('phone'),
				'mobile'       => $this->input->post('mobile'),
				'blood_group'  => $this->input->post('blood_group'),
				'sex' 		   => $this->input->post('sex'),
				'date_of_birth' => date('Y-m-d', strtotime($this->input->post('date_of_birth'))),
				'address' 	   => $this->input->post('address'),
				/*'picture'      => (!empty($picture)?$picture:$this->input->post('old_picture')),*/
				'affliate'     => null, 
				'created_by'   => $this->session->userdata('user_id'),
				'status'       => $this->input->post('status'),
				'guardianname'       => $this->input->post('guardianname'),
				'birthmark'       => $this->input->post('birthmark'),
				'aadharno'       => $this->input->post('aadharno'),
				'religion'       => $this->input->post('religion'),
				'case_type'       => $this->input->post('case_type'),
				'reffer_doctor_name'       => $this->input->post('reffer_doctor_name'),
				'reffer_doctor_phone'       => $this->input->post('reffer_doctor_phone'),
				'maritual_status'       => $this->input->post('maritual_status'),
				/*'consultant_doctor_dept'       => $this->input->post('consultant_doctor_dept'),*/
				'consultant_doctor_name'       => $this->input->post('consultant_doctor_name'),
				/*'consultantation_fee'       => $this->input->post('consultantation_fee'),*/
				'followup_consultation'       => $this->input->post('followup_consultation'),
				'service_tax'       => $this->input->post('service_tax'),
				'total_fee'       => $this->input->post('total_fee'),
				'payment_mode'       => $this->input->post('payment_mode'),
                                'department_id'       => $this->input->post('department_id'),
				'doctor_id'       => $this->input->post('doctor_id'),
                                'package_id'       => $this->input->post('package_id'),
                                'current_patient_status' => 2
			]; 
                        #-------------------------------#
                            $data['bed'] = (object)$postBedData = array( 
                                'patient_id'  => $this->input->post('patient_id',true),
                                'serial'      => $this->randStrGen(2,6),
                                'bed_id'      => $this->input->post('bed_id',true),
                                'description' => '',
                                'assign_date' => date('Y-m-d', strtotime(($this->input->post('assign_date',true) != null)? $this->input->post('assign_date',true): date('Y-m-d'))),
                                'discharge_date' => date('Y-m-d', strtotime(($this->input->post('discharge_date',true) != null)? $this->input->post('discharge_date',true): date('Y-m-d'))),
                                'assign_by'   => $this->session->userdata('user_id'),
                                'status'      => $this->input->post('status',true),
                                'admission_fee' => $this->input->post('admission_fee',true)
                            );  
                            #-------------------------------#				

		}
		#-------------------------------#
                
                #-------------------------------#
                if ($this->check_bed(true) === false) {
                    $this->session->set_flashdata('exception',display('bed_not_available')." / ".display('select_only_avaiable_days'));
                }
                
		if ($this->form_validation->run() === true && $this->check_bed(true) === true) {
			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->patient_model->create($postData)) {
					$patient_id = $this->db->insert_id();
                                        # Add Bed Information
                                            $assign_date = strtotime($this->input->post('assign_date', true));
                                            $discharge_date = strtotime($this->input->post('discharge_date', true));
                                            $timeDiff = abs($discharge_date - $assign_date);
                                            $numberDays = $timeDiff / 86400;
                                            $numberDays = intval($numberDays);

                                            for ($i = 0; $i <= $numberDays; $i++) {
                                                $date = date('Y-m-d', strtotime("$i day", $assign_date));
                                                $postBedData['assign_date'] = $date;
                                                $this->bed_assign_model->create($postBedData);
                                            }
                                            
                                            # Add additional Information of OT
                                            $ot_additional_data = array(
                                                'patient_id' => $patient_id,
                                                'operation_name' => $this->input->post('operation_name'),
                                                'operation_type' => $this->input->post('operation_type'),
                                                'operation_date' => $this->input->post('operation_date'),
                                                'operation_time' => $this->input->post('operation_time'),
                                                'patient_condition' => $this->input->post('patient_condition'),
                                                'staff_finalised' => $this->input->post('staff_finalised'),
                                                'operation_remark' => $this->input->post('operation_remark'),
                                                'ot_checklist_finished' => $this->input->post('ot_checklist_finished'),
                                                'addtional_info' => (!empty($this->input->post('ot'))) ? json_encode($this->input->post('ot')) : null
                                            );
                                            $this->ot_model->addOtAdditionalData($ot_additional_data);
                                            
                                        #set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				redirect('ot_manager/patient/create');
			} else {
				if ($this->patient_model->update($postData)) {
                                        # Add Bed Information
                                            $assign_date = strtotime($this->input->post('assign_date', true));
                                            $discharge_date = strtotime($this->input->post('discharge_date', true));
                                            $timeDiff = abs($discharge_date - $assign_date);
                                            $numberDays = $timeDiff / 86400;
                                            $numberDays = intval($numberDays);
                                            for ($i = 0; $i <= $numberDays; $i++) {
                                                $date = date('Y-m-d', strtotime("$i day", $assign_date));
                                                $postBedData['assign_date'] = $date;
                                                $this->bed_assign_model->create($postBedData);
                                            }
                                       
                                             # Add additional Information of OT
                                            $ot_additional_data = array(
                                                'patient_id' => $this->input->post('id'),
                                                'operation_name' => $this->input->post('operation_name'),
                                                'operation_type' => $this->input->post('operation_type'),
                                                'operation_date' => $this->input->post('operation_date'),
                                                'operation_time' => $this->input->post('operation_time'),
                                                'patient_condition' => $this->input->post('patient_condition'),
                                                'staff_finalised' => $this->input->post('staff_finalised'),
                                                'operation_remark' => $this->input->post('operation_remark'),
                                                'ot_checklist_finished' => $this->input->post('ot_checklist_finished'),
                                                'addtional_info' => (!empty($this->input->post('ot'))) ? json_encode($this->input->post('ot')) : null
                                            );
                                            $this->ot_model->addOtAdditionalData($ot_additional_data);
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
                                redirect('ot_manager/patient/create');
			}

		} else {
                        $data['bed_list'] = $this->bed_model->bed_list();
                        $data['department_list'] = $this->department_model->department_list();
                        $data['package_list'] = $this->patient_model->package_list();
                        $data['is_edit'] = false;
			$data['content'] = $this->load->view('ot_manager/patient_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}
        
    public function get_patient()
    {
        $search = $this->input->post('search');
        if (!empty($search)) {
            $query = $this->db->select('id as value,firstname as label')
                    ->from('patient')
                    ->group_start()
                    ->or_like('firstname', $search)
                    ->or_like('lastname', $search)
                    ->or_like('mobile', $search)
                    ->or_like('patient_id', $search)
                    ->group_end()
                    ->where('status', 1)
                   /* ->where('current_patient_status', 2)*/
                    ->get();

            if ($query->num_rows() > 0) {
                $data['message'] = $query->result();
                $data['status'] = true;
            } else {
                $data['message'] = display('invalid_patient_id');
                $data['status'] = false;
            }
        } else {
            $data['message'] = display('invlid_input');
            $data['status'] = null;
        }

        echo json_encode($data);
    }
    
    public function get_patient_details() {
        
        $patient_id = $this->input->post('userid');
        $patient_data = $this->patient_model->read_by_id($patient_id);
        echo json_encode($patient_data);
    }
    
    public function email_check($email, $id)
    { 
        $emailExists = $this->db->select('email')
            ->where('email',$email) 
            ->where_not_in('id',$id) 
            ->get('patient')
            ->num_rows();

        if ($emailExists > 0) {
            $this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
            return false;
        } else {
            return true;
        }
    }

    public function check_bed($mode = null)
    { 
        $serial      = $this->input->post('serial');
        $bed_id      = $this->input->post('bed_id');
        $assign_date = strtotime($this->input->post('assign_date',true));
        $discharge_date = strtotime($this->input->post('discharge_date',true));
        #----------------------------------------------------#
        if (!empty($bed_id) && !empty($assign_date) && $assign_date <= $discharge_date) {

            $timeDiff = abs($discharge_date - $assign_date);
            $numberDays = $timeDiff/86400;  
            $numberDays = intval($numberDays);

            $result  = "";
            $result .= "<div class=\"alert alert-info\">"; 
            $successCount = 0;
            $errorCount   = 0;
            for ($i = 0; $i <= $numberDays; $i++) {
                $date = date('Y-m-d', strtotime("$i day", $assign_date));

                $query = $this->db->select('bed_id, assign_date, COUNT(assign_date) as allocated')
                    ->from('bm_bed_assign')
                    ->where('assign_date',$date)
                    ->where('bed_id',$bed_id)
                    ->where('status',1)
                    ->where_not_in('serial',$serial)
                    ->group_by('assign_date')
                    ->get()
                    ->row(); 

                $total_bed = $this->db->select("limit")
                    ->from('bm_bed')
                    ->where('id', $bed_id)
                    ->where('status', 1)
                    ->get()
                    ->row()
                    ->limit;

                if (!empty($query)) {
                    $free_bed = $total_bed - $query->allocated; 
                } else {
                    $free_bed = $total_bed;
                }

                if ($free_bed > 0) {
                    $result .= "<p class=\"text-success\">$date [$free_bed ".display('bed_available')."]</p>";  
                    $successCount++; 
                } else {
                    $result .= "<p class=\"text-danger\">$date [".display('bed_not_available')."]</p>"; 
                    $errorCount++;
                }   
            }
            $result .= "</div>";  

            if ($mode == true && $errorCount > 0) {
                return false; 
            } else if ($mode == true && $successCount > 0) {
                return true;
            } 

            $data['status']  = true;
            $data['message'] = $result;
        } else {
            $data['message']     = display('invlid_input');
            $data['status']      = null;

            if ($mode == true) {
                return null;
            }
        }

        if($mode == null) {
            echo json_encode($data);
        } 

    }
    
     public function dischargeReport() {
        $data['title'] = display('report');
        #-------------------------------#
        $data['date'] = (object)$getData = [
                'start_date' => (($this->input->get('start_date') != null) ? $this->input->get('start_date'):date('d-m-Y')),
                'end_date'  => (($this->input->get('end_date') != null) ? $this->input->get('end_date'):date('d-m-Y')) 
        ]; 
        #-------------------------------#
        $data['result'] = $this->ot_model->discharge_report($getData);
        $data['content'] = $this->load->view('ot_manager/report',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }
    
    public function dischargeReportView($id = null) {
        $data['title'] = "Discharge Report";
        #-------------------------------#
        $data['website'] = $this->prescription_model->website();
        $data['prescription'] = $this->ot_model->single_view($id);
        $data['content'] = $this->load->view('ot_manager/report_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }
    
    public function add_prescription() {
        
        $data['title'] = display('add_prescription');
        $appId = "A".$this->randStrGen(2, 6).$this->session->userdata('user_id');
        $data['appointment_id'] = (($this->input->get('aid')!=null)?$this->input->get('aid'):$appId);
        $data['patient_id'] = (($this->input->get('pid')!=null)?$this->input->get('pid'):null);
        #-------------------------------#
        $this->form_validation->set_rules('patient_id', display('patient_id') ,'required|max_length[30]');
        #-------------------------------#
        if ($this->form_validation->run() === true) 
        {
                #----------------------proccess of medicine----------------------#
                $medicine_name = $this->input->post('prescription');
                $medicine = array();
                if (!empty($medicine_name) && is_array($medicine_name) && sizeof($medicine_name) > 0) 
                {
                        foreach ($medicine_name as $key=>$value) {
                                $medicine[$key] = array(
                                        'date' => isset($value['date']) ? $value['date'] : '' ,
                                        'time' => isset($value['time']) ? $value['time'] : '',
                                        'complaints' => isset($value['complaints']) ? $value['complaints'] : '',
                                        'finding_dr' => isset($value['finding_dr']) ? $value['finding_dr'] : '',
                                        'diet' => isset($value['diet']) ? $value['diet'] : ''
                                );
                        }
                } 
                $medicine = json_encode($medicine); 

                #----------------------proccess of data----------------------#  
                $preData = array(
                        'appointment_id' => $this->input->post('appointment_id'),
                        'patient_id'     => $this->input->post('patient_id'),
                        'patient_type'   => $this->input->post('patient_type'),
                        'doctor_id'      => $this->session->userdata('user_id'),
                        'date'           => date('Y-m-d', strtotime($this->input->post('date'))),
                        'chief_complain' => $this->input->post('chief_complain'),
                        'insurance_id'   => $this->input->post('insurance_id'),
                        'blood_pressure' => $this->input->post('blood_pressure'),
                        'weight'         => $this->input->post('weight'),
                        'reference_by'   => $this->input->post('reference_by'),
                        'ipd_medicine'       => $medicine,
                        'visiting_fees'  => $this->input->post('visiting_fees'),
                        'patient_notes'  => $this->input->post('patient_notes'),
                        'status'  => 2,
                ); 
                if ($this->prescription_model->create($preData)) { 
                        #set success message
                        $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                        #set exception message
                        $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('ot_manager/patient/add_prescription');

        } else {
            
                $data['medicine_category'] = $this->patient_model->medicine_category();
                $data['website'] = $this->prescription_model->website();
                $data['content'] = $this->load->view('ot_manager/prescription_form',$data,true);
                $this->load->view('layout/main_wrapper',$data);
        }
    }
    
        public function get_medicine() {
            $medicine   = $this->patient_model->get_medicine($this->input->post('medicine_cat_id'));
            echo json_encode($medicine);
        }
        
        //patient information
	public function patient()
	{
		$patient   = $this->ot_model->prescriptionPatient($this->input->post('patient_id'));
		if ($patient->num_rows() > 0) {
			$data['status']        = true;
			$data['name']          = $patient->row()->firstname.' '.$patient->row()->lastname; 
			$data['sex']           = $patient->row()->sex;
			$data['date_of_birth'] = $patient->row()->date_of_birth;
		} else {
			$data['status'] = false;
		}
		echo json_encode($data);
	}
        
        public function prescription_list() {
            $data['title'] = display('prescription_list');
            #-------------------------------#
            $data['prescription'] = $this->ot_model->get_prescription_list();
            $data['content'] = $this->load->view('ot_manager/prescription',$data,true);
            $this->load->view('layout/main_wrapper',$data);
        }
        
        public function view_prescription($id = null) {
            $data['title'] = display('prescription_information');
            #-------------------------------#
            $data['website'] = $this->prescription_model->website();
            $data['prescription'] = $this->patient_model->single_prescription_view($id); 
            $data['content'] = $this->load->view('ot_manager/prescription_view',$data,true);
            $this->load->view('layout/main_wrapper',$data);
        }
        
        public function before_operation() {
            
            $data['title'] = "Before Operation Checklist";
            $this->form_validation->set_rules('patient_id', display('patient_id') ,'required|max_length[11]');
            if(!empty($this->input->post()) && $this->form_validation->run() == true) {
                $postData['before_operation'] = json_encode($this->input->post('before_operation'));
                $postData['patient_id'] = $this->input->post('patient_id');
                if($this->input->post('id') != null)
                {   
                    $postData['id'] = $this->input->post('id');
                    $this->ot_model->updateChecklist($postData);
                    $this->session->set_flashdata('message', "Checklist updated successfully");                  
                }
                else
                {
                    $this->ot_model->addChecklist($postData);
                    $this->session->set_flashdata('message', "Checklist added successfully");
                }
                redirect('ot_manager/patient/before_operation');
            }
            else {
                $data['content'] = $this->load->view('ot_manager/before_operation',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            }
        }
        
        public function get_ot_checklist() {
            $patient_id = $this->input->post('userid');
            $patient_data = $this->ot_model->get_ot_checklist($patient_id);
            echo json_encode($patient_data);
        }


         public function after_operation() {
            
            $data['title'] = "After Operation Checklist";
            $this->form_validation->set_rules('patient_id', display('patient_id') ,'required|max_length[11]');
            if(!empty($this->input->post()) && $this->form_validation->run() == true) {
                $postData['after_operation'] = json_encode($this->input->post('after_operation'));
                $postData['patient_id'] = $this->input->post('patient_id');
                if($this->input->post('id') != null)
                {   
                    $postData['id'] = $this->input->post('id');
                    $this->ot_model->updateChecklist($postData);
                    $this->session->set_flashdata('message', "Checklist updated successfully");                  
                }
                else
                {
                    $this->ot_model->addChecklist($postData);
                    $this->session->set_flashdata('message', "Checklist added successfully");
                }
                redirect('ot_manager/patient/after_operation');
            }
            else {
                $data['content'] = $this->load->view('ot_manager/after_operation',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            }
        }

        

         public function after_before_operation_list() {
            $data['title'] = "Before After checklist";
            #-------------------------------#
            $data['prescription'] = $this->ot_model->get_before_after_list();
            $data['content'] = $this->load->view('ot_manager/before_after_operation_list',$data,true);
            $this->load->view('layout/main_wrapper',$data);
        }
        

         public function before_after_operation_view($id = null) {
            $data['title'] = "View Before After Checklist";
            #-------------------------------#
            $data['website'] = $this->prescription_model->website();
            $data['prescription'] = $this->ot_model->single_before_after_view($id); 
            $data['content'] = $this->load->view('ot_manager/before_after_view',$data,true);
            $this->load->view('layout/main_wrapper',$data);
        }
        
        public function edit_prescription($id = null) {
            
            if ($this->input->post('id') != null) {
                
                $medicine_name = $this->input->post('prescription');
                $medicine = array();
                if (!empty($medicine_name) && is_array($medicine_name) && sizeof($medicine_name) > 0) 
                {
                        foreach ($medicine_name as $key=>$value) {
                                $medicine[$key] = array(
                                        'date' => isset($value['date']) ? $value['date'] : '' ,
                                        'time' => isset($value['time']) ? $value['time'] : '',
                                        'complaints' => isset($value['complaints']) ? $value['complaints'] : '',
                                        'finding_dr' => isset($value['finding_dr']) ? $value['finding_dr'] : '',
                                        'diet' => isset($value['diet']) ? $value['diet'] : ''
                                );
                        }
                } 
                $medicine = json_encode($medicine); 

                #----------------------proccess of data----------------------#  
                $preData = array(
                        'id'  => $this->input->post('id'),
                        'appointment_id' => $this->input->post('appointment_id'),
                        'patient_id'     => $this->input->post('patient_id'),
                        'patient_type'   => $this->input->post('patient_type'),
                        'doctor_id'      => $this->session->userdata('user_id'),
                        'date'           => date('Y-m-d', strtotime($this->input->post('date'))),
                        'chief_complain' => $this->input->post('chief_complain'),
                        'insurance_id'   => $this->input->post('insurance_id'),
                        'blood_pressure' => $this->input->post('blood_pressure'),
                        'weight'         => $this->input->post('weight'),
                        'reference_by'   => $this->input->post('reference_by'),
                        'ipd_medicine'       => $medicine,
                        'visiting_fees'  => $this->input->post('visiting_fees'),
                        'patient_notes'  => $this->input->post('patient_notes'),
                        'status'  => 2,
                ); 
                
                if ($this->prescription_model->update_prescription($preData)) { 
                        #set success message
                        $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                        #set exception message
                        $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('ot_manager/patient/prescription_list');
            }
            else {
                $data['title'] = "Prescription Edit";
                $data['website'] = $this->prescription_model->website();
                $data['prescription'] = $this->patient_model->single_prescription_view($id);
                //echo "<pre>";print_r($data['prescription']);die;
                $data['content'] = $this->load->view('ot_manager/edit_prescription',$data,true);
                $this->load->view('layout/main_wrapper',$data);
            }
        }
    
}
