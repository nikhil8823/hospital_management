<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model(array(
            'billing/billing_model',
            'doctor_model',
            'prescription/prescription_model'
        ));
        
        if ($this->session->userdata('isLogIn') == false 
            || $this->session->userdata('user_role') != 1
        ) 
        redirect('login'); 
    }
 
    public function index()
    {
        $data['title'] = "Packege";
        #-------------------------------#
        $data['packege'] = $this->billing_model->read();
        $data['content'] = $this->load->view('billing/packege_list',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    } 

    public function create_packege()
    { 
         $data['title'] = "Packege";
        /*----------FORM VALIDATION RULES----------*/
        $this->form_validation->set_rules('packege_name', display('packege_name') ,'required|max_length[20]');
       
        /*-------------STORE DATA------------*/

        #----------------------proccess of medicine----------------------#
                $medicine_name = $this->input->post('prescription');
                //$medicine_category = $this->input->post('medicine_category');
                //$medicine_doses = $this->input->post('doses');
                //$num_of_doses = $this->input->post('num_of_doses');
                //$medicine_diet = $this->input->post('diet');
                 //echo "<pre>";print_r($medicine_name);die;
                $medicine = array();
                if (!empty($medicine_name) && is_array($medicine_name) && sizeof($medicine_name) > 0) 
                {
                        foreach ($medicine_name as $key=>$value) {
                                $medicine[$key] = array(
                                        'name' => isset($value['charges_name']) ? $value['charges_name'] : '' ,
                                        'amount' => isset($value['charges_amount']) ? $value['charges_amount'] : '',
                                        'notes' => isset($value['notes']) ? $value['notes'] : ''
                                );
                        }
                } 

                $medicine = json_encode($medicine); 
      
       
        $data['packege'] = (object)$postData = array( 
            'id'          => $this->input->post('id'),
            'packege_name'  => $this->input->post('packege_name'),
            'medicine'       => $medicine,
            'status'      => $this->input->post('status'),
            'date'        => date('d-m-Y', strtotime((!empty($date) ? $date : date('d-m-Y'))))
        );  

        /*-----------CHECK ID -----------*/
      /*  if (empty($id)) {*/
            /*-----------CREATE A NEW RECORD-----------*/
            if ($this->form_validation->run() === true) { 

              if (empty($postData['id'])) {

                if ($this->billing_model->create_packege($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('billing/billing/create_packege');
            } else{

                 if ($this->billing_model->update_packege($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('update_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('billing/billing/edit/'.$postData['id']);
            }


        } else {
            
                $data['content'] = $this->load->view('billing/create_packege_form',$data,true);
                $this->load->view('layout/main_wrapper',$data);
          
        } 
        
    }

 
    public function packege_list()
    {
        $data['title'] = "Packege List";
        #-------------------------------#
        $data['packege'] = $this->billing_model->read();
        $data['content'] = $this->load->view('billing/create_packege_list', $data, true);
        $this->load->view('layout/main_wrapper',$data);
    } 


    public function edit($id = null) 
    {
        $data['title'] = "Packege Update";
        #-------------------------------#
        $data['packege'] = $this->billing_model->read_by_id($id);
        $data['content'] = $this->load->view('billing/edit_packege_form',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }


    public function delete($id = null) 
    {
        if ($this->billing_model->delete($id)) {
            #set success message
            $this->session->set_flashdata('message', display('delete_successfully'));
        } else {
            #set exception message
            $this->session->set_flashdata('exception', display('please_try_again'));
        }
        redirect('billing/create_packege_list');
    }


    public function dischargeReport() {
        $data['title'] = "packege report";
        #-------------------------------#

        $data['date'] = (object)$getData = [
                'start_date' => (($this->input->get('start_date') != null) ? $this->input->get('start_date'):date('Y-m-d')),
                'end_date'  => (($this->input->get('end_date') != null) ? $this->input->get('end_date'):date('Y-m-d')) 
        ]; 
       
        #-------------------------------#
        $data['result'] = $this->billing_model->discharge_report($getData);
        $data['content'] = $this->load->view('billing/packege_list',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }




      public function create_bill()
    { 
         $data['title'] = "Bill";
        /*----------FORM VALIDATION RULES----------*/
        $this->form_validation->set_rules('patient_id', display('patient_id') ,'required|max_length[20]');
       
        /*-------------STORE DATA------------*/

        #----------------------proccess of medicine----------------------#
                $medicine_name = $this->input->post('prescription');
                //$medicine_category = $this->input->post('medicine_category');
                //$medicine_doses = $this->input->post('doses');
                //$num_of_doses = $this->input->post('num_of_doses');
                //$medicine_diet = $this->input->post('diet');
                 //echo "<pre>";print_r($medicine_name);die;
               // print_r($medicine_name);die;
                $medicine = array();
                if (!empty($medicine_name) && is_array($medicine_name) && sizeof($medicine_name) > 0) 
                {
                        foreach ($medicine_name as $key=>$value) {
                                $medicine[$key] = array(
                                        'name' => isset($value['charges_name']) ? $value['charges_name'] : '' ,
                                        'amount' => isset($value['charges_amount']) ? $value['charges_amount'] : '',
                                        'day' => isset($value['no_of_day']) ? $value['no_of_day'] : '',
                                        'totalamount' => isset($value['total_amount']) ? $value['total_amount'] : ''
                                );
                        }
                } 

                $medicine = json_encode($medicine); 
      
       
        $data['bill'] = (object)$postData = array( 
            'id'          => $this->input->post('id'),
            'hosp_dept'  => $this->input->post('hosp_dept'),
            'patient_id'          => $this->input->post('patient_id'),
            'total_amt'  => $this->input->post('total_amt'),
            'advance_amt'  => $this->input->post('advance_amt'),
            'discount'          => $this->input->post('discount'),
            'payment_mode'  => $this->input->post('payment_mode'),
            'bank_name'          => $this->input->post('bank_name'),
            'net_amount'  => $this->input->post('net_amount'),
            'account_no'          => $this->input->post('account_no'),
            'chq_no'  => $this->input->post('chq_no'),
            'other'  => $this->input->post('other'),
            'medicine'       => $medicine,
            'packege_id'  => $this->input->post('packege_id'),
            'status'      => $this->input->post('status'),
            'date'        => date('d-m-Y', strtotime((!empty($date) ? $date : date('d-m-Y'))))
        );  

        /*-----------CHECK ID -----------*/
      /*  if (empty($id)) {*/
            /*-----------CREATE A NEW RECORD-----------*/
            if ($this->form_validation->run() === true) { 

              if (empty($postData['id'])) {

                if ($this->billing_model->create_bill($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('save_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('billing/billing/create_bill');
            } else{

                 if ($this->billing_model->update_bill($postData)) {
                    #set success message
                    $this->session->set_flashdata('message', display('update_successfully'));
                } else {
                    #set exception message
                    $this->session->set_flashdata('exception',display('please_try_again'));
                }
                redirect('billing/billing/edit_bill/'.$postData['id']);
            }


        } else {
                
                $data['packege_list'] = $this->billing_model->read_packege();
                $data['content'] = $this->load->view('billing/create_bill_form',$data,true);
                $this->load->view('layout/main_wrapper',$data);
          
        } 
        
    }



     public function bill_list()
    {
        $data['title'] = "Bill List";
        #-------------------------------#
        $data['bill'] = $this->billing_model->read_bill();
        $data['content'] = $this->load->view('billing/bill_list', $data, true);
        $this->load->view('layout/main_wrapper',$data);
    } 


    public function edit_bill($id = null) 
    {
        $data['title'] = "Bill Update";
        #-------------------------------#
        $data['bill'] = $this->billing_model->read_by_id_bill($id);
        $data['content'] = $this->load->view('billing/edit_bill_form',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }


    public function delete_bill($id = null) 
    {
        if ($this->billing_model->delete_bill($id)) {
            #set success message
            $this->session->set_flashdata('message', display('delete_successfully'));
        } else {
            #set exception message
            $this->session->set_flashdata('exception', display('please_try_again'));
        }
        redirect('billing/bill_list');
    }


     public function get_patient()
    {

        $search = $this->input->post('search');
        if (!empty($search)) {
            $query = $this->db->select('id as value,firstname as label')
                    ->from('patient')
                    ->group_start()
                    ->or_like('firstname', $search)
                    ->or_like('lastname', $search)
                    ->or_like('mobile', $search)
                    ->group_end()
                    ->where('status', 1)
                    ->where('current_patient_status', 0)
                    ->get();

            if ($query->num_rows() > 0) {
                $data['message'] = $query->result();
                $data['status'] = true;

            } else {
                $data['message'] = display('invalid_patient_id');
                $data['status'] = false;
            }
        } else {
            $data['message'] = display('invlid_input');
            $data['status'] = null;
        }

        echo json_encode($data);
    }

    public function get_patient_details() {
        
        $patient_id = $this->input->post('userid');
        $patient_data = $this->billing_model->read_by_id_billing($patient_id);
        echo json_encode($patient_data);
    }
    


    public function view_bill($bill_id = null) {
        $data['title'] = "Final Bill";
        #-------------------------------#
       $data['website'] = $this->prescription_model->website();
      
       $data['bill'] = $this->billing_model->view_bill($bill_id);
       //echo "<pre>";print_r($data);die;
        $data['content'] = $this->load->view('billing/report_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }


     public function packege_list_idbase()
    {
        $id = $this->input->post('id');
        $packege_data = $this->billing_model->read_by_id_packege_name($id);
        echo json_encode($packege_data);
    } 

  
}
