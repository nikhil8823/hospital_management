<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insurance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array(
			'insurance/insurance_model'
		));
		
		if ($this->session->userdata('isLogIn') == false 
			|| $this->session->userdata('user_role') != 1
		) 
		redirect('login'); 

	}
 
	public function index()
	{
		$data['title'] = display('insurance_list');
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->read();
		$data['content'] = $this->load->view('insurance/insurance',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 

 	public function create()
	{
		$data['title'] = display('add_insurance');
		#-------------------------------#
		$this->form_validation->set_rules('name', display('insurance_name') ,'required|max_length[100]');
		$this->form_validation->set_rules('description', display('description'),'trim');
		$this->form_validation->set_rules('status', display('status') ,'required');
		#-------------------------------#
		$data['insurance'] = (object)$postData = [
			'id' 	  => $this->input->post('id',true),
			'name' 		  => $this->input->post('name',true),
			'description' => $this->input->post('description',true),
			'status'      => $this->input->post('status',true)
		]; 
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->insurance_model->create($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('prescription/insurance/create');
			} else {
				if ($this->insurance_model->update($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('prescription/insurance/edit/'.$postData['id']);
			}

		} else {
			$data['content'] = $this->load->view('prescription/insurance_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}

	public function edit($id = null) 
	{
		$data['title'] = display('insurance_edit');
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id($id);
		$data['content'] = $this->load->view('prescription/insurance_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}
 

	public function delete($id = null) 
	{
		if ($this->insurance_model->delete($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('prescription/insurance');
	}


    /*--- group charges code---*/

	public function creategroup_chargemaster()
	{
		$data['title'] = "Add Group Charge";
		#-------------------------------#
		$this->form_validation->set_rules('disease_name', display('disease_name') ,'required|max_length[100]');
		$this->form_validation->set_rules('status', display('status') ,'required');
		#-------------------------------#
		$data['insurance'] = (object)$postData = [
			'id' 	  => $this->input->post('id',true),
			'disease_name' 		  => $this->input->post('disease_name',true),
			'status'      => $this->input->post('status',true)
		]; 
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->insurance_model->creategroup_chargemaster($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('insurance/insurance/creategroup_chargemaster');
			} else {
				if ($this->insurance_model->update_groupcharge($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('insurance/insurance/edit_groupcharge/'.$postData['id']);
			}

		} else {
			$data['content'] = $this->load->view('insurance/group_charge_master_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}

		public function groupcharge_list()
	{
		$data['title'] = display('insurance_list');
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->readgroup_charge();
		$data['content'] = $this->load->view('insurance/groupcharge',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 

	public function edit_groupcharge($id = null) 
	{
		$data['title'] = display('insurance_edit');
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id_groupcharge($id);
		$data['content'] = $this->load->view('insurance/group_charge_master_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}

  
  public function delete_groupcharge($id = null) 
	{
		if ($this->insurance_model->delete_groupcharge($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('insurance/insurance/groupcharge_list');
	}

	/*---end group charges code---*/

	/*--start sponser details code---*/

     public function create_sponser()
	{
		$data['title'] = "Add Sponser Details";
		#-------------------------------#
		$this->form_validation->set_rules('organisation_name', display('organisation_name') ,'required|max_length[100]');
		$this->form_validation->set_rules('status', display('status') ,'required');
		#-------------------------------#
		#----------------------proccess of medicine----------------------#
			$group_charge = $this->input->post('group_charge');
			$charge_name = $this->input->post('charge_name');
			$hospital_rate = $this->input->post('hospital_rate');
			$insurance_rate = $this->input->post('insurance_rate');

			$disease = array();
			if (!empty($charge_name) && is_array($charge_name) && sizeof($charge_name) > 0) 
			{
				for ($i=0; $i < sizeof($charge_name); $i++) { 
					$disease[$i] = array(
						'disease_name' => $group_charge[$i],
						'charge_name' => $charge_name[$i],
						'hospital_rate' => $hospital_rate[$i],
						'insurance_rate' => $insurance_rate[$i],
					);
				}
			} 
			$disease = json_encode($disease); 

		$data['insurance'] = (object)$postData = [
			'id' 	  => $this->input->post('id',true),
			'organisation_name' 		  => $this->input->post('organisation_name',true),
			'service_tax' 		  => $this->input->post('service_tax',true),
			'discount' 		  => $this->input->post('discount',true),
			'remark' 		  => $this->input->post('remark',true),
			'organisation_no' 		  => $this->input->post('organisation_no',true),
			'organisation_code_no' 		  => $this->input->post('organisation_code_no',true),
			'disease'  => $disease,
			'status'      => $this->input->post('status',true)
		]; 
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->insurance_model->createsponser_details($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('insurance/insurance/create_sponser');
			} else {
				if ($this->insurance_model->update_sponsers_details($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('insurance/insurance/edit_sponser_details/'.$postData['id']);
			}

		} else {
			$data['insurance_list'] = $this->insurance_model->group_charge_dropdown_list();
			$data['content'] = $this->load->view('insurance/sponsers_details_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}


	public function edit_sponser_details($id = null) 
	{
		$data['title'] = "Update Sponseres Details";
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id_sponserdetails($id);
		$data['insurance_list'] = $this->insurance_model->group_charge_dropdown_list();
		$data['content'] = $this->load->view('insurance/sponsers_details_edit_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}

	 public function delete_sponsersdetails($id = null) 
	{
		if ($this->insurance_model->delete_sponsers_details($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('insurance/insurance/sponsers_details_list');
	}


	public function sponsers_details_list()
	{
		$data['title'] = "Sponsers Details List";
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->read_sponsers_details();
		$data['content'] = $this->load->view('insurance/sponsers_details_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	/*--end sponser details code---*/


	/*-- Limit approval code---*/

	  /*
    |----------------------------------------------
    |        id genaretors
    |----------------------------------------------     
    */
    public function randStrGen($mode = null, $len = null){
        $result = "";
        if($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 4):
            $chars = "0123456789";
        endif;

        $charArray = str_split($chars);
        for($i = 0; $i < $len; $i++) {
                $randItem = array_rand($charArray);
                $result .="".$charArray[$randItem];
        }
        return $result;
    }
    /*
    |----------------------------------------------
    |         Ends of id genaretor
    |----------------------------------------------
    */

 public function create_limit_approval()
	{
		$data['title'] = "Limit Approval Form";
		 $id = $this->input->post('id');
		#-------------------------------#
		$this->form_validation->set_rules('firstname',display('firstname'),'required|max_length[50]');
		$this->form_validation->set_rules('phone',display('phone'),'max_length[20]');

		#----------------------proccess of Limit Approval----------------------#
			$disease_name_list = $this->input->post('prescription');
		//	$limit = $this->input->post('limit');
		//	$approval = $this->input->post('approval');
			

              $insurance_limit = array();
                if (!empty($disease_name_list) && is_array($disease_name_list) && sizeof($disease_name_list) > 0) 
                {
                        foreach ($disease_name_list as $key=>$value) {
                                $insurance_limit[$key] = array(
                                        'name' => isset($value['disease_name_list']) ? $value['disease_name_list'] : '' ,
                                        'limit' => isset($value['limit']) ? $value['limit'] : '',
                                        'approval' => isset($value['approval']) ? $value['approval'] : ''
                                );
                        }
                } 

			$insurance_limit = json_encode($insurance_limit); 

			#----------------------proccess of charges Approval braek----------------------#
			$charges_name = $this->input->post('charges_name');
			$amount = $this->input->post('amount');
			
			$limit_approval = array();
			if (!empty($charges_name) && is_array($charges_name) && sizeof($charges_name) > 0) 
			{
				for ($i=0; $i < sizeof($charges_name); $i++) { 
					$limit_approval[$i] = array(
						'name' => $charges_name[$i],
						'amount' => $amount[$i],
					);
				}
			} 
			$limit_approval = json_encode($limit_approval); 
		#-------------------------------#
		if ($this->input->post('id') == null) { //create a patient
			$data['insurance'] = (object)$postData = [
		      	'id'   		   => $this->input->post('id'),
				'patient_id'   => $this->input->post('patient_id'),
				'disease_name'    => $this->input->post('ins_disease_name'),
				'firstname' 	   => $this->input->post('firstname'),
				'phone' 	   => $this->input->post('phone'),
				'age' 	   =>  $this->input->post('age'),
				'sex'   	   => $this->input->post('sex'),
				'address'       => $this->input->post('address'),
				'status'  => $this->input->post('status'),
				'organisation_name' 		   => $this->input->post('organisation_name'), 
				'tpa_name' 	   => $this->input->post('tpa_name'),
				'create_date'  => date('Y-m-d'),
				'ipd_no'       => $this->input->post('ipd_no'),
				'room_no'       => $this->input->post('room_no'),
				'discription'       => $this->input->post('discription'),
				'guardianname'       => $this->input->post('guardianname'),
				'gphone_no'       => $this->input->post('gphone_no'),
				'guar_address'       => $this->input->post('guar_address'),
				'consultant_doctor_name'       => $this->input->post('consultant_doctor_name'),
				'policy_name'       => $this->input->post('policy_name'),
				'policy_no'       => $this->input->post('policy_no'),
				'policy_from_date'       => $this->input->post('policy_from_date'),
				'policy_to_date'       => $this->input->post('policy_to_date'),
				'policy_holder_name'       => $this->input->post('policy_holder_name'),
				'limit_approval'       => $insurance_limit,
				'approval_break'       => $limit_approval,
                                'current_patient_status' => 1
			
			];
		} else { // update patient
			$data['insurance'] = (object)$postData = [
				'id'   		   => $this->input->post('id'),
				'patient_id'   => $this->input->post('patient_id'),
				'disease_name'    => $this->input->post('ins_disease_name'),
				'firstname' 	   => $this->input->post('firstname'),
				'phone' 	   => $this->input->post('phone'),
				'age' 	   => $this->input->post('age'),
				'sex'   	   => $this->input->post('sex'),
				'address'       => $this->input->post('address'),
				'status'  => $this->input->post('status'),
				'organisation_name' 		   => $this->input->post('organisation_name'), 
				'tpa_name' 	   => $this->input->post('tpa_name'),
				'create_date'  => date('Y-m-d'),
				'created_by'   => $this->session->userdata('user_id'),
				'ipd_no'       => $this->input->post('ipd_no'),
				'room_no'       => $this->input->post('room_no'),
				'discription'       => $this->input->post('discription'),
				'guardianname'       => $this->input->post('guardianname'),
				'gphone_no'       => $this->input->post('gphone_no'),
				'guar_address'       => $this->input->post('guar_address'),
				'consultant_doctor_name'       => $this->input->post('consultant_doctor_name'),
				'policy_name'       => $this->input->post('policy_name'),
				'policy_no'       => $this->input->post('policy_no'),
				'policy_from_date'       => $this->input->post('policy_from_date'),
				'policy_to_date'       => $this->input->post('policy_to_date'),
				'policy_holder_name'       => $this->input->post('policy_holder_name'),
				'limit_approval'       => $insurance_limit,
				'approval_break'       => $limit_approval,
                                'current_patient_status' => 1
				
			]; 
		}
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->insurance_model->create_limit_approval($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}

				redirect('insurance/insurance/create_limit_approval');
			} else {
				if ($this->insurance_model->update_limit_approval($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				
                redirect('insurance/insurance/create_limit_approval');
			}

		} else {
			$data['content'] = $this->load->view('insurance/limit_approval_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}


	public function limit_approval_list()
	{
		$data['title'] = "Limit Approval List";
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->read_limit_approval();
		$data['content'] = $this->load->view('insurance/limit_approval_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	public function edit_limit_approval_form($id = null) 
	{
		$data['title'] = "Update Sponseres Details";
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id_limitapproval($id);
		$data['content'] = $this->load->view('insurance/limit_approval_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}



    public function delete_limitapproval($id = null) 
	{
		if ($this->insurance_model->delete_limit_approval($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('insurance/insurance/limit_approval_list');
	}



	/*--end Limit approval code---*/


	/*-- TPA Patient Registration code---*/

 public function create_tpa_patient()
	{
		$data['title'] = "TPA Patient Form";
		 $id = $this->input->post('id');
		#-------------------------------#
		$this->form_validation->set_rules('firstname',display('firstname'),'required|max_length[50]');
		$this->form_validation->set_rules('phone',display('phone'),'max_length[20]');

		#----------------------proccess of Limit Approval----------------------#
			$disease_name_list = $this->input->post('disease_name_list');
			$limit = $this->input->post('limit');
			$approval = $this->input->post('approval');
			
			$insurance_limit = array();
			if (!empty($disease_name_list) && is_array($disease_name_list) && sizeof($disease_name_list) > 0) 
			{
				for ($i=0; $i < sizeof($disease_name_list); $i++) { 
					$insurance_limit[$i] = array(
						'name' => $disease_name_list[$i],
						'limit' => $limit[$i],
						'approval' => $approval[$i],
					);
				}
			} 
			$insurance_limit = json_encode($insurance_limit); 

			#----------------------proccess of charges Approval braek----------------------#
			$charges_name = $this->input->post('charges_name');
			$amount = $this->input->post('amount');
			
			$limit_approval = array();
			if (!empty($charges_name) && is_array($charges_name) && sizeof($charges_name) > 0) 
			{
				for ($i=0; $i < sizeof($charges_name); $i++) { 
					$limit_approval[$i] = array(
						'name' => $charges_name[$i],
						'amount' => $amount[$i],
					);
				}
			} 
			$limit_approval = json_encode($limit_approval); 
		#-------------------------------#
		if ($this->input->post('id') == null) { //create a patient
			$data['insurance'] = (object)$postData = [
		      	'id'   		   => $this->input->post('id'),
				'patient_id'   => $this->input->post('patient_id'),
				'disease_name'    => $this->input->post('ins_disease_name'),
				'firstname' 	   => $this->input->post('firstname'),
				'phone' 	   => $this->input->post('phone'),
				'age' 	   =>  $this->input->post('age'),
				'blood_group' 	   =>  $this->input->post('blood_group'),
				'date_of_birth' 	   =>  $this->input->post('date_of_birth'),
				'maritual_status' 	   =>  $this->input->post('maritual_status'),
				'sex'   	   => $this->input->post('sex'),
				'address'       => $this->input->post('address'),
				'status'  => $this->input->post('status'),
				'organisation_name' 		   => $this->input->post('organisation_name'), 
				'tpa_name' 	   => $this->input->post('tpa_name'),
				'tpa_regno' 	   => $this->input->post('tpa_registration_no'),
				'tpa_code' 	   => $this->input->post('tpa_code'),
				'create_date'  => date('Y-m-d'),
				'ipd_no'       => $this->input->post('ipd_no'),
				'room_no'       => $this->input->post('room_no'),
				'discription'       => $this->input->post('discription'),
				'guardianname'       => $this->input->post('guardianname'),
				'gphone_no'       => $this->input->post('gphone_no'),
				'guar_address'       => $this->input->post('guar_address'),
				'consultant_doctor_name'       => $this->input->post('consultant_doctor_name'),
				'depart_name'       => $this->input->post('department'),
				'doctor_specialisation'       => $this->input->post('specialisation'),
				'policy_name'       => $this->input->post('policy_name'),
				'policy_no'       => $this->input->post('policy_no'),
				'policy_from_date'       => $this->input->post('policy_from_date'),
				'policy_to_date'       => $this->input->post('policy_to_date'),
				'policy_holder_name'       => $this->input->post('policy_holder_name'),
				'drugs_summary'       => $this->input->post('drugs_summary'),
				'advance_amt'       => $this->input->post('advance_amount'),
				'case_paper'       => $this->input->post('case_paper'),
				'payment_type'       => $this->input->post('payment_type'),
				'Payment_mode'       => $this->input->post('payment_mode'),
				'admission_dt'       => $this->input->post('admission_dt'),
				'discharge_dt'       => $this->input->post('discharge_dt'),
				'packege_amount'       => $this->input->post('packege_amount'),
				'limit_approval'       => $insurance_limit,
				'approval_break'       => $limit_approval,
                                'current_patient_status' => 1
			
			];
		} else { // update patient
			$data['insurance'] = (object)$postData = [
				'id'   		   => $this->input->post('id'),
				'patient_id'   => $this->input->post('patient_id'),
				'disease_name'    => $this->input->post('ins_disease_name'),
				'firstname' 	   => $this->input->post('firstname'),
				'phone' 	   => $this->input->post('phone'),
				'age' 	   =>  $this->input->post('age'),
				'blood_group' 	   =>  $this->input->post('blood_group'),
				'date_of_birth' 	   =>  $this->input->post('date_of_birth'),
				'maritual_status' 	   =>  $this->input->post('maritual_status'),
				'sex'   	   => $this->input->post('sex'),
				'address'       => $this->input->post('address'),
				'status'  => $this->input->post('status'),
				'organisation_name' 		   => $this->input->post('organisation_name'), 
				'tpa_name' 	   => $this->input->post('tpa_name'),
				'tpa_regno' 	   => $this->input->post('tpa_registration_no'),
				'tpa_code' 	   => $this->input->post('tpa_code'),
				'create_date'  => date('Y-m-d'),
				'ipd_no'       => $this->input->post('ipd_no'),
				'room_no'       => $this->input->post('room_no'),
				'discription'       => $this->input->post('discription'),
				'guardianname'       => $this->input->post('guardianname'),
				'gphone_no'       => $this->input->post('gphone_no'),
				'guar_address'       => $this->input->post('guar_address'),
				'consultant_doctor_name'       => $this->input->post('consultant_doctor_name'),
				'depart_name'       => $this->input->post('department'),
				'doctor_specialisation'       => $this->input->post('specialisation'),
				'policy_name'       => $this->input->post('policy_name'),
				'policy_no'       => $this->input->post('policy_no'),
				'policy_from_date'       => $this->input->post('policy_from_date'),
				'policy_to_date'       => $this->input->post('policy_to_date'),
				'policy_holder_name'       => $this->input->post('policy_holder_name'),
				'drugs_summary'       => $this->input->post('drugs_summary'),
				'advance_amt'       => $this->input->post('advance_amount'),
				'case_paper'       => $this->input->post('case_paper'),
				'payment_type'       => $this->input->post('payment_type'),
				'Payment_mode'       => $this->input->post('Payment_mode'),
				'admission_dt'       => $this->input->post('admission_dt'),
				'discharge_dt'       => $this->input->post('discharge_dt'),
				'packege_amount'       => $this->input->post('packege_amount'),
				'limit_approval'       => $insurance_limit,
				'approval_break'       => $limit_approval,
                                'current_patient_status' => 1
				
			]; 
		}
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->insurance_model->create_tpa_patient($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}

				redirect('insurance/insurance/create_tpa_patient');
			} else {
				if ($this->insurance_model->update_tpa_patient($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				
                redirect('insurance/insurance/create_tpa_patient');
			}

		} else {
			$data['content'] = $this->load->view('insurance/tpa_patient_registration_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}


	public function tpa_patient_list()
	{
		$data['title'] = "TPA Patient List";
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->read_tpa_patient();
		$data['content'] = $this->load->view('insurance/tpa_patient_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	public function edit_tpa_patient_form($id = null) 
	{
		$data['title'] = "Update TPA PAtient Details";
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id_tpa_patient($id);
		$data['content'] = $this->load->view('insurance/tpa_patient_registration_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}



    public function delete_tpa_patient($id = null) 
	{
		if ($this->insurance_model->delete_tpa_patient($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('insurance/insurance/tpa_patient_list');
	}


	   public function get_patient()
    {

        $search = $this->input->post('search_patient');
        if (!empty($search)) {
            $query = $this->db->select('id as value,firstname as label')
                    ->from('ins_limit_approval')
                    ->group_start()
                    ->or_like('firstname', $search)
                    ->group_end()
                    ->where('current_patient_status', 1)
                    ->get();

            if ($query->num_rows() > 0) {
                $data['message'] = $query->result();
                $data['status'] = true;

            } else {
                $data['message'] = display('invalid_patient_id');
                $data['status'] = false;
            }
        } else {
            $data['message'] = display('invlid_input');
            $data['status'] = null;
        }

        echo json_encode($data);
    }

    public function get_patient_details() {
        
        $patient_id = $this->input->post('id');
        $patient_data = $this->insurance_model->read_by_id_tpa_patient($patient_id);
        echo json_encode($patient_data);
    }
    
     public function email_check($email, $id)
    { 
        $emailExists = $this->db->select('email')
            ->where('email',$email) 
            ->where_not_in('id',$id) 
            ->get('patient')
            ->num_rows();

        if ($emailExists > 0) {
            $this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
            return false;
        } else {
            return true;
        }
    }
    



	/*--end TPA Patient Registration code---*/

}
