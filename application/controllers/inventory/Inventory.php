<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array(
			'inventory/inventory_model',
			'prescription/prescription_model'
		));
		
		if ($this->session->userdata('isLogIn') == false 
			|| $this->session->userdata('user_role') != 1
		) 
		redirect('login'); 

	}
 
	public function index()
	{
		$data['title'] = display('insurance_list');
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->read();
		$data['content'] = $this->load->view('insurance/insurance',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 



    	/*-- Supplier Registration code---*/

 public function create_supplier()
	{
		$data['title'] = "Add Supplier";
		 $id = $this->input->post('id');
		#-------------------------------#
		$this->form_validation->set_rules('owner_name',display('owner_name'),'required|max_length[50]');
		$this->form_validation->set_rules('phone',display('phone'),'max_length[20]');
		$this->form_validation->set_rules('firm_name','Firm Name','max_length[20]');
		$this->form_validation->set_rules('phone',display('phone'),'max_length[20]');

		#-------------------------------#
		if ($this->input->post('id') == null) { //create a supplier
			$data['supplier'] = (object)$postData = [
		      	'id'   		   => $this->input->post('id'),
				'owner_name'   => $this->input->post('owner_name'),
				'supplier_name'    => $this->input->post('supplier_name'),
				'reg_no' 	   => $this->input->post('reg_no'),
				'firm_name' 	   => $this->input->post('firm_name'),
				'address' 	   =>  $this->input->post('address'),
				'phone' 	   =>  $this->input->post('phone'),
				'state' 	   =>  $this->input->post('state'),
				'city' 	   =>  $this->input->post('city'),
				'pincode'   	   => $this->input->post('pincode'),
				'licence_no'       => $this->input->post('licence_no'),
				'gst_no'  => $this->input->post('gst_no'),
				'status' 		   => $this->input->post('status'), 
				'party_code' 	   => $this->input->post('party_code'),
				'dl_no' 	   => $this->input->post('dl_no'),
				'credit_limit' 	   => $this->input->post('credit_limit'),
				'create_date'  => date('Y-m-d'),
				'payment_mode'       => $this->input->post('payment_mode'),
				'side_days'       => $this->input->post('side_days'),
				'guar_name'       => $this->input->post('guar_name'),
				'email_id'       => $this->input->post('email_id'),
				'pan_no'       => $this->input->post('pan_no'),
				'transporter_name'       => $this->input->post('transporter_name'),
				'transporter_address'       => $this->input->post('transporter_address'),
				'transporter_phone'       => $this->input->post('transporter_phone'),
                                'current_supplier_status' => 1
			
			];
		} else { // update supplier
			$data['supplier'] = (object)$postData = [
					'id'   		   => $this->input->post('id'),
				'owner_name'   => $this->input->post('owner_name'),
				'supplier_name'    => $this->input->post('supplier_name'),
				'reg_no' 	   => $this->input->post('reg_no'),
				'firm_name' 	   => $this->input->post('firm_name'),
				'address' 	   =>  $this->input->post('address'),
				'phone' 	   =>  $this->input->post('phone'),
				'state' 	   =>  $this->input->post('state'),
				'city' 	   =>  $this->input->post('city'),
				'pincode'   	   => $this->input->post('pincode'),
				'licence_no'       => $this->input->post('licence_no'),
				'gst_no'  => $this->input->post('gst_no'),
				'status' 		   => $this->input->post('status'), 
				'party_code' 	   => $this->input->post('party_code'),
				'dl_no' 	   => $this->input->post('dl_no'),
				'credit_limit' 	   => $this->input->post('credit_limit'),
				'create_date'  => date('Y-m-d'),
				'payment_mode'       => $this->input->post('payment_mode'),
				'side_days'       => $this->input->post('side_days'),
				'guar_name'       => $this->input->post('guar_name'),
				'email_id'       => $this->input->post('email_id'),
				'pan_no'       => $this->input->post('pan_no'),
				'transporter_name'       => $this->input->post('transporter_name'),
				'transporter_address'       => $this->input->post('transporter_address'),
				'transporter_phone'       => $this->input->post('transporter_phone'),
                                'current_supplier_status' => 1
				
			]; 
		}
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->inventory_model->create_supplier($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}

				redirect('inventory/inventory/create_supplier');
			} else {
				if ($this->inventory_model->update_supplier($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				
                redirect('inventory/inventory/create_supplier');
			}

		} else {
			$data['content'] = $this->load->view('inventory/supplier_registration_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}


	public function supplier_list()
	{
		$data['title'] = "Supplier List";
		#-------------------------------#
		$data['supplier'] = $this->inventory_model->read_supplier();
		$data['content'] = $this->load->view('inventory/supplier_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	public function edit_supplier_form($id = null) 
	{
		$data['title'] = "Update Supplier Details";
		#-------------------------------#
		$data['supplier'] = $this->inventory_model->read_by_id_supplier($id);
		$data['content'] = $this->load->view('inventory/supplier_registration_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}



    public function delete_supplier($id = null) 
	{
		if ($this->inventory_model->delete_supplier($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('inventory/inventory/supplier_list');
	}


	   public function get_supplier()
    {

       $query = $this->db->select('id as value,supplier_name as label')
                    ->from('tbl_supplier')
                    ->where('current_supplier_status', 1)
                    ->get();

            if ($query->num_rows() > 0) {
                $data['message'] = $query->result();
                $data['status'] = true;

            } else {
                $data['message'] = display('invalid_patient_id');
                $data['status'] = false;
            }
       
        echo json_encode($data);
    }

   
    
     public function email_check($email, $id)
    { 
        $emailExists = $this->db->select('email')
            ->where('email',$email) 
            ->where_not_in('id',$id) 
            ->get('patient')
            ->num_rows();

        if ($emailExists > 0) {
            $this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
            return false;
        } else {
            return true;
        }
    }
    



	/*--end Supplier Registration code---*/



    /*-- Supplier product code---*/

    public function create_supplier_product()
	{
		$data['title'] = "Add Supplier Product";
		 $id = $this->input->post('id');
		#-------------------------------#
		$this->form_validation->set_rules('supplier_name',display('supplier_name'),'required|max_length[50]');
		$this->form_validation->set_rules('pack_name',display('pack_name'),'required|max_length[20]');

		
		#-------------------------------#
		if ($this->input->post('id') == null) { //create a supplier product
			$data['supplier'] = (object)$postData = [
		      	'id'   		   => $this->input->post('id'),
				'supplier_name'   => $this->input->post('supplier_name'),
				'item_dept'    => $this->input->post('item_dept'),
				'pack_name' 	   => $this->input->post('pack_name'),
				'unit' 	   => $this->input->post('unit'),
				'product_name' 	   =>  $this->input->post('product_name'),
				'strength'   	   => $this->input->post('strength'),
				'size'       => $this->input->post('size'),
				'ptr'  => $this->input->post('ptr'),
				'status' 		   => $this->input->post('status'), 
				'mfg_date' 	   => $this->input->post('mfg_date'),
				'create_date'  => date('Y-m-d'),
				'exp_date'       => $this->input->post('exp_date'),
				'hsn_code'       => $this->input->post('hsn_code'),
				'product_type'       => $this->input->post('product_type'),
				'gst_rate'       => $this->input->post('gst_rate'),
				'quantity'       => $this->input->post('quantity'),
				'rate'       => $this->input->post('rate'),
                            'current_s_product_status' => 1
			
			];
		} else { // update supplier product
			$data['supplier'] = (object)$postData = [
				'id'   		   => $this->input->post('id'),
				'supplier_name'   => $this->input->post('supplier_name'),
				'item_dept'    => $this->input->post('item_dept'),
				'pack_name' 	   => $this->input->post('pack_name'),
				'unit' 	   => $this->input->post('unit'),
				'product_name' 	   =>  $this->input->post('product_name'),
				'strength'   	   => $this->input->post('strength'),
				'size'       => $this->input->post('size'),
				'ptr'  => $this->input->post('ptr'),
				'status' 		   => $this->input->post('status'), 
				'mfg_date' 	   => $this->input->post('mfg_date'),
				'create_date'  => date('Y-m-d'),
				'exp_date'       => $this->input->post('exp_date'),
				'hsn_code'       => $this->input->post('hsn_code'),
				'product_type'       => $this->input->post('product_type'),
				'gst_rate'       => $this->input->post('gst_rate'),
				'quantity'       => $this->input->post('quantity'),
				'rate'       => $this->input->post('rate'),
                            'current_s_product_status' => 1
				
			]; 
		}
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->inventory_model->create_supplier_product($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}

				redirect('inventory/inventory/create_supplier_product');
			} else {
				if ($this->inventory_model->update_supplier_product($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				
                redirect('inventory/inventory/create_supplier_product');
			}

		} else {
			$data['supplier_list'] = $this->inventory_model->supplier_list();
			$data['content'] = $this->load->view('inventory/supplier_product_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}



    public function supplier_product_list()
	{
		$data['title'] = "Supplier Product List";
		#-------------------------------#
		$data['supplier'] = $this->inventory_model->read_supplier_product();
		$data['content'] = $this->load->view('inventory/supplier_product_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 
	

	public function edit_supplier_product_form($id = null) 
	{
		$data['title'] = "Update Supplier Details";
		#-------------------------------#
		$data['supplier_list'] = $this->inventory_model->supplier_list();
		$data['supplier'] = $this->inventory_model->read_by_id_supplier_product($id);
		$data['content'] = $this->load->view('inventory/supplier_product_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}



    public function delete_supplier_product($id = null) 
	{
		if ($this->inventory_model->delete_supplier_product($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('inventory/inventory/supplier_product_list');
	}

  /*--end Supplier Product code---*/



	/*-- purchase order form code---*/

 public function create_purchase_order()
	{
		$data['title'] = "Add Purchase Order";
		 $id = $this->input->post('id');
		#-------------------------------#
		$this->form_validation->set_rules('order_date',display('order_date'),'required|max_length[50]');
		$this->form_validation->set_rules('order_raised_by_name',display('order_raised_by_name'),'max_length[20]');
       
        #----------------------proccess of purchase order details ----------------------#
        	$disease_name_list = $this->input->post('prescription');
		//	$limit = $this->input->post('limit');
		//	$approval = $this->input->post('approval');
			

              $medicine = array();
                if (!empty($disease_name_list) && is_array($disease_name_list) && sizeof($disease_name_list) > 0) 
                {
                        foreach ($disease_name_list as $key=>$value) {
                                $medicine[$key] = array(
                                        'name' => isset($value['product_name']) ? $value['product_name'] : '' ,
                                        'rate' => isset($value['rate']) ? $value['rate'] : '',
                                        'qty' => isset($value['qty']) ? $value['qty'] : '',
                                        'amount' => isset($value['amount']) ? $value['amount'] : '',
                                        'gst' => isset($value['gst']) ? $value['gst'] : '',
                                        'remark' => isset($value['remark']) ? $value['remark'] : ''
                                );
                        }
                } 

			$medicine = json_encode($medicine);

         
		#-------------------------------#
		if ($this->input->post('id') == null) { //create a supplier
			$data['supplier'] = (object)$postData = [
		      	'id'   		   => $this->input->post('id'),
		      	'order_id'   => "PO".$this->randStrGen(2,7),
				'order_date'   => $this->input->post('order_date'),
				'order_raised_by_name'    => $this->input->post('order_raised_by_name'),
				'max_amount' 	   => $this->input->post('max_amount'),
				'supplier_name' 	   => $this->input->post('supplier_name'),
				'total_amount' 	   =>  $this->input->post('total_amount'),
				'adv_deposit' 	   =>  $this->input->post('adv_deposit'),
				'credit_note' 	   =>  $this->input->post('credit_note'),
				'grand_tot' 	   =>  $this->input->post('grand_tot'),
				'discount'   	   => $this->input->post('discount'),
				'netpay_amt'       => $this->input->post('netpay_amt'),
				'other'  => $this->input->post('other'),
				'medicine'  => $medicine,
				'status' 		   => $this->input->post('status'), 
                                'current_po_status' => 1
			
			];
		} else { // update supplier
			$data['supplier'] = (object)$postData = [
						'id'   		   => $this->input->post('id'),
				'order_date'   => $this->input->post('order_date'),
				'order_raised_by_name'    => $this->input->post('order_raised_by_name'),
				'max_amount' 	   => $this->input->post('max_amount'),
				'supplier_name' 	   => $this->input->post('supplier_name'),
				'total_amount' 	   =>  $this->input->post('total_amount'),
				'adv_deposit' 	   =>  $this->input->post('adv_deposit'),
				'credit_note' 	   =>  $this->input->post('credit_note'),
				'grand_tot' 	   =>  $this->input->post('grand_tot'),
				'discount'   	   => $this->input->post('discount'),
				'netpay_amt'       => $this->input->post('netpay_amt'),
				'other'  => $this->input->post('other'),
				'medicine'  => $medicine,
				'status' 		   => $this->input->post('status'), 
                                'current_po_status' => 1
				
			]; 
		}
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->inventory_model->create_purchase_order($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}

				redirect('inventory/inventory/create_purchase_order');
			} else {
				if ($this->inventory_model->update_purchase_order($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				
                redirect('inventory/inventory/create_purchase_order');
			}

		} else {
			$data['supplier_list'] = $this->inventory_model->supplier_list();
			$data['supplier_list'] = $this->inventory_model->supplier_list();
			$data['content'] = $this->load->view('inventory/purchase_order_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}


	public function purchase_order_list()
	{
		$data['title'] = "Purchase Order List";
		#-------------------------------#
		$data['supplier'] = $this->inventory_model->read_purchase_order();
		$data['content'] = $this->load->view('inventory/purchase_order_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	public function edit_purchase_order_form($id = null) 
	{
		$data['title'] = "Update Purchase Order";
		#-------------------------------#
		$data['supplier_list'] = $this->inventory_model->supplier_list();
		$data['supplier'] = $this->inventory_model->read_by_id_purchasse_order($id);
		$data['content'] = $this->load->view('inventory/purchase_order_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}



    public function delete_purchase_order($id = null) 
	{
		if ($this->inventory_model->delete_purchase_order($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('inventory/inventory/purchase_order_list');
	}


	 public function get_supplier_product() {
        
        $id = $this->input->post('id');

       $data = $this->inventory_model->read_by_id_supplier_product_purchase_order($id);
       $html .=  '<option value="">Select Product</option>';
		foreach($data as $city)
		{
			$html .=  '<option value="'.$city->id.'">'.$city->product_name.'</option>';
		}
		echo $html;
		exit;

       /* $data['product_name'] = $this->inventory_model->read_by_id_supplier_product_purchase_order($id);
        echo json_encode($data['product_name']);*/

    }

     public function get_product_details() {
        
        $id = $this->input->post('id');
        $data = $this->inventory_model->read_by_id_product_details($id);
        echo json_encode($data);

    }


     public function view_purchase_order($id = null) {
        $data['title'] = "Purchase Order Bill";
        #-------------------------------#
       $data['website'] = $this->prescription_model->website();
      
       $data['bill'] = $this->inventory_model->view_po_bill($id);
       //echo "<pre>";print_r($data);die;
        $data['content'] = $this->load->view('inventory/purchase_order_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }
    

/*-- End purchase order form code---*/



/*-- Stock Return form code---*/

 public function create_stock_return()
	{
		$data['title'] = "Stock Return Entry";
		 $id = $this->input->post('id');
		#-------------------------------#
		$this->form_validation->set_rules('order_date',display('order_date'),'required|max_length[50]');
		$this->form_validation->set_rules('order_raised_by_name',display('order_raised_by_name'),'max_length[20]');
       
        #----------------------proccess of purchase order details ----------------------#
				$disease_name_list = $this->input->post('prescription');
		//	$limit = $this->input->post('limit');
		//	$approval = $this->input->post('approval');
			

              $medicine = array();
                if (!empty($disease_name_list) && is_array($disease_name_list) && sizeof($disease_name_list) > 0) 
                {
                        foreach ($disease_name_list as $key=>$value) {
                                $medicine[$key] = array(
                                        'name' => isset($value['product_name']) ? $value['product_name'] : '' ,
                                        'rate' => isset($value['rate']) ? $value['rate'] : '',
                                         'qty' => isset($value['qty']) ? $value['qty'] : '',
                                        'amount' => isset($value['amount']) ? $value['amount'] : '',
                                        'gst' => isset($value['gst']) ? $value['gst'] : '',
                                        'remark' => isset($value['remark']) ? $value['remark'] : ''
                                );
                        }
                } 

			$medicine = json_encode($medicine);

         
         
		#-------------------------------#
		if ($this->input->post('id') == null) { //create a supplier
			$data['supplier'] = (object)$postData = [
		      	'id'   		   => $this->input->post('id'),
		      	'order_id'   => $this->input->post('order_id'),
				'order_date'   => $this->input->post('order_date'),
				'order_raised_by_name'    => $this->input->post('order_raised_by_name'),
				'max_amount' 	   => $this->input->post('max_amount'),
				'supplier_name' 	   => $this->input->post('supplier_name'),
				'total_amount' 	   =>  $this->input->post('total_amount'),
				'adv_deposit' 	   =>  $this->input->post('adv_deposit'),
				'credit_note' 	   =>  $this->input->post('credit_note'),
				'grand_tot' 	   =>  $this->input->post('grand_tot'),
				'discount'   	   => $this->input->post('discount'),
				'netpay_amt'       => $this->input->post('netpay_amt'),
				'other'  => $this->input->post('other'),
				'medicine'  => $medicine,
				'status' 		   => $this->input->post('status'), 
                                'current_po_status' => 1
			
			];
		} else { // update supplier
			$data['supplier'] = (object)$postData = [
						'id'   		   => $this->input->post('id'),
						'order_id'   => $this->input->post('order_id'),
				'order_date'   => $this->input->post('order_date'),
				'order_raised_by_name'    => $this->input->post('order_raised_by_name'),
				'max_amount' 	   => $this->input->post('max_amount'),
				'supplier_name' 	   => $this->input->post('supplier_name'),
				'total_amount' 	   =>  $this->input->post('total_amount'),
				'adv_deposit' 	   =>  $this->input->post('adv_deposit'),
				'credit_note' 	   =>  $this->input->post('credit_note'),
				'grand_tot' 	   =>  $this->input->post('grand_tot'),
				'discount'   	   => $this->input->post('discount'),
				'netpay_amt'       => $this->input->post('netpay_amt'),
				'other'  => $this->input->post('other'),
				'medicine'  => $medicine,
				'status' 		   => $this->input->post('status'), 
                                'current_po_status' => 1
				
			]; 
		}
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->inventory_model->create_stock_return($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}

				redirect('inventory/inventory/create_stock_return');
			} else {
				if ($this->inventory_model->update_stock_return($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				
                redirect('inventory/inventory/create_stock_return');
			}

		} else {
			$data['supplier_list'] = $this->inventory_model->supplier_list();
			$data['supplier_list'] = $this->inventory_model->supplier_list();
			$data['content'] = $this->load->view('inventory/stock_return_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}


	public function stock_return_list()
	{
		$data['title'] = "Stock Return List";
		#-------------------------------#
		$data['supplier'] = $this->inventory_model->read_stock_return();
		$data['content'] = $this->load->view('inventory/stock_return_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	public function edit_stock_return_form($id = null) 
	{
		$data['title'] = "Update stock Return";
		#-------------------------------#
		$data['supplier_list'] = $this->inventory_model->supplier_list();
		$data['supplier'] = $this->inventory_model->read_by_id_purchasse_order($id);
		$data['content'] = $this->load->view('inventory/stock_return_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}



    public function delete_stock_return($id = null) 
	{
		if ($this->inventory_model->delete_stock_return($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('inventory/inventory/purchase_order_list');
	}


 public function view_stock_return($id = null) {
        $data['title'] = "Stock Return Bill";
        #-------------------------------#
       $data['website'] = $this->prescription_model->website();
      
       $data['bill'] = $this->inventory_model->view_po_return_bill($id);
       //echo "<pre>";print_r($data);die;
        $data['content'] = $this->load->view('inventory/stock_return_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }
    

/*-- End stock return form code---*/




/*-- Stock Entry form code---*/

 public function create_stock_entry()
	{
		$data['title'] = "Stock Entry";
		 $id = $this->input->post('id');

		 $this->form_validation->set_rules('bill_no','Bill No','required');

		      #----------------------proccess of purchase order details ----------------------#
				$disease_name_list = $this->input->post('prescription');
		//	$limit = $this->input->post('limit');
		//	$approval = $this->input->post('approval');
			

              $medicine = array();
                if (!empty($disease_name_list) && is_array($disease_name_list) && sizeof($disease_name_list) > 0) 
                {
                        foreach ($disease_name_list as $key=>$value) {
                                $medicine[$key] = array(
                                        'name' => isset($value['product_name']) ? $value['product_name'] : '' ,
                                        'rate' => isset($value['rate']) ? $value['rate'] : '',
                                        'qty' => isset($value['qty']) ? $value['qty'] : '',
                                        'free_qty' => isset($value['free_qty']) ? $value['free_qty'] : '',
                                        'batch_no' => isset($value['batch_no']) ? $value['batch_no'] : '',
                                        'exp_date' => isset($value['exp_date']) ? $value['exp_date'] : '',
                                        'tax' => isset($value['tax']) ? $value['tax'] : '',
                                        'amount' => isset($value['amount']) ? $value['amount'] : '',
                                        'mrp' => isset($value['mrp']) ? $value['mrp'] : ''
                                );
                        }
                } 

			$medicine = json_encode($medicine);

         
         
		#-------------------------------#
		if ($this->input->post('id') == null) { //create a supplier
			$data['supplier'] = (object)$postData = [
		      	'id'   		   => $this->input->post('id'),
				'bill_no'   => $this->input->post('bill_no'),
				'bill_date'   => $this->input->post('bill_date'),
				'supplier_name' 	   => $this->input->post('supplier_name'),
				'total_amount' 	   =>  $this->input->post('total_amount'),
				'adv_deposit' 	   =>  $this->input->post('adv_deposit'),
				'credit_note' 	   =>  $this->input->post('credit_note'),
				'grand_tot' 	   =>  $this->input->post('grand_tot'),
				'discount'   	   => $this->input->post('discount'),
				'netpay_amt'       => $this->input->post('netpay_amt'),
				'gst'  => $this->input->post('gst'),
				'other'  => $this->input->post('other'),
				'medicine'  => $medicine,
				'status' 		   => $this->input->post('status')
                 
			
			];
		} else { // update supplier
			$data['supplier'] = (object)$postData = [
						'id'   		   => $this->input->post('id'),
				'bill_no'   => $this->input->post('bill_no'),
				'bill_date'   => $this->input->post('bill_date'),
				'supplier_name' 	   => $this->input->post('supplier_name'),
				'total_amount' 	   =>  $this->input->post('total_amount'),
				'adv_deposit' 	   =>  $this->input->post('adv_deposit'),
				'credit_note' 	   =>  $this->input->post('credit_note'),
				'grand_tot' 	   =>  $this->input->post('grand_tot'),
				'discount'   	   => $this->input->post('discount'),
				'netpay_amt'       => $this->input->post('netpay_amt'),
				'gst'  => $this->input->post('gst'),
				'other'  => $this->input->post('other'),
				'medicine'  => $medicine,
				'status' 		   => $this->input->post('status')
                               
				
			]; 
		}
		#-------------------------------#----------#

		if ($this->form_validation->run() === true) {
          // print_r($postData);die;
			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->inventory_model->create_stock_entry($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}

				redirect('inventory/inventory/create_stock_entry');
			} else {
				if ($this->inventory_model->update_stock_entry($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				
                redirect('inventory/inventory/create_stock_entry');
			}

		} else {
			
			$data['supplier_list'] = $this->inventory_model->supplier_list();
			$data['content'] = $this->load->view('inventory/stock_entry_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}



	public function stock_entry_list()
	{
		$data['title'] = "Stock Entry List";
		#-------------------------------#
		$data['supplier'] = $this->inventory_model->read_stock_entry();
		$data['content'] = $this->load->view('inventory/stock_entry_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	public function view_stock_entry($id = null) {
        $data['title'] = "Stock Entry Bill";
        #-------------------------------#
       $data['website'] = $this->prescription_model->website();
      
       $data['bill'] = $this->inventory_model->view_stock_entry_bill($id);
       //echo "<pre>";print_r($data);die;
        $data['content'] = $this->load->view('inventory/stock_entry_view',$data,true);
        $this->load->view('layout/main_wrapper',$data);
    }



/*-- End stock entry form code---*/


/*-- start stock Report code---*/


public function stock_report()
	{
		$data['title'] = "Stock Report";
		#-------------------------------#
		$data['website'] = $this->prescription_model->website();
		$data['supplier'] = $this->inventory_model->read_stock_report();
		$data['content'] = $this->load->view('inventory/stock_report',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


/*-- End stock Report code---*/







 	public function create()
	{
		$data['title'] = display('add_insurance');
		#-------------------------------#
		$this->form_validation->set_rules('name', display('insurance_name') ,'required|max_length[100]');
		$this->form_validation->set_rules('description', display('description'),'trim');
		$this->form_validation->set_rules('status', display('status') ,'required');
		#-------------------------------#
		$data['insurance'] = (object)$postData = [
			'id' 	  => $this->input->post('id',true),
			'name' 		  => $this->input->post('name',true),
			'description' => $this->input->post('description',true),
			'status'      => $this->input->post('status',true)
		]; 
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->insurance_model->create($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('prescription/insurance/create');
			} else {
				if ($this->insurance_model->update($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('prescription/insurance/edit/'.$postData['id']);
			}

		} else {
			$data['content'] = $this->load->view('prescription/insurance_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}

	public function edit($id = null) 
	{
		$data['title'] = display('insurance_edit');
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id($id);
		$data['content'] = $this->load->view('prescription/insurance_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}
 

	public function delete($id = null) 
	{
		if ($this->insurance_model->delete($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('prescription/insurance');
	}


    /*--- group charges code---*/

	public function creategroup_chargemaster()
	{
		$data['title'] = "Add Group Charge";
		#-------------------------------#
		$this->form_validation->set_rules('disease_name', display('disease_name') ,'required|max_length[100]');
		$this->form_validation->set_rules('status', display('status') ,'required');
		#-------------------------------#
		$data['insurance'] = (object)$postData = [
			'id' 	  => $this->input->post('id',true),
			'disease_name' 		  => $this->input->post('disease_name',true),
			'status'      => $this->input->post('status',true)
		]; 
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->insurance_model->creategroup_chargemaster($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('insurance/insurance/creategroup_chargemaster');
			} else {
				if ($this->insurance_model->update_groupcharge($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('insurance/insurance/edit_groupcharge/'.$postData['id']);
			}

		} else {
			$data['content'] = $this->load->view('insurance/group_charge_master_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}

		public function groupcharge_list()
	{
		$data['title'] = display('insurance_list');
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->readgroup_charge();
		$data['content'] = $this->load->view('insurance/groupcharge',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 

	public function edit_groupcharge($id = null) 
	{
		$data['title'] = display('insurance_edit');
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id_groupcharge($id);
		$data['content'] = $this->load->view('insurance/group_charge_master_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}

  
  public function delete_groupcharge($id = null) 
	{
		if ($this->insurance_model->delete_groupcharge($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('insurance/insurance/groupcharge_list');
	}

	/*---end group charges code---*/

	/*--start sponser details code---*/

     public function create_sponser()
	{
		$data['title'] = "Add Sponser Details";
		#-------------------------------#
		$this->form_validation->set_rules('organisation_name', display('organisation_name') ,'required|max_length[100]');
		$this->form_validation->set_rules('status', display('status') ,'required');
		#-------------------------------#
		#----------------------proccess of medicine----------------------#
			$group_charge = $this->input->post('group_charge');
			$charge_name = $this->input->post('charge_name');
			$hospital_rate = $this->input->post('hospital_rate');
			$insurance_rate = $this->input->post('insurance_rate');

			$disease = array();
			if (!empty($charge_name) && is_array($charge_name) && sizeof($charge_name) > 0) 
			{
				for ($i=0; $i < sizeof($charge_name); $i++) { 
					$disease[$i] = array(
						'disease_name' => $group_charge[$i],
						'charge_name' => $charge_name[$i],
						'hospital_rate' => $hospital_rate[$i],
						'insurance_rate' => $insurance_rate[$i],
					);
				}
			} 
			$disease = json_encode($disease); 

		$data['insurance'] = (object)$postData = [
			'id' 	  => $this->input->post('id',true),
			'organisation_name' 		  => $this->input->post('organisation_name',true),
			'service_tax' 		  => $this->input->post('service_tax',true),
			'discount' 		  => $this->input->post('discount',true),
			'remark' 		  => $this->input->post('remark',true),
			'organisation_no' 		  => $this->input->post('organisation_no',true),
			'organisation_code_no' 		  => $this->input->post('organisation_code_no',true),
			'disease'  => $disease,
			'status'      => $this->input->post('status',true)
		]; 
		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->insurance_model->createsponser_details($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('insurance/insurance/create_sponser');
			} else {
				if ($this->insurance_model->update_sponsers_details($postData)) {
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception',display('please_try_again'));
				}
				redirect('insurance/insurance/edit_sponser_details/'.$postData['id']);
			}

		} else {
			$data['insurance_list'] = $this->insurance_model->group_charge_dropdown_list();
			$data['content'] = $this->load->view('insurance/sponsers_details_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}


	public function edit_sponser_details($id = null) 
	{
		$data['title'] = "Update Sponseres Details";
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id_sponserdetails($id);
		$data['insurance_list'] = $this->insurance_model->group_charge_dropdown_list();
		$data['content'] = $this->load->view('insurance/sponsers_details_edit_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}

	 public function delete_sponsersdetails($id = null) 
	{
		if ($this->insurance_model->delete_sponsers_details($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('insurance/insurance/sponsers_details_list');
	}


	public function sponsers_details_list()
	{
		$data['title'] = "Sponsers Details List";
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->read_sponsers_details();
		$data['content'] = $this->load->view('insurance/sponsers_details_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	/*--end sponser details code---*/


	/*-- Limit approval code---*/

	  /*
    |----------------------------------------------
    |        id genaretors
    |----------------------------------------------     
    */
    public function randStrGen($mode = null, $len = null){
        $result = "";
        if($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 4):
            $chars = "0123456789";
        endif;

        $charArray = str_split($chars);
        for($i = 0; $i < $len; $i++) {
                $randItem = array_rand($charArray);
                $result .="".$charArray[$randItem];
        }
        return $result;
    }
    /*
    |----------------------------------------------
    |         Ends of id genaretor
    |----------------------------------------------
    */

 


	public function limit_approval_list()
	{
		$data['title'] = "Limit Approval List";
		#-------------------------------#
		$data['insurances'] = $this->insurance_model->read_limit_approval();
		$data['content'] = $this->load->view('insurance/limit_approval_list',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	} 


	public function edit_limit_approval_form($id = null) 
	{
		$data['title'] = "Update Sponseres Details";
		#-------------------------------#
		$data['insurance'] = $this->insurance_model->read_by_id_limitapproval($id);
		$data['content'] = $this->load->view('insurance/limit_approval_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}



    public function delete_limitapproval($id = null) 
	{
		if ($this->insurance_model->delete_limit_approval($id)) {
			#set success message
			$this->session->set_flashdata('message', display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception', display('please_try_again'));
		}
		redirect('insurance/insurance/limit_approval_list');
	}



	/*--end Limit approval code---*/




}
