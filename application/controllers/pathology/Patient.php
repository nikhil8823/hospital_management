<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array(
			'patient_model',
                        'document_model',
                        'department_model'
		));

		if ($this->session->userdata('isLogIn') == false
			|| $this->session->userdata('user_role') != 1) 
			redirect('login');
	}
 
        
        public function index()
	{ 
		$data['title'] = display('patient_list');
		$data['patients'] = $this->patient_model->getpathologyPatient();
		$data['content'] = $this->load->view('pathology/patient',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}
        
        public function profile($patient_id = null)
	{ 
		$data['title'] =  display('patient_information');
		#-------------------------------#
		$data['website'] = $this->patient_model->website();
		$data['profile'] = $this->patient_model->read_by_id($patient_id);
		$data['documents'] = $this->document_model->read_by_patient($patient_id);
		$data['content'] = $this->load->view('pathology/patient_profile',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}
        
        public function edit($patient_id = null) 
	{ 
		$data['title'] = display('patient_edit');
		#-------------------------------#
		$data['bed_list'] = $this->patient_model->bed_list();
		$data['department_list'] = $this->department_model->department_list();
		$data['patient'] = $this->patient_model->read_by_id($patient_id);
		$data['content'] = $this->load->view('pathology/patient_form',$data,true);
		$this->load->view('layout/main_wrapper',$data);
	}
        
        public function delete($patient_id = null) 
	{ 
		if ($this->patient_model->delete($patient_id)) {
                        $this->patient_model->deleteIpdPatient($patient_id);
			#set success message
			$this->session->set_flashdata('message',display('delete_successfully'));
		} else {
			#set exception message
			$this->session->set_flashdata('exception',display('please_try_again'));
		}
		redirect('pathology/patient');
	}
        
         /*
    |----------------------------------------------
    |        id genaretors
    |----------------------------------------------     
    */
    public function randStrGen($mode = null, $len = null){
        $result = "";
        if($mode == 1):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 2):
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif($mode == 3):
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif($mode == 4):
            $chars = "0123456789";
        endif;

        $charArray = str_split($chars);
        for($i = 0; $i < $len; $i++) {
                $randItem = array_rand($charArray);
                $result .="".$charArray[$randItem];
        }
        return $result;
    }
    /*
    |----------------------------------------------
    |         Ends of id genaretor
    |----------------------------------------------
    */

	public function create()
	{
		$data['title'] = display('add_patient');
                $id = $this->input->post('id');
		#-------------------------------#
		$this->form_validation->set_rules('firstname', display('first_name'),'required|max_length[50]');
		$this->form_validation->set_rules('lastname', display('last_name'),'required|max_length[50]');
		if ($this->input->post('id') == null) {
			$this->form_validation->set_rules('email', display('email'),'max_length[100]|is_unique[patient.email]|valid_email');
                               
		} else {        
			$this->form_validation->set_rules('email',display('email'), "max_length[50]|valid_email|callback_email_check[$id]");
		}

		$this->form_validation->set_rules('mobile', display('mobile'),'required|max_length[20]');
		$this->form_validation->set_rules('sex', display('sex'),'required|max_length[10]');
		$this->form_validation->set_rules('date_of_birth', display('date_of_birth'),'required|max_length[10]');
		$this->form_validation->set_rules('address', display('address'),'required|max_length[255]');
		$this->form_validation->set_rules('status', display('status'),'required');
		#-------------------------------#
		//picture upload
		$picture = $this->fileupload->do_upload(
			'assets/images/patient/',
			'picture'
		);
		// if picture is uploaded then resize the picture
		if ($picture !== false && $picture != null) {
			$this->fileupload->do_resize(
				$picture, 
				200,
				150
			);
		}
		//if picture is not uploaded
		if ($picture === false) {
			$this->session->set_flashdata('exception', display('invalid_picture'));
		}

		 #----------------------proccess of medicine----------------------#
                $medicine_name = $this->input->post('prescription');
                //$medicine_category = $this->input->post('medicine_category');
                //$medicine_doses = $this->input->post('doses');
                //$num_of_doses = $this->input->post('num_of_doses');
                //$medicine_diet = $this->input->post('diet');
                 //echo "<pre>";print_r($medicine_name);die;

                $medicine = array();
                if (!empty($medicine_name) && is_array($medicine_name) && sizeof($medicine_name) > 0) 
                {
                        foreach ($medicine_name as $key=>$value) {
                                $medicine[$key] = array(
                                        'name' => isset($value['test_name']) ? $value['test_name'] : '' ,
                                        'normal_value' => isset($value['normal_value']) ? $value['normal_value'] : '',
                                        'actual_value' => isset($value['actual_value']) ? $value['actual_value'] : ''
                                );
                        }
                } 

                $medicine = json_encode($medicine); 

		if ($this->input->post('id') == null) { //create a patient
		
		#-------------------------------#
			$data['patient'] = (object)$postData = [
				'id'   		   => $this->input->post('id'),
				'patient_id'   => "P".$this->randStrGen(2,7),
				'firstname'    => $this->input->post('firstname'),
				'lastname' 	   => $this->input->post('lastname'),
				'email' 	   => $this->input->post('email'),
				/*'password' 	   => md5($this->input->post('password')),*/
				'phone'   	   => $this->input->post('phone'),
				'mobile'       => $this->input->post('mobile'),
				'blood_group'  => $this->input->post('blood_group'),
				'sex' 		   => $this->input->post('sex'), 
				'date_of_birth' => date('Y-m-d', strtotime(($this->input->post('date_of_birth') != null)? $this->input->post('date_of_birth'): date('Y-m-d'))),
				'address' 	   => $this->input->post('address'),
				/*'picture'      => (!empty($picture)?$picture:$this->input->post('old_picture')),*/
				'affliate'     => null,
				'create_date'  => date('Y-m-d'),
				'created_by'   => $this->session->userdata('user_id'),
				'status'       => $this->input->post('status'),
				'guardianname'       => $this->input->post('guardianname'),
				'birthmark'       => $this->input->post('birthmark'),
				'aadharno'       => $this->input->post('aadharno'),
				'religion'       => $this->input->post('religion'),
				'case_type'       => $this->input->post('case_type'),
				/*'reffer_doctor_name'       => $this->input->post('reffer_doctor_name'),
				'reffer_doctor_phone'       => $this->input->post('reffer_doctor_phone'),*/
				'maritual_status'       => $this->input->post('maritual_status'),
				'consultant_doctor_dept'       => $this->input->post('consultant_doctor_dept'),
				'consultant_doctor_name'       => $this->input->post('consultant_doctor_name'),
				'consultantation_fee'       => $this->input->post('consultantation_fee'),
				'followup_consultation'       => $this->input->post('followup_consultation'),
				'service_tax'       => $this->input->post('service_tax'),
				'total_fee'       => $this->input->post('total_fee'),
				'payment_mode'       => $this->input->post('payment_mode'),
                                'current_patient_status' => 1,
                'medicine'   => $medicine
			
			];
		} else { // update patient
			$data['patient'] = (object)$postData = [
				'id'   		   => $this->input->post('id'),
				'firstname'    => $this->input->post('firstname'),
				'lastname' 	   => $this->input->post('lastname'),
				'email' 	   => $this->input->post('email'),
				/*'password' 	   => md5($this->input->post('password')),*/
				'phone'   	   => $this->input->post('phone'),
				'mobile'       => $this->input->post('mobile'),
				'blood_group'  => $this->input->post('blood_group'),
				'sex' 		   => $this->input->post('sex'),
				'date_of_birth' => date('Y-m-d', strtotime($this->input->post('date_of_birth'))),
				'address' 	   => $this->input->post('address'),
				/*'picture'      => (!empty($picture)?$picture:$this->input->post('old_picture')),*/
				'affliate'     => null, 
				'created_by'   => $this->session->userdata('user_id'),
				'status'       => $this->input->post('status'),
				'guardianname'       => $this->input->post('guardianname'),
				'birthmark'       => $this->input->post('birthmark'),
				'aadharno'       => $this->input->post('aadharno'),
				'religion'       => $this->input->post('religion'),
				'case_type'       => $this->input->post('case_type'),
				/*'reffer_doctor_name'       => $this->input->post('reffer_doctor_name'),
				'reffer_doctor_phone'       => $this->input->post('reffer_doctor_phone'),*/
				'maritual_status'       => $this->input->post('maritual_status'),
				'consultant_doctor_dept'       => $this->input->post('consultant_doctor_dept'),
				'consultant_doctor_name'       => $this->input->post('consultant_doctor_name'),
				'consultantation_fee'       => $this->input->post('consultantation_fee'),
				'followup_consultation'       => $this->input->post('followup_consultation'),
				'service_tax'       => $this->input->post('service_tax'),
				'total_fee'       => $this->input->post('total_fee'),
				'payment_mode'       => $this->input->post('payment_mode'),
                'medicine'   => $medicine,
                'current_patient_status' => 1
				
			]; 
		}

		#-------------------------------#
		if ($this->form_validation->run() === true) {

			#if empty $id then insert data
			if (empty($postData['id'])) {
				if ($this->patient_model->create($postData)) {					
					#set success message
					$this->session->set_flashdata('message', display('save_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}

				//redirect('patient/profile/' . $patient_id);
				redirect('pathology/patient/create');
			} else {
				if ($this->patient_model->update($postData)) {
                                    
					#set success message
					$this->session->set_flashdata('message', display('update_successfully'));
				} else {
					#set exception message
					$this->session->set_flashdata('exception', display('please_try_again'));
				}
				//redirect('patient/edit/'.$postData['id']);
                                redirect('pathology/patient/create');
			}

		} else {
			$data['bed_list'] = $this->patient_model->bed_list();
			$data['department_list'] = $this->department_model->department_list();
			$data['content'] = $this->load->view('pathology/patient_form',$data,true);
			$this->load->view('layout/main_wrapper',$data);
		} 
	}

        public function get_patient()
    {

        $search = $this->input->post('search');
        if (!empty($search)) {
            $query = $this->db->select('id as value,firstname as label')
                    ->from('patient')
                    ->group_start()
                    ->or_like('firstname', $search)
                    ->or_like('lastname', $search)
                    ->or_like('mobile', $search)
                    ->group_end()
                    ->where('status', 1)
                    ->where('current_patient_status', 0)
                    ->get();

            if ($query->num_rows() > 0) {
                $data['message'] = $query->result();
                $data['status'] = true;

            } else {
                $data['message'] = display('invalid_patient_id');
                $data['status'] = false;
            }
        } else {
            $data['message'] = display('invlid_input');
            $data['status'] = null;
        }

        echo json_encode($data);
    }

    public function get_patient_details() {
        
        $patient_id = $this->input->post('userid');
        $patient_data = $this->patient_model->read_by_id($patient_id);
        //print_r($patient_data);die;
        echo json_encode($patient_data);
    }
    
     public function email_check($email, $id)
    { 
        $emailExists = $this->db->select('email')
            ->where('email',$email) 
            ->where_not_in('id',$id) 
            ->get('patient')
            ->num_rows();

        if ($emailExists > 0) {
            $this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
            return false;
        } else {
            return true;
        }
    }


     public function get_normalvalue() {
            $medicine   = $this->patient_model->get_normal_value($this->input->post('medicine_cat_id'));
            echo json_encode($medicine);
        }
}
